# Overview

This repository provides Python implementation of
variational inference (VI)
mainly for
HDP-HMM/PCFG (Liang et. al., 2007, 2009).

- [`README.md`](README.md) - This document.
- [`code/`](code) - Python codes.
	- [`train_hdp_pcfg.py`](code/train_hdp_pcfg.py) - Train HDP-PCFG.
	- [`train_hdp_hmm.py`](code/train_hdp_hmm.py) - Train HDP-HMM.
	- [`vi/`](code/vi) - Modules for VI.
	- [`result_dir_info.txt`](code/result_dir_info.txt) - Path to the directory to save results. You can also specify the saving directory by the `-s` option.

# Description

TO BE ADDED.




# References

Bishop, C. M.
2006.
Pattern recognition and machine learning.
New York: Springer.

Blei, David M. and M. Jordan.
2006.
Variational inference for Dirichlet process mixtures.
Bayesian Analysis.
121 - 144.


Liang, P., S. Petrov, M. Jordan, and D. Klein.
2007.
The Infinite PCFG Using Hierarchical Dirichlet Processes.
Proceedings of the 2007 Joint Conference on Empirical Methods in Natural Language Processing and Computational Natural Language Learning (EMNLP-CoNLL).
688 - 697.

Liang, P., M. Jordan, and D. Klein.
2009.
Probabilistic grammars and hierarchical Dirichlet processes.
The Oxford Handbook of Applied Bayesian Analysis.

