# Data formatting

## Sequences of multivariate data
The VI of HDP-PCFG/HMM takes sequences of multivariate data (e.g. sequences of MFCCs) as its input.
We assume the data are stored in a csv file with at least 4 columns.

- Indices of the sequences (default column name: 'sequence_ix').
- Indices of the discrete time of each sequence (default column name: 'time_ix').
- Indices of the data dimension at each discrete time (default column name: 'dim').
- Data value (default column name: 'value')

See [the sample data](20xPoissin_5x3-iid-samples-from-Gauss-0-I.csv) for an example.



## Non-sequential multivariate data
The VI of DP mixture takes multivariate data as its input.
We assume the data are stored in a csv file with at least 3 columns.

- Indices of the data points (default column name: 'data_ix').
- Indices of the data dimension (default column name: 'dim').
- Data value (default column name: 'value')

See [the sample data](20x3-iid-samples-from-Gauss-0-I.csv) for an example.