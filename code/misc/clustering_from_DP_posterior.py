# coding: utf-8

import sys, os.path
file_dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(file_dir_path, '..'))
import numpy as np
import scipy.special as sps
import scipy.misc as spm
import pandas as pd
import vi.variational_inference as vi
import argparse
import vi.dp_mixture as dp_mix
import vi.distributions
import data_parser as dtp
from statsmodels.sandbox.distributions.multivariate import multivariate_t_rvs



class DP_Mix_Predictor(dp_mix.DP_mix):
	def __init__(self, hdf5_path):
		df_stick = pd.read_hdf(hdf5_path, key='/dp/stick')
		num_clusters = df_stick.shape[0] + 1

		clustering_params = {
			'num_clusters':num_clusters,
			'concentration_prior_rate':10, # Unused
			'concentration_prior_shape':10, # Unused
		}
		self.dp = vi.distributions.DP(clustering_params)
		self.dp.varpar_stick = df_stick.values
		self.dp.sum_stick  = np.sum(self.dp.varpar_stick, axis=-1)
		self.dp.E_log_stick = sps.digamma(self.dp.varpar_stick) - sps.digamma(self.dp.sum_stick)[:,np.newaxis]


		df_mean_mean = pd.read_hdf(hdf5_path, key='/emission/mean/mean')
		dims = [col_name for col_name in df_mean_mean.columns if col_name.startswith('dim_')]
		dimension = len(dims)
		emission_params = { # not used
			'distribution':'Gaussian',
			'dimension':dimension,
			'mean_prior_mean':np.zeros(dimension),
			'mean_prior_denominator':dimension,
			'covmat_prior_scale_mat':np.eye(dimension),
			'covmat_prior_df':dimension,
			'num_components':num_clusters
		}
		self.emission_distributions = vi.distributions.Gaussian(emission_params)
		self.emission_distributions.varpar_mean_mean = df_mean_mean.sort_values('mix_component_ix').loc[:,dims].values
		df_mean_denom = pd.read_hdf(hdf5_path, key='/emission/mean/denominator')
		self.emission_distributions.varpar_mean_denominator = df_mean_denom.sort_values('mix_component_ix').value.values
		df_covmat_df = pd.read_hdf(hdf5_path, key='/emission/covmat/df')
		self.emission_distributions.varpar_covmat_df = df_covmat_df.sort_values('mix_component_ix').df.values
		df_covmat_sm = pd.read_hdf(hdf5_path, key='/emission/covmat/scale_mat')
		varpar_covmat_scale_mat = np.zeros((num_clusters,dimension,dimension))
		for row_tuple in df_covmat_sm.itertuples():
			varpar_covmat_scale_mat[row_tuple.mix_component_ix, row_tuple.row_id, row_tuple.col_id] = row_tuple.matrix_entry
			varpar_covmat_scale_mat[row_tuple.mix_component_ix, row_tuple.col_id, row_tuple.row_id] = row_tuple.matrix_entry
		self.emission_distributions.inv_varpar_covmat_scale_mat = np.linalg.inv(varpar_covmat_scale_mat)
		
		self.emission_distributions.num_dimensions_over_vpdenom = dimension / self.emission_distributions.varpar_mean_denominator
		self.emission_distributions.half_varpar_covmat_df = self.emission_distributions.varpar_covmat_df * 0.5
		self.emission_distributions.multidigamma_nover2 = self.emission_distributions._multidigamma(self.emission_distributions.half_varpar_covmat_df) # multivariate_digamma(n/2)
		logdet_inv_vp_covmat_sm = np.linalg.slogdet(self.emission_distributions.inv_varpar_covmat_scale_mat)[1] # log |varpar_covmat_scale_ma|
		self.emission_distributions.E_log_det_inv_vp_cm_sm = self.emission_distributions.multidigamma_nover2+logdet_inv_vp_covmat_sm
		self.emission_distributions.vpdf_x_invvpsm = self.emission_distributions.varpar_covmat_df[:,np.newaxis,np.newaxis]*self.emission_distributions.inv_varpar_covmat_scale_mat

		self.prepare_posterior_prediction()


	def prepare_posterior_prediction(self):
		self.emission_distributions.set_log_posterior_expectation()


	def get_log_prob(self, data):
		return self.get_posterior_predictive_classification_probs(data)

	def _get_posterior_predictive_classification_probs_per_cluster(self, new_data):
		log_mix_weights = self.dp.get_log_posterior_weights()
		log_emission_probs = self.emission_distributions.get_log_posterior_pred(
								new_data[:,np.newaxis,:] # num_strings (= datasize) x string_len (=1) x ndim
								).squeeze(axis=1) # num_strings (= datasize) x num_clusters
		return log_emission_probs + log_mix_weights

	def get_posterior_predictive_classification_probs(self, new_data):
		log_probs_per_cluster = self._get_posterior_predictive_classification_probs_per_cluster(new_data)
		return log_probs_per_cluster - spm.logsumexp(log_probs_per_cluster, axis=-1)[:,np.newaxis]

	def get_posterior_predictive_probs(self, new_data):
		log_probs_per_cluster = self._get_posterior_predictive_classification_probs_per_cluster(new_data)
		return spm.logsumexp(log_probs_per_cluster, axis=-1)




def get_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('hdf_path', type=str, help='path to the result HDF5 file')
	parser.add_argument('data_path', type=str, help='path to data to predict.')
	parser.add_argument('save_path', type=str, help='path to the csv where results are saved.')
	parser.add_argument(
				"--data_ix_col",
				type=str,
				default='data_ix',
				help="Name of the csv column containing the index of the data points."
	)
	parser.add_argument(
				"--dim_col",
				type=str,
				default='dim',
				help="Name of the csv column containing the index of the dimensions of the data points."
	)
	parser.add_argument(
				"--val_col",
				type=str,
				default='value',
				help="Name of the csv column containing the data values."
	)
	return parser.parse_args()


if __name__ == '__main__':
	options = get_arguments()

	df_data = pd.read_csv(options.data_path).sort_values([options.data_ix_col, options.dim_col])
	df_data = df_data.pivot(index=options.data_ix_col, columns=options.dim_col, values=options.val_col)
	data = df_data.values

	predictor = DP_Mix_Predictor(options.hdf_path)
	log_probs = predictor.get_log_prob(data)
	probs = np.exp(log_probs)

	df_result = pd.DataFrame(probs)
	df_result[options.data_ix_col] = df_data.index
	df_result = df_result.melt(id_vars=[options.data_ix_col], var_name='cluster_ix', value_name='prob')

	df_result.to_csv(options.save_path, index = False)




