# coding: utf-8

import numpy as np
import pandas as pd
import posterior_prediction as pp
import data_parser as dtp
import argparse, os, sys, json
file_dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(file_dir_path, '..'))
import vi.distributions

def get_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('hdf_path', type=str, help = 'path to the result HDF5 file')
	parser.add_argument('data_path', type = str, help = 'path to data to predict.')
	parser.add_argument('-m', '--model', type = str, default = 'HMM', help = 'Model type ("PCFG" or "HMM"). Default is HMM (with Gaussian emission).')
	parser.add_argument('-p', '--prob_type', type = str, default = 'E_p', help = 'Type of probability used. E[p] ("E_p") or E[log p] ("E_log"). Default is "E_p"')
	# parser.add_argument('-P', '--parse', type = str, default = 'all', help = 'Parse types. All the parses are taken into account by default ("all"). Only left/right-parses are used if "left"/"right".')
	parser.add_argument(
				"--seq_col",
				type=str,
				default='sequence_ix',
				help="Name of the csv column containing the index of the sequences."
	)
	parser.add_argument(
				"--time_col",
				type=str,
				default='time_ix',
				help="Name of the csv column containing the discrete time indexes of the sequences."
	)
	parser.add_argument(
				"--dim_col",
				type=str,
				default='dim',
				help="Name of the csv column containing the index of the dimensions of the data points."
	)
	parser.add_argument(
				"--val_col",
				type=str,
				default='value',
				help="Name of the csv column containing the data values."
	)
	parser.add_argument('--locality', type=int, default=None, help='If specified, attention is limited to neighbors whose distance threshold is determined by this value.')
	parser.add_argument('--save_as_loss', action='store_true', help='If selected, negative log probabilities are saved as loss.')
	return parser.parse_args()

def rename_existing_file(filepath):
	if os.path.isfile(filepath):
		new_path = filepath+'.orig'
		rename_existing_file(new_path)
		os.rename(filepath, new_path)

if __name__ == '__main__':
	options = get_arguments()

	result_dir = os.path.dirname(options.hdf_path)
	result_filename = 'POST-PRED-PER-TIME_'
	if not options.locality is None:
		result_filename += 'locality-{}_'.format(options.locality)
	result_filename += os.path.splitext(os.path.basename(options.data_path))[0] + '.csv'
	save_path = os.path.join(result_dir, result_filename)
	rename_existing_file(save_path)

	if options.model == 'PCFG':
		raise ValueError('PCFG not supported yet.')
		# predictor = HDP_PCFG_Gaussian_Predictor(options.hdf_path)
	elif options.model == 'HMM':
		predictor = pp.HDP_HMM_Gaussian_Predictor(options.hdf_path)
	else:
		raise ValueError('Invalid model name: {model}'.format(model=options.model))

	discrete = False
	if isinstance(predictor.emission_distributions, vi.distributions.Dirichlet):
		discrete = True
	data = dtp.Dataset_of_Sequences(options.data_path, seq_col=options.seq_col, time_col=options.time_col, dim_col=options.dim_col, val_col=options.val_col, singleton_batch=True, discrete=discrete)

	if options.prob_type == 'E_p':
		predictor.prepare_posterior_prediction()
	elif options.prob_type == 'E_log':
		predictor.set_E_log_probs()
	else:
		raise ValueError('Invalid probability type: {p_type}'.format(p_type=options.prob_type))

	df_data = pd.read_csv(options.data_path)
	with open(os.path.join(result_dir,'label2ix.json'), 'r') as f:
		label2ix = json.load(f)
	for seq_ix, df_seq in df_data.groupby(options.seq_col, sort=True):
		seq = []
		for time_ix, df_time in df_seq.groupby(options.time_col, sort=True):
			if discrete:
				label = df_time[options.val_col].values[0]
				d = label2ix[label]
			else:
				d = df_time.sort_values(options.dim_col)[options.val_col].values
			seq.append(d)
		seq = np.array(seq)[np.newaxis]
		predictions = predictor.get_log_prob_per_time(seq, locality=options.locality)
		if len(predictions):
			predictions = [[seq_ix]+list(row) for row in predictions]
			df_pred = pd.DataFrame(predictions, columns=[options.seq_col, options.time_col, 'log_prob'])
			if options.save_as_loss:
				df_pred.loc[:,'loss'] = - df_pred.log_prob
			if os.path.isfile(save_path):
					df_pred.to_csv(save_path, index=False, mode='a', header=False)
			else:
				df_pred.to_csv(save_path, index=False)