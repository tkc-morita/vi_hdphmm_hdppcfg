# coding: utf-8

import numpy as np
import pandas as pd
import argparse

def get_grammar_size(options):
	if options.model == 'PCFG':
		df_branch = pd.read_hdf(options.hdf5_path, key='/branching')
		df_init = pd.read_hdf(options.hdf5_path, key='/root')
		df_base_count_nt = pd.read_hdf(options.hdf5_path, key='/non_terminal')
		num_nt = df_init.shape[0]
		branch_base_count = options.concentration_branching * df_base_count_nt.p.values[:,np.newaxis] * df_base_count_nt.p.values[np.newaxis,:]
		expected_rule_tokens = df_branch.dirichlet_par.values.reshape(num_nt,num_nt,num_nt) - branch_base_count[np.newaxis,:,:]
		expected_rule_counts_step = step_counter(expected_rule_tokens, options.step_threshold)
		expected_rule_counts_tanh = tanh_counter(expected_rule_tokens)

		expected_init_tokens = df_init.dirichlet_par.values - options.concentration_root * df_base_count_nt.p.values
		expected_init_counts_step = step_counter(expected_init_tokens, options.step_threshold)
		expected_init_counts_tanh = tanh_counter(expected_init_tokens)
		
		df_switch = pd.read_hdf(options.hdf5_path, key='/switch')
		expected_non_terminal_tokens = df_switch.branch.values - options.concentration_switch + df_switch.emit.values - options.concentration_switch
		expected_non_terminal_counts_step = step_counter(expected_non_terminal_tokens, options.step_threshold)
		expected_non_terminal_counts_tanh = tanh_counter(expected_non_terminal_tokens)

		df_nt2t = pd.read_hdf(options.hdf5_path, key='/nt2t')
		df_base_count_terminal = pd.read_hdf(options.hdf5_path, key='/terminal')
		expected_nt2t_tokens = df_nt2t.dirichlet_par.values.reshape(num_nt,-1) - df_base_count_terminal.p.values[np.newaxis,:]
		expected_nt2t_counts_step = step_counter(expected_nt2t_tokens, options.step_threshold)
		expected_nt2t_counts_tanh = tanh_counter(expected_nt2t_tokens)

		df_emit = pd.read_hdf(options.hdf5_path, key='/emission/covmat/df')
		expected_terminal_tokens = df_emit.df.values - options.df_prior
		expected_terminal_counts_step = step_counter(expected_terminal_tokens, options.step_threshold)
		expected_terminal_counts_tanh = tanh_counter(expected_terminal_tokens)

		print(
"""
HDP-PCFG estimated grammar size.
============================================
PARAMETERS

Concentration for root-labeling: {concentration_root}
Concentration for branching: {concentration_branching}
Concentration for switching: {concentration_switch}
============================================
ESTIMATION BY STEP FUNCTION W/ THRESHOLD={threshold}

# of non-terminals: {expected_non_terminal_counts_step}
# of terminals: {expected_terminal_counts_step}
# of root-labeling rules: {expected_init_counts_step}
# of branching rules: {expected_rule_counts_step}
# of non-terminal-to-terminal: {expected_nt2t_counts_step}
============================================
ESTIMATION BY tanh

# of non-terminals: {expected_non_terminal_counts_tanh}
# of terminals: {expected_terminal_counts_tanh}
# of root-labeling rules: {expected_init_counts_tanh}
# of branching rules: {expected_rule_counts_tanh}
# of non-terminal-to-terminal: {expected_nt2t_counts_tanh}
""".format(
			threshold = options.step_threshold,
			concentration_root = options.concentration_root,
			concentration_branching = options.concentration_branching,
			concentration_switch = options.concentration_switch,
			# Step
			expected_non_terminal_counts_step = expected_non_terminal_counts_step,
			expected_init_counts_step = expected_init_counts_step,
			expected_rule_counts_step = expected_rule_counts_step,
			expected_terminal_counts_step = expected_terminal_counts_step,
			expected_nt2t_counts_step = expected_nt2t_counts_step,
			# tanh
			expected_non_terminal_counts_tanh = expected_non_terminal_counts_tanh,
			expected_init_counts_tanh = expected_init_counts_tanh,
			expected_rule_counts_tanh = expected_rule_counts_tanh,
			expected_terminal_counts_tanh = expected_terminal_counts_tanh,
			expected_nt2t_counts_tanh = expected_nt2t_counts_tanh
			)
		)




	elif options.model == 'HMM':
		df_trans = pd.read_hdf(options.hdf5_path, key='/transition')
		df_init = pd.read_hdf(options.hdf5_path, key='/init')
		df_base_count = pd.read_hdf(options.hdf5_path, key='/state')
		num_states = df_init.shape[0]
		expected_rule_tokens = df_trans.dirichlet_par.values.reshape(num_states,num_states) - options.concentration_transition * df_base_count.p.values[np.newaxis,:]
		expected_rule_counts_step = step_counter(expected_rule_tokens, options.step_threshold)
		expected_rule_counts_tanh = tanh_counter(expected_rule_tokens)

		expected_init_tokens = df_init.dirichlet_par.values - options.concentration_init * df_base_count.p.values
		expected_init_counts_step = step_counter(expected_init_tokens, options.step_threshold)
		expected_init_counts_tanh = tanh_counter(expected_init_tokens)
		
		df_emit = pd.read_hdf(options.hdf5_path, key='/emission/covmat/df')
		expected_state_tokens = df_emit.df.values - options.df_prior
		expected_state_counts_step = step_counter(expected_state_tokens, options.step_threshold)
		expected_state_counts_tanh = tanh_counter(expected_state_tokens)

		df_length = pd.read_hdf(options.hdf5_path, key='/length')
		expected_halt_state_tokens = df_length.stop - options.concentration_stop
		expected_halt_state_counts_step = step_counter(expected_halt_state_tokens, options.step_threshold)
		expected_halt_state_counts_tanh = tanh_counter(expected_halt_state_tokens)


		print(
"""
HDP-HMM estimated grammar size.
============================================
PARAMETERS

Concentration for initial-transition: {concentration_init}
Concentration for transition: {concentration_transition}
============================================
ESTIMATION BY STEP FUNCTION W/ THRESHOLD={threshold}

# of states: {expected_state_counts_step}
# of haltable states: {expected_halt_state_counts_step}
# of init transition: {expected_init_counts_step}
# of inter-state transition: {expected_rule_counts_step}
============================================
ESTIMATION BY tanh

# of states: {expected_state_counts_tanh}
# of haltable states: {expected_halt_state_counts_tanh}
# of init transition: {expected_init_counts_tanh}
# of inter-state transition: {expected_rule_counts_tanh}
""".format(
			threshold = options.step_threshold,
			concentration_init = options.concentration_init,
			concentration_transition = options.concentration_transition,
			# Step
			expected_init_counts_step = expected_init_counts_step,
			expected_halt_state_counts_step = expected_halt_state_counts_step,
			expected_rule_counts_step = expected_rule_counts_step,
			expected_state_counts_step = expected_state_counts_step,
			# tanh
			expected_init_counts_tanh = expected_init_counts_tanh,
			expected_halt_state_counts_tanh = expected_halt_state_counts_tanh,
			expected_rule_counts_tanh = expected_rule_counts_tanh,
			expected_state_counts_tanh = expected_state_counts_tanh
			)
		)

	else:
		raise ValueError('Invalid model name: {model}'.format(model=options.model))


def step_counter(expected_tokens, threshold):
	return np.sum(expected_tokens >= threshold)

def tanh_counter(expected_tokens):
	return np.sum(np.tanh(expected_tokens))


def get_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('hdf5_path', type=str, help = 'path to the result HDF5 file')
	parser.add_argument('-m', '--model', type = str, default = 'PCFG', help = 'Model type (PCFG or HMM). Default is PCFG (with Gaussian emission).')
	parser.add_argument('-t', '--step_threshold', type = np.float64, default = 1.0, help = 'Threshold for grammar counting by the step function.')
	parser.add_argument('-b', '--concentration_branching', type = np.float64, default = 1.0, help = 'Concentration of the DP for sampling of branching rules in HDP-PCFG.')
	parser.add_argument('-T', '--concentration_nt2t', type = np.float64, default = 1.0, help = 'Concentration of the DP for sampling of non-terminal-to-terminal rules in HDP-PCFG.')
	parser.add_argument('-r', '--concentration_root', type = np.float64, default = 1.0, help = 'Concentration of the DP for sampling of root-labeling rules in HDP-PCFG.')
	parser.add_argument('-i', '--concentration_init', type = np.float64, default = 1.0, help = 'Concentration of the DP for sampling of initial transitions for HDP-HMM.')
	parser.add_argument('-s', '--concentration_switch', type = np.float64, default = 1.0, help = 'Concentration of the Dirichlet distribution for branching-emission switching in HDP-PCFG.')
	parser.add_argument('-R', '--concentration_transition', type = np.float64, default = 1.0, help = 'Concentration of the DP for sampling of transitions in HDP-HMM.')
	parser.add_argument('-S', '--concentration_stop', type = np.float64, default = 1.0, help = 'Concentration of the Dirichlet distribution for the termination probability in HDP-HMM.')
	parser.add_argument('-f', '--df_prior', type = np.float64, default = 13 - 1 + 0.01, help = 'The d.f. of the IW prior on the covariance matrix of the Gaussian emission.')
	
	return parser.parse_args()

if __name__ == '__main__':
	options = get_arguments()

	get_grammar_size(options)
