# coding: utf-8

import numpy as np
import scipy.special as sspec
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import seaborn as sns
import os.path, argparse


def visualize_parse_ratios(df, save_path=None):
	cm = ListedColormap(['C0','C1','C2','C6'])
	ax = df.plot.bar(stacked=True, colormap=cm)
	xticklabels = ax.get_xticklabels()
	ax.set_xticklabels(xticklabels, rotation=True)
	ax.set_xlabel('')
	ax.set_ylabel('Expected parse counts')
	plt.title('Expected parse counts')
	# for p in ax.patches:
	# 	width = p.get_width()
	# 	height = p.get_height()
	# 	x,y = p.get_xy()
	# 	height_ratio = 0.99
	# 	# width_ratio = width/40.0
	# 	ax.annotate('{height:.2f} ({percent:.1f}%)'.format(height=height, percent=(100.0*height_ratio)), (x+width*0.1, y+.4*height))
	if save_path is None:
		plt.tight_layout()
		plt.show()
	else:
		plt.savefig(save_path, bbox_inches='tight')
	print(df)
	# plt.show()

def get_uniform_baseline(length, minimum_length=0):
	if length<minimum_length:
		return 0.0
	else:
		length_minus_1 = length - 1
		catalan = sspec.binom(2*length_minus_1, length_minus_1) / length
		return catalan**-1


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('data_path', type = str, help = 'path to the data to plot.') #, nargs="+")
	# parser.add_argument('-n','--names', type=str, default=None, help='Names of the data.', nargs="*")
	parser.add_argument('-s', '--save_path', type=str, default=None, help='Path to the img file to save the result.')
	parser.add_argument('-O', '--optimal', type=str, default=None, help='path to the optimal parse data.')#, nargs='*')
	parser.add_argument('-U', '--uniform_baseline', action='store_true', help='If selected, shows the uniform baseline.')
	options = parser.parse_args()

	# dfs = [pd.read_csv(p) for p in options.data_path]
	# # print('# length>=4', dfs[0][dfs[0].string_length>=4].shape[0])
	# if options.names is None:
	# 	options.names = range(len(dfs))
	# for df,name in zip(dfs,options.names):
	# 	df['name'] = name
	# df = pd.concat(dfs, axis=0)
	df = pd.read_csv(options.data_path)

	threshold = 3
	below_threshold = 'length<{threshold}'.format(threshold=threshold)
	df[below_threshold] = 0
	df.loc[df.string_length<threshold,below_threshold] = 1.0
	df['left-branching'] = df.left_parse_ratio
	df.loc[df.string_length<threshold,'left-branching'] = 0.0
	df['right-branching'] = df.right_parse_ratio
	df.loc[df.string_length<threshold,'right-branching'] = 0.0
	df['non-regular'] = df.non_regular_parse_ratio
	df.loc[df.string_length<threshold,'non-regular'] = 0.0
	# df.loc[:,'name'] = pd.Categorical(df.name, categories=options.names, ordered=True)
	# df_count = df.groupby('name')[['non-regular','left-branching','right-branching',below_threshold]].sum(axis=0)
	df_count = df[['non-regular','left-branching','right-branching',below_threshold]].sum(axis=0).to_frame(name='total').T

	if not options.optimal is None:
		# dfs_optimal = [pd.read_csv(p) for p in options.optimal]
		# for df_opt,name in zip(dfs_optimal,options.names):
			# df_opt['name'] = name+' (OPT)'
		# df_opt = pd.concat(dfs_optimal, axis=0)
		df_opt = pd.read_csv(options.optimal)
		df_opt[below_threshold] = 0
		df_opt.loc[df_opt.string_length<threshold,below_threshold] = 1.0
		df_opt['left-branching'] = df_opt.viterbi_parse_ratio
		df_opt.loc[(df_opt.string_length<threshold)|(~df_opt.is_left_branching),'left-branching'] = 0.0
		df_opt['right-branching'] = df_opt.viterbi_parse_ratio
		df_opt.loc[(df_opt.string_length<threshold)|(~df_opt.is_right_branching),'right-branching'] = 0.0
		df_opt['non-regular'] = df_opt.viterbi_parse_ratio
		df_opt.loc[(df_opt.string_length<threshold)|df_opt.is_left_branching|df_opt.is_right_branching,'non-regular'] = 0.0
		# df_count_opt = df_opt.groupby('name')[['non-regular','left-branching','right-branching',below_threshold]].sum(axis=0)
		df_count_opt = df_opt[['non-regular','left-branching','right-branching',below_threshold]].sum(axis=0).to_frame(name='optimal').T
		df_count = pd.concat([df_count,df_count_opt], axis=0, sort=False)
	
	if options.uniform_baseline:
		minimum_length = 4
		uniform_baseline = df.string_length.map(lambda l: get_uniform_baseline(l,minimum_length=minimum_length)).sum()
		df_base = pd.DataFrame([[uniform_baseline]], columns=['non-regular'], index=['uniform baseline'])
		df_count = pd.concat([df_count,df_base], axis=0, sort=False)

	visualize_parse_ratios(df_count, save_path=options.save_path)