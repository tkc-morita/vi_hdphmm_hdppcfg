# coding: utf-8

import sys, os.path, argparse
file_dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(file_dir_path, '..'))
import numpy as np
import pandas as pd
import posterior_prediction as pp
import data_parser as dtp


def sample(options):
	if options.model == 'PCFG':
		raise NotImplementedError('Sampling from PCFG has not been implemented.')
		predictor = pp.HDP_PCFG_Gaussian_Predictor(options.hdf_path)
	elif options.model == 'HMM':
		predictor = pp.HDP_HMM_Gaussian_Predictor(options.hdf_path)

	if not options.data_path is None:
		data = dtp.Dataset_of_Sequences(
					options.data_path,
					seq_col=options.seq_col,
					time_col=options.time_col,
					dim_col=options.dim_col,
					val_col=options.val_col,
					discrete=True if hasattr(predictor, 'ix2label') else False,
					old_format=options.old_data_format,
					singleton_batch=True
					)
		lengths = [len(string) if isinstance(string, list) else string.shape[1] for string in data.iter_data()]
	elif not options.data_size is None:
		lengths = [None] * options.data_size
	else:
		raise ValueError('Either data_path or data_size must be specified.')

	predictor.prepare_posterior_prediction()

	rename_existing_file(options.save_path)

	for seq_ix,l in enumerate(lengths):
		seq,terminal_or_state,visit_counts_during_training = predictor.sample(length=l, only_visited=options.only_visited)
		if seq.ndim==1:
			seq = seq[:,None]
			multivariate = False
		else:
			multivariate = True
		df = pd.DataFrame(seq)
		df[options.time_col] = df.index
		df['terminal_or_state'] = terminal_or_state
		df['visit_counts_during_training'] = visit_counts_during_training
		if multivariate:
			df = df.melt(
					id_vars=[options.time_col,'terminal_or_state','visit_counts_during_training'],
					var_name=options.dim_col,
					value_name=options.val_col
					)
		else:
			df = df.rename(columns={0:options.val_col})
		df[options.seq_col] = seq_ix
		if os.path.isfile(options.save_path):
			df.to_csv(options.save_path, index=False, mode='a', header=False)
		else:
			df.to_csv(options.save_path, index=False)


def rename_existing_file(filepath):
	if os.path.isfile(filepath):
		new_path = filepath+'.orig'
		rename_existing_file(new_path)
		os.rename(filepath, new_path)

def get_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('hdf_path', type=str, help = 'path to the result HDF5 file')
	parser.add_argument('-s', '--save_path', type=str, default=None, help = 'path to csv where samples are saved.')
	parser.add_argument('-d','--data_path', type = str, default=None, help = 'path to data whose lengths are used.')
	parser.add_argument('-D', '--data_size', type=int, help='# of sequences to sample.')
	parser.add_argument(
				"--seq_col",
				type=str,
				default='sequence_ix',
				help="Name of the csv column containing the index of the sequences."
	)
	parser.add_argument(
				"--time_col",
				type=str,
				default='time_ix',
				help="Name of the csv column containing the discrete time indexes of the sequences."
	)
	parser.add_argument(
				"--dim_col",
				type=str,
				default='dim',
				help="Name of the csv column containing the index of the dimensions of the data points."
	)
	parser.add_argument(
				"--val_col",
				type=str,
				default='value',
				help="Name of the csv column containing the data values."
	)
	parser.add_argument(
		'-O',
		'--old_data_format',
		action='store_true',
		help='If selected, each row of the data file is assumed to represent a single sequence, each discrete time is tab-stop-separated, and each data dimension is comma-separated.'
	)
	parser.add_argument('-m', '--model', type = str, default = 'HMM', help = 'Model type (PCFG or HMM). Default is PCFG (with Gaussian emission).')
	parser.add_argument('--seed', type=int, default=111, help='Random seed.')
	parser.add_argument('--only_visited', action='store_true', help='If selected, use only states whose expected counts are at least 1 based on the training data.')
	return parser.parse_args()


if __name__ == '__main__':
	options = get_arguments()

	np.random.seed(options.seed)

	if options.save_path is None:
		options.save_path = os.path.join(os.path.dirname(options.hdf_path), 'samples.csv')

	df = sample(options)