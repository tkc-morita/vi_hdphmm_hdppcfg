# coding: utf-8

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import argparse, os.path


def visualize_predictive_prob_ratio(df, x, y, hue = None):
	# sns.violinplot(
	# 	x=x,
	# 	y=y,
	# 	hue=hue,
	# 	data=df
	# )

	ax = sns.barplot(
		y=y,
		x=x,
		data=df,
		orient='h'
	)
	# ax = df.plot.barh(x=x,y=y)

	# sns.boxplot(
	# 	x=x,
	# 	y=y,
	# 	hue=hue,
	# 	data=df
	# )
	
	# sns.pairplot(
	# 	x_vars = [x],
	# 	y_vars=[y],
	# 	data = df,
	# 	hue=hue,
	# 	size=7
	# )
	# ax = df[df.model == 'PCFG'].plot.scatter(x = x, y= y, color = 'b', label = 'PCFG')
	# df[df.model == 'HMM'].plot.scatter(x = x, y= y, color = 'o', label = 'HMM', ax = ax)
	# df.groupby('model').plot.scatter(x = x, y= y)

	# plt.xlabel('string length')
	# plt.ylabel('log ratio')
	plt.title('Log ratio of per-note posterior predictive probability density (PCFG/HMM)')
	# plt.tight_layout()
	# plt.show()
	plt.savefig('/Users/takashi/Dropbox (MIT)/gibbon_song/drafts/hmm_vs_pcfg/figures/log_posterior_predictive_prob_ratio.png', bbox_inches='tight')

def visualize_predictive_prob(df, x, y, hue=None, save_path=None):
	# ax = sns.barplot(
	# 	y=y,
	# 	x=x,
	# 	data=df
	# )
	# ax = sns.boxplot(x=x,y=y,data=df)
	ax = sns.violinplot(x=x, y=y, data=df)
	plt.title('Log per-data-point posterior predictive probability density')
	plt.ylabel('Log per-data-point post. pred. prob. density')
	if save_path is None:
		plt.tight_layout()
		plt.show()
	else:
		plt.savefig(save_path, bbox_inches='tight')
	print(df.groupby(x)[y].describe())
	# print('mean')
	# print(df.groupby(x)[y].mean())
	# print('std')
	# print(df.groupby(x)[y].std())


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('PCFG_path', type=str, help='Path to the csv file containing posterior predictive probability based on PCFG.')
	parser.add_argument('HMM_path', type=str, help='Path to the csv file containing posterior predictive probability based on HMM.')
	parser.add_argument('-s', '--save_path', type=str, default=None, help='Path to the img file to save the result.')
	args = parser.parse_args()

	result_path_PCFG = args.PCFG_path
	result_path_HMM = args.HMM_path
	# result_path_fin_HMM = sys.argv[3]

	prob_type = 'mean_per_point_E_p_all'

	df_PCFG = pd.read_csv(result_path_PCFG).rename(columns = {prob_type:'mean_log_prob_per_note'})
	df_PCFG['model'] = 'PCFG'
	df_HMM = pd.read_csv(result_path_HMM).rename(columns = {prob_type:'mean_log_prob_per_note'})
	df_HMM['model'] = 'HMM'
	# df_fin = pd.read_csv(result_path_fin_HMM)
	# df_fin['model'] = 'fin-HMM'

	df = pd.concat([df_PCFG, df_HMM], axis=0)
	# df = df[df.length>=4]

	# df_diff = pd.DataFrame()
	# target = 'mean_log_prob_per_note'
	# target = 'total_log_post_pred_prob'
	# df_diff['PCFG-HMM'] = df_PCFG[target] - df_HMM[target]
	# df_diff['PCFG-fin'] = df_PCFG[target] - df_fin[target]
	# df_diff['length'] = df_PCFG.length
	# df_diff['string_id'] = df_PCFG.string_id
	# for length,sub_df in df_diff.groupby('length'):
		# print(length)
		# print(sub_df['PCFG-HMM'].std())

	# y = 'PCFG-HMM'
	# y = 'PCFG-fin'

	x = 'model'
	# x = 'length'

	# hue = None
	# hue = 'model'

	y = 'mean_log_prob_per_note'



	visualize_predictive_prob(df,x,y,save_path=args.save_path)
	# visualize_predictive_prob_ratio(df_diff,x,y)