# coding: utf-8

import sys, os.path, argparse
file_dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(file_dir_path, '..'))
import numpy as np
import pandas as pd
import posterior_prediction as pp
import data_parser as dtp


def get_viterbi_parse(options):
	data = dtp.Dataset_of_Sequences(options.data_path, seq_col=options.seq_col, time_col=options.time_col, dim_col=options.dim_col, val_col=options.val_col, old_format=options.old_data_format, singleton_batch=True)
	predictor = pp.HDP_PCFG_Gaussian_Predictor(options.hdf_path)

	if options.prob_type == 'E_p':
		predictor.prepare_posterior_prediction()
	elif options.prob_type == 'E_log':
		predictor.set_E_log_probs()

	results = []
	
	for string in data.iter_data():
		log_like_whole = predictor.get_log_prob(string)
		viterbi_parse,max_log_like = predictor.get_viterbi_parse(string)
		viterbi_parse_ratio = np.exp(max_log_like - log_like_whole)
		string_length = string.shape[1]
		is_left_branching = viterbi_parse.is_left_branching()
		is_right_branching = viterbi_parse.is_right_branching()
		results.append([log_like_whole,max_log_like,viterbi_parse_ratio,string_length,viterbi_parse.print_structure(),is_left_branching,is_right_branching])
	return pd.DataFrame(results, columns = ['log_like_whole','max_log_like','viterbi_parse_ratio','string_length','viterbi_parse','is_left_branching','is_right_branching'])




def get_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('hdf_path', type=str, help = 'path to the result HDF5 file')
	parser.add_argument('data_path', type = str, help = 'path to data to predict.')
	parser.add_argument('-p', '--prob_type', type = str, default = 'E_log', help = 'Type of probability used. E[p] ("E_p") or E[log p] ("E_log"). Default is "E_log."')
	parser.add_argument(
				"--seq_col",
				type=str,
				default='sequence_ix',
				help="Name of the csv column containing the index of the sequences."
	)
	parser.add_argument(
				"--time_col",
				type=str,
				default='time_ix',
				help="Name of the csv column containing the discrete time indexes of the sequences."
	)
	parser.add_argument(
				"--dim_col",
				type=str,
				default='dim',
				help="Name of the csv column containing the index of the dimensions of the data points."
	)
	parser.add_argument(
				"--val_col",
				type=str,
				default='value',
				help="Name of the csv column containing the data values."
	)
	parser.add_argument(
		'-O',
		'--old_data_format',
		action='store_true',
		help='If selected, each row of the data file is assumed to represent a single sequence, each discrete time is tab-stop-separated, and each data dimension is comma-separated.'
	)
	return parser.parse_args()


if __name__ == '__main__':
	options = get_arguments()

	df = get_viterbi_parse(options)

	result_dir = os.path.split(options.hdf_path)[0]
	filename = os.path.splitext(os.path.split(options.data_path)[1])[0] + '_viterbi-parse.csv'
	df.to_csv(os.path.join(result_dir, filename), index=False)