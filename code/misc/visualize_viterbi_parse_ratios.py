# coding: utf-8

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os, argparse
import my_autopct



def get_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('viterbi_data_path', type = str, help = 'path to Viterbi parse data.')
	parser.add_argument('regular_data_path', type = str, help = 'path to regular parse data.')
	return parser.parse_args()

def get_uniform_base_line(length):
	length_and_shorter = np.arange(2,length+1)
	baseline = np.log(2) * (length - 1) - np.sum(np.log(length_and_shorter) + np.log(length_and_shorter - 1))
	return baseline


def visualize_left_and_right_branching_ratios_in_viterbi_parses(df, save_dir):
	df_4_or_longer = df[df.string_length >= 4]
	print('# of strings whose length >= 4: {data_size}'.format(data_size=df_4_or_longer.shape[0]))
	parse_type_counts = df_4_or_longer.parse_type.value_counts()
	parse_type_counts /= parse_type_counts.sum().astype(np.float64)
	parse_type_counts.plot.pie(legend=True, autopct=my_autopct.my_autopct)
	plt.ylabel('')
	plt.title('Types of Viterbi Parses')
	# plt.show()
	plt.savefig(os.path.join(save_dir, 'left-and-right-branching-ratios_in_viterbi-parses.png'), bbox_inches='tight')
	plt.clf()

def visualize_prob_of_non_regular_viterbi_parses(df, save_dir):
	df_non_regular = df[df.parse_type == 'non-regular']
	# sns.distplot(df_non_regular.viterbi_parse_ratio, kde=False, label='non-regular Viterbi', color='blue')
	df_non_regular.viterbi_parse_ratio.plot.hist(label='non-regular Viterbi', bins=30)
	# sns.distplot(df_non_regular.uniform_baseline, kde=False, label='uniform baseline', color='orange')
	df_non_regular.uniform_baseline.plot.hist(label='uniform baseline', color='brown')
	plt.xlabel('Parse Probability')
	plt.legend()
	# plt.show()
	plt.savefig(os.path.join(save_dir, 'prob_of_non-regular_viterbi-parses.png'), bbox_inches='tight')
	plt.clf()

def visualize_prob_ratio_of_non_regular_Viterbi_to_regular(df, save_dir):
	df_non_regular_unmelted = df[df.parse_type == 'non-regular']
	print('# of strings with non-regular Viterbi parse: {data_size}'.format(data_size=df_non_regular_unmelted.shape[0]))
	df_non_regular = pd.melt(df_non_regular_unmelted, value_vars=['log_viterbi_to_left','log_viterbi_to_right'], var_name='denominator', value_name='log_viterbi_to_regular')
	df_non_regular.loc[:,'denominator'] = df_non_regular.loc[:,'denominator'].str.replace('log_viterbi_to_','').map(lambda x: x+'-branching')
	sns.barplot(x = 'denominator', y = 'log_viterbi_to_regular', data=df_non_regular, palette={'left-branching':'C2', 'right-branching':'C1'})
	plt.ylabel('Log Probability Ratio')
	plt.xlabel('Regular Parse Type')
	plt.title('Log Probability Ratio of Non-Regular Viterbi Parse to Regular Parses')
	# plt.show()
	plt.savefig(os.path.join(save_dir, 'log_viterbi_to_regular.png'), bbox_inches='tight')
	plt.clf()


if __name__ == '__main__':
	options = get_arguments()

	df = pd.read_csv(options.viterbi_data_path)
	df.loc[(df.is_left_branching & df.is_right_branching), 'parse_type'] = 'both'
	df.loc[(~df.is_left_branching & df.is_right_branching), 'parse_type'] = 'right-branching'
	df.loc[(df.is_left_branching & ~df.is_right_branching), 'parse_type'] = 'left-branching'
	df.loc[(~df.is_left_branching & ~df.is_right_branching), 'parse_type'] = 'non-regular'
	df['uniform_baseline'] = np.exp(df.string_length.map(get_uniform_base_line))

	df_regular = pd.read_csv(options.regular_data_path)
	df['log_viterbi_to_left'] = df.max_log_like - df_regular.log_like_left
	df['log_viterbi_to_right'] = df.max_log_like - df_regular.log_like_right

	save_dir = os.path.splitext(options.viterbi_data_path)[0]
	if not os.path.isdir(save_dir):
		os.makedirs(save_dir)

	visualize_left_and_right_branching_ratios_in_viterbi_parses(df, save_dir)
	# visualize_prob_of_non_regular_viterbi_parses(df, save_dir)

	visualize_prob_ratio_of_non_regular_Viterbi_to_regular(df, save_dir)

	