# coding: utf-8

import numpy as np
import pandas as pd

class Dataset_of_Sequences(object):
	def __init__(
			self,
			data_path,
			seq_col='sequence_ix',
			time_col='time_ix',
			dim_col='dim',
			val_col='value',
			discrete=False,
			manual_num_categories=None,
			singleton_batch=False,
			old_format = False
			):
		self.label2ix = {}
		if old_format:
			assert not discrete, 'Discrete data are not supported with the old format.'
			with open(data_path,'r') as f: # Read the phrase file.
				if singleton_batch: # Treat each string as a singleton batch.
					self.data = [
							np.array([
								map(np.float64, data_point.split(','))
								for data_point in string.rstrip('\n').split('\t')
							])[np.newaxis,:,:]
							for string in f.readlines()
							if string.rstrip('\n')
						]
				else:
					self.data = [
							[
								np.array(map(np.float64, data_point.split(',')))
								for data_point in string.rstrip('\n').split('\t')
							]
							for string in f.readlines()
							if string.rstrip('\n')
							]
		else:
			df_data = pd.read_csv(data_path)
			self.data = []
			label_ix = 0
			for seq_ix, df_seq in df_data.groupby(seq_col, sort=True):
				seq = []
				for time_ix, df_time in df_seq.groupby(time_col, sort=True):
					if discrete:
						label = df_time[val_col].values[0]
						if label in self.label2ix:
							d = self.label2ix[label]
						else:
							self.label2ix[label] = label_ix
							d = label_ix
							label_ix += 1
					else:
						d = df_time.sort_values(dim_col)[val_col].values
					seq.append(d)
				if singleton_batch:
					seq = np.array(seq)[np.newaxis]
				self.data.append(seq)
		self.num_sequences = len(self.data)
		self.grouped = False
		if self.label2ix:
			self.ix2label = {ix:label for label,ix in self.label2ix.items()}
			assert (manual_num_categories is None) or (manual_num_categories >= len(self.label2ix)),\
					'manual_num_categories should be equal to or greater than # of categories included in the data.'
			self.manual_num_categories = manual_num_categories
		else:
			self.label2ix = None
			self.ix2label = None
		
	def group_by_length(self, max_batch_array_size=650):
		"""
		Group data by string length.

		Option
		max_batch_array_size (int): Maximum size of the batch ndarrays.
		"""
		if not self.grouped:
			grouped_data = {}
			for string in self.data:
				string_length = len(string)
				if string_length in grouped_data.keys():
					batch_list = grouped_data[string_length]
					if batch_list[-1].size <= max_batch_array_size:
						batch_list[-1] = np.append(
												batch_list[-1],
												np.array(string)[np.newaxis]
												,
												axis=0
												)
					else:
						batch_list.append(np.array(string)[np.newaxis])
				else:
					grouped_data[string_length] = [np.array(string)[np.newaxis]]
			self.data = grouped_data
			self.grouped = True

	def iter_data(self, with_length = False):
		"""
		If grouped by string length, iterate over lists of batches of strings.
		Otherwise, iterate over strings.
		"""
		if self.grouped:
			if with_length:
				return self.data.iteritems()
			else:
				return self.data.itervalues()
		else:
			assert not with_length, 'Call "group_by_length()" before iterate data with length.'
			return self.data

	def mean(self):
		if self.grouped:
			data_size_tpl,mean_tpl = zip(*[(length*batch.shape[0]*batch.shape[1], np.sum(batch, axis=(0,1))) for length,batch_list in self.data.iteritems() for batch in batch_list])
			mean = np.sum(np.array(mean_tpl), axis=0) / np.sum(data_size_tpl)
		else:
			mean = np.mean(np.array([data_point for seq in self.data for data_point in seq]), axis=0)
		return mean

	def get_dimension(self):
		if self.grouped:
			return self.data.values[0][0].shape[-1] # self.data.values[0][0] is num_batches x str_len x dimension
		else:
			return np.array(self.data[0]).shape[-1]

	def get_num_sequences(self):
		return self.num_sequences

	def get_num_categories(self):
		if self.label2ix is None:
			return None
		elif self.manual_num_categories is None:
			return len(self.label2ix)
		else:
			return self.manual_num_categories