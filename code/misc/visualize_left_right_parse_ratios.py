# coding: utf-8

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os.path, argparse


def visualize_left_right_parse_ratios(df, save_path):
	plt.figure(figsize=(8,4))
	max_length = df.string_length.max() #10
	ax = sns.barplot(x='string_length', y='log_ratio', hue='parse_type', data=df[(df.string_length <= max_length) & (df.string_length >= 3)])
	# ax.set_facecolor('whitesmoke')
	
	
	# Draw random baseline.
	df_length = df.string_length.drop_duplicates().sort_values().reset_index(drop=True).to_frame(name='string_length')
	df_length['uniform_baseline'] = df_length.string_length.map(get_uniform_base_line)
	df_length['ix'] = df_length.index
	df_length.plot.scatter(x='ix',y='uniform_baseline',c='xkcd:yellow', ax=ax, zorder=2, label='uniform_baseline')
	ax.legend(facecolor='lightgrey')
	ax.set_xlabel('string length')
	ax.set_ylabel('log ratio of expected parse counts')
	ax.set_title('Log ratio of left/right parse counts to all')

	# plt.show()
	plt.savefig(save_path)

def get_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('data_path', type = str, help = 'path to data to plot.')
	return parser.parse_args()

def get_uniform_base_line(length):
	return -np.sum(np.log(np.arange(2,length))


def visualize_cumulative_left_right_parse_counts(df, save_path):
	print df
	df[df.string_length >= 6].groupby('parse_type').expected_counts.sum().plot.barh()
	plt.show()

if __name__ == '__main__':
	options = get_arguments()

	df = pd.read_csv(options.data_path)
	df['left_parse_log_ratio'] = df.log_like_left - df.log_like_whole
	df['right_parse_log_ratio'] = df.log_like_right - df.log_like_whole

	# df = df.rename(columns={'left_parse_log_ratio':'left_branching','right_parse_log_ratio':'right_branching'})
	# df = pd.melt(df, id_vars=['string_length'], value_vars=['left_branching','right_branching'], var_name='parse_type', value_name='log_ratio')
	# visualize_left_right_parse_ratios(df, os.path.splitext(options.data_path)[0]+'_all.png')


	df = df.rename(columns={'left_parse_ratio':'left_branching','right_parse_ratio':'right_branching'})
	df['uniform_baseline'] = np.exp(df.string_length.map(get_uniform_base_line))
	df = pd.melt(df, id_vars=['string_length'], value_vars=['left_branching','right_branching', 'uniform_baseline'], var_name='parse_type', value_name='expected_counts')
	visualize_cumulative_left_right_parse_counts(df, os.path.splitext(options.data_path)[0]+'_cum.png')

	