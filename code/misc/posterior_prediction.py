# coding: utf-8

import sys, os.path
file_dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(file_dir_path, '..'))
import numpy as np
import scipy.special as sps
import pandas as pd
import nltk
import vi.algorithm.inner_outer_algorithm as ioa
import vi.algorithm.forward_backward_algorithm as fba
import vi.variational_inference as vi
import argparse
import vi.hdp_pcfg as hdp_pcfg
import vi.hdp_hmm as hdp_hmm
import vi.distributions
import data_parser as dtp
from statsmodels.sandbox.distributions.multivariate import multivariate_t_rvs

def get_viterbi_parse(data, branch_rule_weights, emission_rule_weights, init_weights):
	inner_outer_algorithm = ioa.Inner_Outer_Algorithm(branch_rule_weights, emission_rule_weights, init_weights)
	return [inner_outer_algorithm.get_viterbi_parse(string) for string in data]

def save_parse(trees, hdf_path):
	with open(hdf_path, 'w') as f:
		for tree in trees:
			f.write(tree.print_structure())
			f.write('\n')






class HDP_PCFG_Gaussian_Predictor(hdp_pcfg.HDP_PCFG):
	def __init__(self, hdf5_path):
		df_branch = pd.read_hdf(hdf5_path, key='/branching')
		df_root = pd.read_hdf(hdf5_path, key='/root')
		df_switch = pd.read_hdf(hdf5_path, key='/switch')
		num_non_terminals = df_switch.shape[0]

		branch_params = { # Not used.
			'concentration_top':1.0,
			'concentration_bottom':1.0,
			'concentration_init':1.0,
			'num_non_terminals':num_non_terminals,
			'gradient_projection_tolerance':0.01
		}
		self.branch_hdp = vi.distributions.HDP_branch(branch_params)
		self.branch_hdp.varpar_rule_weight = df_branch.sort_values(['LHS','RHS_left','RHS_right']).dirichlet_par.values.reshape((num_non_terminals,)*3)
		self.branch_hdp.sum_rule_weight = np.sum(self.branch_hdp.varpar_rule_weight, axis=(1,2))
		self.branch_hdp.E_log_rule_weight = sps.digamma(self.branch_hdp.varpar_rule_weight) - sps.digamma(self.branch_hdp.sum_rule_weight)[:,np.newaxis,np.newaxis]

		self.branch_hdp.varpar_init_weight = df_root.sort_values('non_terminal').dirichlet_par.values
		self.branch_hdp.sum_init_weight = np.sum(self.branch_hdp.varpar_init_weight)
		self.branch_hdp.E_log_init_weight = sps.digamma(self.branch_hdp.varpar_init_weight) - sps.digamma(self.branch_hdp.sum_init_weight)
		

		
		switch_params = { # Not used.
			'base_counts':(1.0, 1.0),
			'num_conditions':num_non_terminals,
			'num_categories':2
		}
		self.switch_distributions = vi.distributions.Dirichlet(switch_params)
		self.switch_distributions.varpar_rule_weight[:,0] = df_switch.branch.values
		self.switch_distributions.varpar_rule_weight[:,1] = df_switch.emit.values
		self.switch_distributions.sum_rule_weight = np.sum(self.switch_distributions.varpar_rule_weight, axis=-1)
		self.switch_distributions.E_log_rule_weight = sps.digamma(self.switch_distributions.varpar_rule_weight) - sps.digamma(self.switch_distributions.sum_rule_weight)[:,np.newaxis]
		

		df_nt2t = pd.read_hdf(hdf5_path, key='/nt2t')
		df_terminal = pd.read_hdf(hdf5_path, key='/terminal')
		num_terminals = df_terminal.shape[0]
		nt2t_params = { # not used.
			'concentration_top':1.0,
			'concentration_bottom':1.0,
			'start_size':num_non_terminals,
			'goal_size':num_terminals,
			'gradient_projection_tolerance':0.01
		}
		self.nt2t_rule_hdp = vi.distributions.HDP_trans(nt2t_params)
		self.nt2t_rule_hdp.varpar_transition = df_nt2t.sort_values(['non_terminal','terminal']).dirichlet_par.values.reshape(num_non_terminals, num_terminals)
		self.nt2t_rule_hdp.sum_transition = np.sum(self.nt2t_rule_hdp.varpar_transition, axis=-1)
		self.nt2t_rule_hdp.E_log_transition = sps.digamma(self.nt2t_rule_hdp.varpar_transition) - sps.digamma(self.nt2t_rule_hdp.sum_transition)[:,np.newaxis]


		df_mean_mean = pd.read_hdf(hdf5_path, key='/emission/mean/mean')
		dims = [col_name for col_name in df_mean_mean.columns if col_name.startswith('dim_')]
		dimension = len(dims)
		emission_params = { # not used
			'distribution':'Gaussian',
			'dimension':dimension,
			'mean_prior_mean':np.zeros(dimension),
			'mean_prior_denominator':dimension,
			'covmat_prior_scale_mat':np.eye(dimension),
			'covmat_prior_df':dimension,
			'num_components':num_terminals
		}
		self.emission_distributions = vi.distributions.Gaussian(emission_params)
		self.emission_distributions.varpar_mean_mean = df_mean_mean.sort_values('cluster').loc[:,dims].values
		df_mean_denom = pd.read_hdf(hdf5_path, key='/emission/mean/denominator')
		self.emission_distributions.varpar_mean_denominator = df_mean_denom.sort_values('cluster').value.values
		df_covmat_df = pd.read_hdf(hdf5_path, key='/emission/covmat/df')
		self.emission_distributions.varpar_covmat_df = df_covmat_df.sort_values('cluster').df.values
		df_covmat_sm = pd.read_hdf(hdf5_path, key='/emission/covmat/scale_mat')
		varpar_covmat_scale_mat = np.zeros((num_terminals,dimension,dimension))
		for row_tuple in df_covmat_sm.itertuples():
			varpar_covmat_scale_mat[row_tuple.cluster, row_tuple.row_id, row_tuple.col_id] = row_tuple.matrix_entry
			varpar_covmat_scale_mat[row_tuple.cluster, row_tuple.col_id, row_tuple.row_id] = row_tuple.matrix_entry
		self.emission_distributions.inv_varpar_covmat_scale_mat = np.linalg.inv(varpar_covmat_scale_mat)
		
		self.emission_distributions.num_dimensions_over_vpdenom = dimension / self.emission_distributions.varpar_mean_denominator
		self.emission_distributions.half_varpar_covmat_df = self.emission_distributions.varpar_covmat_df * 0.5
		self.emission_distributions.multidigamma_nover2 = self.emission_distributions._multidigamma(self.emission_distributions.half_varpar_covmat_df) # multivariate_digamma(n/2)
		logdet_inv_vp_covmat_sm = np.linalg.slogdet(self.emission_distributions.inv_varpar_covmat_scale_mat)[1] # log |varpar_covmat_scale_ma|
		self.emission_distributions.E_log_det_inv_vp_cm_sm = self.emission_distributions.multidigamma_nover2+logdet_inv_vp_covmat_sm
		self.emission_distributions.vpdf_x_invvpsm = self.emission_distributions.varpar_covmat_df[:,np.newaxis,np.newaxis]*self.emission_distributions.inv_varpar_covmat_scale_mat

	def prepare_posterior_prediction(self):
		self.switch_distributions.set_log_posterior_expectation()
		log_switch_weights = self.switch_distributions.log_posterior_expectation
		self.branch_hdp.set_log_posterior_expectation()
		log_branch_rule_weights = self.branch_hdp.log_posterior_expectation + log_switch_weights[:,0,np.newaxis,np.newaxis]
		self.nt2t_rule_hdp.set_log_posterior_expectation()
		log_nt2t_rule_weights = self.nt2t_rule_hdp.log_posterior_expectation + log_switch_weights[:,1,np.newaxis]
		self.emission_distributions.set_log_posterior_expectation()
		self.inner_outer_algorithm = ioa.Inner_Outer_Algorithm(
											self.branch_hdp.log_posterior_expectation_init,
											log_branch_rule_weights,
											self.emission_distributions.get_log_posterior_pred,
											log_nt2t_rule_weights = log_nt2t_rule_weights
										)


	def get_log_prob(self, string, batched = False):
		if batched:
			return self.inner_outer_algorithm.get_expected_rule_counts(string)[-1]
		else:
			return self.inner_outer_algorithm.get_expected_rule_counts(string)[-1][0]


	def get_expected_counts(self, data, max_batch_array_size = 900):
		self.data = data
		self.data.group_by_length(max_batch_array_size=max_batch_array_size)
		self._update_expected_rule_counts()
		return self.expected_branch_rule_counts, self.expected_root_counts, self.emission_distributions.expected_emission_counts

	def set_E_log_probs(self):
		self._update_arrays()
		self.inner_outer_algorithm = ioa.Inner_Outer_Algorithm(
											self.log_init_weights,
											self.log_branch_rule_weights,
											self.emission_distributions.get_log_prob_for_strings,
											log_nt2t_rule_weights = self.log_nt2t_rule_weights,
										)

	def get_log_prob_left_branching(self, string, batched = False):
		if batched:
			return self.inner_outer_algorithm.get_log_like_left_branching(string)
		else:
			return self.inner_outer_algorithm.get_log_like_left_branching(string)[0]

	def get_log_prob_right_branching(self, string, batched = False):
		if batched:
			return self.inner_outer_algorithm.get_log_like_right_branching(string)
		else:
			return self.inner_outer_algorithm.get_log_like_right_branching(string)[0]

	def get_viterbi_parse(self, string, batched = False):
		viterbi_parses, max_log_like = self.inner_outer_algorithm.get_viterbi_parse(string)
		if batched:
			return viterbi_parses, max_log_like
		else:
			return viterbi_parses[0], max_log_like[0]


class HDP_HMM_Gaussian_Predictor(hdp_hmm.HDP_HMM):
	def __init__(self, hdf5_path):
		df_trans = pd.read_hdf(hdf5_path, key='/transition')
		df_init = pd.read_hdf(hdf5_path, key='/init')
		self.num_states = df_init.shape[0]

		trans_params = { # not used.
			'concentration_top':1.0,
			'concentration_bottom':1.0,
			'start_size':self.num_states+1,
			'goal_size':self.num_states,
			'gradient_projection_tolerance':0.01
		}
		self.trans_hdp = vi.distributions.HDP_trans(trans_params)
		self.trans_hdp.varpar_transition[:-1,:] = df_trans.sort_values(['from_state','to_state']).dirichlet_par.values.reshape((self.num_states,)*2)
		self.trans_hdp.varpar_transition[-1,:] = df_init.sort_values('state').dirichlet_par.values
		self.trans_hdp.sum_transition = np.sum(self.trans_hdp.varpar_transition, axis=-1)
		self.trans_hdp.E_log_transition = sps.digamma(self.trans_hdp.varpar_transition) - sps.digamma(self.trans_hdp.sum_transition)[:,np.newaxis]
		

		try:
			df_length = pd.read_hdf(hdf5_path, key='/length')
			length_params = {
				'base_counts':(1.0, 1.0),
				'num_states':self.num_states
			}
			self.length_distributions = hdp_hmm.Bernoulli_length(length_params)
			self.length_distributions.varpar_stop[:,0] = df_length.stop.values
			self.length_distributions.varpar_stop[:,1] = df_length.through.values
			self.length_distributions.sum_stop = np.sum(self.length_distributions.varpar_stop, axis=-1)
			self.length_distributions.E_log_stop = sps.digamma(self.length_distributions.varpar_stop) - sps.digamma(self.length_distributions.sum_stop)[:,np.newaxis]
		except KeyError:
			self.length_distributions = hdp_hmm.No_length()

		try:
			df_mean_mean = pd.read_hdf(hdf5_path, key='/emission/mean/mean')
			dims = [col_name for col_name in df_mean_mean.columns if col_name.startswith('dim_')]
			dimension = len(dims)
			emission_params = { # not used
				'distribution':'Gaussian',
				'dimension':dimension,
				'mean_prior_mean':np.zeros(dimension),
				'mean_prior_denominator':dimension - 1 + 0.001,
				'covmat_prior_scale_mat':np.eye(dimension),
				'covmat_prior_df':dimension - 1 + 0.001,
				'num_components':self.num_states
			}
			self.emission_distributions = vi.distributions.Gaussian(emission_params)
			self.emission_distributions.varpar_mean_mean = df_mean_mean.sort_values('state').loc[:,dims].values
			df_mean_denom = pd.read_hdf(hdf5_path, key='/emission/mean/denominator')
			self.emission_distributions.varpar_mean_denominator = df_mean_denom.sort_values('state').value.values
			df_covmat_df = pd.read_hdf(hdf5_path, key='/emission/covmat/df')
			self.emission_distributions.varpar_covmat_df = df_covmat_df.sort_values('state').df.values
			df_covmat_sm = pd.read_hdf(hdf5_path, key='/emission/covmat/scale_mat')
			varpar_covmat_scale_mat = np.zeros((self.num_states,dimension,dimension))
			for row_tuple in df_covmat_sm.itertuples():
				varpar_covmat_scale_mat[row_tuple.state, row_tuple.row_id, row_tuple.col_id] = row_tuple.matrix_entry
				varpar_covmat_scale_mat[row_tuple.state, row_tuple.col_id, row_tuple.row_id] = row_tuple.matrix_entry
			self.emission_distributions.inv_varpar_covmat_scale_mat = np.linalg.inv(varpar_covmat_scale_mat)
			

			self.emission_distributions.num_dimensions_over_vpdenom = dimension / self.emission_distributions.varpar_mean_denominator
			self.emission_distributions.half_varpar_covmat_df = self.emission_distributions.varpar_covmat_df * 0.5
			self.emission_distributions.multidigamma_nover2 = self.emission_distributions._multidigamma(self.emission_distributions.half_varpar_covmat_df) # multivariate_digamma(n/2)
			logdet_inv_vp_covmat_sm = np.linalg.slogdet(self.emission_distributions.inv_varpar_covmat_scale_mat)[1] # log |varpar_covmat_scale_ma|
			self.emission_distributions.E_log_det_inv_vp_cm_sm = self.emission_distributions.multidigamma_nover2+logdet_inv_vp_covmat_sm
			self.emission_distributions.vpdf_x_invvpsm = self.emission_distributions.varpar_covmat_df[:,np.newaxis,np.newaxis]*self.emission_distributions.inv_varpar_covmat_scale_mat
		except KeyError:
			df_emit = pd.read_hdf(hdf5_path, key='/emission/dirichlet')
			emission_params = { # not used
				'distribution':'Dirichlet',
				'num_conditions':self.num_states,
				'num_categories':df_emit.label_ix.unique().size
			}
			emission_params['base_counts'] = np.ones(emission_params['num_categories'])
			self.emission_distributions = vi.distributions.Dirichlet(emission_params)
			df_emit = df_emit.sort_values(['state','label_ix'])
			self.emission_distributions.varpar_rule_weight = df_emit.pivot(index='state', columns='label_ix', values='dirichlet_par').values
			self.emission_distributions.sum_rule_weight = np.sum(self.emission_distributions.varpar_rule_weight, axis=-1)
			self.emission_distributions.E_log_rule_weight = sps.digamma(self.emission_distributions.varpar_rule_weight) - sps.digamma(self.emission_distributions.sum_rule_weight)[:,np.newaxis]
			self.ix2label = {tpl.label_ix:tpl.label for tpl in df_emit.loc[:,['label_ix','label']].drop_duplicates().itertuples(index=False)}



	

	def prepare_posterior_prediction(self):
		self.length_distributions.set_log_posterior_expectation()
		self.trans_hdp.set_log_posterior_expectation()
		log_branch_rule_weights = self.trans_hdp.log_posterior_expectation
		self.emission_distributions.set_log_posterior_expectation()
		self.forward_backward_algorithm = fba.Forward_Backward_Algorithm_Multivariate_Emission_Sort_By_Length_Log_Sum_Exp(
											log_branch_rule_weights[:-1,:],
											self.emission_distributions.get_log_posterior_pred,
											log_branch_rule_weights[-1,:],
											length_weight_func=self.length_distributions.weight_log_emission_by_posterior
										)

	def set_E_log_probs(self):
		self._update_arrays()
		self.forward_backward_algorithm = fba.Forward_Backward_Algorithm_Multivariate_Emission_Sort_By_Length_Log_Sum_Exp(
											self.log_transition_weights,
											self.emission_distributions.get_log_prob_for_strings,
											self.log_init_weights,
											length_weight_func=self.length_distributions.weight_log_emission
										)

	def get_log_prob(self, string):
		return self.forward_backward_algorithm.get_expected_visit_counts(string)[2][0]

	def get_log_prob_per_time(self, string, locality=None):
		log_probs = []
		if locality is None:
			prev_log_joint_prob = 0.0
			state_log_probs_for_target = self.forward_backward_algorithm.log_init_weights
			for target_time_ix in range(string.shape[1]):
				target_parser = fba.Forward_Backward_Algorithm_Multivariate_Emission_Sort_By_Length_Log_Sum_Exp(
					self.forward_backward_algorithm.log_transition_weights,
					self.forward_backward_algorithm.log_emission_pdf,
					state_log_probs_for_target,
					length_weight_func=self.forward_backward_algorithm.length_weight_func
				)
				log_joint_prob = target_parser.get_expected_visit_counts(string[:,target_time_ix,np.newaxis])[2][0]
				log_probs.append((target_time_ix,log_joint_prob-prev_log_joint_prob))
				prev_log_joint_prob = log_joint_prob
				state_log_probs_for_target = sps.logsumexp(
										target_parser.current_log_forward_probs[0,:,np.newaxis]
										+
										target_parser.log_transition_weights
										,
										axis=-2
										)
		else:
			assert locality>0, 'locality must be at least 1.'
			log_init_weights = self.forward_backward_algorithm.log_init_weights
			for target_time_ix in range(locality,string.shape[1]):
				context_onset = target_time_ix - locality
				context = string[:,context_onset:target_time_ix]
				target = string[:,target_time_ix,np.newaxis]
				context_parser = fba.Forward_Backward_Algorithm_Multivariate_Emission_Sort_By_Length_Log_Sum_Exp(
					self.forward_backward_algorithm.log_transition_weights,
					self.forward_backward_algorithm.log_emission_pdf,
					log_init_weights,
					length_weight_func=self.forward_backward_algorithm.length_weight_func
				)
				log_context_prob = context_parser.get_expected_visit_counts(context)[2][0]
				state_log_probs_for_target = sps.logsumexp(
										context_parser.current_log_forward_probs[0,:,np.newaxis]
										+
										context_parser.log_transition_weights
										,
										axis=-2
										)
				target_parser = fba.Forward_Backward_Algorithm_Multivariate_Emission_Sort_By_Length_Log_Sum_Exp(
					self.forward_backward_algorithm.log_transition_weights,
					self.forward_backward_algorithm.log_emission_pdf,
					state_log_probs_for_target,
					length_weight_func=self.forward_backward_algorithm.length_weight_func
				)
				log_joint_prob = target_parser.get_expected_visit_counts(target)[2][0]
				log_probs.append((target_time_ix,log_joint_prob-log_context_prob))
				log_init_weights = sps.logsumexp(
										log_init_weights
										+
										self.forward_backward_algorithm.log_transition_weights
										,
										axis=0
									)
		return log_probs

	def get_expected_counts(self, data, max_batch_array_size = 900):
		self.data = data
		self.data.group_by_length(max_batch_array_size=max_batch_array_size)
		self._update_expected_counts()
		return self.expected_trans_counts, self.expected_init_counts, self.emission_distributions.expected_emission_counts

	def sample(self, length=None, only_visited=False, threshold=1.0):
		init_probs = np.exp(self.forward_backward_algorithm.log_init_weights)
		trans_probs = np.exp(self.forward_backward_algorithm.log_transition_weights)
		if hasattr(self.emission_distributions, 'varpar_mean_mean'):
			state_visit_counts = self.emission_distributions.varpar_covmat_df-self.emission_distributions.covmat_prior_df
			sampler = self._get_t_sampler()
		else:
			sampler = self._get_dirichlet_categorical_sampler()
			state_visit_counts = np.sum(self.emission_distributions.varpar_rule_weight - self.emission_distributions.base_counts[np.newaxis,:], axis=-1)
		if only_visited:
			visited_states = state_visit_counts>=threshold
			init_probs *= visited_states
			init_probs /= np.sum(init_probs)
			trans_probs *= visited_states[np.newaxis,:]
			trans_probs /= np.sum(trans_probs, axis=-1)[:,np.newaxis]
		state = np.random.choice(
					np.arange(self.num_states),
					p=init_probs
					)
		seq = []
		states = []
		state_visits_seq = []
		if length is None:
			stop_probs = self.length_distributions.log_posterior_expectation[0,:].exp()
		while True:
			sample = sampler(state)
			seq.append(sample)
			states.append(state)
			state_visits_seq.append(state_visit_counts[state])
			if length is None:
				if np.random.rand() < stop_probs[state]:
					break
			elif length == 1:
				break
			else:
				length -= 1
			prev_state = state
			state = np.random.choice(
						np.arange(self.num_states),
						p=trans_probs[state]
						)
		return np.array(seq), np.array(states), np.array(state_visits_seq)

	def _get_t_sampler(self):
		mean = self.emission_distributions.varpar_mean_mean
		df = self.emission_distributions.df
		cov_mat = np.linalg.inv(self.emission_distributions.posterior_precision)
		def sampler(state):
			return multivariate_t_rvs(mean[state], cov_mat[state], df=df[state]).reshape(-1)
		return sampler

	def _get_dirichlet_categorical_sampler(self):
		p = np.exp(self.emission_distributions.log_posterior_expectation)
		label_ixs = np.arange(p.shape[-1])
		def sampler(state):
			sample = np.random.choice(label_ixs, p=p[state])
			return self.ix2label[sample]
		return sampler


def get_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('hdf_path', type=str, help = 'path to the result HDF5 file')
	parser.add_argument('data_path', type = str, help = 'path to data to predict.')
	parser.add_argument('-m', '--model', type = str, default = 'PCFG', help = 'Model type ("PCFG" or "HMM"). Default is PCFG (with Gaussian emission).')
	parser.add_argument('-p', '--prob_type', type = str, default = 'E_p', help = 'Type of probability used. E[p] ("E_p") or E[log p] ("E_log"). Default is "E_p"')
	parser.add_argument('-P', '--parse', type = str, default = 'all', help = 'Parse types. All the parses are taken into account by default ("all"). Only left/right-parses are used if "left"/"right".')
	parser.add_argument(
				"--seq_col",
				type=str,
				default='sequence_ix',
				help="Name of the csv column containing the index of the sequences."
	)
	parser.add_argument(
				"--time_col",
				type=str,
				default='time_ix',
				help="Name of the csv column containing the discrete time indexes of the sequences."
	)
	parser.add_argument(
				"--dim_col",
				type=str,
				default='dim',
				help="Name of the csv column containing the index of the dimensions of the data points."
	)
	parser.add_argument(
				"--val_col",
				type=str,
				default='value',
				help="Name of the csv column containing the data values."
	)
	parser.add_argument(
		'-O',
		'--old_data_format',
		action='store_true',
		help='If selected, each row of the data file is assumed to represent a single sequence, each discrete time is tab-stop-separated, and each data dimension is comma-separated.'
	)
	return parser.parse_args()


if __name__ == '__main__':
	options = get_arguments()

	result_dir = os.path.dirname(options.hdf_path)
	result_filename = 'POST-PRED_' + os.path.splitext(os.path.basename(options.data_path))[0] + '.csv'
	save_path = os.path.join(result_dir, result_filename)


	data = dtp.Dataset_of_Sequences(options.data_path, seq_col=options.seq_col, time_col=options.time_col, dim_col=options.dim_col, val_col=options.val_col, old_format=options.old_data_format, singleton_batch=True)

	if options.model == 'PCFG':
		predictor = HDP_PCFG_Gaussian_Predictor(options.hdf_path)
	elif options.model == 'HMM':
		predictor = HDP_HMM_Gaussian_Predictor(options.hdf_path)
	else:
		raise ValueError('Invalid model name: {model}'.format(model=options.model))

	if options.prob_type == 'E_p':
		predictor.prepare_posterior_prediction()
	elif options.prob_type == 'E_log':
		predictor.set_E_log_probs()
	else:
		raise ValueError('Invalid probability type: {p_type}'.format(p_type=options.prob_type))

	if options.parse == 'all':
		log_pred_probs = [predictor.get_log_prob(string) for string in data.iter_data()]
	elif options.parse == 'left':
		log_pred_probs = [predictor.get_log_prob_left_branching(string) for string in data.iter_data()]
	elif options.parse == 'right':
		log_pred_probs = [predictor.get_log_prob_right_branching(string) for string in data.iter_data()]
	else:
		raise ValueError('Invalid parse type: {parse}'.format(parse=options.parse))

	if os.path.isfile(save_path):
		df_result = pd.read_csv(save_path)
	else:
		df_result = pd.DataFrame()
		df_result['length'] = [string.shape[1] for string in data.iter_data()]
		df_result['string_id'] = df_result.index
	
	df_result[
		'total_{p_type}_{parse}'.format(p_type = options.prob_type, parse = options.parse)
		] = log_pred_probs
	df_result[
		'mean_per_point_{p_type}_{parse}'.format(p_type = options.prob_type, parse = options.parse)
		] = df_result[
				'total_{p_type}_{parse}'.format(p_type = options.prob_type, parse = options.parse)
				] / df_result.length
	df_result.to_csv(save_path, index = False)




