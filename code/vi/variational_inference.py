# coding: utf-8

import numpy as np
import pandas as pd
from logging import getLogger,FileHandler,DEBUG,Formatter
import os, datetime, sys
import hdp_pcfg, hdp_hmm, dp_mixture

np.seterr(invalid='raise')
logger = getLogger(__name__)

def setup_log_handler(log_path, model_name):
	current_handlers=logger.handlers[:]
	for h in current_handlers:
		logger.removeHandler(h)
	handler = FileHandler(filename=os.path.join(log_path,'VI_DP_{model_name}.log'.format(model_name=model_name)))	#Define the handler.
	handler.setLevel(DEBUG)
	formatter = Formatter('%(asctime)s - %(levelname)s - %(message)s')	#Define the log format.
	handler.setFormatter(formatter)
	logger.setLevel(DEBUG)
	logger.addHandler(handler)	#Register the handler for the logger.
	logger.info("Logger (re)set up.")


def code_data_cfg(training_data,test_data=None):
	if test_data is None:
		str_data=training_data
	else:
		str_data=training_data+test_data
	inventory = sorted(set(reduce(lambda x,y: x+y, str_data)))
	num_symbols = len(inventory)
	encoder = {symbol:code for code,symbol in enumerate(inventory, start=0)}
	decoder = {code:symbol for code,symbol in enumerate(inventory, start=0)}
	if test_data is None:
		coded_data = [map(lambda s: encoder[s],phrase) for phrase in str_data]
		return (coded_data,encoder,decoder)
	else:
		coded_training_data=[map(lambda s: encoder[s],phrase) for phrase in training_data]
		coded_test_data=[map(lambda s: encoder[s],phrase) for phrase in test_data]
		return (coded_training_data,coded_test_data,encoder,decoder)


class Variational_Inference_Template(object):
	def __init__(self, result_path, model_name, random_seed=111):
		setup_log_handler(result_path, model_name)
		logger.info('Random seed: {seed}'.format(seed=random_seed))
		np.random.seed(random_seed)


	def get_result_path(self):
		return self.result_path

	def get_log_posterior_pred(self, test_data):
		return self.model.get_log_posterior_pred(test_data)

		
	
	def train(self, max_iters, tolerance, decoder=None):
		logger.info("Main loop started.")
		logger.info("Max iteration is %i." % max_iters)
		logger.info("Will be terminated if variational bound is only improved by <=%f." % tolerance)
		converged=False
		iter_id=0
		self.current_var_bound = self.get_var_bound()
		logger.info("Initial var_bound is %0.12f." % self.current_var_bound)
		while iter_id<max_iters:
			iter_id+=1
			self.update_varpars()
			logger.info("Variational parameters updated (Iteration ID: %i)." % iter_id)
			new_var_bound = self.get_var_bound()
			improvement = new_var_bound-self.current_var_bound
			self.current_var_bound = new_var_bound
			logger.info("Current var_bound is %0.12f (%+0.12f)." % (self.current_var_bound,improvement))
			if np.isnan(self.current_var_bound):
				raise Exception("nan detected.")
			if improvement<0:
				logger.error("variational bound decreased. Something wrong.")
				if improvement < - 0.000001: # [2017/10/11] We ignore small decrease.
					if not decoder is None:
						self.save_results(decoder)
					raise Exception("variational bound decreased. Something wrong.")
			if improvement<=tolerance:
				converged = True
				break
		if converged:
			logger.info('Converged after %i iterations.' % iter_id)
		else:
			logger.info('Failed to converge after max iterations.')
		logger.info('Final variational bound is %0.12f.' % self.current_var_bound)



	def update_varpars(self):
		self.model.update_varpars()
		
		
	def get_var_bound(self):
		"""
		Calculate the KL divergence bound based on the current variational parameters.
		We ignore the constant terms.
		"""
		return self.model.get_var_bound()





class Variational_Inference_HDP_PCFG(Variational_Inference_Template):
	def __init__(
			self,
			data,
			num_non_terminals,
			result_path,
			emission_distribution='Gaussian',
			concentration_top_non_terminal=1.0,
			concentration_bottom_non_terminal=1.0,
			concentration_init=1.0,
			switch_base_counts_branch=1.0,
			switch_base_counts_emit=1.0,
			num_terminals=None,
			concentration_top_terminal=1.0,
			concentration_bottom_terminal=1.0,
			gradient_projection_tolerance=0.01,
			max_batch_array_size=650,
			random_seed=111,
			):
		super(Variational_Inference_HDP_PCFG, self).__init__(result_path, 'HDP-PCFG', random_seed=random_seed)
		
		logger.info('HDP-CFG with {emission_distribution} emission. Script last updated at {dt}'.format(
							emission_distribution = emission_distribution,
							dt = datetime.datetime.fromtimestamp(
									os.stat(sys.argv[0]).st_mtime
									).strftime('%Y-%m-%d-%H:%M:%S')
							))

		branch_params = {}
		branch_params['concentration_top'] = concentration_top_non_terminal
		logger.info('Concentration of top stick breaking for non-terminals: %s' % str(concentration_top_non_terminal))
		branch_params['concentration_bottom'] = concentration_bottom_non_terminal
		logger.info('Concentration of branching rules: %s' % str(concentration_bottom_non_terminal))
		branch_params['concentration_init'] = concentration_init
		logger.info('Concentration of root labeling: %s' % str(concentration_init))
		branch_params['num_non_terminals'] = num_non_terminals
		logger.info('(Max) # of non-terminals: %i' % num_non_terminals)
		branch_params['gradient_projection_tolerance'] = gradient_projection_tolerance
		logger.info('Tolerance of gradient projection: %s' % str(gradient_projection_tolerance))
		

		switch_params = {}
		switch_base_counts = (switch_base_counts_branch,switch_base_counts_emit)
		switch_params['base_counts'] = switch_base_counts
		logger.info('Base count of switch rules: (branch,emit) = %s' % str(switch_base_counts))
		switch_params['num_conditions'] = num_non_terminals
		switch_params['num_categories'] = 2


		# Priors on the states' emission.
		emission_params = {}
		emission_params['distribution'] = emission_distribution
		if emission_distribution == 'Gaussian':
			# Following Feldman et al. (2013).
			dimension = data.get_dimension()
			emission_params['dimension'] = dimension
			emission_params['mean_prior_mean'] = data.mean()
			emission_params['covmat_prior_scale_mat'] = np.identity(dimension)
			covmat_prior_df = dimension - 1 + 0.001
			emission_params['covmat_prior_df'] = covmat_prior_df
			logger.info('Prior on covariance matrix for each non-terminal: IW(I, %s)' % str(covmat_prior_df))
			mean_prior_denominator = covmat_prior_df
			emission_params['mean_prior_denominator'] = mean_prior_denominator
			logger.info('Prior on mean for each non-terminal: N(sample_mean, state_cov_mat/%s)' % mean_prior_denominator)
			logger.info('sample_mean is the sample mean of all the data points.')
			num_sequences = data.get_num_sequences()
		elif emission_distribution=='HMM':
			# UNDER CONSTRUCTION!!!
			# data is assumed to be a pd.DataFrame instance that contains paths to wav files.
			num_sequences = data.string_ix.unique.size
			dimension = None
		else:
			raise ValueError('Unsupported emission distribution: {emission_distribution}'.format(emission_distribution=emission_distribution))
		
		
		
		
		
		if num_terminals is None:
			emission_params['num_components'] = num_non_terminals
			logger.info('Data emitted from non-terminals.')
			nt2t_params = None
		else:
			emission_params['num_components'] = num_terminals
			logger.info('Data emitted from terminals that are distinct from non-terminals.')

			nt2t_params = {}
			nt2t_params['concentration_top'] = concentration_top_terminal
			logger.info('Concentration of top stick breaking for terminal: %s' % str(concentration_top_terminal))
			nt2t_params['concentration_bottom'] = concentration_bottom_terminal
			logger.info('Concentration of non-terminal-to-terminal rules: %s' % str(concentration_bottom_terminal))
			nt2t_params['start_size'] = num_non_terminals
			nt2t_params['goal_size'] = num_terminals
			logger.info('(Max) # of terminals: %i' % num_terminals)
			nt2t_params['gradient_projection_tolerance'] = gradient_projection_tolerance
		
		

		logger.info('# of strings: %i' % num_sequences)
		logger.info('Max batch size of data array to be processed in parallel in inner-outer algorithm: %i' % max_batch_array_size)
		self.model = hdp_pcfg.HDP_PCFG(
							data,
							branch_params,
							emission_params,
							switch_params,
							nt2t_params=nt2t_params,
							max_batch_array_size=max_batch_array_size
							)
		
		self.result_path = result_path 
		logger.info('Initialization complete.')


	def save_results(self):
		num_non_terminals = self.model.branch_hdp.num_non_terminals
		with pd.HDFStore(
				os.path.join(
					self.result_path,
					'variational_parameters.h5'
					)
				) as hdf5_store:
			# branching
			df_rule = pd.DataFrame(
								self.model.branch_hdp.varpar_rule_weight.flatten()[:,np.newaxis]
								,
								columns=['dirichlet_par']
							)
			lhss, rhs_lefts, rhs_rights = np.indices(self.model.branch_hdp.varpar_rule_weight.shape)
			df_rule['LHS'] = lhss.flatten()
			df_rule['RHS_left'] = rhs_lefts.flatten()
			df_rule['RHS_right'] = rhs_rights.flatten()
			hdf5_store['branching'] = df_rule

			# Top-level weights of non-terminals.
			df_top = pd.DataFrame(
							self.model.branch_hdp.non_terminal_weight_estimate[:,np.newaxis]
							,
							columns=['p']
							)
			df_top['non_terminal'] = df_top.index
			hdf5_store['non_terminal'] = df_top

			# Root labeling
			df_init = pd.DataFrame(
							self.model.branch_hdp.varpar_init_weight[:,np.newaxis]
							,
							columns=['dirichlet_par']
							)
			df_init['non_terminal'] = df_init.index
			hdf5_store['root'] = df_init

			# emission
			num_clusters = self.model.emission_distributions.varpar_mean_denominator.size
			df_mean=pd.DataFrame(self.model.emission_distributions.varpar_mean_mean
										,
										columns=[('dim_%i' % dim)
													for dim in range(self.model.emission_distributions.dimension)
													]
										)
			
			df_mean['cluster'] = df_mean.index
			hdf5_store.put(
						'emission/mean/mean'
						,
						df_mean
						)

			df_denom = pd.DataFrame(
							self.model.emission_distributions.varpar_mean_denominator[:,np.newaxis]
							,
							columns=['value']
							)
			df_denom['cluster'] = df_denom.index
			hdf5_store.put(
						'emission/mean/denominator'
						,
						df_denom
						)

			upper_triangle_ids1,upper_triangle_ids2 = np.triu_indices(self.model.emission_distributions.dimension)
			df_scale_mat=pd.DataFrame(
							np.linalg.inv(self.model.emission_distributions.inv_varpar_covmat_scale_mat)[
								:,
								upper_triangle_ids1,
								upper_triangle_ids2
								].flatten()[:,np.newaxis]
							,
							columns=['matrix_entry']
							)
			df_scale_mat['cluster'] = np.repeat(
											np.arange(num_clusters)
											,
											upper_triangle_ids1.size
										)
			df_scale_mat['row_id'] = np.tile(upper_triangle_ids1, num_clusters)
			df_scale_mat['col_id'] = np.tile(upper_triangle_ids2, num_clusters)
			hdf5_store.put(
						'emission/covmat/scale_mat'
						,
						df_scale_mat
						)

			df_df = pd.DataFrame(
						self.model.emission_distributions.varpar_covmat_df[:,np.newaxis],
						columns=['df']
						)
			df_df['cluster'] = df_df.index
			hdf5_store.put(
						'emission/covmat/df'
						,
						df_df
						)

			# switch
			df_rule = pd.DataFrame(
							self.model.switch_distributions.varpar_rule_weight
							,
							columns=['branch','emit']
							)
			df_rule['LHS'] = df_rule.index
			hdf5_store['switch'] = df_rule
			
			
			if not self.model.nt2t_rule_hdp is None:
				# non-terminals-to-terminal rules
				num_terminals = self.model.nt2t_rule_hdp.varpar_transition.shape[-1]
				df_rule = pd.DataFrame(
									self.model.nt2t_rule_hdp.varpar_transition.flatten()[:,np.newaxis]
									,
									columns=['dirichlet_par']
								)
				non_terminals, terminals = np.indices(self.model.nt2t_rule_hdp.varpar_transition.shape)
				df_rule['non_terminal'] = non_terminals.flatten()
				df_rule['terminal'] = terminals.flatten()
				hdf5_store.put(
					'nt2t',
					df_rule
				)

				# Top-level weights of states.
				df_top = pd.DataFrame(
								self.model.nt2t_rule_hdp.goal_weight_estimate[:,np.newaxis]
								,
								columns=['p']
								)
				df_top['terminal'] = df_top.index
				hdf5_store.put(
					'terminal',
					df_top
				)




class Variational_Inference_HDP_HMM(Variational_Inference_Template):
	def __init__(
			self,
			data,
			num_states,
			result_path,
			emission_distribution='Gaussian',
			concentration_top=1.0,
			concentration_bottom=1.0,
			concentration_categorical_emission=1.0,
			gradient_projection_tolerance=0.01,
			stop_base_counts=1.0,
			cont_base_counts=1.0,
			max_batch_array_size = 650,
			random_seed=111,
			no_length_prediction=False
			):
		super(Variational_Inference_HDP_HMM, self).__init__(result_path, 'HDP-HMM', random_seed=random_seed)
		
		logger.info('HDP-HMM with {emission_distribution} emission. Script last updated at {dt}'.format(
							emission_distribution = emission_distribution,
							dt=datetime.datetime.fromtimestamp(
									os.stat(sys.argv[0]).st_mtime
									).strftime('%Y-%m-%d-%H:%M:%S')
							))

		trans_params = {}
		trans_params['concentration_top'] = concentration_top
		logger.info('Concentration of top stick breaking for states: %s' % str(concentration_top))
		trans_params['concentration_bottom'] = concentration_bottom
		logger.info('Concentration of transition: %s' % str(concentration_bottom))
		trans_params['start_size'] = num_states + 1
		trans_params['goal_size'] = num_states
		logger.info('(Max) # of states: %i.' % num_states)
		trans_params['gradient_projection_tolerance'] = gradient_projection_tolerance
		logger.info('Tolerance for gradient projection: %s' % str(gradient_projection_tolerance))


		# Priors on the states' emission.
		emission_params = {}
		emission_params['distribution'] = emission_distribution
		if emission_distribution == 'Gaussian':
			# Following Feldman et al. (2013).
			dimension = data.get_dimension()
			emission_params['dimension'] = dimension
			emission_params['mean_prior_mean'] = data.mean()
			emission_params['covmat_prior_scale_mat'] = np.identity(dimension)
			covmat_prior_df = dimension - 1 + 0.001
			emission_params['covmat_prior_df'] = covmat_prior_df
			logger.info('Prior on covariance matrix for each state: IW(I, %s)' % str(covmat_prior_df))
			mean_prior_denominator = covmat_prior_df
			emission_params['mean_prior_denominator'] = mean_prior_denominator
			logger.info('Prior on mean for each state: N(sample_mean, state_cov_mat/%s)' % mean_prior_denominator)
			logger.info('sample_mean is the sample mean of all the data points.')
			emission_params['num_components'] = num_states
		elif emission_distribution == 'Categorical':
			emission_params['distribution'] = 'Dirichlet'
			emission_params['num_conditions'] = num_states
			emission_params['num_categories'] = data.get_num_categories()
			logger.info('# of discrete labels in the data: {}'.format(emission_params['num_categories']))
			emission_params['base_counts'] = np.ones(emission_params['num_categories']) * concentration_categorical_emission
			logger.info('Prior on the emission probabilities is Dirichlet({}).'.format(concentration_categorical_emission))
		else:
			raise ValueError('Unsupported emission distribution: {emission_distribution}'.format(emission_distribution=emission_distribution))

		length_params = {}
		length_params['base_counts'] = (stop_base_counts,cont_base_counts)
		length_params['num_states'] = num_states
		length_params['no_length'] = no_length_prediction
		if no_length_prediction:
			logger.info('No prediction of the string length.')
		else:
			logger.info('Length predictions by Bernouli at each time step given current state.')
			logger.info('Base count of stop vs. continue: (stop,continue) = %s' % str(length_params['base_counts']))
		
		num_sequences = data.get_num_sequences()
		logger.info('# of strings: %i' % num_sequences)
		logger.info('Max batch size of data array to be processed in parallel in forward-backward algorithm: %i' % max_batch_array_size)
		
		
		
		
		

		
		self.model = hdp_hmm.HDP_HMM(
										data,
										trans_params,
										emission_params,
										length_params,
										max_batch_array_size = max_batch_array_size
										)
		
		self.result_path = result_path 
		logger.info('Initialization complete.')


	def save_results(self):
		num_states = self.model.trans_hdp.varpar_transition.shape[-1]
		with pd.HDFStore(
				os.path.join(
					self.result_path,
					'variational_parameters.h5'
					)
				) as hdf5_store:


			# transition
			df_rule = pd.DataFrame(
								self.model.trans_hdp.varpar_transition[:-1,:].flatten()[:,np.newaxis]
								,
								columns=['dirichlet_par']
							)
			from_states,to_states = np.indices(self.model.trans_hdp.varpar_transition[:-1,:].shape)
			df_rule['from_state'] = from_states.flatten()
			df_rule['to_state'] = to_states.flatten()
			hdf5_store['transition'] = df_rule

			# Top-level weights of states.
			df_top = pd.DataFrame(
							self.model.trans_hdp.goal_weight_estimate[:,np.newaxis]
							,
							columns=['p']
							)
			df_top['state'] = df_top.index
			hdf5_store['state'] = df_top

			# initial transition
			df_init = pd.DataFrame(
							self.model.trans_hdp.varpar_transition[-1,:,np.newaxis]
							,
							columns=['dirichlet_par']
							)
			df_init['state'] = df_init.index
			hdf5_store['init'] = df_init

			# emission
			if hasattr(self.model.emission_distributions, 'varpar_mean_mean'):
				df_mean=pd.DataFrame(self.model.emission_distributions.varpar_mean_mean
											,
											columns=[('dim_%i' % dim)
														for dim in range(self.model.emission_distributions.dimension)
														]
											)
				
				df_mean['state'] = df_mean.index
				hdf5_store.put(
							'emission/mean/mean'
							,
							df_mean
							)

				df_denom = pd.DataFrame(
								self.model.emission_distributions.varpar_mean_denominator[:,np.newaxis]
								,
								columns=['value']
								)
				df_denom['state'] = df_denom.index
				hdf5_store.put(
							'emission/mean/denominator'
							,
							df_denom
							)

				upper_triangle_ids1,upper_triangle_ids2 = np.triu_indices(self.model.emission_distributions.dimension)
				df_scale_mat=pd.DataFrame(
								np.linalg.inv(self.model.emission_distributions.inv_varpar_covmat_scale_mat)[
									:,
									upper_triangle_ids1,
									upper_triangle_ids2
									].flatten()[:,np.newaxis]
								,
								columns=['matrix_entry']
								)
				df_scale_mat['state'] = np.repeat(
												np.arange(num_states)
												,
												upper_triangle_ids1.size
											)
				df_scale_mat['row_id'] = np.tile(upper_triangle_ids1, num_states)
				df_scale_mat['col_id'] = np.tile(upper_triangle_ids2, num_states)
				hdf5_store.put(
							'emission/covmat/scale_mat'
							,
							df_scale_mat
							)

				df_df = pd.DataFrame(
							self.model.emission_distributions.varpar_covmat_df[:,np.newaxis],
							columns=['df']
							)
				df_df['state'] = df_df.index
				hdf5_store.put(
							'emission/covmat/df'
							,
							df_df
							)
			elif hasattr(self.model.emission_distributions, 'varpar_rule_weight'):
				df_emit = pd.DataFrame(self.model.emission_distributions.varpar_rule_weight)
				df_emit['state'] = df_emit.index
				df_emit = df_emit.melt(id_vars=['state'], var_name='label_ix', value_name='dirichlet_par')
				df_emit.loc[:,'label_ix'] = df_emit.label_ix.astype(int)
				df_emit.loc[:,'label'] = df_emit.label_ix.map(self.model.data.ix2label)
				hdf5_store.put(
							'emission/dirichlet'
							,
							df_emit
							)

			# length
			if isinstance(self.model.length_distributions, hdp_hmm.Bernoulli_length):
				df_length = pd.DataFrame(
								self.model.length_distributions.varpar_stop,
								columns=['stop','through']
								)
				df_length['state'] = df_length.index
				hdf5_store['length'] = df_length


class Variational_Inference_DP_Mix(Variational_Inference_Template):
	def __init__(
			self,
			data,
			num_clusters,
			result_path,
			emission_distribution='Gaussian',
			concentration=1.0,
			concentration_prior_shape=None,
			concentration_prior_rate=None,
			max_batch_array_size=650,
			random_seed=111,
			):
		super(Variational_Inference_DP_Mix, self).__init__(result_path, 'DP-Mix', random_seed=random_seed)
		
		logger.info('DP mixture of {emission_distribution} distribution. Script last updated at {dt}'.format(
							emission_distribution = emission_distribution,
							dt=datetime.datetime.fromtimestamp(
									os.stat(sys.argv[0]).st_mtime
									).strftime('%Y-%m-%d-%H:%M:%S')
							))

		clustering_params = {}
		clustering_params['num_clusters'] = num_clusters
		logger.info('(Max) # of clusters: %i.' % num_clusters)
		if not (concentration_prior_shape is None or concentration_prior_rate is None):
			logger.info('Gamma({shape},{rate}^-1) prior on the DP concentration is assumed.'.format(shape=concentration_prior_shape, rate=concentration_prior_rate))
			clustering_params['concentration_prior_shape'] = concentration_prior_shape
			clustering_params['concentration_prior_rate'] = concentration_prior_rate
		else:
			clustering_params['concentration'] = concentration
			logger.info('Fixed DP concentration: {concentration}'.format(concentration=concentration))

		# Priors on the mixture components.
		emission_params = {}
		emission_params['distribution'] = emission_distribution
		if emission_distribution == 'Gaussian':
			# Following Feldman et al. (2013).
			dimension = data.shape[1]
			emission_params['dimension'] = dimension
			emission_params['mean_prior_mean'] = np.mean(data, axis=0)
			emission_params['covmat_prior_scale_mat'] = np.identity(dimension)
			covmat_prior_df = dimension - 1 + 0.001
			emission_params['covmat_prior_df'] = covmat_prior_df
			logger.info('Prior on covariance matrix for each cluster: IW(I, %s)' % str(covmat_prior_df))
			mean_prior_denominator = covmat_prior_df
			emission_params['mean_prior_denominator'] = mean_prior_denominator
			logger.info('Prior on mean for each cluster: N(sample_mean, state_cov_mat/%s)' % mean_prior_denominator)
			logger.info('sample_mean is the sample mean of all the data points.')
			emission_params['num_components'] = num_clusters
			
		else:
			raise ValueError('Unsupported emission distribution: {emission_distribution}'.format(emission_distribution=emission_distribution))

		
		logger.info('Data size: %i' % data.shape[0])
		logger.info('Max batch size of data array to be processed in parallel when computing expected counts: %i' % max_batch_array_size)

		self.model = dp_mixture.DP_mix(
										data,
										clustering_params,
										emission_params,
										max_batch_array_size=max_batch_array_size
										)
		
		self.result_path = result_path 
		logger.info('Initialization complete.')



	def save_results(self):
		num_clusters = self.model.dp.varpar_stick.shape[0] + 1
		with pd.HDFStore(
				os.path.join(
					self.result_path,
					'variational_parameters.h5'
					)
				) as hdf5_store:

			# Classification
			classification_probs = self.model.get_classification_probs()
			df_classification = pd.DataFrame(classification_probs.reshape(-1), columns=['prob'])
			data_ixs, cluster_ixs = np.indices(classification_probs.shape)
			df_classification['data_ix'] = data_ixs.flatten()
			df_classification['cluster_ix'] = cluster_ixs.flatten()
			hdf5_store['classification'] = df_classification

			# DP parameters
			df_stick = pd.DataFrame(
								self.model.dp.varpar_stick
								,
								columns=['stop','cont']
							)
			hdf5_store['dp/stick'] = df_stick

			if hasattr(self.model.dp, 'varpar_concent_rate'):
				df_concent = pd.DataFrame(
								[
									['shape',self.model.dp.varpar_concent_shape],
									['rate',self.model.dp.varpar_concent_rate]
								]
								,
								columns=['parameter_name','value']
								)
				hdf5_store['df/concentration'] = df_concent


			# emission
			df_mean=pd.DataFrame(self.model.emission_distributions.varpar_mean_mean
										,
										columns=[('dim_%i' % dim)
													for dim in range(self.model.emission_distributions.dimension)
													]
										)
			
			df_mean['mix_component_ix'] = df_mean.index
			hdf5_store.put(
						'emission/mean/mean'
						,
						df_mean
						)

			df_denom = pd.DataFrame(
							self.model.emission_distributions.varpar_mean_denominator[:,np.newaxis]
							,
							columns=['value']
							)
			df_denom['mix_component_ix'] = df_denom.index
			hdf5_store.put(
						'emission/mean/denominator'
						,
						df_denom
						)

			upper_triangle_ids1,upper_triangle_ids2 = np.triu_indices(self.model.emission_distributions.dimension)
			df_scale_mat=pd.DataFrame(
							np.linalg.inv(self.model.emission_distributions.inv_varpar_covmat_scale_mat)[
								:,
								upper_triangle_ids1,
								upper_triangle_ids2
								].flatten()[:,np.newaxis]
							,
							columns=['matrix_entry']
							)
			df_scale_mat['mix_component_ix'] = np.repeat(
											np.arange(num_clusters)
											,
											upper_triangle_ids1.size
										)
			df_scale_mat['row_id'] = np.tile(upper_triangle_ids1, num_clusters)
			df_scale_mat['col_id'] = np.tile(upper_triangle_ids2, num_clusters)
			hdf5_store.put(
						'emission/covmat/scale_mat'
						,
						df_scale_mat
						)

			df_df = pd.DataFrame(
						self.model.emission_distributions.varpar_covmat_df[:,np.newaxis],
						columns=['df']
						)
			df_df['mix_component_ix'] = df_df.index
			hdf5_store.put(
						'emission/covmat/df'
						,
						df_df
						)
