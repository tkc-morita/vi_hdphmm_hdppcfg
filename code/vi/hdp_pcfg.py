# coding: utf-8

"""
Variational inference for infinite probabilistic context-free grammar.

Reference:
Liang, Percy, Slav Petrov, Michael Jordan and Dan Klein. (2007)
	"The Infinite PCFG Using Hierarchical Dirichlet Processes."
Liang, Percy, Michael Jordan and Dan Klein. (2009)
	"Probabilistic grammars and hierarchical Dirichlet processes."
"""

import numpy as np
import algorithm.inner_outer_algorithm as ioa
import distributions



class HDP_PCFG(object):
	"""
	HDP-PCFG with point-estimate of top-level non-terminal weights
	and Gaussian emission.
	Terminals are associated with Gaussian and distinguished from non-terminals.
	"""
	def __init__(self,
				data,
				branch_params,
				emission_params,
				switch_params,
				nt2t_params = None,
				max_batch_array_size=20000
				):
		self.data = data
		if not self.data.grouped:
			self.data.group_by_length(max_batch_array_size=max_batch_array_size)
		
		self.branch_hdp = distributions.HDP_branch(branch_params)
		if nt2t_params is None:
			self.nt2t_rule_hdp = None
		else:
			self.nt2t_rule_hdp = distributions.HDP_trans(nt2t_params)# non-terminal to terminals
		self.emission_distributions = getattr(distributions, emission_params['distribution'])(emission_params)

		self.switch_distributions = distributions.Dirichlet(switch_params)
		
		self._update_arrays()
		self._update_expected_rule_counts()


	def _update_expected_rule_counts(self):
		"""
		Stolcke's (1995) algorithm
		"""
		self.expected_branch_rule_counts = np.zeros(self.log_branch_rule_weights.shape)
		self.expected_nt2t_rule_counts = np.zeros(self.log_nt2t_rule_weights.shape)
		self.emission_distributions.reset_expected_counts()
		self.expected_root_counts = np.zeros(self.log_init_weights.shape)
		self.inner_outer_algorithm = ioa.Inner_Outer_Algorithm(
											self.log_init_weights,
											self.log_branch_rule_weights,
											self.emission_distributions.get_log_prob_for_strings,
											log_nt2t_rule_weights = self.log_nt2t_rule_weights,
										)
		self.E_log_p_trees = 0.0
		[self._increment_expected_rule_counts(string_batch)
			for batch_list in self.data.iter_data()
			for string_batch in batch_list
			]
		self.branch_hdp.update_expected_counts(self.expected_branch_rule_counts, self.expected_root_counts)
		if not self.nt2t_rule_hdp is None:
			self.nt2t_rule_hdp.update_expected_counts(self.expected_nt2t_rule_counts)
		self.switch_distributions.update_expected_counts(
											np.array(
												(
												np.sum(self.expected_branch_rule_counts, axis=(1,2))
												,
												np.sum(self.expected_nt2t_rule_counts, axis=-1)
												)
											).T
										)
										

	def _increment_expected_rule_counts(
								self,
								string_batch
								):
		(
			expected_branch_rule_counts,
			expected_nt2t_rule_counts_per_loc,
			expected_root_counts,
			log_like
		) = self.inner_outer_algorithm.get_expected_rule_counts(string_batch)
		self.expected_branch_rule_counts += np.sum(expected_branch_rule_counts, axis=0)
		expected_emission_counts_per_loc = np.sum(expected_nt2t_rule_counts_per_loc, axis=-2)
		self.expected_nt2t_rule_counts += np.sum(expected_nt2t_rule_counts_per_loc, axis=(0,1))
		self.emission_distributions.increment_expected_counts(
														string_batch,
														expected_emission_counts_per_loc
														)
		self.expected_root_counts += np.sum(expected_root_counts, axis=0)
		self.E_log_p_trees += np.sum(log_like)
		

	def update_varpars(self):

		self.branch_hdp.update_varpars()
		if not self.nt2t_rule_hdp is None:
			self.nt2t_rule_hdp.update_varpars()
		self.emission_distributions.update_varpars()
		self.switch_distributions.update_varpars()
		
		self._update_arrays()
		self._update_expected_rule_counts()




	def get_var_bound(self):
		elbo = (
				self.E_log_p_trees
				+
				self.branch_hdp.get_var_bound()
				+
				self.emission_distributions.get_var_bound()
				+
				self.switch_distributions.get_var_bound()
				)
		if not self.nt2t_rule_hdp is None:
			elbo += self.nt2t_rule_hdp.get_var_bound()
		return elbo

		
	def _update_arrays(self):
		log_switch_weights = self.switch_distributions.get_log_weights()
		self.log_branch_rule_weights=(
									self.branch_hdp.get_log_weights()
									+
									log_switch_weights[:,0,np.newaxis,np.newaxis]
								)
		self.log_nt2t_rule_weights = (
									self.nt2t_rule_hdp.get_log_weights()
									+
									log_switch_weights[:,1,np.newaxis]
								)
		self.emission_distributions.set_prob_function()
		self.log_init_weights = self.branch_hdp.get_log_init_weight()


