# coding: utf-8

import numpy as np
import distributions
import scipy.misc as spm



class DP_mix(object):
	def __init__(
			self,
			training_data,
			clustering_params,
			emission_params,
			max_batch_array_size=2500,
			):
		self.data = self.get_batches(training_data, max_batch_array_size)
		self.emission_distributions = getattr(distributions, emission_params['distribution'])(emission_params)
		self.dp = distributions.DP(clustering_params)

		self._update_arrays()
		self._update_expected_counts()

	def get_batches(self, data, max_batch_array_size):
		batches = []
		last_batch = np.array([]).reshape(0,data.shape[1])
		for d in data:
			if last_batch.size<=max_batch_array_size:
				last_batch = np.append(last_batch,d[np.newaxis,:], axis=0)
			else:
				batches.append(last_batch)
				last_batch = d[np.newaxis,:]
		batches.append(last_batch)
		return batches

	def _update_arrays(self):
		self.emission_distributions.set_prob_function()

	def _update_expected_counts(self):
		self.emission_distributions.reset_expected_counts()
		self.expected_counts = np.zeros(self.dp.get_num_clusters())
		self.log_like = 0.0
		[
			self._increment_expected_counts(batch)
			for batch in self.data
		]
		self.dp.update_expected_counts(self.expected_counts)
		

	def _increment_expected_counts(self, batched_data):
		expected_counts, log_marg_probs = self._get_expected_counts(batched_data)
		self.emission_distributions.increment_expected_counts(batched_data[:,np.newaxis,:], expected_counts[:,np.newaxis,:])
		self.expected_counts += np.sum(expected_counts, axis=0)
		self.log_like += np.sum(log_marg_probs)


	def _get_expected_counts(self, batched_data):
		log_emission_probs = self.emission_distributions.get_log_prob_for_strings(
								batched_data[:,np.newaxis,:] # num_strings (= datasize) x string_len (=1) x ndim
								).squeeze(axis=1) # num_strings (= datasize) x num_clusters
		log_dp_weights = self.dp.get_log_weights() # num_clusters
		log_total_weights = log_emission_probs + log_dp_weights[np.newaxis,:]
		log_marg_probs = spm.logsumexp(log_total_weights, axis=-1)
		expected_counts = np.exp(log_total_weights - log_marg_probs[:,np.newaxis])
		return expected_counts, log_marg_probs


	def update_varpars(self):
		self.emission_distributions.update_varpars()
		self.dp.update_varpars()
		self._update_arrays()
		self._update_expected_counts()

	def get_var_bound(self):
		return (
			self.emission_distributions.get_var_bound()
			+
			self.dp.get_var_bound()
			+
			self.log_like
		)

	def get_classification_probs(self):
		self._update_arrays()
		expected_counts = np.array([]).reshape(0,self.dp.get_num_clusters())
		for batch in self.data:
			expected_counts_per_batch, _ = self._get_expected_counts(batch)
			expected_counts = np.append(expected_counts, expected_counts_per_batch, axis=0)
		return expected_counts