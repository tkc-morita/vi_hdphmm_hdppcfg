# coding: utf-8

import numpy as np
import scipy.special as sps
import scipy.misc as spm
import inner_outer_algorithm as ioa


class HDPCFG(object):
	"""
	HDP-CFG with noise learning.
	"""
	def __init__(
				self,
				num_clusters_scalar,
				num_terminals,
				num_non_terminals,
				gem_concent_priors,
				emission_base_counts,
				switch_base_counts,
				noise_concent_prior,
				strings
				):
		self.num_non_terminals = num_non_terminals
		
		self.strings=strings
		num_emit=np.sum([len(s) for s in strings])
		num_branch=np.sum([len(s)-1 for s in strings])
		data_size=len(strings)
		self.num_rules=num_emit+num_branch+data_size # # of rules to be used.
		

		self.vpc_branch_base=VarParConcent(gem_concent_priors)
		self.branch_base=DP_b_top(
							num_clusters_scalar*num_non_terminals,
							self.vpc_branch_base,
							noise_concent_prior
							self
							)
		
		self.vpc_branch_base_LR=VarParConcent(gem_concent_priors)
		self.branch_base_L=DP_b_base_LR(
							num_clusters_scalar*num_non_terminals,
							self.vpc_branch_base_LR,
							self.branch_base,
							self
							)
		
		self.branch_base_R=DP_b_base_LR(
							num_clusters_scalar*num_non_terminals,
							self.vpc_branch_base_LR,
							self.branch_base,
							self
							)
							
		
		self.emit_dirichlets = Dirichlets(
							num_terminals,
							num_non_terminals,
							emission_base_counts,
							self
							)

		self.switch_dirichlets = Dirichlets(
							2,
							num_non_terminals
							switch_base_counts,
							self
							)

		self.vpc_branch=VarParConcent(gem_concent_priors)
		self.branch_dps=[]
		self.vpc_branch_LR=VarParConcent(gem_concent_priors)
		self.branch_dps_L=[]
		self.branch_dps_R=[]

		for nt_id in xrange(num_non_terminals):
			b_dp_L=DP_b_LR(
							num_clusters_scalar*num_non_terminals,
							self.vpc_branch_LR,
							self.branch_base_L,
							self,
							'L'
							)
			self.branch_dps_L.append(b_dp_L)
			b_dp_R=DP_b_LR(
							num_clusters_scalar*num_non_terminals,
							self.vpc_branch_LR,
							self.branch_base_R,
							self,
							'R'
							)
			self.branch_dps_R.append(b_dp_R)
			self.branch_dps.append(
							DP_branch(
								num_clusters_scalar*num_non_terminals,
								self.vpc_branch,
								b_dp_L,
								b_dp_R,
								self
								)
							)
		
		
		self.vpc_init=VarParConcent(gem_concent_priors)
		self.init_dp=DP_init(
							num_clusters_scalar*num_non_terminals,
							self.vpc_init,
							self.branch_base,
							self
							)

	def set_log_posterior_expectation(self):
		self.emit_base.set_log_posterior_expectation()
		self.branch_base.set_log_posterior_expectation()
		self.branch_base_L.set_log_posterior_expectation()
		self.branch_base_R.set_log_posterior_expectation()
		self.switch_base.set_log_posterior_expectation()
		self.init_dp.set_log_posterior_expectation()
		[
			(
			branch_dp_L.set_log_posterior_expectation(),
			branch_dp_R.set_log_posterior_expectation(),
			branch_dp.set_log_posterior_expectation()
			)
				for branch_dp_L,
					branch_dp_R,
					branch_dp
				in zip(
					self.branch_dps_L,
					self.branch_dps_R,
					self.branch_dps
					)
			]
		self.emit_dirichlets.set_log_posterior_expectation()
		self.switch_dirichlets.set_log_posterior_expectation()


	def get_log_posterior_arrays(self):
		self.set_log_posterior_expectation()
		log_switch_weights = self.switch_dirichlets.log_posterior_expectation

		log_emit_rule_weights = (
							self.emit_dirichlets.log_posterior_expectation
							+
							log_switch_weights[:,1,np.newaxis]
							)
		
		log_init_weights=self.init_dp.log_posterior_expectation
		log_branch_rule_weights = np.array(
							[
								dp.log_posterior_expectation
								+
								log_b_weight
							for dp,log_b_weight in zip(self.branch_dps,log_switch_weights[:,0])
							]
							)
		return (log_branch_rule_weights,log_emit_rule_weights,log_init_weights)

	def get_log_posterior_pred(self, test_data):
		log_branch_rule_weights,log_emit_rule_weights,log_init_weights=self.get_log_posterior_arrays()
		log_cross_LHS_normalizer=self._get_log_cross_LHS_normalizer(log_branch_rule_weights,log_emit_rule_weights,log_init_weights)
		branch_rule_weights=np.exp(log_branch_rule_weights+log_cross_LHS_normalizer)
		left_corner_mat=np.linalg.inv(
								np.identity(self.num_non_terminals)
								-
								np.sum(branch_rule_weights, axis=2)
								)
		emit_rule_weights=np.exp(log_emit_rule_weights+log_cross_LHS_normalizer)
		init_weights=np.exp(log_init_weights+log_cross_LHS_normalizer)
		log_posterior_predictive_probs=[]
		for string in test_data:
			str_len=len(string)
			if str_len==1:
				log_posterior_predictive_probs.append(
					spm.logsumexp(np.log(init_weights)+np.log(emit_rule_weights[:,string[0]]))
					-
					log_cross_LHS_normalizer*2*str_len
					)
			else:
				(forward_branch,
						inner_branch,
						forward_emit,
						inner_emit,
						scaling_factors,
						completed_inner_branch)=ioa.get_forward_inner_probs(
												string,
												str_len,
												branch_rule_weights,
												emit_rule_weights,
												init_weights,
												left_corner_mat
												)
				log_posterior_predictive_probs.append(
							spm.logsumexp(
								np.log(init_weights)
								+
								np.log(np.sum(inner_branch[-1,0,:,:,:,2], axis=(1,2)))
								-
								np.sum(np.log(scaling_factors))
								)
							-
							log_cross_LHS_normalizer*2*str_len # The scalar is multiplied (2*str_len) times.
							)
		return log_posterior_predictive_probs





	def _update_expected_rule_counts(self):
		"""
		Stolcke's (1995) algorithm
		"""
		self.expected_branch_rule_counts = np.zeros(self.branch_rule_weights.shape)
		self.expected_emit_rule_counts = np.zeros(self.emit_rule_weights.shape)
		left_corner_mat=ioa.get_left_corner_probs(self.branch_rule_weights)
		self.expected_init_weights=np.zeros(self.init_weight_array.shape)
		self.E_log_p_trees=-self.log_cross_LHS_normalizer*self.num_rules#np.float64(0)
		[self._increment_expected_rule_counts(
										string,
										left_corner_mat
										)
			for string
			in self.strings
			]
		[
			branch_dp.update_expected_customer_counts(expected_branch_rule_counts_per_LHS)
				for branch_dp,
					expected_branch_rule_counts_per_LHS
				in 
					zip(
						self.branch_dps
						,
						self.expected_branch_rule_counts
					)
			]
		self.emit_dirichlets.update_expected_customer_counts(self.expected_emit_rule_counts)
		self.switch_dirichlets.update_expected_customer_counts(
											np.array(
												(
												np.sum(self.expected_branch_rule_counts, axis=(1,2))
												,
												np.sum(self.expected_emit_rule_counts, axis=1)
												)
											).T
										)
		self.init_dp.update_expected_customer_counts(self.expected_init_weights)

	def _increment_expected_rule_counts(
								self,
								string,
								left_corner_mat
								):
		str_len=len(string)
		if str_len==1:
			expected_init_weights=self.init_weight_array*self.emit_rule_weights[:,string[0]]
			likelihood=np.sum(expected_init_weights)
			expected_init_weights/=likelihood
			self.expected_emit_rule_counts[:,string[0]]+=expected_init_weights
			self.expected_init_weights+=expected_init_weights
			self.E_log_p_trees+=np.log(likelihood)
		else:
			self._increment_expected_rule_counts_non_singleton(
								string,
								str_len,
								left_corner_mat
								)
		
	def _increment_expected_rule_counts_non_singleton(
								self,
								string,
								str_len,
								left_corner_mat
								):
		(forward_branch,
				inner_branch,
				forward_emit,
				inner_emit,
				scaling_factors,
				completed_inner_branch)=ioa.get_forward_inner_probs(
													string,
													str_len,
													self.branch_rule_weights,
													self.emit_rule_weights,
													self.init_weight_array,
													left_corner_mat
													)
		(outer_branch,outer_emit)=ioa.get_outer_probs(
													string,
													str_len,
													self.init_weight_array,
													inner_branch,
													inner_emit,
													scaling_factors,
													completed_inner_branch
													)
		scaled_likelihoods_per_nt=(
									np.sum(inner_branch[-1,0,:,:,:,2], axis=(1,2))
									*
									self.init_weight_array
									)
		scaled_likelihood=np.sum(
								scaled_likelihoods_per_nt
							)
		self.expected_branch_rule_counts+=(
											np.sum(
												outer_branch[...,0].diagonal() # Scaled.
												*
												inner_branch[...,0].diagonal() # Not scaled.
												,
												axis=-1
												)
											/
											scaled_likelihood
											)
		
		self.expected_emit_rule_counts+=(
											np.sum(
												outer_emit[...,0] # Scaled.
												*
												inner_emit[...,0] # Not scaled.
												,
												axis=0
												)
											/
											scaled_likelihood
											)
		self.expected_init_weights+=(
											scaled_likelihoods_per_nt
											/
											scaled_likelihood
											)
		self.E_log_p_trees+=np.log(scaled_likelihood)-np.sum(np.log(scaling_factors))
		


		
	def _update_arrays(self):
		log_switch_weights = self.switch_dirichlets.get_log_rule_weights()
		log_branch_rule_weights=np.array(
							[
									branch_dp.get_log_rule_weights()
									+
									log_switch_to_branch
								for branch_dp
									,
									log_switch_to_branch
								in zip(
									self.branch_dps,
									log_switch_weights[:,0]
									)
								]
							)
		log_emit_rule_weights=(
								self.emit_dirichlets.get_log_rule_weights()
								+
								log_switch_weights[:,1,np.newaxis]
								) # num_nt x num_terminals

		log_init_weight_array=self.init_dp.get_log_rule_weights()
		self.log_cross_LHS_normalizer=self._get_log_cross_LHS_normalizer(log_branch_rule_weights,log_emit_rule_weights,log_init_weight_array)
		self.branch_rule_weights=np.exp(log_branch_rule_weights+self.log_cross_LHS_normalizer)
		self.emit_rule_weights=np.exp(log_emit_rule_weights+self.log_cross_LHS_normalizer)
		self.init_weight_array=np.exp(log_init_weight_array+self.log_cross_LHS_normalizer)
		

	def _get_log_cross_LHS_normalizer(self,log_branch_rule_weights,log_emit_rule_weights,log_init_weight_array):
		mean_weight=spm.logsumexp(
						(
							spm.logsumexp(log_branch_rule_weights)
							,
							spm.logsumexp(log_emit_rule_weights)
							,
							spm.logsumexp(log_init_weight_array)
						)
						)
		return -mean_weight


	def set_varpar_assignment(self):
		self.branch_base.set_varpar_assignment()
		self.branch_base_L.set_varpar_assignment()
		self.branch_base_R.set_varpar_assignment()

		self.init_dp.set_varpar_assignment()

		self.emit_dirichlets.set_varpar_assignment()
		self.switch_dirichlets.set_varpar_assignment()

		[
			(
			branch_dp_L.set_varpar_assignment(),
			branch_dp_R.set_varpar_assignment(),
			branch_dp.set_varpar_assignment()
			)
				for branch_dp_L,
					branch_dp_R,
					branch_dp
				in zip(
					self.branch_dps_L,
					self.branch_dps_R,
					self.branch_dps,
					)
			]
		self._update_arrays()
		self._update_expected_rule_counts()


	def update_varpars(self):

		self.vpc_branch_base.update()
		self.vpc_branch_base_LR.update()
		self.vpc_branch_LR.update()
		self.vpc_branch.update()
		self.vpc_init.update()


		self.branch_base.update_varpars()

		self.branch_base_L.update_varpars()
		self.branch_base_R.update_varpars()


		
		self.init_dp.update_varpars()

		self.emit_dirichlets.update_varpars()
		self.switch_dirichlets.update_varpars()

		[
			(
			branch_dp_L.update_varpars(),
			branch_dp_R.update_varpars(),
			branch_dp.update_varpars()
			)
				for 
					branch_dp_L,
					branch_dp_R,
					branch_dp
				in
					zip(
					self.branch_dps_L,
					self.branch_dps_R,
					self.branch_dps
					)
			]
		
		self._update_arrays()
		self._update_expected_rule_counts()




	def get_var_bound(self):
		return (
				self.E_log_p_trees
				+
				self.init_dp.get_var_bound()
				+
				self.emit_dirichlets.get_var_bound()
				+
				self.switch_dirichlets.get_var_bound()
				+
				np.sum([
					branch_dp.get_var_bound()
					+
					branch_dp_L.get_var_bound()
					+
					branch_dp_R.get_var_bound()
					for branch_dp_L,
						branch_dp_R,
						branch_dp
					in zip(
						self.branch_dps_L,
						self.branch_dps_R,
						self.branch_dps
						)
					]
					)
				+
				self.branch_base_L.get_var_bound()
				+
				self.branch_base_R.get_var_bound()
				+
				self.branch_base.get_var_bound()
				+
				self.vpc_branch_base.get_var_bound()
				+
				self.vpc_branch_base_LR.get_var_bound()
				+
				self.vpc_branch_LR.get_var_bound()
				+
				self.vpc_branch.get_var_bound()
				+
				self.vpc_init.get_var_bound()
				)






class VarParConcent(object):
	def __init__(self, priors):
		self.prior_rate = priors[1]
		self.dps = []
		self.shape = priors[0]
		self.rate = np.random.gamma(1,20)#(self.prior_rate**-1)


	def add_dp(self, dp):
		self.dps.append(dp)
		self.shape+=dp.num_clusters-1
		self.mean=self.shape/self.rate
	
	def update(self):
		self.rate = (self.prior_rate
							-
							np.sum(
								[
								dp.E_log_stick[:,1]
								for dp in self.dps
								]
								)
							)
		self.mean=self.shape/self.rate
						
	def get_var_bound(self):
		return -(
				self.shape*np.log(self.rate)
				+
				self.prior_rate*self.mean
				)
	

class DP_b_top(object): # Stick breaking process or label-less DP
	def __init__(self, num_clusters, varpar_concent, noise_concent_prior, hdp):
		self.hdp=hdp
		self.num_clusters = num_clusters
		self.varpar_concent = varpar_concent
		self.varpar_stick = np.random.gamma(1,#10,
												20,#0.1,
												size=(num_clusters-1,2)
												) # gamma in Blei and Jordan.
		self.sum_stick = np.sum(self.varpar_stick, axis=-1)
		self.E_log_stick = sps.digamma(self.varpar_stick)-sps.digamma(self.sum_stick)[:, np.newaxis]

		# Data production noise.
		self.noise_concent_prior = noise_concent_prior
		self.varpar_noise = np.random.gamma(1, 20, size=2)

		self.varpar_concent.add_dp(self)
		self.new_id=0
		self.children=[]
		
	def add_customer(self, child):
		self.children.append(child)
		issued_id = self.new_id
		self.new_id+=1
		return issued_id
		
	def set_varpar_assignment(self):
		num_children = len(self.children)
		num_tables_child = self.children[0].num_clusters # num_clusters for children.
		self.varpar_assignment = np.random.dirichlet(np.ones(self.num_clusters), size=(num_children,num_tables_child))
		self._update_log_like_top_down()
		

		
	def _update_varpar_stick(self):
		self.varpar_stick[...,0] = np.sum(self.varpar_assignment[...,:-1], axis=(0, 1))+1
		self.varpar_stick[...,1] = np.sum(
										np.cumsum(
											self.varpar_assignment[...,:0:-1],
											axis=-1
												)[...,::-1],
										axis=(0,1)
										)+self.varpar_concent.mean
		self.sum_stick = np.sum(self.varpar_stick, axis=-1)
		self.E_log_stick = sps.digamma(self.varpar_stick)-sps.digamma(self.sum_stick)[:, np.newaxis]

	def _update_varpar_assignment(self):
		log_varpar_assignment = (
									np.append(
										self.E_log_stick[:,0],
										0
										)[np.newaxis,np.newaxis,:]
									+np.append(
										0,
										np.cumsum(
											self.E_log_stick[:,1]
											)
										)[np.newaxis,np.newaxis,:]
									+np.sum(
										self.child_log_like_bottom_up[:,:,np.newaxis,:] # num_children x num_customers x num_terminals
										*
										self.E_log_atom[np.newaxis,np.newaxis,:,:] #  num_clusters x num_terminals
										,
										axis=-1
										)
								)
		self.varpar_assignment=np.exp(log_varpar_assignment-spm.logsumexp(log_varpar_assignment, axis=-1)[:,:,np.newaxis])


	def _update_varpar_noise(self):
		self.varpar_noise[0] = self.


	def update_varpars(self):
		self._update_varpar_stick()
		self._update_varpar_noise()
		self._update_varpar_assignment()
		self._update_log_like_top_down()
	
	def get_var_bound(self):
		self._update_log_like_bottom_up()
		return self.get_E_log_p()-self.get_E_log_q()

	def _update_log_like_top_down(self):
		self.log_like_top_down = np.sum(
										self.varpar_assignment[:,:,:,np.newaxis]
										*
										self.E_log_atom[np.newaxis,np.newaxis,:,:]
										,
										axis=-2
										)
	
	def _update_log_like_bottom_up(self):
		self._set_child_log_like_bottom_up()
		self.log_like_bottom_up=np.sum(
										self.child_log_like_bottom_up[:,:,np.newaxis,:] # num_children x num_customers x num_terminals
										*
										self.varpar_assignment[:,:,:,np.newaxis]
										,
										axis=(0,1)
										) #  num_clusters x num_terminals


	def _set_child_log_like_bottom_up(self):
		self.child_log_like_bottom_up = np.array(
												[child.log_like_bottom_up # num_customers x num_terminals
													for child
													in self.children
													]
											) # num_children x num_customers x num_terminals
		

	def get_E_log_p(self):
		return (
					
					(self.varpar_concent.mean-1)*np.sum(self.E_log_stick[:,1]) # E[alpha-1]*E[log (1-V)]
					# E[log p(V, alpha)] joint distr is easier to compute than likelihood and prior independently.
					+
					(self.atomic_concent_mean-1)*np.sum(self.E_log_atomic_stick[:,:,1])
					+
					np.sum(
							self.E_log_stick[:,1]*np.sum(np.cumsum(
																self.varpar_assignment[...,:0:-1],
																axis=-1
																)[...,::-1], axis=(0,1)
																)
							+
							self.E_log_stick[:,0]*np.sum(self.varpar_assignment[...,:-1], axis=(0,1))
							) # E[log p(Z | V)]
				)
		

	def get_E_log_q(self):
		return(
					(np.sum(self.E_log_stick[:,0]*(self.varpar_stick[:,0]-1))
						+
						np.sum(self.E_log_stick[:,1]*(self.varpar_stick[:,1]-1))
						-
						np.sum(
							sps.gammaln(self.varpar_stick),
							)
						+
						np.sum(sps.gammaln(self.sum_stick))
					) # E[log q(V)]
					+
					(np.sum(self.E_log_atomic_stick[:,:,0]*(self.varpar_atomic_stick[:,:,0]-1))
						+
						np.sum(self.E_log_atomic_stick[:,:,1]*(self.varpar_atomic_stick[:,:,1]-1))
						-
						np.sum(
							sps.gammaln(self.varpar_atomic_stick)
							)
						+
						np.sum(sps.gammaln(self.sum_atomic_stick))
					) # E[log q(atomic_stick)]
					+
					np.sum(self.varpar_assignment*np.ma.log(self.varpar_assignment)) # E[log q(Z)]
					+
					self.varpar_atomic_concent_shape*np.log(self.varpar_atomic_concent_rate)
					+
					self.atomic_concent_prior_rate*self.atomic_concent_mean
					)

	def set_log_posterior_expectation(self):
		log_expectation_atom = (
										np.append(
											np.log(self.varpar_atomic_stick[:,:,0])
											-
											np.log(self.sum_atomic_stick)
											,
											np.zeros((self.num_clusters,1))
											,
											axis=1
											)
										+
										np.append(
											np.zeros((self.num_clusters,1)),
											np.cumsum(
												np.log(self.varpar_atomic_stick[:,:,1])
												-
												np.log(self.sum_atomic_stick)
												,
												axis=-1
												)
											,
											axis=1
											)
										)
		self.log_posterior_expectation_top_down = spm.logsumexp(
															np.log(self.varpar_assignment)[:,:,:,np.newaxis] # num_children x num_customers x num_clusters
															+
															log_expectation_atom[np.newaxis,np.newaxis,:,:] # num_clusters x num_terminals
															,
															axis=-2
														)
	






class DP_b_base_LR(DP_b_top):
	def __init__(self, num_clusters, varpar_concent, mother, hdp):
		self.hdp=hdp
		self.mother=mother
		self.varpar_concent = varpar_concent
		self.varpar_stick = np.random.gamma(1,#10,
												20,#0.1,
												size=(num_clusters-1,2)
												) # gamma in Blei and Jordan.
		self.sum_stick = np.sum(self.varpar_stick, axis=-1)
		self.E_log_stick = sps.digamma(self.varpar_stick)-sps.digamma(self.sum_stick)[:, np.newaxis]
		self.num_clusters = num_clusters
		self.varpar_concent.add_dp(self)
		self.new_id=0
		self.children=[]
		self.id = self.mother.add_customer(self)


	def _update_varpar_assignment(self):
		log_varpar_assignment = (
									np.append(
										self.E_log_stick[:,0],
										0
										)[np.newaxis,np.newaxis,:]
									+np.append(
										0,
										np.cumsum(
											self.E_log_stick[:,1]
											)
										)[np.newaxis,np.newaxis,:]
									+
									np.sum(
										self.child_log_like_bottom_up[:,:,np.newaxis,:] # num_children x num_customers x num_terminals
										*
										self.mother.log_like_top_down[np.newaxis,np.newaxis,self.id,:,:] #  num_clusters x num_terminals
										,
										axis=-1
										)
								)
		self.varpar_assignment=np.exp(log_varpar_assignment-spm.logsumexp(log_varpar_assignment, axis=-1)[:,:,np.newaxis])


	def _update_log_like_top_down(self):
		self.log_like_top_down = np.sum(
										self.varpar_assignment[:,:,:,np.newaxis] 
										*
										self.mother.log_like_top_down[np.newaxis,np.newaxis,self.id,:,:] # num_clusters x num_terminals
										,
										axis=-2
										)

	def update_varpars(self):
		self._update_varpar_stick()
		self._update_varpar_assignment()
		self._update_log_like_top_down()

	def get_E_log_p(self):
		return (
					(
						(self.varpar_concent.mean-1)*np.sum(self.E_log_stick[:,1]) # E[alpha-1]*E[log (1-V)]
					) # E[log p(V, alpha)] joint distr is easier to compute than likelihood and prior independently.
					+ # E[log p(eta_m | Z_m-1, eta_m-1)] is a constant and is skipped.
					np.sum(
							self.E_log_stick[:,1]*np.sum(np.cumsum(
																self.varpar_assignment[...,:0:-1],
																axis=-1
																)[...,::-1], axis=(0,1)
																)
							+
							self.E_log_stick[:,0]*np.sum(self.varpar_assignment[...,:-1], axis=(0,1))
							) # E[log p(Z | V)]
				)

	def get_E_log_q(self):
		return(
					(np.sum(self.E_log_stick[:,0]*(self.varpar_stick[:,0]-1))
						+
						np.sum(self.E_log_stick[:,1]*(self.varpar_stick[:,1]-1))
						-
						np.sum(
							sps.gammaln(self.varpar_stick),
							)
						+
						np.sum(sps.gammaln(self.sum_stick))
					) # E[log q(V)]
					+
					np.sum(self.varpar_assignment*np.ma.log(self.varpar_assignment)) # E[log q(Z)]
					)

	def set_log_posterior_expectation(self):
		self.log_posterior_expectation_top_down = spm.logsumexp(
															np.log(self.varpar_assignment)[:,:,:,np.newaxis] # num_children x num_customers x num_clusters
															+
															self.mother.log_posterior_expectation_top_down[self.id,np.newaxis,np.newaxis,:,:] # num_clusters x num_terminals
															,
															axis=-2
														)


class DP_b_LR(DP_b_base_LR):
	def __init__(self, num_clusters, varpar_concent, mother, hdp, LR):
		self.hdp=hdp
		self.mother=mother
		self.varpar_concent = varpar_concent
		self.varpar_stick = np.random.gamma(1,#10,
												20,#0.1,
												size=(num_clusters-1,2)
												) # gamma in Blei and Jordan.
		self.sum_stick = np.sum(self.varpar_stick, axis=-1)
		self.E_log_stick = sps.digamma(self.varpar_stick)-sps.digamma(self.sum_stick)[:, np.newaxis]
		self.num_clusters = num_clusters
		self.varpar_concent.add_dp(self)
		self.id = self.mother.add_customer(self)
		if LR=='L':
			self.marg_axis=2
		else:
			self.marg_axis=1
		
		
	def add_customer(self, child):
		self.child=child
		return self.id

	def set_varpar_assignment(self):
		self.varpar_assignment = np.random.dirichlet(
											np.ones(self.num_clusters),
											self.child.num_clusters
											) # phi in Blei and Jordan.
		self._update_log_like_top_down()

	def _update_varpar_stick(self):
		self.varpar_stick[...,0] = np.sum(self.varpar_assignment, axis=0)[:-1]+1
		self.varpar_stick[...,1] = np.sum(
										np.cumsum(
											self.varpar_assignment[...,:0:-1],
											axis=-1
												)[...,::-1],
										axis=0
										)+self.varpar_concent.mean
		self.sum_stick = np.sum(self.varpar_stick, axis=-1)
		self.E_log_stick = sps.digamma(self.varpar_stick)-sps.digamma(self.sum_stick)[:, np.newaxis]



	def _update_varpar_assignment(self):
		log_varpar_assignment = (
									np.append(
										self.E_log_stick[:,0],
										0
										)[np.newaxis,:]
									+np.append(
										0,
										np.cumsum(
											self.E_log_stick[:,1]
											)
										)[np.newaxis,:]
									+np.matmul(
										self.child_log_like_bottom_up # num_customers x num_terminals
										,
										self.mother.log_like_top_down[self.id].T #  num_clusters x num_terminals
										)
								)

		self.varpar_assignment=np.exp(
								log_varpar_assignment
								-
								spm.logsumexp(
									log_varpar_assignment,
									axis=-1
									)[:,np.newaxis]
								)
	

	def _update_log_like_top_down(self):
		self.log_like_top_down = np.matmul(
										self.varpar_assignment # num_customers x num_clusters
										,
										self.mother.log_like_top_down[self.id] # num_clusters x num_terminals
										)

	def _update_log_like_bottom_up(self):
		self._set_child_log_like_bottom_up()
		self.log_like_bottom_up=np.matmul(
										self.varpar_assignment.T # num_clusters x num_customers
										,
										self.child_log_like_bottom_up # num_customers x num_terminals
										) #  num_clusters x num_terminals

	def _set_child_log_like_bottom_up(self):
		self.child_log_like_bottom_up = np.sum(
											self.child.log_like_bottom_up
											,
											axis=self.marg_axis
											)

	def get_E_log_p(self):
		return (
					(
						(self.varpar_concent.mean-1)*np.sum(self.E_log_stick[:,1]) # E[alpha-1]*E[log (1-V)]
					) # E[log p(V, alpha)] joint distr is easier to compute than likelihood and prior independently.
					+
					np.sum(
							self.E_log_stick[:,1]*np.sum(np.cumsum(
																self.varpar_assignment[...,:0:-1],
																axis=-1
																)[...,::-1], axis=0)
							+
							self.E_log_stick[:,0]*np.sum(self.varpar_assignment[...,:-1], axis=0)
							) # E[log p(Z | V)]
					)
	
	def set_log_posterior_expectation(self):
		self.log_posterior_expectation_top_down = spm.logsumexp(
															np.log(self.varpar_assignment)[:,:,np.newaxis] # num_customers x num_clusters
															+
															self.mother.log_posterior_expectation_top_down[self.id,np.newaxis,:,:] # num_clusters x num_terminals
															,
															axis=-2
														)

class DP_init(DP_b_LR):
	def __init__(self, num_clusters, varpar_concent, mother, hdp):
		self.hdp=hdp
		self.mother = mother
		self.varpar_concent=varpar_concent
		self.varpar_stick = np.random.gamma(1,#10,
												20,#0.1,
												size=(num_clusters-1,2)
												) # gamma in Blei and Jordan.
		self.sum_stick = np.sum(self.varpar_stick, axis=-1)
		self.E_log_stick = sps.digamma(self.varpar_stick)-sps.digamma(self.sum_stick)[:, np.newaxis]
		self.num_clusters = num_clusters
		self.varpar_concent.add_dp(self)
		self.word_ids=[]
		self.id = self.mother.add_customer(self)
		
		
		
		
	def set_varpar_assignment(self):
		self._update_log_assignment()
		
		
	def _update_varpar_stick(self):
		self.varpar_stick[...,0] = np.sum(self.expected_customer_counts[:-1], axis=1)+1
		self.varpar_stick[...,1] = np.sum(
										np.cumsum(
											self.expected_customer_counts[:0:-1],
											axis=0
												)[::-1],
										axis=1)+self.varpar_concent.mean
		self.sum_stick = np.sum(self.varpar_stick, axis=-1)
		self.E_log_stick = sps.digamma(self.varpar_stick)-sps.digamma(self.sum_stick)[:, np.newaxis]
		
		
	def _update_log_assignment(self):
		self.log_assignment=(
									np.append(
										self.E_log_stick[:,0],
										0
										)[:,np.newaxis]
									+np.append(
										0,
										np.cumsum(
											self.E_log_stick[:,1]
											)
										)[:,np.newaxis]
									+
									self.mother.log_like_top_down[self.id] # num_clusters x num_terminals
								)
		
	def update_expected_customer_counts(self, expected_rule_counts):
		self.expected_customer_counts=(
										expected_rule_counts[np.newaxis,:]
										*
										self._get_assign_probs()
										)
				
	def _get_assign_probs(self):
		return np.exp(
					self.log_assignment # num_clusters x num_terminals
					-
					spm.logsumexp(self.log_assignment, axis=0)[np.newaxis,:]
					)


	def update_varpars(self):
		self._update_varpar_stick()
		self._update_log_assignment()

	def _update_log_like_bottom_up(self):
		self.log_like_bottom_up=self.expected_customer_counts # num_clusters x num_terminals

	def get_log_rule_weights(self):
		return spm.logsumexp(self.log_assignment, axis=0)

	def get_E_log_p(self):
		return (self.varpar_concent.mean-1)*np.sum(self.E_log_stick[:,1]) # E[alpha-1]*E[log (1-V)]
				# E[log p(V, alpha)] joint distr is easier to compute than likelihood and prior independently.

	def get_E_log_q(self):
		return (
						np.sum(self.E_log_stick[:,0]*(self.varpar_stick[:,0]-1))
						+
						np.sum(self.E_log_stick[:,1]*(self.varpar_stick[:,1]-1))
						-
						np.sum(
							sps.gammaln(self.varpar_stick),
							)
						+
						np.sum(sps.gammaln(self.sum_stick))
					) # E[log q(V)]

	def set_log_posterior_expectation(self):
		self.log_posterior_expectation = spm.logsumexp(np.append(
											np.log(self.varpar_stick[:,0])
											-
											np.log(self.sum_stick)
											,
											0
											)[:,np.newaxis]
										+
										np.append(
											0,
											np.cumsum(
												np.log(self.varpar_stick[:,1])
												-
												np.log(self.sum_stick)
												)
											)[:,np.newaxis]
										+
										self.mother.log_posterior_expectation_top_down[self.id]
										,
										axis=0
										)



class Dirichlets(DP_init):
	def __init__(self, num_symbols, num_LHS, base_counts, hdp):
		self.hdp = hdp
		self.base_counts = base_counts
		self.varpar_atom = np.random.dirichlet(np.ones(num_symbols), size=num_LHS)
		self.sum_atom = np.sum(self.varpar_atom, axis=-1)
		self.E_log_atom = sps.digamma(self.varpar_atom) - sps.digamma(self.sum_atom)[:,np.newaxis]


	def _update_varpar_atom(self):
		self.varpar_atom = self.expected_customer_counts + self.base_counts[np.newaxis,:]
		self.sum_atom = np.sum(self.varpar_atom, axis=-1)
		self.E_log_atom = sps.digamma(self.varpar_atom) - sps.digamma(self.sum_atom)[:,np.newaxis]

	def update_expected_customer_counts(self, expected_rule_counts):
		self.expected_customer_counts = expected_rule_counts

	def update_varpars(self):
		self._update_varpar_atom()


	def get_E_log_p(self):
		return np.sum((self.base_counts-1)*np.sum(self.E_log_atom, axis=0))

	def get_E_log_q(self):
		return (
					np.sum(
						self.E_log_atom*(self.varpar_atom-1)
					)
					-
					np.sum(
						sps.gammaln(self.varpar_atom)
					)
					+
					np.sum(
						sps.gammaln(self.sum_atom)
					)
					)

	def set_log_posterior_expectation(self):
		self.log_posterior_expectation = np.log(self.varpar_atom)-np.log(self.sum_atom)[:,np.newaxis]



class DP_branch(DP_init):
	def __init__(self, num_clusters, varpar_concent, mother_L, mother_R, hdp):
		self.hdp=hdp
		self.mother_L = mother_L
		self.mother_R = mother_R
		self.varpar_concent=varpar_concent
		self.varpar_stick = np.random.gamma(1,#10,
												20,#0.1,
												size=(num_clusters-1,2)
												) # gamma in Blei and Jordan.
		self.sum_stick = np.sum(self.varpar_stick, axis=-1)
		self.E_log_stick = sps.digamma(self.varpar_stick)-sps.digamma(self.sum_stick)[:, np.newaxis]
		self.num_clusters = num_clusters
		self.varpar_concent.add_dp(self)
		self.id=self.mother_L.add_customer(self)
		self.mother_R.add_customer(self)
		


	def update_expected_customer_counts(self, expected_rule_counts):
		self.expected_customer_counts=(
				expected_rule_counts[np.newaxis,:,:] # 1 x num_nt x num_nt
				*
				self._get_assign_probs()
				)

	def _get_assign_probs(self):
		return np.exp(
					self.log_assignment # num_clusters x num_nt x num_nt
					-
					spm.logsumexp(self.log_assignment, axis=0)[np.newaxis,:,:]
					)
	
	def _update_varpar_stick(self):
		self.varpar_stick[...,0] = np.sum(self.expected_customer_counts[:-1], axis=(1,2))+1
		self.varpar_stick[...,1] = np.sum(
										np.cumsum(
											self.expected_customer_counts[:0:-1],
											axis=0
												)[::-1],
										axis=(1,2))+self.varpar_concent.mean
		self.sum_stick = np.sum(self.varpar_stick, axis=-1)
		self.E_log_stick = sps.digamma(self.varpar_stick)-sps.digamma(self.sum_stick)[:, np.newaxis]

	def _update_log_assignment(self):
		self.log_assignment=(
									np.append(
										self.E_log_stick[:,0],
										0
										)[:,np.newaxis,np.newaxis]
									+np.append(
										0,
										np.cumsum(
											self.E_log_stick[:,1]
											)
										)[:,np.newaxis,np.newaxis]
									+
									(
										self.mother_L.log_like_top_down[:,:,np.newaxis] # num_clusters x num_terminals
										+
										self.mother_R.log_like_top_down[:,np.newaxis,:] # num_clusters x num_terminals
									)
								)



	def _update_log_like_bottom_up(self):
		self.log_like_bottom_up=self.expected_customer_counts

	def set_log_posterior_expectation(self):
		self.log_posterior_expectation = spm.logsumexp(
											np.append(
												np.log(self.varpar_stick[:,0])
												-
												np.log(self.sum_stick)
												,
												0
												)[:,np.newaxis,np.newaxis]
											+
											np.append(
												0,
												np.cumsum(
													np.log(self.varpar_stick[:,1])
													-
													np.log(self.sum_stick)
													)
												)[:,np.newaxis,np.newaxis]
											+
											(
												self.mother_L.log_posterior_expectation_top_down[:,:,np.newaxis] # num_clusters x num_terminals
												+
												self.mother_R.log_posterior_expectation_top_down[:,np.newaxis,:] # num_clusters x num_terminals
											)
											,
											axis=0
											)

