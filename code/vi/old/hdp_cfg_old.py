# coding: utf-8

"""
Variational inference for infinite probabilistic context-free grammar.

Reference:
Liang, Percy, Slav Petrov, Michael Jordan and Dan Klein. (2007)
	"The Infinite PCFG Using Hierarchical Dirichlet Processes."
Liang, Percy, Michael Jordan and Dan Klein. (2009)
	"Probabilistic grammars and hierarchical Dirichlet processes."
"""

import numpy as np
import scipy.special as sps
import scipy.misc as spm
import algorithms.inner_outer_algorithm_sorted_by_length as ioa
import algorithms.gradient_projection_method_old as gpm


class HDPCFG(object):
	"""
	HDP-CFG with point-estimate of top-level non-terminal weights.
	"""
	def __init__(
				self,
				num_terminals,
				num_non_terminals,
				concentration_top,
				concentration_bottom,
				emission_base_counts,
				switch_base_counts,
				projection_method_tolerence,
				strings,
				max_batch_size=50
				):
		self.num_non_terminals = num_non_terminals
		
		self.sorted_strings = ioa.sort_data_by_length(strings, max_batch_size=max_batch_size)
		

		
		self.branch_hdp = HDP_branch(
									concentration_top,
									concentration_bottom,
									num_non_terminals,
									projection_method_tolerence,
									self
									)
		
		self.emit_dirichlets = Dirichlets(
							num_terminals,
							num_non_terminals,
							emission_base_counts,
							self
							)

		self.switch_dirichlets = Dirichlets(
							2,
							num_non_terminals,
							switch_base_counts,
							self
							)
		
		# We may assume multiple root non-terminals.
		# If this is the case, self.init_weights will describe
		# weights of non-terminals being a root.
		# For now, we assume only non-terminal 0 can be a root.
		self.init_weights = np.zeros(num_non_terminals)
		self.init_weights[0] += 1
		self._update_arrays()
		self._update_expected_rule_counts()

	def set_log_posterior_expectation(self):
		self.branch_hdp.set_log_posterior_expectation()
		self.emit_dirichlets.set_log_posterior_expectation()
		self.switch_dirichlets.set_log_posterior_expectation()


	def get_log_posterior_arrays(self):
		self.set_log_posterior_expectation()
		log_switch_weights = self.switch_dirichlets.log_posterior_expectation

		log_emit_rule_weights = (
							self.emit_dirichlets.log_posterior_expectation
							+
							log_switch_weights[:,1,np.newaxis]
							)
		log_branch_rule_weights = (
									self.branch_hdp.log_posterior_expectation
									+
									log_switch_weights[:,0,np.newaxis,np.newaxis]
									)
		return (log_branch_rule_weights,log_emit_rule_weights)

	def get_log_posterior_pred(self, test_data):
		pass
		# log_branch_rule_weights,log_emit_rule_weights = self.get_log_posterior_arrays()
		# branch_rule_weights = np.exp(log_branch_rule_weights)
		# emission_rule_weights = np.exp(log_emit_rule_weights)
		# init_weights = np.zeros(emission_rule_weights.shape[0])
		# init_weights[0] += 1
		# log_posterior_predictive_probs = []
		# for string in test_data:
		# 	str_len=len(string)
		# 	if str_len==1:
		# 		log_posterior_predictive_probs.append(
		# 			spm.logsumexp(np.log(init_weights)+np.log(emission_rule_weights[:,string[0]]))
		# 			)
		# 	else:
		# 		(forward_branch,
		# 				inner_branch,
		# 				forward_emit,
		# 				inner_emit,
		# 				scaling_factors,
		# 				completed_inner_branch)=ioa.get_forward_inner_probs(
		# 										string,
		# 										str_len,
		# 										branch_rule_weights,
		# 										emission_rule_weights,
		# 										init_weights,
		# 										left_corner_mat
		# 										)
		# 		log_posterior_predictive_probs.append(
		# 					spm.logsumexp(
		# 						np.log(init_weights)
		# 						+
		# 						np.log(np.sum(inner_branch[-1,0,:,:,:,2], axis=(1,2)))
		# 						-
		# 						np.sum(np.log(scaling_factors))
		# 						)
		# 					)
		# return log_posterior_predictive_probs





	def _update_expected_rule_counts(self):
		"""
		Stolcke's (1995) algorithm
		"""
		self.expected_branch_rule_counts = np.zeros(self.branch_rule_weights.shape)
		self.expected_emission_rule_counts = np.zeros(self.emission_rule_weights.shape)
		self.expected_init_weights = np.zeros(self.init_weights.shape)
		self.inner_outer_algorithm = ioa.InnerOuterAlgorithm(
											self.branch_rule_weights,
											self.emission_rule_weights,
											self.init_weights
										)
		self.E_log_p_trees = np.float64(0)
		[self._increment_expected_rule_counts(
										string_batch
										)
			for batch_list in self.sorted_strings.itervalues()
			for string_batch in batch_list
			]
		self.branch_hdp.update_expected_rule_counts(self.expected_branch_rule_counts)
		self.emit_dirichlets.update_expected_rule_counts(self.expected_emission_rule_counts)
		self.switch_dirichlets.update_expected_rule_counts(
											np.array(
												(
												np.sum(self.expected_branch_rule_counts, axis=(1,2))
												,
												np.sum(self.expected_emission_rule_counts, axis=1)
												)
											).T
										)

	def _increment_expected_rule_counts(
								self,
								string_batch
								):
		(
			expected_branch_rule_counts,
			expected_emission_rule_counts,
			expected_init_weights,
			log_like
		) = self.inner_outer_algorithm.get_expected_rule_counts(string_batch)
		self.expected_branch_rule_counts += np.sum(expected_branch_rule_counts, axis=0)
		self.expected_emission_rule_counts += np.sum(expected_emission_rule_counts, axis=0)
		self.expected_init_weights += np.sum(expected_init_weights, axis=0)
		self.E_log_p_trees += np.sum(log_like)
		


		
	def _update_arrays(self):
		log_switch_weights = self.switch_dirichlets.get_log_rule_weights()
		log_branch_rule_weights=(
									self.branch_hdp.get_log_rule_weights()
									+
									log_switch_weights[:,0,np.newaxis,np.newaxis]
								)
		log_emit_rule_weights=(
								self.emit_dirichlets.get_log_rule_weights()
								+
								log_switch_weights[:,1,np.newaxis]
								) # num_nt x num_terminals

		self.branch_rule_weights=np.exp(log_branch_rule_weights)
		self.emission_rule_weights=np.exp(log_emit_rule_weights)
		# self.init_weights=np.exp(log_init_weight_array)
		
		




	def update_varpars(self):

		self.branch_hdp.update_varpars()
		self.emit_dirichlets.update_varpars()
		self.switch_dirichlets.update_varpars()
		
		self._update_arrays()
		self._update_expected_rule_counts()




	def get_var_bound(self):
		return (
				self.E_log_p_trees
				+
				self.branch_hdp.get_var_bound()
				+
				self.emit_dirichlets.get_var_bound()
				+
				self.switch_dirichlets.get_var_bound()
				)



class Dirichlets(object):
	"""
	For emission and switch rules.
	"""
	def __init__(self, num_symbols, num_LHS, base_counts, hdp):
		self.hdp = hdp
		self.base_counts = base_counts
		self.varpar_rule_weight = np.random.gamma(1, 20, size=(num_LHS, num_symbols))
		self.sum_rule_weight = np.sum(self.varpar_rule_weight, axis=-1)
		self.E_log_rule_weight = sps.digamma(self.varpar_rule_weight) - sps.digamma(self.sum_rule_weight)[:,np.newaxis]


	def _update_varpar_rule_weight(self):
		self.varpar_rule_weight = self.expected_rule_counts + self.base_counts[np.newaxis,:]
		self.sum_rule_weight = np.sum(self.varpar_rule_weight, axis=-1)
		self.E_log_rule_weight = sps.digamma(self.varpar_rule_weight) - sps.digamma(self.sum_rule_weight)[:,np.newaxis]

	def update_expected_rule_counts(self, expected_rule_counts):
		self.expected_rule_counts = expected_rule_counts

	def update_varpars(self):
		self._update_varpar_rule_weight()

	def get_log_rule_weights(self):
		return self.E_log_rule_weight

	def get_var_bound(self):
		return self.get_E_log_p() - self.get_E_log_q()

	def get_E_log_p(self):
		return np.sum((self.base_counts-1)*np.sum(self.E_log_rule_weight, axis=0))

	def get_E_log_q(self):
		return (
					np.sum(
						self.E_log_rule_weight*(self.varpar_rule_weight-1)
					)
					-
					np.sum(
						sps.gammaln(self.varpar_rule_weight)
					)
					+
					np.sum(
						sps.gammaln(self.sum_rule_weight)
					)
					)

	def set_log_posterior_expectation(self):
		self.log_posterior_expectation = np.log(self.varpar_rule_weight)-np.log(self.sum_rule_weight)[:,np.newaxis]



class HDP_branch(Dirichlets):
	"""
	For branching rules and top-level non-terminal weights.
	"""
	def __init__(
			self,
			concentration_top,
			concentration_bottom,
			num_non_terminals,
			projection_method_tolerence,
			hdp
			):
		self.hdp=hdp
		self.num_non_terminals = num_non_terminals
		self.projection_method_tolerence = projection_method_tolerence

		self.top_concent_minus_1 = concentration_top-1
		self.concentration_bottom = concentration_bottom


		full_weights = np.random.dirichlet(np.ones(num_non_terminals))
		self.non_terminal_weight_estimate = full_weights[:-1]
		self.base_counts = (
							self.concentration_bottom
							*
							full_weights[:,np.newaxis]
							*
							full_weights[np.newaxis,:]
						)

		
		self.varpar_rule_weight = np.random.gamma(
												1, 20,
												size=(
													num_non_terminals,
													num_non_terminals,
													num_non_terminals
												)
												)
		self.sum_rule_weight = np.sum(self.varpar_rule_weight, axis=(1,2))
		self.E_log_rule_weight = sps.digamma(self.varpar_rule_weight) - sps.digamma(self.sum_rule_weight)[:,np.newaxis,np.newaxis]

		# Index array of the following form.
		# 0 1 2 3 4 5 ...
		# 1 1 2 3 4 5 ...
		# 2 2 2 3 4 5 ...
		# 3 3 3 3 4 5 ...
		# 4 4 4 4 4 5 ...
		# 5 5 5 5 5 5 ...
		# ...............
		self.radial_ids = np.maximum(
							np.arange(num_non_terminals)[:,np.newaxis],
							np.arange(num_non_terminals)[np.newaxis,:]
							)


		
	def _update_non_terminal_weight_estimate(self):
		self.non_terminal_weight_estimate = gpm.gradient_projection_optimization(
												self.non_terminal_weight_estimate,
												self.objective_func,
												self.get_gradient_and_hessian,
												self.projection_method_tolerence,
												)
		full_weights = np.append(
							self.non_terminal_weight_estimate,
							1-np.sum(self.non_terminal_weight_estimate)
							)
		self.base_counts = (
							self.concentration_bottom
							*
							full_weights[:,np.newaxis]
							*
							full_weights[np.newaxis,:]
						)
	
	def _update_varpar_rule_weight(self):
		self.varpar_rule_weight = (
									self.expected_rule_counts
									+
									self.base_counts[np.newaxis,:,:]
									)
		self.sum_rule_weight = np.sum(self.varpar_rule_weight, axis=(1,2))
		self.E_log_rule_weight = sps.digamma(self.varpar_rule_weight) - sps.digamma(self.sum_rule_weight)[:,np.newaxis,np.newaxis]

	def update_varpars(self):
		self._update_non_terminal_weight_estimate()
		self._update_varpar_rule_weight()

	def get_E_log_p(self):
		return -self.objective_func(
						self.non_terminal_weight_estimate
						)

	def get_E_log_q(self): # No log q(non_terminal_weight_estimate)?
		return (
					np.sum(
						self.E_log_rule_weight*(self.varpar_rule_weight-1)
					)
					-
					np.sum(
						sps.gammaln(self.varpar_rule_weight)
					)
					+
					np.sum(
						sps.gammaln(self.sum_rule_weight)
					)
					)

	def set_log_posterior_expectation(self):
		self.log_posterior_expectation = np.log(self.varpar_rule_weight)-np.log(self.sum_rule_weight)[:,np.newaxis,np.newaxis]

	def get_prob_non_stop(self, non_terminal_weights):
		prob_non_stop = 1 - np.cumsum(non_terminal_weights)
		last_portion = prob_non_stop[-1]
		full_weights = np.append(non_terminal_weights, last_portion)
		return prob_non_stop, last_portion, full_weights



	def objective_func(self, non_terminal_weights):
		"""
		The objective function of non_terminal_weights that must be minimized.
		i.e. (-1)*(function to be maximized).
		"""
		prob_non_stop, last_portion, full_weights = self.get_prob_non_stop(non_terminal_weights)
		prior_term = (
						self.top_concent_minus_1*np.log(last_portion)
						-
						np.sum(np.log(prob_non_stop[:-1]))
						)

		concent_x_weights_x_weights = self.concentration_bottom*full_weights[:,np.newaxis]*full_weights[np.newaxis,:]
		rule_term = (
			np.sum(
				(concent_x_weights_x_weights-1)
				*
				np.sum(self.E_log_rule_weight, axis=0)
			)
			-
			self.num_non_terminals*
			np.sum(
				sps.gammaln(
					concent_x_weights_x_weights
					)
					)
			)
		return -(prior_term + rule_term)


	def get_gradient_and_hessian(self, non_terminal_weights):
		"""
		Get gradient and hessian of the objective function.
		"""
		
		(gradient,
			prob_non_stop,
			full_weights,
			concent_x_weights_x_weights,
			digamma_concent_x_weights_x_weights
			) = self.get_gradient(non_terminal_weights)

		hessian = self.get_hessian(
					prob_non_stop,
					full_weights,
					concent_x_weights_x_weights,
					digamma_concent_x_weights_x_weights,
					)

		return gradient, hessian


	def get_gradient(self, non_terminal_weights):
		"""
		Get the derivative of objective function
		from pre-computed information.
		"""
		prob_non_stop, last_portion, full_weights = self.get_prob_non_stop(non_terminal_weights)


		inverse_prob_non_stop = 1.0 / (prob_non_stop)
		gradient_prior = np.append(
							np.append(
								np.cumsum(inverse_prob_non_stop[-2::-1])[::-1]
								,
								0.0
							)
							-
							inverse_prob_non_stop[-1]*self.top_concent_minus_1
							,
							0.0
							)
		
		concent_x_weights_x_weights = self.concentration_bottom*full_weights[:,np.newaxis]*full_weights[np.newaxis,:]
		digamma_concent_x_weights_x_weights = sps.digamma(
													concent_x_weights_x_weights
													)
		gradient_rules_K = (
							self.concentration_bottom
							*
							(
								np.sum(
									full_weights[np.newaxis,:,np.newaxis]
									*
									self.E_log_rule_weight
									,
									axis=(0,1)
								)
								+
								np.sum(
									full_weights[np.newaxis,np.newaxis,:]
									*
									self.E_log_rule_weight
									,
									axis=(0,2)
								)
								-
								2
									*self.num_non_terminals
									*np.sum(
										full_weights[:,np.newaxis]
										*
										digamma_concent_x_weights_x_weights
										,
										axis=(0)
									)
							)
							)
		gradient_rules = np.append(
							gradient_rules_K[:-1] - gradient_rules_K[-1]
							,
							gradient_rules_K[-1]
							)
		return (
				-(gradient_prior + gradient_rules),
				prob_non_stop,
				full_weights,
				concent_x_weights_x_weights,
				digamma_concent_x_weights_x_weights
				)


	def get_hessian(
				self,
				prob_non_stop,
				full_weights,
				concent_x_weights_x_weights,
				digamma_concent_x_weights_x_weights
				):
		"""
		Get the hessian matrix of the objective function.
		"""
		prob_non_stop_inv_square = prob_non_stop**-2
		hessian_prior_vec = np.append(
								np.append(
										np.cumsum(prob_non_stop_inv_square[-2::-1])[::-1]
										,
										0.0
									)
								-
								self.top_concent_minus_1 * prob_non_stop_inv_square[-1]
								,
								0.0
								)
		hessian_prior = hessian_prior_vec[self.radial_ids]


		sum_E_log_rule_over_LHS = np.sum(self.E_log_rule_weight, axis=0)
		E_log_rule_plus_E_log_rule_T = sum_E_log_rule_over_LHS+sum_E_log_rule_over_LHS.T
		

		trigamma_cww = sps.polygamma(
								1,
								concent_x_weights_x_weights
								)
		cww_trigamma_cww = (
							concent_x_weights_x_weights
							*
							trigamma_cww
							)
		
		concent_x_sum_square_weights_x_trigamma = np.sum(
													(full_weights**2)[:,np.newaxis]
													*
													trigamma_cww
													,
													axis=0
													)*self.concentration_bottom


		deldel_rules_components = self.concentration_bottom*(
									E_log_rule_plus_E_log_rule_T
									-
									2 * self.num_non_terminals
									*
									(
										cww_trigamma_cww
										+
										digamma_concent_x_weights_x_weights
										+
										np.diag(concent_x_sum_square_weights_x_trigamma)
									)
									)

		
		hessian_rules = np.copy(deldel_rules_components)
		hessian_rules[:-1,:-1] += (
									deldel_rules_components[-1,-1]
									-
									deldel_rules_components[:-1,-1,np.newaxis]
									-
									deldel_rules_components[-1,np.newaxis,:-1]
									)
		hessian_rules[:-1,-1] -= deldel_rules_components[-1,-1]
		hessian_rules[-1,:-1] -= deldel_rules_components[-1,-1]
		return -(hessian_prior+hessian_rules)

