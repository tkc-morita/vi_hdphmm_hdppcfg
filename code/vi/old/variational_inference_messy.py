# coding: utf-8

import numpy as np
import pandas as pd
from logging import getLogger,FileHandler,DEBUG,Formatter
import os, datetime, sys
import hdp_pcfg
import hdp_hmm
import hdp_gaussian_hmm_mixture_Poisson_length as hdp_gauss_hmm_mix_poisson
import hdp_gaussian_hmm_mixture_Bernoulli_length as hdp_gauss_hmm_mix_bernoulli
import hdp_gaussian_hmm_poisson_length as hdp_gauss_hmm_poisson
import dp_gaussian_mixture
import finite_hmm

np.seterr(invalid='raise')
logger = getLogger(__name__)

def update_log_handler(log_path, model):
	current_handlers=logger.handlers[:]
	for h in current_handlers:
		logger.removeHandler(h)
	handler = FileHandler(filename=os.path.join(log_path,'VI_DP_%s.log' % model))	#Define the handler.
	handler.setLevel(DEBUG)
	formatter = Formatter('%(asctime)s - %(levelname)s - %(message)s')	#Define the log format.
	handler.setFormatter(formatter)
	logger.setLevel(DEBUG)
	logger.addHandler(handler)	#Register the handler for the logger.
	logger.info("Logger (re)set up.")


def code_data_cfg(training_data,test_data=None):
	if test_data is None:
		str_data=training_data
	else:
		str_data=training_data+test_data
	inventory = sorted(set(reduce(lambda x,y: x+y, str_data)))
	num_symbols = len(inventory)
	encoder = {symbol:code for code,symbol in enumerate(inventory, start=0)}
	decoder = {code:symbol for code,symbol in enumerate(inventory, start=0)}
	if test_data is None:
		coded_data = [map(lambda s: encoder[s],phrase) for phrase in str_data]
		return (coded_data,encoder,decoder)
	else:
		coded_training_data=[map(lambda s: encoder[s],phrase) for phrase in training_data]
		coded_test_data=[map(lambda s: encoder[s],phrase) for phrase in test_data]
		return (coded_training_data,coded_test_data,encoder,decoder)


class Variational_Inference_HDP_PCFG(object):
	def __init__(
			self,
			data,
			num_non_terminals,
			concentration_top,
			concentration_bottom,
			emission_base_counts,
			switch_base_counts,
			gradient_projection_tolerance,
			result_path,
			trial_id=None,
			entire_inventory_size=None,
			max_batch_array_size=100
			):
		update_log_handler(result_path, 'CFG')
		
		logger.info('HDP-PCFG. Script last updated at %s'
							% datetime.datetime.fromtimestamp(
									os.stat(sys.argv[0]).st_mtime
									).strftime('%Y-%m-%d-%H:%M:%S')
							)
		
		if not trial_id is None:
			logger.info('Trial ID: %i' % trial_id)
		
		
		if entire_inventory_size is None:
			num_terminals = len(set(reduce(lambda x,y: x+y, data)))
		else:
			num_terminals=entire_inventory_size
		logger.info('# of symbols: %i' % num_terminals)
		
		
		data_size = len(data)
		logger.info('# of strings: %i' % data_size)
		logger.info('Max batch size of data array to be processed in parallel in inner-outer algorithm: %i' % max_batch_array_size)
		
		
		self.model = hdp_pcfg.HDP_PCFG(
							num_terminals,
							num_non_terminals,
							concentration_top,
							concentration_bottom,
							emission_base_counts,
							switch_base_counts,
							gradient_projection_tolerance,
							data,
							max_batch_array_size=max_batch_array_size
							)
		
		
		logger.info('(Max) # of non-terminals: %i' % num_non_terminals)
		logger.info('Base count of emission rules: %s' % str(emission_base_counts))
		logger.info('Base count of switch rules: %s' % str(switch_base_counts))
		logger.info('Concentration of branching rules: %s' % str(concentration_bottom))
		logger.info('Concentration of top stick breaking for non-terminals: %s' % str(concentration_top))

		self.result_path = result_path 
		logger.info('Initialization complete.')
		
	def get_result_path(self):
		return self.result_path

	def get_log_posterior_pred(self, test_data):
		return self.model.get_log_posterior_pred(test_data)

		
	
	def train(self, max_iters, tolerance, decoder=None):
		logger.info("Main loop started.")
		logger.info("Max iteration is %i." % max_iters)
		logger.info("Will be terminated if variational bound is only improved by <=%f." % tolerance)
		converged=False
		iter_id=0
		self.current_var_bound = self.get_var_bound()
		logger.info("Initial var_bound is %0.12f." % self.current_var_bound)
		while iter_id<max_iters:
			iter_id+=1
			self.update_varpars()
			logger.info("Variational parameters updated (Iteration ID: %i)." % iter_id)
			new_var_bound = self.get_var_bound()
			improvement = new_var_bound-self.current_var_bound
			logger.info("Current var_bound is %0.12f (%+0.12f)." % (new_var_bound,improvement))
			if np.isnan(new_var_bound):
				raise Exception("nan detected.")
			if improvement<0:
				logger.error("variational bound decreased. Something wrong.")
				if improvement < - 0.000001: # [2017/10/11] We ignore small decrease.
					if not decoder is None:
						self.save_results(decoder)
					raise Exception("variational bound decreased. Something wrong.")
			if improvement<=tolerance:
				converged = True
				break
			else:
				self.current_var_bound=new_var_bound
		if converged:
			logger.info('Converged after %i iterations.' % iter_id)
		else:
			logger.error('Failed to converge after max iterations.')
		logger.info('Final variational bound is %0.12f.' % self.current_var_bound)
		

		
	def save_results(self, decoder):
		with pd.HDFStore(
				os.path.join(
					self.result_path,
					'variational_parameters.h5'
					)
				) as hdf5_store:
			

			num_non_terminals = self.model.emission_distributions.varpar_rule_weight.shape[0]
			num_terminals = self.model.emission_distributions.varpar_rule_weight.shape[1]

			# branching
			df_rule = pd.DataFrame(
								self.model.branch_hdp.varpar_rule_weight.flatten()[:,np.newaxis]
								,
								columns=['dirichlet_par']
							)
			df_rule['LHS'] = np.repeat(
								np.arange(num_non_terminals),
								num_non_terminals**2
								)
			df_rule['RHS_left'] = np.repeat(
											np.tile(
												np.arange(num_non_terminals)
												,
												num_non_terminals
											)
											,
											num_non_terminals
										)
			df_rule['RHS_right'] = np.tile(
										np.arange(num_non_terminals),
										num_non_terminals**2
										)
			hdf5_store['branching'] = df_rule

			df_top = pd.DataFrame(
							self.model.branch_hdp.non_terminal_weight_estimate[:,np.newaxis]
							,
							columns=['p']
							)
			df_top['non_terminal'] = df_top.index
			hdf5_store['non_terminal'] = df_top

			# emission
			df_rule = pd.DataFrame(
							self.model.emission_distributions.varpar_rule_weight.flatten()[:np.newaxis]
							,
							columns=['dirichlet_par']
							)
			
			df_rule['terminal'] = pd.Series(
										np.tile(np.arange(num_terminals), num_non_terminals)
										).map(decoder)
			df_rule['LHS'] = np.repeat(np.arange(num_non_terminals), num_terminals)
			hdf5_store['emission'] = df_rule

			# switch
			df_rule = pd.DataFrame(
							self.model.switch_distributions.varpar_rule_weight
							,
							columns=['branch','emit']
							)
			df_rule['LHS'] = df_rule.index
			hdf5_store['switch'] = df_rule
		


		log_branch_array,log_emit_array=self.model.get_log_posterior_arrays()
		
		
		df_emit_array=pd.DataFrame(
								log_emit_array.flatten()[:,np.newaxis]
								,
								columns=['log_p']
							)
		df_emit_array['terminal'] = pd.Series(
										np.tile(np.arange(num_terminals), num_non_terminals)
										).map(decoder)
		df_emit_array['LHS'] = np.repeat(np.arange(num_non_terminals), num_terminals)
		
		
		df_emit_array.to_csv(
							os.path.join(
								self.result_path,
								"log_emission_probs.csv"
								)
							,
							index=False
							)

	
		df_branch_array=pd.DataFrame(
							log_branch_array.flatten()[:,np.newaxis]
							,
							columns=['log_p']
							)
		df_branch_array['LHS'] = np.repeat(
								np.arange(num_non_terminals),
								num_non_terminals**2
								)
		df_branch_array['RHS_left'] = np.repeat(
										np.tile(
											np.arange(num_non_terminals)
											,
											num_non_terminals
										)
										,
										num_non_terminals
									)
		df_branch_array['RHS_right'] = np.tile(
									np.arange(num_non_terminals),
									num_non_terminals**2
									)
		
		df_branch_array.to_csv(
								os.path.join(
									self.result_path,
									"log_branch_probs.csv"
									)
								,
								index=False
								)
		


	def update_varpars(self):
		self.model.update_varpars()
		
		
	def get_var_bound(self):
		"""
		Calculate the KL divergence bound based on the current variational parameters.
		We ignore the constant terms.
		"""
		return self.model.get_var_bound()



class Variational_Inference_HDP_Gaussian_PCFG(Variational_Inference_HDP_PCFG):
	def __init__(
			self,
			data,
			num_non_terminals,
			concentration_top,
			concentration_bottom,
			concentration_init,
			switch_base_counts,
			gradient_projection_tolerance,
			result_path,
			trial_id=None,
			max_batch_array_size=650
			):
		update_log_handler(result_path, 'Gauss-CFG')
		
		logger.info('HDP-CFG with Gaussian emission. Script last updated at %s'
							% datetime.datetime.fromtimestamp(
									os.stat(sys.argv[0]).st_mtime
									).strftime('%Y-%m-%d-%H:%M:%S')
							)
		
		if not trial_id is None:
			logger.info('Trial ID: %i' % trial_id)
		
		

		num_dimensions = data[0][0].size
		global_mean = np.mean(
						np.array(
							[
								data_point
								for string in data
								for data_point in string
							]
						), 
						axis=0
						)
		


		# Priors for states' emission.
		# Following Feldman et al. (2013).
		covmat_prior_scale_mat = np.identity(num_dimensions)
		covmat_prior_df = num_dimensions - 1 + 0.001
		logger.info('Prior on covariance matrix for each non-terminal: IW(I, %s)' % str(covmat_prior_df))
		mean_prior_denominator = covmat_prior_df
		logger.info('Prior on mean for each non-terminal: N(global_mean, state_cov_mat/%s)' % mean_prior_denominator)
		logger.info('global_mean is the sample mean of all the data points.')
		
		
		
		data_size = len(data)
		logger.info('# of strings: %i' % data_size)
		logger.info('Max batch size of data array to be processed in parallel in inner-outer algorithm: %i' % max_batch_array_size)
		
		
		self.model = hdp_pcfg.HDP_PCFG_Gaussian(
							num_dimensions,
							num_non_terminals,
							concentration_top,
							concentration_bottom,
							concentration_init,
							global_mean,
							mean_prior_denominator,
							covmat_prior_scale_mat,
							covmat_prior_df,
							switch_base_counts,
							gradient_projection_tolerance,
							data,
							max_batch_array_size=max_batch_array_size
							)
		
		
		logger.info('(Max) # of non-terminals: %i' % num_non_terminals)
		logger.info('Base count of switch rules: %s' % str(switch_base_counts))
		logger.info('Concentration of branching rules: %s' % str(concentration_bottom))
		logger.info('Concentration of top stick breaking for non-terminals: %s' % str(concentration_top))
		logger.info('Concentration of root labeling: %s' % str(concentration_init))

		logger.info('Tolerance for gradient projection: %s' % str(gradient_projection_tolerance))

		self.result_path = result_path 
		logger.info('Initialization complete.')
		


		

		
	def save_results(self):
		with pd.HDFStore(
				os.path.join(
					self.result_path,
					'variational_parameters.h5'
					)
				) as hdf5_store:

			# branching
			df_rule = pd.DataFrame(
								self.model.branch_hdp.varpar_rule_weight.flatten()[:,np.newaxis]
								,
								columns=['dirichlet_par']
							)
			df_rule['LHS'] = np.repeat(
								np.arange(self.model.num_non_terminals),
								self.model.num_non_terminals**2
								)
			df_rule['RHS_left'] = np.repeat(
											np.tile(
												np.arange(self.model.num_non_terminals)
												,
												self.model.num_non_terminals
											)
											,
											self.model.num_non_terminals
										)
			df_rule['RHS_right'] = np.tile(
										np.arange(self.model.num_non_terminals),
										self.model.num_non_terminals**2
										)
			hdf5_store['branching'] = df_rule

			# Top-level weights of non-terminals.
			df_top = pd.DataFrame(
							self.model.branch_hdp.non_terminal_weight_estimate[:,np.newaxis]
							,
							columns=['p']
							)
			df_top['non_terminal'] = df_top.index
			hdf5_store['non_terminal'] = df_top

			# Root labeling
			df_init = pd.DataFrame(
							self.model.branch_hdp.varpar_init_weight[:,np.newaxis]
							,
							columns=['dirichlet_par']
							)
			df_init['non_terminal'] = df_init.index
			hdf5_store['root'] = df_init

			# emission
			num_clusters = self.model.emission_distributions.varpar_mean_denominator.size
			df_mean=pd.DataFrame(self.model.emission_distributions.varpar_mean_mean
										,
										columns=[('dim_%i' % dim)
													for dim in range(self.model.emission_distributions.num_dimensions)
													]
										)
			
			df_mean['cluster'] = df_mean.index
			hdf5_store.put(
						'emission/mean/mean'
						,
						df_mean
						)

			df_denom = pd.DataFrame(
							self.model.emission_distributions.varpar_mean_denominator[:,np.newaxis]
							,
							columns=['value']
							)
			df_denom['cluster'] = df_denom.index
			hdf5_store.put(
						'emission/mean/denominator'
						,
						df_denom
						)

			upper_triangle_ids1,upper_triangle_ids2 = np.triu_indices(self.model.emission_distributions.num_dimensions)
			df_scale_mat=pd.DataFrame(
							np.linalg.inv(self.model.emission_distributions.inv_varpar_covmat_scale_mat)[
								:,
								upper_triangle_ids1,
								upper_triangle_ids2
								].flatten()[:,np.newaxis]
							,
							columns=['matrix_entry']
							)
			df_scale_mat['cluster'] = np.repeat(
											np.arange(num_clusters)
											,
											upper_triangle_ids1.size
										)
			df_scale_mat['row_id'] = np.tile(upper_triangle_ids1, num_clusters)
			df_scale_mat['col_id'] = np.tile(upper_triangle_ids2, num_clusters)
			hdf5_store.put(
						'emission/covmat/scale_mat'
						,
						df_scale_mat
						)

			df_df = pd.DataFrame(
						self.model.emission_distributions.varpar_covmat_df[:,np.newaxis],
						columns=['df']
						)
			df_df['cluster'] = df_df.index
			hdf5_store.put(
						'emission/covmat/df'
						,
						df_df
						)

			# switch
			df_rule = pd.DataFrame(
							self.model.switch_distributions.varpar_rule_weight
							,
							columns=['branch','emit']
							)
			df_rule['LHS'] = df_rule.index
			hdf5_store['switch'] = df_rule
		



class Variational_Inference_HDP_Gaussian_PCFG_Distinct_Terminals(Variational_Inference_HDP_Gaussian_PCFG):
	def __init__(
			self,
			data,
			num_non_terminals,
			num_terminals,
			concentration_top,
			concentration_bottom,
			concentration_init,
			concentration_terminal_top,
			concentration_terminal_bottom,
			switch_base_counts,
			gradient_projection_tolerance,
			result_path,
			trial_id=None,
			max_batch_array_size=650
			):
		update_log_handler(result_path, 'Gauss-PCFG')
		
		logger.info('HDP-CFG with Gaussian emission. Script last updated at %s'
							% datetime.datetime.fromtimestamp(
									os.stat(sys.argv[0]).st_mtime
									).strftime('%Y-%m-%d-%H:%M:%S')
							)
		logger.info('Each terminal is associated with Gaussian and is distinguished from non-terminals.')
		
		if not trial_id is None:
			logger.info('Trial ID: %i' % trial_id)
		
		

		num_dimensions = data[0][0].size
		global_mean = np.mean(
						np.array(
							[
								data_point
								for string in data
								for data_point in string
							]
						), 
						axis=0
						)
		


		# Priors for states' emission.
		# Following Feldman et al. (2013).
		covmat_prior_scale_mat = np.identity(num_dimensions)
		covmat_prior_df = num_dimensions - 1 + 0.001
		logger.info('Prior on covariance matrix for each non-terminal: IW(I, %s)' % str(covmat_prior_df))
		mean_prior_denominator = covmat_prior_df
		logger.info('Prior on mean for each non-terminal: N(global_mean, state_cov_mat/%s)' % mean_prior_denominator)
		logger.info('global_mean is the sample mean of all the data points.')
		
		
		
		data_size = len(data)
		logger.info('# of strings: %i' % data_size)
		logger.info('Max batch size of data array to be processed in parallel in inner-outer algorithm: %i' % max_batch_array_size)
		
		
		self.model = hdp_pcfg.HDP_PCFG_Gaussian_With_Terminals(
							num_dimensions,
							num_non_terminals,
							num_terminals,
							concentration_top,
							concentration_bottom,
							concentration_init,
							concentration_terminal_top,
							concentration_terminal_bottom,
							global_mean,
							mean_prior_denominator,
							covmat_prior_scale_mat,
							covmat_prior_df,
							switch_base_counts,
							gradient_projection_tolerance,
							data,
							max_batch_array_size=max_batch_array_size
							)
		
		
		logger.info('(Max) # of non-terminals: %i' % num_non_terminals)
		logger.info('(Max) # of terminals: %i' % num_terminals)
		logger.info('Base count of switch rules: %s' % str(switch_base_counts))
		logger.info('Concentration of branching rules: %s' % str(concentration_bottom))
		logger.info('Concentration of top stick breaking for non-terminals: %s' % str(concentration_top))
		logger.info('Concentration of root labeling: %s' % str(concentration_init))
		logger.info('Concentration of non-terminal-to-terminal rules: %s' % str(concentration_terminal_bottom))
		logger.info('Concentration of top stick breaking for terminal: %s' % str(concentration_terminal_top))

		logger.info('Tolerance for gradient projection: %s' % str(gradient_projection_tolerance))

		self.result_path = result_path 
		logger.info('Initialization complete.')


	def save_results(self):
		super(Variational_Inference_HDP_Gaussian_PCFG_Distinct_Terminals, self).save_results()
		with pd.HDFStore(
				os.path.join(
					self.result_path,
					'variational_parameters.h5'
					)
				) as hdf5_store:
			# non-terminals-to-terminal rules
			df_rule = pd.DataFrame(
								self.model.nt2t_rule_hdp.varpar_transition.flatten()[:,np.newaxis]
								,
								columns=['dirichlet_par']
							)
			df_rule['non_terminal'] = np.repeat(
								np.arange(self.model.num_non_terminals),
								self.model.num_terminals
								)
			df_rule['terminal'] = np.tile(
										np.arange(self.model.num_terminals),
										self.model.num_non_terminals
										)
			hdf5_store.put(
				'nt2t',
				df_rule
			)

			# Top-level weights of states.
			df_top = pd.DataFrame(
							self.model.nt2t_rule_hdp.goal_weight_estimate[:,np.newaxis]
							,
							columns=['p']
							)
			df_top['terminal'] = df_top.index
			hdf5_store.put(
				'terminal',
				df_top
			)




class Variational_Inference_HDP_Gaussian_HMM_Bernoulli_Length(Variational_Inference_HDP_Gaussian_PCFG_Distinct_Terminals):
	def __init__(
			self,
			data,
			num_states,
			concentration_top,
			concentration_bottom,
			gradient_projection_tolerance,
			result_path,
			stop_base_counts,
			max_batch_array_size = 650,
			trial_id=None,
			entire_inventory_size=None # Without END observation
			):
		update_log_handler(result_path, 'Gauss-HMM')
		
		logger.info('HDP HMM mixture with Gaussian emission with Bernoulli(-like) length model. Script last updated at %s'
							% datetime.datetime.fromtimestamp(
									os.stat(sys.argv[0]).st_mtime
									).strftime('%Y-%m-%d-%H:%M:%S')
							)


		num_dimensions = data[0][0].size
		global_mean = np.mean(
						np.array(
							[
								data_point
								for string in data
								for data_point in string
							]
						), 
						axis=0
						)
		# Priors for states' emission.
		# Following Feldman et al. (2013).
		covmat_prior_scale_mat = np.identity(num_dimensions)
		covmat_prior_df = num_dimensions - 1 + 0.001
		logger.info('Prior on covariance matrix for each HMM state: IW(I, %s)' % str(covmat_prior_df))
		mean_prior_denominator = covmat_prior_df
		logger.info('Prior on covariance matrix for each HMM state: N(model_mean, state_cov_mat/%s)' % mean_prior_denominator)

		
		
		
		data_size = len(data)
		logger.info('# of strings: %i' % data_size)
		logger.info('Max batch size of data array to be processed in parallel in inner-outer algorithm: %i' % max_batch_array_size)
		
		
		


		if not trial_id is None:
			logger.info('Trial ID: %i' % trial_id)
		
		

		
		self.model = hdp_hmm.HDP_HMM_Gaussian(
										num_dimensions,
										num_states,
										concentration_top,
										concentration_bottom,
										gradient_projection_tolerance,
										global_mean,
										mean_prior_denominator,
										covmat_prior_scale_mat,
										covmat_prior_df,
										stop_base_counts,
										data,
										max_batch_array_size = max_batch_array_size
										)
		
		logger.info('(Max) # of states: %i.' % num_states)
		logger.info('Concentration of transition: %s' % str(concentration_bottom))
		logger.info('Concentration of top stick breaking for states: %s' % str(concentration_top))

		logger.info('Tolerance for gradient projection: %s' % str(gradient_projection_tolerance))

		self.result_path = result_path 
		logger.info('Initialization complete.')


	def save_results(self):
		with pd.HDFStore(
				os.path.join(
					self.result_path,
					'variational_parameters.h5'
					)
				) as hdf5_store:
			


			# transition
			df_rule = pd.DataFrame(
								self.model.trans_hdp.varpar_transition[:-1,:].flatten()[:,np.newaxis]
								,
								columns=['dirichlet_par']
							)
			df_rule['from_state'] = np.repeat(
								np.arange(self.model.num_states),
								self.model.num_states
								)
			df_rule['to_state'] = np.tile(
										np.arange(self.model.num_states),
										self.model.num_states
										)
			hdf5_store['transition'] = df_rule

			# Top-level weights of states.
			df_top = pd.DataFrame(
							self.model.trans_hdp.goal_weight_estimate[:,np.newaxis]
							,
							columns=['p']
							)
			df_top['state'] = df_top.index
			hdf5_store['state'] = df_top

			# initial transition
			df_init = pd.DataFrame(
							self.model.trans_hdp.varpar_transition[-1,:,np.newaxis]
							,
							columns=['dirichlet_par']
							)
			df_init['state'] = df_init.index
			hdf5_store['init'] = df_init

			# emission
			df_mean=pd.DataFrame(self.model.emission_distributions.varpar_mean_mean
										,
										columns=[('dim_%i' % dim)
													for dim in range(self.model.emission_distributions.num_dimensions)
													]
										)
			
			df_mean['state'] = df_mean.index
			hdf5_store.put(
						'emission/mean/mean'
						,
						df_mean
						)

			df_denom = pd.DataFrame(
							self.model.emission_distributions.varpar_mean_denominator[:,np.newaxis]
							,
							columns=['value']
							)
			df_denom['state'] = df_denom.index
			hdf5_store.put(
						'emission/mean/denominator'
						,
						df_denom
						)

			upper_triangle_ids1,upper_triangle_ids2 = np.triu_indices(self.model.emission_distributions.num_dimensions)
			df_scale_mat=pd.DataFrame(
							np.linalg.inv(self.model.emission_distributions.inv_varpar_covmat_scale_mat)[
								:,
								upper_triangle_ids1,
								upper_triangle_ids2
								].flatten()[:,np.newaxis]
							,
							columns=['matrix_entry']
							)
			df_scale_mat['state'] = np.repeat(
											np.arange(self.model.num_states)
											,
											upper_triangle_ids1.size
										)
			df_scale_mat['row_id'] = np.tile(upper_triangle_ids1, self.model.num_states)
			df_scale_mat['col_id'] = np.tile(upper_triangle_ids2, self.model.num_states)
			hdf5_store.put(
						'emission/covmat/scale_mat'
						,
						df_scale_mat
						)

			df_df = pd.DataFrame(
						self.model.emission_distributions.varpar_covmat_df[:,np.newaxis],
						columns=['df']
						)
			df_df['state'] = df_df.index
			hdf5_store.put(
						'emission/covmat/df'
						,
						df_df
						)

			# length
			df_length = pd.DataFrame(
							self.model.length_distributions.varpar_stop,
							columns=['stop','through']
							)
			df_length['state'] = df_length.index
			hdf5_store['length'] = df_length





class Variational_Inference_finite_Gaussian_HMM_Bernoulli_Length(Variational_Inference_HDP_Gaussian_HMM_Bernoulli_Length):
	def __init__(
			self,
			data,
			num_states,
			concentration_trans,
			result_path,
			stop_base_counts,
			max_batch_array_size = 650,
			trial_id=None,
			entire_inventory_size=None # Without END observation
			):
		update_log_handler(result_path, 'Gauss-fin-HMM')
		
		logger.info('Finite HMM mixture (with Dirichlet prior) with Gaussian emission with Bernoulli(-like) length model. Script last updated at %s'
							% datetime.datetime.fromtimestamp(
									os.stat(sys.argv[0]).st_mtime
									).strftime('%Y-%m-%d-%H:%M:%S')
							)


		num_dimensions = data[0][0].size
		global_mean = np.mean(
						np.array(
							[
								data_point
								for string in data
								for data_point in string
							]
						), 
						axis=0
						)
		# Priors for states' emission.
		# Following Feldman et al. (2013).
		covmat_prior_scale_mat = np.identity(num_dimensions)
		covmat_prior_df = num_dimensions - 1 + 0.001
		logger.info('Prior on covariance matrix for each HMM state: IW(I, %s)' % str(covmat_prior_df))
		mean_prior_denominator = covmat_prior_df
		logger.info('Prior on covariance matrix for each HMM state: N(model_mean, state_cov_mat/%s)' % mean_prior_denominator)

		
		
		
		data_size = len(data)
		logger.info('# of strings: %i' % data_size)
		logger.info('Max batch size of data array to be processed in parallel in inner-outer algorithm: %i' % max_batch_array_size)
		
		
		


		if not trial_id is None:
			logger.info('Trial ID: %i' % trial_id)
		
		

		
		self.model = finite_hmm.Finite_HMM_Gaussian(
										num_dimensions,
										num_states,
										concentration_trans,
										global_mean,
										mean_prior_denominator,
										covmat_prior_scale_mat,
										covmat_prior_df,
										stop_base_counts,
										data,
										max_batch_array_size = max_batch_array_size
										)
		
		logger.info('(Max) # of states: %i.' % num_states)
		logger.info('Concentration of transition: %s' % str(concentration_trans))

		self.result_path = result_path 
		logger.info('Initialization complete.')


	def save_results(self):
		with pd.HDFStore(
				os.path.join(
					self.result_path,
					'variational_parameters.h5'
					)
				) as hdf5_store:
			


			# transition
			df_rule = pd.DataFrame(
								self.model.trans_dirichlet.varpar_rule_weight[:-1,:].flatten()[:,np.newaxis]
								,
								columns=['dirichlet_par']
							)
			df_rule['from_state'] = np.repeat(
								np.arange(self.model.num_states),
								self.model.num_states
								)
			df_rule['to_state'] = np.tile(
										np.arange(self.model.num_states),
										self.model.num_states
										)
			hdf5_store['transition'] = df_rule

			

			# initial transition
			df_init = pd.DataFrame(
							self.model.trans_dirichlet.varpar_rule_weight[-1,:,np.newaxis]
							,
							columns=['dirichlet_par']
							)
			df_init['state'] = df_init.index
			hdf5_store['init'] = df_init

			# emission
			df_mean=pd.DataFrame(self.model.emission_distributions.varpar_mean_mean
										,
										columns=[('dim_%i' % dim)
													for dim in range(self.model.emission_distributions.num_dimensions)
													]
										)
			
			df_mean['state'] = df_mean.index
			hdf5_store.put(
						'emission/mean/mean'
						,
						df_mean
						)

			df_denom = pd.DataFrame(
							self.model.emission_distributions.varpar_mean_denominator[:,np.newaxis]
							,
							columns=['value']
							)
			df_denom['state'] = df_denom.index
			hdf5_store.put(
						'emission/mean/denominator'
						,
						df_denom
						)

			upper_triangle_ids1,upper_triangle_ids2 = np.triu_indices(self.model.emission_distributions.num_dimensions)
			df_scale_mat=pd.DataFrame(
							np.linalg.inv(self.model.emission_distributions.inv_varpar_covmat_scale_mat)[
								:,
								upper_triangle_ids1,
								upper_triangle_ids2
								].flatten()[:,np.newaxis]
							,
							columns=['matrix_entry']
							)
			df_scale_mat['state'] = np.repeat(
											np.arange(self.model.num_states)
											,
											upper_triangle_ids1.size
										)
			df_scale_mat['row_id'] = np.tile(upper_triangle_ids1, self.model.num_states)
			df_scale_mat['col_id'] = np.tile(upper_triangle_ids2, self.model.num_states)
			hdf5_store.put(
						'emission/covmat/scale_mat'
						,
						df_scale_mat
						)

			df_df = pd.DataFrame(
						self.model.emission_distributions.varpar_covmat_df[:,np.newaxis],
						columns=['df']
						)
			df_df['state'] = df_df.index
			hdf5_store.put(
						'emission/covmat/df'
						,
						df_df
						)

			# length
			df_length = pd.DataFrame(
							self.model.length_distributions.varpar_stop,
							columns=['stop','through']
							)
			df_length['state'] = df_length.index
			hdf5_store['length'] = df_length



# =====================================================================
# The variational inference classes below are unsupported
# =====================================================================

class Variational_Inference_HDP_Gaussian_HMM_Mix_Poisson_Length(Variational_Inference_HDP_PCFG):
	def __init__(
			self,
			data,
			num_hmms,
			num_states,
			concentration_top,
			concentration_bottom,
			dp_concent_priors,
			gradient_projection_tolerance,
			result_path,
			trial_id=None,
			entire_inventory_size=None # Without END observation
			):
		update_log_handler(result_path, 'gaussHMMmix')
		
		logger.info('HDP HMM mixture with Gaussian emission with Poisson length model. Script last updated at %s'
							% datetime.datetime.fromtimestamp(
									os.stat(sys.argv[0]).st_mtime
									).strftime('%Y-%m-%d-%H:%M:%S')
							)
		if not trial_id is None:
			logger.info('Trial ID: %i' % trial_id)
		
		
		data_size = len(data)
		logger.info('# of strings: %i' % data_size)

		num_dimensions = data[0][0].size
		global_mean = np.mean(
						np.array(
							[
								data_point
								for string in data
								for data_point in string
							]
						), 
						axis=0
						)
		logger.info('Global mean is chosen from the sample mean of all the data points.')

		global_cov_mat = np.identity(num_dimensions)
		logger.info('Each model mean is sampled from Gaussian(global_mean, I).')

		# Priors for states' emission.
		# Following Feldman et al. (2013).
		covmat_prior_scale_mat = np.identity(num_dimensions)
		covmat_prior_df = num_dimensions - 1 + 0.001
		logger.info('Prior on covariance matrix for each HMM state: IW(I, %s)' % str(covmat_prior_df))
		mean_prior_denominator = covmat_prior_df
		logger.info('Prior on mean for each HMM state: N(model_mean, state_cov_mat/%s)' % mean_prior_denominator)
		
		
		self.model = hdp_gauss_hmm_mix_poisson.Gaussian_HDPHMM_mix(
										num_states,
										num_hmms,
										concentration_top,
										concentration_bottom,
										gradient_projection_tolerance,
										global_mean,
										global_cov_mat,
										mean_prior_denominator,
										covmat_prior_scale_mat,
										covmat_prior_df,
										dp_concent_priors,
										data
										)
		
		
		logger.info('(Max) # of states: %i.' % num_states)
		logger.info('Concentration of transition: %s' % str(concentration_bottom))
		logger.info('Concentration of top stick breaking for states: %s' % str(concentration_top))
		
		



		self.result_path = result_path 
		logger.info('Initialization complete.')

		

		
	def save_results(self):

		with pd.HDFStore(os.path.join(self.result_path,'variational_parameters.h5')) as hdf5_store:
			df_assignment = pd.DataFrame()
			df_assignment['p'] = self.model.varpar_assignment.flatten()
			df_assignment['hmm_id'] = np.tile(
											np.arange(self.model.varpar_assignment.shape[1]),
											self.model.varpar_assignment.shape[0]
											)
			df_assignment['string_id'] = np.repeat(
											np.arange(self.model.varpar_assignment.shape[0]),
											self.model.varpar_assignment.shape[1]
											)
			hdf5_store.put(
							"string_classification/assignment"
							,
							df_assignment
							)
			df_assignment.to_csv(os.path.join(self.result_path,"string_classification.csv"), index=False)
			
			df_stick=pd.DataFrame(
								self.model.varpar_stick, columns=('beta_par1','beta_par2')
								)
			df_stick['cluster_id']=df_stick.index
			hdf5_store.put(
					"string_classification/stick"
					,
					df_stick
					)

			df_concent = pd.DataFrame(
									[
										[
											self.model.varpar_concent.shape,
											self.model.varpar_concent.rate
											]
										]
										,
										columns=['shape','rate']
										)
			hdf5_store.put(
					"string_classification/concentration"
					,
					df_concent
					)

			# Transition
			for hmm_id,trans_hdp in enumerate(self.model.trans_hdps):
				df_state_weight = pd.DataFrame(
										trans_hdp.state_weight_estimate[:,np.newaxis]
										,
										columns=['p']
										)
				df_state_weight['state_id'] = df_state_weight.index
				hdf5_store.put(
					"hmm_%i/state_weights" % hmm_id
					,
					df_state_weight
					)

				df_trans = pd.DataFrame(
								trans_hdp.varpar_transition.flatten()[:,np.newaxis]
								,
								columns=['dirichlet_par']
								)
				df_trans['from_state'] = np.repeat(
											map(str,range(trans_hdp.varpar_transition.shape[0]-1))+['init']
											,
											trans_hdp.varpar_transition.shape[1]
											)
				df_trans['to_state'] = np.tile(
												np.arange(trans_hdp.varpar_transition.shape[1])
												,
												trans_hdp.varpar_transition.shape[0]
											)
				hdf5_store.put(
					"hmm_%i/transition" % hmm_id
					,
					df_trans
					)

			# Emission base

			df_base_mean = pd.DataFrame(
								self.model.emit_base.varpar_mean_mean
								,
								columns=[
									('dim_%i' % dim)
									for dim
									in xrange(self.model.emit_base.varpar_mean_mean.shape[1])
									]
								)
			df_base_mean['hmm_id'] = df_base_mean.index
			hdf5_store.put(
					"emission_base/mean"
					,
					df_base_mean
					)

			upper_triangle_ids1,upper_triangle_ids2 = np.triu_indices(self.model.gauss_emit.num_dimensions)
			df_base_covmat = pd.DataFrame(
									self.model.emit_base.varpar_mean_covmat[
										:,
										upper_triangle_ids1,
										upper_triangle_ids2
										].flatten()[:,np.newaxis]
									,
									columns=['matrix_entry']
									)

			df_base_covmat['hmm_id'] = np.repeat(
										np.arange(self.model.num_hmms)
										,
										upper_triangle_ids1.size
										)
			df_base_covmat['row_id'] = np.tile(upper_triangle_ids1, self.model.num_hmms)
			df_base_covmat['col_id'] = np.tile(upper_triangle_ids2, self.model.num_hmms)
			hdf5_store.put(
					"emission_base/covmat"
					,
					df_base_covmat
					)



			# Emission
			df_mean=pd.DataFrame(self.model.gauss_emit.varpar_mean_mean.reshape(
													self.model.num_hmms*self.model.num_states,
													self.model.gauss_emit.num_dimensions
												)
										,
										columns=[('dim_%i' % dim)
													for dim in range(self.model.gauss_emit.num_dimensions)
													]
										)
			
			df_mean['hmm_id'] = np.repeat(np.arange(self.model.num_hmms), self.model.num_states)
			df_mean['state_id'] = np.tile(np.arange(self.model.num_states), self.model.num_hmms)
			hdf5_store.put(
						'emission/mean/mean'
						,
						df_mean
						)

			df_denom = pd.DataFrame(
							self.model.gauss_emit.varpar_mean_denominator.flatten()[:,np.newaxis]
							,
							columns=['value']
							)
			df_denom['hmm_id'] = np.repeat(np.arange(self.model.num_hmms), self.model.num_states)
			df_denom['state_id'] = np.tile(np.arange(self.model.num_states), self.model.num_hmms)
			hdf5_store.put(
						'emission/mean/denominator'
						,
						df_denom
						)


			df_scale_mat=pd.DataFrame(
							np.linalg.inv(self.model.gauss_emit.inv_varpar_covmat_scale_mat)[
								:,
								:,
								upper_triangle_ids1,
								upper_triangle_ids2
								].flatten()[:,np.newaxis]
							,
							columns=['matrix_entry']
							)
			df_scale_mat['hmm_id'] = np.repeat(
										np.arange(self.model.num_hmms)
										,
										self.model.num_states*upper_triangle_ids1.size
										)
			df_scale_mat['state_id'] = np.repeat(
											np.tile(
												np.arange(self.model.num_states)
												,
												self.model.num_hmms
											)
											,
											upper_triangle_ids1.size
										)
			df_scale_mat['row_id'] = np.tile(upper_triangle_ids1, self.model.num_hmms*self.model.num_states)
			df_scale_mat['col_id'] = np.tile(upper_triangle_ids2, self.model.num_hmms*self.model.num_states)
			hdf5_store.put(
						'emission/covmat/scale_mat'
						,
						df_scale_mat
						)

			df_df = pd.DataFrame(
						self.model.gauss_emit.varpar_covmat_df.flatten()[:,np.newaxis],
						columns=['df']
						)
			df_df['hmm_id'] = np.repeat(np.arange(self.model.num_hmms), self.model.num_states)
			df_df['state_id'] = np.tile(np.arange(self.model.num_states), self.model.num_hmms)
			hdf5_store.put(
						'emission/covmat/df'
						,
						df_df
						)

			df_length = pd.DataFrame(
							self.model.poisson_length.varpar_length
							,
							columns=['shape','rate']
							)
			df_length['hmm_id'] = df_length.index
			hdf5_store.put(
						'length'
						,
						df_length
						)
				

class Variational_Inference_HDP_Gaussian_HMM_Mix_Bernoulli_Length(Variational_Inference_HDP_Gaussian_HMM_Mix_Poisson_Length):
	def __init__(
			self,
			data,
			num_hmms,
			num_states,
			concentration_top,
			concentration_bottom,
			dp_concent_priors,
			bernoulli_beta_priors,
			gradient_projection_tolerance,
			result_path,
			trial_id=None,
			entire_inventory_size=None # Without END observation
			):
		update_log_handler(result_path, 'gaussHMMmix')
		
		logger.info('HDP HMM mixture with Gaussian emission with Bernoulli length model. Script last updated at %s'
							% datetime.datetime.fromtimestamp(
									os.stat(sys.argv[0]).st_mtime
									).strftime('%Y-%m-%d-%H:%M:%S')
							)
		if not trial_id is None:
			logger.info('Trial ID: %i' % trial_id)
		
		
		data_size = len(data)
		logger.info('# of strings: %i' % data_size)

		num_dimensions = data[0][0].size
		global_mean = np.mean(
						np.array(
							[
								data_point
								for string in data
								for data_point in string
							]
						), 
						axis=0
						)
		logger.info('Global mean is chosen from the sample mean of all the data points.')

		global_cov_mat = np.identity(num_dimensions)
		logger.info('Each model mean is sampled from Gaussian(global_mean, I).')

		# Priors for states' emission.
		# Following Feldman et al. (2013).
		covmat_prior_scale_mat = np.identity(num_dimensions)
		covmat_prior_df = num_dimensions - 1 + 0.001
		logger.info('Prior on covariance matrix for each HMM state: IW(I, %s)' % str(covmat_prior_df))
		mean_prior_denominator = covmat_prior_df
		logger.info('Prior on mean for each HMM state: N(model_mean, state_cov_mat/%s)' % mean_prior_denominator)

		logger.info('Beta priors on Bernoulli length prob.: %s' % str(bernoulli_beta_priors))
		
		
		self.model = hdp_gauss_hmm_mix_bernoulli.Gaussian_HDPHMM_mix(
										num_states,
										num_hmms,
										concentration_top,
										concentration_bottom,
										gradient_projection_tolerance,
										global_mean,
										global_cov_mat,
										mean_prior_denominator,
										covmat_prior_scale_mat,
										covmat_prior_df,
										dp_concent_priors,
										bernoulli_beta_priors,
										data
										)
		
		
		logger.info('(Max) # of states: %i.' % num_states)
		logger.info('Concentration of transition: %s' % str(concentration_bottom))
		logger.info('Concentration of top stick breaking for states: %s' % str(concentration_top))
		
		



		self.result_path = result_path 
		logger.info('Initialization complete.')

		

		
	def save_results(self):

		with pd.HDFStore(os.path.join(self.result_path,'variational_parameters.h5')) as hdf5_store:
			df_assignment = pd.DataFrame()
			df_assignment['p'] = self.model.varpar_assignment.flatten()
			df_assignment['hmm_id'] = np.tile(
											np.arange(self.model.varpar_assignment.shape[1]),
											self.model.varpar_assignment.shape[0]
											)
			df_assignment['string_id'] = np.repeat(
											np.arange(self.model.varpar_assignment.shape[0]),
											self.model.varpar_assignment.shape[1]
											)
			hdf5_store.put(
							"string_classification/assignment"
							,
							df_assignment
							)
			df_assignment.to_csv(os.path.join(self.result_path,"string_classification.csv"), index=False)
			
			df_stick=pd.DataFrame(
								self.model.varpar_stick, columns=('beta_par1','beta_par2')
								)
			df_stick['cluster_id']=df_stick.index
			hdf5_store.put(
					"string_classification/stick"
					,
					df_stick
					)

			df_concent = pd.DataFrame(
									[
										[
											self.model.varpar_concent.shape,
											self.model.varpar_concent.rate
											]
										]
										,
										columns=['shape','rate']
										)
			hdf5_store.put(
					"string_classification/concentration"
					,
					df_concent
					)

			# Transition
			for hmm_id,trans_hdp in enumerate(self.model.trans_hdps):
				df_state_weight = pd.DataFrame(
										trans_hdp.state_weight_estimate[:,np.newaxis]
										,
										columns=['p']
										)
				df_state_weight['state_id'] = df_state_weight.index
				hdf5_store.put(
					"hmm_%i/state_weights" % hmm_id
					,
					df_state_weight
					)

				df_trans = pd.DataFrame(
								trans_hdp.varpar_transition.flatten()[:,np.newaxis]
								,
								columns=['dirichlet_par']
								)
				df_trans['from_state'] = np.repeat(
											map(str,range(trans_hdp.varpar_transition.shape[0]-1))+['init']
											,
											trans_hdp.varpar_transition.shape[1]
											)
				df_trans['to_state'] = np.tile(
												np.arange(trans_hdp.varpar_transition.shape[1])
												,
												trans_hdp.varpar_transition.shape[0]
											)
				hdf5_store.put(
					"hmm_%i/transition" % hmm_id
					,
					df_trans
					)

			# Emission base

			df_base_mean = pd.DataFrame(
								self.model.emit_base.varpar_mean_mean
								,
								columns=[
									('dim_%i' % dim)
									for dim
									in xrange(self.model.emit_base.varpar_mean_mean.shape[1])
									]
								)
			df_base_mean['hmm_id'] = df_base_mean.index
			hdf5_store.put(
					"emission_base/mean"
					,
					df_base_mean
					)

			upper_triangle_ids1,upper_triangle_ids2 = np.triu_indices(self.model.gauss_emit.num_dimensions)
			df_base_covmat = pd.DataFrame(
									self.model.emit_base.varpar_mean_covmat[
										:,
										upper_triangle_ids1,
										upper_triangle_ids2
										].flatten()[:,np.newaxis]
									,
									columns=['matrix_entry']
									)

			df_base_covmat['hmm_id'] = np.repeat(
										np.arange(self.model.num_hmms)
										,
										upper_triangle_ids1.size
										)
			df_base_covmat['row_id'] = np.tile(upper_triangle_ids1, self.model.num_hmms)
			df_base_covmat['col_id'] = np.tile(upper_triangle_ids2, self.model.num_hmms)
			hdf5_store.put(
					"emission_base/covmat"
					,
					df_base_covmat
					)



			# Emission
			df_mean=pd.DataFrame(self.model.gauss_emit.varpar_mean_mean.reshape(
													self.model.num_hmms*self.model.num_states,
													self.model.gauss_emit.num_dimensions
												)
										,
										columns=[('dim_%i' % dim)
													for dim in range(self.model.gauss_emit.num_dimensions)
													]
										)
			
			df_mean['hmm_id'] = np.repeat(np.arange(self.model.num_hmms), self.model.num_states)
			df_mean['state_id'] = np.tile(np.arange(self.model.num_states), self.model.num_hmms)
			hdf5_store.put(
						'emission/mean/mean'
						,
						df_mean
						)

			df_denom = pd.DataFrame(
							self.model.gauss_emit.varpar_mean_denominator.flatten()[:,np.newaxis]
							,
							columns=['value']
							)
			df_denom['hmm_id'] = np.repeat(np.arange(self.model.num_hmms), self.model.num_states)
			df_denom['state_id'] = np.tile(np.arange(self.model.num_states), self.model.num_hmms)
			hdf5_store.put(
						'emission/mean/denominator'
						,
						df_denom
						)


			df_scale_mat=pd.DataFrame(
							np.linalg.inv(self.model.gauss_emit.inv_varpar_covmat_scale_mat)[
								:,
								:,
								upper_triangle_ids1,
								upper_triangle_ids2
								].flatten()[:,np.newaxis]
							,
							columns=['matrix_entry']
							)
			df_scale_mat['hmm_id'] = np.repeat(
										np.arange(self.model.num_hmms)
										,
										self.model.num_states*upper_triangle_ids1.size
										)
			df_scale_mat['state_id'] = np.repeat(
											np.tile(
												np.arange(self.model.num_states)
												,
												self.model.num_hmms
											)
											,
											upper_triangle_ids1.size
										)
			df_scale_mat['row_id'] = np.tile(upper_triangle_ids1, self.model.num_hmms*self.model.num_states)
			df_scale_mat['col_id'] = np.tile(upper_triangle_ids2, self.model.num_hmms*self.model.num_states)
			hdf5_store.put(
						'emission/covmat/scale_mat'
						,
						df_scale_mat
						)

			df_df = pd.DataFrame(
						self.model.gauss_emit.varpar_covmat_df.flatten()[:,np.newaxis],
						columns=['df']
						)
			df_df['hmm_id'] = np.repeat(np.arange(self.model.num_hmms), self.model.num_states)
			df_df['state_id'] = np.tile(np.arange(self.model.num_states), self.model.num_hmms)
			hdf5_store.put(
						'emission/covmat/df'
						,
						df_df
						)

			df_length = pd.DataFrame(
							# self.model.bernoulli_length.varpar_beta
							# ,
							# columns=['stop_weight','through_weight']
							)
			df_length['stop_weight'] = self.model.bernoulli_length.varpar_beta[...,0].flatten()
			df_length['through_weight'] = self.model.bernoulli_length.varpar_beta[...,1].flatten()
			df_length['hmm_id'] = np.repeat(
									np.arange(self.model.bernoulli_length.varpar_beta.shape[0])
									,
									self.model.bernoulli_length.varpar_beta.shape[1]
									)
			df_length['state_id'] = np.tile(
									np.arange(self.model.bernoulli_length.varpar_beta.shape[1])
									,
									self.model.bernoulli_length.varpar_beta.shape[0]
									)
			hdf5_store.put(
						'length'
						,
						df_length
						)






class Variational_Inference_DP_Gauss_Mix(Variational_Inference_HDP_Gaussian_HMM_Mix_Bernoulli_Length):
	def __init__(
			self,
			data, # data matrix. Each row represents a data point.
			num_clusters,
			concent_priors,
			result_path,
			trial_id=None,
			):
		update_log_handler(result_path, 'DPGaussMix')
		
		num_dimensions = data.shape[1]
		logger.info('DP %i-dimensional Gaussian mixture model.' % num_dimensions)
		logger.info('Script last updated at %s'
							% datetime.datetime.fromtimestamp(
									os.stat(sys.argv[0]).st_mtime
									).strftime('%Y-%m-%d-%H:%M:%S')
							)
		if not trial_id is None:
			logger.info('Trial ID: %i' % trial_id)
		
		data_size=data.shape[0]

		logger.info('Data size: %i' % data_size)
		
		covmat_prior_scale_mat=np.identity(num_dimensions)
		logger.info('Inverse Wishart prior on covariance matrix (Sigma) has an identity scale matrix parameter.')

		# covmat_prior_df=num_dimensions*(1+np.finfo(np.float64).eps)-1
		covmat_prior_df = np.float64(num_dimensions-1)+0.001 # Following Feldman et al (2013)
		# print 'multigamma test', sps.multigammaln(covmat_prior_df*0.5, num_dimensions)
		logger.info('Inverse Wishart prior on covariance matrix (Sigma) has df: %f' % covmat_prior_df)

		mean_prior_denominator=covmat_prior_df
		logger.info('Gaussian prior on mean (mu) has covariance matrix: Sigma/%f'
					% mean_prior_denominator
					)

		mean_prior_mean=np.mean(data, axis=0)
		logger.info('Gaussian prior on mean (mu) has mean: %s'
					% str(mean_prior_mean)
					)
		
		self.model = dp_gaussian_mixture.DP_Gauss_mix(
							data,
							num_clusters,
							concent_priors,
							mean_prior_mean,
							mean_prior_denominator,
							covmat_prior_scale_mat,
							covmat_prior_df
							)

		logger.info('Max # of clusters: %i' % num_clusters)
		logger.info('Gamma priors on concentration: (%f,%f)'
							% (concent_priors[0],concent_priors[1]**-1)
							)
		
		




		self.result_path = result_path 
		logger.info('Initialization complete.')


	def save_results(self):
		with pd.HDFStore(os.path.join(self.result_path,'variational_parameters.h5')) as hdf5_store:
			df_assignment = pd.DataFrame(
								self.model.varpar_assignment.flatten()[:,np.newaxis]
								,
								columns=["p"]
# 									columns=[
# 											('cluster_id_%i' % table_id)
# 											for table_id in range(bottom_rst.T)
# 											]
								)
			df_assignment['customer_id']=np.repeat(
											np.arange(self.model.varpar_assignment.shape[0])
											,
											self.model.varpar_assignment.shape[1]
										)
			df_assignment['cluster_id']=np.tile(
											np.arange(self.model.varpar_assignment.shape[1])
											,
											self.model.varpar_assignment.shape[0]
										)
			hdf5_store.put(
						'assignment'
						,
						df_assignment
						)
			
			df_stick=pd.DataFrame(self.model.varpar_stick, columns=('beta_par1','beta_par2'))
			df_stick['cluster_id']=df_stick.index
			hdf5_store.put(
						'stick'
						,
						df_stick
						)
			
			
			df_mean=pd.DataFrame(self.model.varpar_mean_mean
										,
										columns=[('mean_dim_%i' % cluster)
													for cluster in range(self.model.ndim)
													]
										)
			df_mean['denominator_of_covmat']=self.model.varpar_mean_denominator
			df_mean['cluster_id']=df_mean.index
			hdf5_store.put(
						'mean'
						,
						df_mean
						)

			upper_triangle_ids1,upper_triangle_ids2 = np.triu_indices(self.model.inv_varpar_covmat_scale_mat.shape[1])
			df_scale_mat=pd.DataFrame(
							np.linalg.inv(self.model.inv_varpar_covmat_scale_mat)[
								:,
								upper_triangle_ids1,
								upper_triangle_ids2
								].flatten()[:,np.newaxis]
							,
							columns=['cov_mat_scale_mat_entry']
							)
			df_scale_mat['cluster_id'] = np.repeat(
										np.arange(self.model.num_clusters)
										,
										upper_triangle_ids1.size
										)
			df_scale_mat['row_id'] = np.tile(upper_triangle_ids1, self.model.num_clusters)
			df_scale_mat['col_id'] = np.tile(upper_triangle_ids2, self.model.num_clusters)
			hdf5_store.put(
						'cov_mat/scale_mat'
						,
						df_scale_mat
						)

			df_df = pd.DataFrame(self.model.varpar_covmat_df[:,np.newaxis], columns=['cov_mat_df'])
			df_df['cluster_id'] = df_df.index
			hdf5_store.put(
						'cov_mat/df'
						,
						df_df
						)


			
			df_concent = pd.DataFrame(
										[[
											self.model.varpar_concent_shape
											,
											self.model.varpar_concent_inv_scale**-1
										]]
										,
										columns=['shape','scale']
										)
			hdf5_store.put(
							'concentration'
							,
							df_concent
							)



class Variational_Inference_HDP_Gaussian_HMM_Poisson_Length(Variational_Inference_HDP_Gaussian_HMM_Mix_Poisson_Length):
	def __init__(
			self,
			data,
			# num_hmms,
			num_states,
			concentration_top,
			concentration_bottom,
			dp_concent_priors,
			gradient_projection_tolerance,
			result_path,
			trial_id=None,
			entire_inventory_size=None # Without END observation
			):
		update_log_handler(result_path, 'gaussHMMmix')
		
		logger.info('HDP HMM mixture with Gaussian emission with Poisson length model. Script last updated at %s'
							% datetime.datetime.fromtimestamp(
									os.stat(sys.argv[0]).st_mtime
									).strftime('%Y-%m-%d-%H:%M:%S')
							)
		if not trial_id is None:
			logger.info('Trial ID: %i' % trial_id)
		
		
		data_size = len(data)
		logger.info('# of strings: %i' % data_size)

		num_dimensions = data[0][0].size
		global_mean = np.mean(
						np.array(
							[
								data_point
								for string in data
								for data_point in string
							]
						), 
						axis=0
						)
		logger.info('Global mean is chosen from the sample mean of all the data points.')

		global_cov_mat = np.identity(num_dimensions)
		logger.info('Each model mean is sampled from Gaussian(global_mean, I).')

		# Priors for states' emission.
		# Following Feldman et al. (2013).
		covmat_prior_scale_mat = np.identity(num_dimensions)
		covmat_prior_df = num_dimensions - 1 + 0.001
		logger.info('Prior on covariance matrix for each HMM state: IW(I, %s)' % str(covmat_prior_df))
		mean_prior_denominator = covmat_prior_df
		logger.info('Prior on covariance matrix for each HMM state: N(model_mean, state_cov_mat/%s)' % mean_prior_denominator)
		
		
		self.model = hdp_gauss_hmm_poisson.Gaussian_HDPHMM(
										num_states,
										# num_hmms,
										concentration_top,
										concentration_bottom,
										gradient_projection_tolerance,
										global_mean,
										global_cov_mat,
										mean_prior_denominator,
										covmat_prior_scale_mat,
										covmat_prior_df,
										data
										)
		
		
		logger.info('(Max) # of states: %i.' % num_states)
		logger.info('Concentration of transition: %s' % str(concentration_bottom))
		logger.info('Concentration of top stick breaking for states: %s' % str(concentration_top))
		
		



		self.result_path = result_path 
		logger.info('Initialization complete.')

	def save_results(self):
		pass

