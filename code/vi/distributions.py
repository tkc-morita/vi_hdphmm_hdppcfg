# coding: utf-8

import numpy as np
import scipy.special as sps
import scipy.misc as spm
import scipy.stats
import algorithm.gradient_projection_method as gpm



class Dirichlet(object):
	"""
	params:
		'base_counts': positive float.
		'num_conditions': natural number.
		'num_categories': natural number.
	"""
	def __init__(self, params):
		self.base_counts = np.array(params['base_counts'])
		self.varpar_rule_weight = np.random.gamma(1, 20, size=(params['num_conditions'], params['num_categories']))
		self.sum_rule_weight = np.sum(self.varpar_rule_weight, axis=-1)
		self.E_log_rule_weight = sps.digamma(self.varpar_rule_weight) - sps.digamma(self.sum_rule_weight)[:,np.newaxis]


	def _update_varpar_rule_weight(self):
		self.varpar_rule_weight = self.expected_rule_counts + self.base_counts[np.newaxis,:]
		self.sum_rule_weight = np.sum(self.varpar_rule_weight, axis=-1)
		self.E_log_rule_weight = sps.digamma(self.varpar_rule_weight) - sps.digamma(self.sum_rule_weight)[:,np.newaxis]

	def update_expected_counts(self, expected_rule_counts):
		self.expected_rule_counts = expected_rule_counts

	def update_varpars(self):
		self._update_varpar_rule_weight()

	def get_log_weights(self):
		return self.E_log_rule_weight

	def get_var_bound(self):
		return self.get_E_log_p() - self.get_E_log_q()

	def get_E_log_p(self):
		return np.sum((self.base_counts-1)*np.sum(self.E_log_rule_weight, axis=0))

	def get_E_log_q(self):
		return (
					np.sum(
						self.E_log_rule_weight*(self.varpar_rule_weight-1)
					)
					-
					np.sum(
						sps.gammaln(self.varpar_rule_weight)
					)
					+
					np.sum(
						sps.gammaln(self.sum_rule_weight)
					)
					)

	def set_log_posterior_expectation(self):
		self.log_posterior_expectation = np.log(self.varpar_rule_weight)-np.log(self.sum_rule_weight)[:,np.newaxis]

	def get_log_posterior_pred(self, string):
		log_prob = self.log_posterior_expectation[:,
						string.reshape(string.shape[0],string.shape[1]) # num_strings x string_len (x 1)
						] # num_conditions x num_strings x string_len
		return np.transpose(log_prob, (1,2,0))


	def reset_expected_counts(self):
		self.expected_rule_counts = np.zeros(self.varpar_rule_weight.shape)

	def increment_expected_counts(self, string_batch, expected_emission_counts_per_loc):
		# expected_emission_counts_per_loc: # num_strings x string_len x num_conditions
		self.expected_rule_counts += np.sum(
										np.eye(self.expected_rule_counts.shape[-1])[
											string_batch.reshape(string_batch.shape[0],string_batch.shape[1])
											][:,:,np.newaxis,:]
										*
										expected_emission_counts_per_loc[:,:,:,np.newaxis]
										,
										axis=(0,1)
										)

	def set_prob_function(self):
		pass

	def get_log_prob_for_strings(self, string_batch):
		log_prob = self.E_log_rule_weight[:,
						string_batch.reshape(string_batch.shape[0],string_batch.shape[1]) # num_strings x string_len (x 1)
						] # num_conditions x num_strings x string_len
		return np.transpose(log_prob, (1,2,0))




class Gaussian(object):
	def __init__(self, params):
		"""
		params:
			'dimension': natural number.
			'num_components': natural number.
			'mean_prior_mean': 1dim ndarray.
			'mean_prior_denominator': positive float,
			'covmat_prior_scale_mat': 2dim ndarray,
			'covmat_prior_df': positive float s.t. covmat_prior_df > dimension - 1,
		"""
		self.dimension = params['dimension']
		self.arange_num_dimensions_over_2 = np.arange(self.dimension)*0.5
		self.num_dimensions_x_log_pi = self.dimension * np.log(np.pi)

		self.mean_prior_mean = params['mean_prior_mean']
		self.mean_prior_denominator = params['mean_prior_denominator']
		self.covmat_prior_scale_mat = params['covmat_prior_scale_mat']
		self.covmat_prior_df = params['covmat_prior_df']
		num_components = params['num_components']


		if self.covmat_prior_df<self.dimension:
			initial_df = self.dimension
		else:
			initial_df = self.covmat_prior_df
		varpar_covmat_scale_mat = scipy.stats.invwishart.rvs(
										initial_df,
										self.covmat_prior_scale_mat,
										size=num_components
										)
		self.varpar_covmat_df = np.random.gamma(
											1, 20,
											size=num_components
											) + self.dimension - 1

		self.varpar_mean_denominator = np.random.gamma(
												1, 20,
												size=num_components
												)
		varpar_mean_mean=[]
		for cov_mat in varpar_covmat_scale_mat:
			varpar_mean_mean.append(
				np.random.multivariate_normal(
					self.mean_prior_mean,
					cov_mat/self.mean_prior_denominator
					)
				)
		self.varpar_mean_mean=np.array(varpar_mean_mean)

		self.num_dimensions_over_vpdenom = self.dimension / self.varpar_mean_denominator

		dif_vpmeanmean_vs_mean_prior = (
										self.varpar_mean_mean
										-
										self.mean_prior_mean[np.newaxis,:]
										) # num_nt x num_dims
		self.denom_prior_x_mat_dif_vpmeanmean_vs_mean_prior = (
													dif_vpmeanmean_vs_mean_prior[:,:,np.newaxis]
													*
													self.mean_prior_denominator
													*
													dif_vpmeanmean_vs_mean_prior[:,np.newaxis,:]
													)


		self.half_varpar_covmat_df = self.varpar_covmat_df * 0.5
		self.multidigamma_nover2 = self._multidigamma(self.half_varpar_covmat_df) # multivariate_digamma(n/2)
		self.inv_varpar_covmat_scale_mat = np.linalg.inv(varpar_covmat_scale_mat)
		logdet_inv_vp_covmat_sm = np.linalg.slogdet(self.inv_varpar_covmat_scale_mat)[1] # log |varpar_covmat_scale_ma|
		self.E_log_det_inv_vp_cm_sm = self.multidigamma_nover2+logdet_inv_vp_covmat_sm # ndim*log(2) omitted, because it cancels out with ndim*log(2*pi) - ndim*log(pi).
		self.vpdf_x_invvpsm = self.varpar_covmat_df[:,np.newaxis,np.newaxis]*self.inv_varpar_covmat_scale_mat

		self.expected_emission_counts = np.zeros(num_components)
		self.expected_sum_obs = np.zeros((num_components, self.dimension))
		self.expected_sum_covmat = np.zeros((num_components, self.dimension, self.dimension))




	def _update_varpar_mean(self):
		self.varpar_mean_denominator = self.mean_prior_denominator+self.expected_emission_counts
		self.num_dimensions_over_vpdenom = self.dimension / self.varpar_mean_denominator

		self.varpar_mean_mean = (
									(
										self.mean_prior_mean
										*
										self.mean_prior_denominator
									)[np.newaxis,:]
									+
									self.expected_sum_obs
								) / self.varpar_mean_denominator[:,np.newaxis]
		dif_vpmeanmean_vs_mean_prior = (
										self.varpar_mean_mean
										-
										self.mean_prior_mean[np.newaxis,:]
										) # num_nt x num_dims
		self.denom_prior_x_mat_dif_vpmeanmean_vs_mean_prior = (
													self.mean_prior_denominator # scalar
													*
													dif_vpmeanmean_vs_mean_prior[:,:,np.newaxis]
													*
													dif_vpmeanmean_vs_mean_prior[:,np.newaxis,:]
													)


	def _update_varpar_covmat(self):
		self.varpar_covmat_df = self.covmat_prior_df + self.expected_emission_counts
		self.half_varpar_covmat_df = self.varpar_covmat_df * 0.5
		self.multidigamma_nover2 = self._multidigamma(self.half_varpar_covmat_df) # multivariate_digamma(n/2)

		self.inv_varpar_covmat_scale_mat = np.linalg.inv(
										self.expected_sum_covmat # num_nt x dimension x dimension
										+
										self.denom_prior_x_mat_dif_vpmeanmean_vs_mean_prior # num_nt x dimension x dimension
										+
										self.covmat_prior_scale_mat[np.newaxis,:,:] # dimension x dimension
									)
		logdet_inv_vp_covmat_sm = np.linalg.slogdet(self.inv_varpar_covmat_scale_mat)[1] # log |varpar_covmat_scale_ma|
		self.E_log_det_inv_vp_cm_sm = self.multidigamma_nover2+logdet_inv_vp_covmat_sm
		self.vpdf_x_invvpsm = self.varpar_covmat_df[:,np.newaxis,np.newaxis]*self.inv_varpar_covmat_scale_mat
		

	def reset_expected_counts(self):
		self.expected_emission_counts = np.zeros(self.expected_emission_counts.shape)
		self.expected_sum_obs = np.zeros(self.expected_sum_obs.shape)
		self.expected_sum_covmat = np.zeros(self.expected_sum_covmat.shape)

	def increment_expected_counts(self, string_batch, expected_emission_counts_per_loc):
		self.expected_emission_counts += np.sum(expected_emission_counts_per_loc, axis=(0,1))
		self.expected_sum_obs += np.sum(
										string_batch[:,:,np.newaxis,:] # num_strings x string_len x ndim
										*
										expected_emission_counts_per_loc[:,:,:,np.newaxis] # num_strings x string_len x num_nt
										,
										axis=(0,1)
									)
		self.expected_sum_covmat += np.sum(
										self.omcm_x_omcm # num_strings x string_len x num_nt x ndim x ndim
										*
										expected_emission_counts_per_loc[:,:,:,np.newaxis,np.newaxis] # num_strings x string_len x num_nt
										,
										axis=(0,1)
									)

		
	def set_prob_function(self):
		self.log_emission_constant = (
										self.E_log_det_inv_vp_cm_sm # num_nt
										-
										self.num_dimensions_over_vpdenom # num_nt
										-
										self.num_dimensions_x_log_pi # scalar
									) * 0.5

	def get_log_prob_for_strings(self, string_batch):
		obs_minus_cluster_mean = (
									string_batch[:,:,np.newaxis,:] # num_strings x string_len x ndim
									-
									self.varpar_mean_mean[np.newaxis,np.newaxis,:,:] # num_nt x ndim
									) # num_strings x string_len x num_nt x ndim
		self.omcm_x_omcm = (
							obs_minus_cluster_mean[:,:,:,:,np.newaxis]
							*
							obs_minus_cluster_mean[:,:,:,np.newaxis,:]
						)
		return (
						(
							self.log_emission_constant[np.newaxis,np.newaxis,:]
							-
							np.trace(
									np.sum(
										self.vpdf_x_invvpsm[np.newaxis,np.newaxis,:,:,:,np.newaxis]
										*
										self.omcm_x_omcm[:,:,:,np.newaxis,:,:]
										,
										axis=-2
									)
									,
									axis1=-2
									,
									axis2=-1
								) * 0.5
						) # num_strings x string_len x num_nt
					)





	def update_varpars(self):
		self._update_varpar_covmat() # expected_sum_covmat is based on pre-updated varpar_mean_mean
		self._update_varpar_mean()


	def get_var_bound(self):
		return (
					self._get_var_bound_mean()
					+
					self._get_var_bound_covmat()
				)


	def _get_var_bound_mean(self):
		return -(
					np.sum(
						np.trace( # E[log p(µ | ∑)]
							np.sum(
								self.vpdf_x_invvpsm[:,:,:,np.newaxis] # num_nt x dimension x dimension
								*
								(
									self.denom_prior_x_mat_dif_vpmeanmean_vs_mean_prior # num_nt x ndim x ndim
								)[:,np.newaxis,:,:]
								,
								axis=-2
								) # num_nt x dimension x dimension
								,
							axis1=-2,
							axis2=-1
						) # num_nt
					)
					+
					np.sum(
						self.num_dimensions_over_vpdenom # num_nt
						)* self.mean_prior_denominator
					+
					self.dimension * np.sum(np.log(self.varpar_mean_denominator)) # E[log q(µ)]
					) * 0.5
	
	def _get_var_bound_covmat(self):
		return (
				np.sum( # 1st part of E[log p(∑)]
							self.E_log_det_inv_vp_cm_sm
						) * self.covmat_prior_df*0.5 # scalar
				-
				np.sum( # 2nd part of E[log p(∑)]
					np.trace(
						np.sum(
							self.vpdf_x_invvpsm[:,:,:,np.newaxis] # num_nt x dimension x dimension
							*
							self.covmat_prior_scale_mat[np.newaxis,np.newaxis,:,:] # dimension x dimension
							,
							axis=-2
						) # num_nt x dimension x dimension
						,
						axis1=-2
						,
						axis2=-1
					) # num_nt
				) * 0.5
				+
				np.sum(
					sps.multigammaln(self.half_varpar_covmat_df, self.dimension)
					) # 1st part of E[log q(∑)]
				-
				np.sum( # 2nd part of E[log q(∑)]
					self.varpar_covmat_df # num_nt
					*
					self.multidigamma_nover2 # num_nt
				) * 0.5
				+
				0.5*self.dimension*np.sum(self.varpar_covmat_df) # 3rd part of E[log q(∑)]
				)



	def _multidigamma(self, array):
		"""
		Calculates multivariate digamma.
		"""
		return np.sum(
					sps.digamma(
						array[:,np.newaxis]
						-
						self.arange_num_dimensions_over_2[np.newaxis,:]
					)
					,
					axis=-1
				)

	def set_log_posterior_expectation(self):
		self.df = self.varpar_covmat_df + 1 - self.dimension
		self.posterior_precision = (
										(
											self.df * self.varpar_mean_denominator
											/
											(1 + self.varpar_mean_denominator)
										)[:,np.newaxis,np.newaxis]
										*
										self.inv_varpar_covmat_scale_mat # num_nt x ndim x ndim
									)
		self.log_post_pred_constant_1 = 0.5 * (self.df + self.dimension) # num_nt
		self.log_post_pred_constant_2 = (
										sps.gammaln(self.log_post_pred_constant_1)
										-
										sps.gammaln(self.df * 0.5)
										+
										0.5 * (
											np.linalg.slogdet(self.posterior_precision)[1]
											-
											(self.dimension * (np.log(self.df) + np.log(np.pi)))
											)
										)

	def get_log_posterior_pred(self, string):
		"""
		The expected posterior predictive probability density
		is given by a multivariate t-distribution.
		See Bishop (2006, pp. 483).
		"""
		obs_minus_cluster_mean = (
									string[:,:,np.newaxis,:] # 1 x string_len x ndim
									-
									self.varpar_mean_mean[np.newaxis,np.newaxis,:,:] # num_nt x ndim
									) # 1 x string_len x num_nt x ndim
		return (
			self.log_post_pred_constant_2[np.newaxis, np.newaxis, :]
			-
			self.log_post_pred_constant_1[np.newaxis, np.newaxis, :]
				* np.log(
					1
					+
					np.sum(
						np.sum(
							obs_minus_cluster_mean[:,:,:,:,np.newaxis] # 1 x string_len x num_nt x ndim
							*
							self.posterior_precision[np.newaxis,np.newaxis,:,:,:] # num_nt x ndim x ndim
							,
							axis=-2
						) # 1 x string_len x num_nt x ndim
						*
						obs_minus_cluster_mean
						,
						axis=-1
					)
					/
					self.df[np.newaxis, np.newaxis, :]
				) # 1 x string_len x num_nt
			)

class HDP_branch(Dirichlet):
	"""
	Branching rules and top-level non-terminal weights of HDP-PCFG
	in Chomsky-normal forms with variable root labeling
	(i.e. random root sampling).

	params:
			'concentration_top': : positive float.
			'concentration_bottom': : positive float.
			'concentration_init': : positive float.
			'num_non_terminals': natural number,
			'gradient_projection_tolerance': positive float,
	"""
	def __init__(self,params):
		self.num_non_terminals = params['num_non_terminals']
		self.gradient_projection_tolerance = params['gradient_projection_tolerance']

		self.top_concent_minus_1 = params['concentration_top']-1
		self.concentration_bottom = params['concentration_bottom']


		self.non_terminal_weight_estimate = np.random.dirichlet(np.ones(params['num_non_terminals']))
		self.base_counts = (
							self.concentration_bottom
							*
							self.non_terminal_weight_estimate[:,np.newaxis]
							*
							self.non_terminal_weight_estimate[np.newaxis,:]
						)

		
		self.varpar_rule_weight = np.random.gamma(
												1, 20,
												size=(
													self.num_non_terminals,
													self.num_non_terminals,
													self.num_non_terminals
												)
												)
		self.sum_rule_weight = np.sum(self.varpar_rule_weight, axis=(1,2))
		self.E_log_rule_weight = sps.digamma(self.varpar_rule_weight) - sps.digamma(self.sum_rule_weight)[:,np.newaxis,np.newaxis]

		# Index array of the following form.
		# 0 1 2 3 4 5 ...
		# 1 1 2 3 4 5 ...
		# 2 2 2 3 4 5 ...
		# 3 3 3 3 4 5 ...
		# 4 4 4 4 4 5 ...
		# 5 5 5 5 5 5 ...
		# ...............
		self.radial_ids = np.maximum(
							np.arange(self.num_non_terminals)[:,np.newaxis],
							np.arange(self.num_non_terminals)[np.newaxis,:]
							)

		self.concentration_init = params['concentration_init']
		self.base_counts_init = self.concentration_init * self.non_terminal_weight_estimate

		self.varpar_init_weight = np.random.gamma(1, 20, size=self.num_non_terminals)
		self.sum_init_weight = np.sum(self.varpar_init_weight)
		self.E_log_init_weight = sps.digamma(self.varpar_init_weight) - sps.digamma(self.sum_init_weight)


		
	def _update_non_terminal_weight_estimate(self):
		self.non_terminal_weight_estimate = gpm.gradient_projection_optimization(
												self.non_terminal_weight_estimate,
												self.objective_func,
												self.get_gradient_and_hessian,
												self.gradient_projection_tolerance,
												)
		self.base_counts = (
							self.concentration_bottom
							*
							self.non_terminal_weight_estimate[:,np.newaxis]
							*
							self.non_terminal_weight_estimate[np.newaxis,:]
						)
		self.base_counts_init = self.concentration_init * self.non_terminal_weight_estimate


	def _update_varpar_init_weight(self):
		self.varpar_init_weight = (
									self.expected_root_counts
									+
									self.base_counts_init
									)
		self.sum_init_weight = np.sum(self.varpar_init_weight)
		self.E_log_init_weight = sps.digamma(self.varpar_init_weight) - sps.digamma(self.sum_init_weight)
	
	def _update_varpar_rule_weight(self):
		self.varpar_rule_weight = (
									self.expected_rule_counts
									+
									self.base_counts[np.newaxis,:,:]
									)
		self.sum_rule_weight = np.sum(self.varpar_rule_weight, axis=(1,2))
		self.E_log_rule_weight = sps.digamma(self.varpar_rule_weight) - sps.digamma(self.sum_rule_weight)[:,np.newaxis,np.newaxis]

	def update_varpars(self):
		self._update_non_terminal_weight_estimate()
		self._update_varpar_rule_weight()
		self._update_varpar_init_weight()

	def get_E_log_p(self):
		return -self.objective_func(
						self.non_terminal_weight_estimate
						)

	def get_E_log_q(self): # No log q(non_terminal_weight_estimate)?
		return (
					np.sum(
						self.E_log_rule_weight*(self.varpar_rule_weight-1)
					)
					-
					np.sum(
						sps.gammaln(self.varpar_rule_weight)
					)
					+
					np.sum(
						sps.gammaln(self.sum_rule_weight)
					)
					+
					np.sum(
						self.E_log_init_weight*(self.varpar_init_weight-1)
					)
					-
					np.sum(
						sps.gammaln(self.varpar_init_weight)
					)
					+
					np.sum(
						sps.gammaln(self.sum_init_weight)
					)
					)
	

	def get_log_init_weight(self):
		return self.E_log_init_weight

	def update_expected_counts(self, expected_rule_counts, expected_root_counts):
		super(HDP_branch, self).update_expected_counts(expected_rule_counts)
		self.expected_root_counts = expected_root_counts



	def set_log_posterior_expectation(self):
		self.log_posterior_expectation = np.log(self.varpar_rule_weight)-np.log(self.sum_rule_weight)[:,np.newaxis,np.newaxis]
		self.log_posterior_expectation_init = np.log(self.varpar_init_weight)-np.log(self.sum_init_weight)


	def get_prob_non_stop(self, non_terminal_weights):
		prob_non_stop = 1.0 - np.cumsum(non_terminal_weights[:-1])
		prob_non_stop[-1] = non_terminal_weights[-1] # To avoid underflow.
		return prob_non_stop

	def objective_func(self, non_terminal_weights):
		"""
		The objective function of non_terminal_weights that must be minimized.
		i.e. (-1)*(function to be maximized).
		"""
		prob_non_stop = self.get_prob_non_stop(non_terminal_weights)
		prior_term = (
						self.top_concent_minus_1*np.log(prob_non_stop[-1])
						-
						np.sum(np.log(prob_non_stop[:-1]))
						)

		concent_x_weights_x_weights = self.concentration_bottom * non_terminal_weights[:,np.newaxis] * non_terminal_weights[np.newaxis,:]
		rule_term = (
			np.sum(
				(concent_x_weights_x_weights-1.0)
				*
				np.sum(self.E_log_rule_weight, axis=0)
			)
			-
			self.num_non_terminals*
			np.sum(
				sps.gammaln(
					concent_x_weights_x_weights
					)
					)
			)
		
		init_concent_x_weights = self.concentration_init * non_terminal_weights
		init_term = (
					np.sum(
						(init_concent_x_weights - 1.0)
						*
						self.E_log_init_weight
					)
					-
					np.sum(
						sps.gammaln(
							init_concent_x_weights
						)
					)
				)
		return -(prior_term + rule_term + init_term)



	def get_gradient_and_hessian(self, non_terminal_weights):
		"""
		Get gradient and hessian of the objective function.
		"""
		
		(gradient,
			prob_non_stop,
			concent_x_weights_x_weights,
			digamma_concent_x_weights_x_weights,
			init_concent_x_weights
			) = self.get_gradient(non_terminal_weights)

		hessian = self.get_hessian(
					non_terminal_weights,
					prob_non_stop,
					concent_x_weights_x_weights,
					digamma_concent_x_weights_x_weights,
					init_concent_x_weights
					)

		return gradient, hessian


	def get_gradient(self, non_terminal_weights):
		"""
		Get the derivative of objective function
		from pre-computed information.
		Unlike Liang (2009), we treat all the weights as if they were independent,
		and leave necessary transformation for the gradient projection method.
		"""
		prob_non_stop = self.get_prob_non_stop(non_terminal_weights)

		inverse_prob_non_stop = 1.0 / (prob_non_stop)
		gradient_prior = np.append(
							np.append(
								np.cumsum(inverse_prob_non_stop[-2::-1])[::-1]
								,
								0.0
							)
							-
							inverse_prob_non_stop[-1]*self.top_concent_minus_1
							,
							0.0
							)
		
		concent_x_weights_x_weights = self.concentration_bottom*non_terminal_weights[:,np.newaxis]*non_terminal_weights[np.newaxis,:]
		digamma_concent_x_weights_x_weights = sps.digamma(
													concent_x_weights_x_weights
													)
		gradient_rules_K = (
							self.concentration_bottom
							*
							(
								np.sum(
									non_terminal_weights[np.newaxis,:,np.newaxis]
									*
									self.E_log_rule_weight
									,
									axis=(0,1)
								)
								+
								np.sum(
									non_terminal_weights[np.newaxis,np.newaxis,:]
									*
									self.E_log_rule_weight
									,
									axis=(0,2)
								)
								-
								2
									*self.num_non_terminals
									*np.sum(
										non_terminal_weights[:,np.newaxis]
										*
										digamma_concent_x_weights_x_weights
										,
										axis=(0)
									)
							)
							)

		init_concent_x_weights = self.concentration_init * non_terminal_weights
		digamma_init_concent_x_weights = sps.digamma(init_concent_x_weights)
		gradient_init = (
							self.concentration_init
							*
							(
								self.E_log_init_weight
								-
								digamma_init_concent_x_weights
							)
							)
		return (
				-(gradient_prior + gradient_rules_K + gradient_init),
				prob_non_stop,
				concent_x_weights_x_weights,
				digamma_concent_x_weights_x_weights,
				init_concent_x_weights
				)


	def get_hessian(
				self,
				non_terminal_weights,
				prob_non_stop,
				concent_x_weights_x_weights,
				digamma_concent_x_weights_x_weights,
				init_concent_x_weights,
				):
		"""
		Get the hessian matrix of the objective function.
		"""
		prob_non_stop_inv_square = prob_non_stop**-2
		hessian_prior_vec = np.append(
								np.append(
										np.cumsum(prob_non_stop_inv_square[-2::-1])[::-1]
										,
										0.0
									)
								-
								self.top_concent_minus_1 * prob_non_stop_inv_square[-1]
								,
								0.0
								)
		hessian_prior = hessian_prior_vec[self.radial_ids]


		sum_E_log_rule_over_LHS = np.sum(self.E_log_rule_weight, axis=0)
		E_log_rule_plus_E_log_rule_T = sum_E_log_rule_over_LHS+sum_E_log_rule_over_LHS.T
		

		trigamma_cww = sps.polygamma(
								1,
								concent_x_weights_x_weights
								)
		cww_trigamma_cww = (
							concent_x_weights_x_weights
							*
							trigamma_cww
							)
		
		concent_x_sum_square_weights_x_trigamma = np.sum(
													(non_terminal_weights**2)[:,np.newaxis]
													*
													trigamma_cww
													,
													axis=0
													)*self.concentration_bottom


		hessian_rules = self.concentration_bottom*(
									E_log_rule_plus_E_log_rule_T
									-
									2 * self.num_non_terminals
									*
									(
										cww_trigamma_cww
										+
										digamma_concent_x_weights_x_weights
										+
										np.diag(concent_x_sum_square_weights_x_trigamma) # Get a diagonal matrix.
									)
									)


		deldel_init_diag = -(
									self.concentration_init**2
									*
									sps.polygamma(1, init_concent_x_weights)
									)


		hessian_init = np.diag(deldel_init_diag)

		return -(hessian_prior + hessian_rules + hessian_init)


class HDP_trans(object):
	"""
	For transition from a non-terminal to a terminal.
	"""
	def __init__(self,params):
		"""
		params:
			concentration_top: positive float,
			concentration_bottom: : positive float,
			start_size: natural number,
			goal_size: natural number,
			gradient_projection_tolerance: : positive float,
		"""
		self.gradient_projection_tolerance = params['gradient_projection_tolerance']

		self.top_concent_minus_1 = params['concentration_top']-1
		self.concentration_bottom = params['concentration_bottom']

		start_size = params['start_size']
		goal_size = params['goal_size']


		self.goal_weight_estimate = np.random.dirichlet(np.ones(goal_size))
		self.base_counts = self.concentration_bottom * self.goal_weight_estimate
		
		self.varpar_transition = np.random.gamma(
												1, 20,
												size=(
													start_size,
													goal_size
												)
												)
		self.sum_transition = np.sum(self.varpar_transition, axis=-1)
		self.E_log_transition = sps.digamma(self.varpar_transition) - sps.digamma(self.sum_transition)[:,np.newaxis]

		# Index array of the following form.
		# 0 1 2 3 4 5 ...
		# 1 1 2 3 4 5 ...
		# 2 2 2 3 4 5 ...
		# 3 3 3 3 4 5 ...
		# 4 4 4 4 4 5 ...
		# 5 5 5 5 5 5 ...
		# ...............
		self.radial_ids = np.maximum(
							np.arange(goal_size)[:,np.newaxis],
							np.arange(goal_size)[np.newaxis,:]
							)


		
	def _update_goal_weight_estimate(self):
		self.goal_weight_estimate = gpm.gradient_projection_optimization(
												self.goal_weight_estimate,
												self.objective_func,
												self.get_gradient_and_hessian,
												self.gradient_projection_tolerance,
												)
		self.base_counts = self.concentration_bottom * self.goal_weight_estimate
	
	def _update_varpar_transition(self):
		self.varpar_transition = (
									self.expected_trans_counts
									+
									self.base_counts[np.newaxis,:]
									)
		self.sum_transition = np.sum(self.varpar_transition, axis=-1)
		self.E_log_transition = sps.digamma(self.varpar_transition) - sps.digamma(self.sum_transition)[:,np.newaxis]

	def update_expected_counts(self, expected_trans_counts):
		self.expected_trans_counts = expected_trans_counts

	def update_varpars(self):
		self._update_goal_weight_estimate()
		self._update_varpar_transition()

	def get_log_weights(self):
		return self.E_log_transition

	def get_var_bound(self):
		return self.get_E_log_p() - self.get_E_log_q()

	def get_E_log_p(self):
		return -self.objective_func(
						self.goal_weight_estimate
						)

	def get_E_log_q(self): # No log q(goal_weight_estimate)?
		return (
					np.sum(
						self.E_log_transition*(self.varpar_transition-1)
					)
					-
					np.sum(
						sps.gammaln(self.varpar_transition)
					)
					+
					np.sum(
						sps.gammaln(self.sum_transition)
					)
					)

	def set_log_posterior_expectation(self):
		self.log_posterior_expectation = np.log(self.varpar_transition)-np.log(self.sum_transition)[:,np.newaxis]

	def get_prob_non_stop(self, goal_weights):
		prob_non_stop = 1 - np.cumsum(goal_weights[:-1]) # T2,...,TK
		return prob_non_stop


	def objective_func(self, goal_weights):
		"""
		The objective function of goal_weights that must be minimized.
		i.e. (-1)*(function to be maximized).
		"""
		prob_non_stop = self.get_prob_non_stop(goal_weights)
		prior_term = (
						self.top_concent_minus_1*np.log(prob_non_stop[-1])
						-
						np.sum(np.log(prob_non_stop[:-1]))
						)
		# print 'prior_term', prior_term

		concent_x_weights = self.concentration_bottom * goal_weights
		rule_term = (
			np.sum(
				(concent_x_weights-1)
				*
				np.sum(self.E_log_transition, axis=0)
			)
			-
			self.E_log_transition.shape[0]*
			np.sum(
				sps.gammaln(
					concent_x_weights
					)
					)
			)
		# print 'rule_term', rule_term
		return -(prior_term + rule_term)


	def get_gradient_and_hessian(self, goal_weights):
		"""
		Get gradient and hessian of the objective function.
		"""
		
		(gradient,
			prob_non_stop,
			concent_x_weights
			) = self.get_gradient(goal_weights)

		hessian = self.get_hessian(
					prob_non_stop,
					concent_x_weights
					)

		return gradient, hessian


	def get_gradient(self, goal_weights):
		"""
		Get the derivative of objective function
		from pre-computed information.
		"""
		prob_non_stop = self.get_prob_non_stop(goal_weights)


		inverse_prob_non_stop = 1.0 / (prob_non_stop)
		gradient_prior = np.append(
							np.append(
								np.cumsum(inverse_prob_non_stop[-2::-1])[::-1]
								,
								0.0
							)
							-
							inverse_prob_non_stop[-1]*self.top_concent_minus_1
							,
							0.0
							)
		
		concent_x_weights = self.concentration_bottom*goal_weights
		digamma_concent_x_weights = sps.digamma(concent_x_weights)
		gradient_rules = (
							self.concentration_bottom
							*
							(
								np.sum(
									self.E_log_transition
									,
									axis=0
									)
								-
								self.E_log_transition.shape[0]
								*
								digamma_concent_x_weights
							)
							)
		return (
				-(gradient_prior + gradient_rules),
				prob_non_stop,
				concent_x_weights
				)


	def get_hessian(
				self,
				prob_non_stop,
				concent_x_weights
				):
		"""
		Get the hessian matrix of the objective function.
		"""
		prob_non_stop_inv_square = prob_non_stop**-2
		hessian_prior_vec = np.append(
								np.append(
										np.cumsum(prob_non_stop_inv_square[-2::-1])[::-1]
										,
										0.0
									)
								-
								self.top_concent_minus_1 * prob_non_stop_inv_square[-1]
								,
								0.0
								)
		hessian_prior = hessian_prior_vec[self.radial_ids]


		deldel_rules_components = -(
									self.concentration_bottom**2
									*
									self.E_log_transition.shape[0]
									*
									sps.polygamma(1, concent_x_weights)
									)


		hessian_rules = np.diag(deldel_rules_components)

		return -(hessian_prior+hessian_rules)



class DP(object):
	"""
	Blei and Jordan (2006).
	"""
	def __init__(self, params):
		"""
		params:
			num_clusters: natural number,
			EITHER:
				concentration: positive float,
			OR:
				concentration_prior_shape: positive float,
				concentration_prior_rate: positive float,
		"""
		self.varpar_stick = np.random.gamma(1,20, size=(params['num_clusters']-1,2))
		self.sum_stick = np.sum(self.varpar_stick, axis=-1)
		self.E_log_stick = sps.digamma(self.varpar_stick) - sps.digamma(self.sum_stick)[:,np.newaxis]
		if 'concentration_prior_shape' in params and 'concentration_prior_rate' in params:
			self.concentration_prior_rate = params['concentration_prior_rate']
			self.varpar_concent_rate = np.random.gamma(1,20)
			self.varpar_concent_shape = params['concentration_prior_shape'] + params['num_clusters']-1
			self.concentration = self.varpar_concent_shape / self.varpar_concent_rate
			self.update_varpars = self._update_varpar_full
			self.get_E_log_q = lambda: self._get_E_log_q_concentration() + self._get_E_log_q_stick()
		else:
			self.concentration = params['concentration']
			self.update_varpars = self._update_varpar_basic
			self.get_E_log_q = self._get_E_log_q_stick

	def _update_varpar_basic(self):
		self._update_varpar_stick()

	def _update_varpar_full(self):
		self._update_varpar_concentration()
		self._update_varpar_basic()

	def _update_varpar_concentration(self):
		self.varpar_concent_rate = (self.concentration_prior_rate - np.sum(self.E_log_stick[:,1]))
		self.concentration = self.varpar_concent_shape / self.varpar_concent_rate

	def _update_varpar_stick(self):
		self.varpar_stick[:,0] = self.expected_counts[:-1] + 1
		self.varpar_stick[:,1] = np.cumsum(
									self.expected_counts[:0:-1]
									)[::-1] + self.concentration
		self.sum_stick = np.sum(self.varpar_stick, axis=-1)
		self.E_log_stick = sps.digamma(self.varpar_stick) - sps.digamma(self.sum_stick)[:,np.newaxis]

	def update_expected_counts(self, expected_counts_per_cluster):
		self.expected_counts = expected_counts_per_cluster # num_clusters

	def get_log_weights(self):
		return (
				np.append(self.E_log_stick[:,0], 0)
				+
				np.append(0, np.cumsum(self.E_log_stick[:,1]))
				)

	def get_var_bound(self):
		return self.get_E_log_p() - self.get_E_log_q()

	def get_E_log_p(self):
		return np.sum(
					(self.concentration - 1)*np.sum(self.E_log_stick[:,1])
				)

	def _get_E_log_q_stick(self):
		return (
					np.sum(
						self.E_log_stick*(self.varpar_stick-1)
					)
					-
					np.sum(
						sps.gammaln(self.varpar_stick)
					)
					+
					np.sum(
						sps.gammaln(self.sum_stick)
					)
					)

	def _get_E_log_q_concentration(self):
		return (
				self.varpar_concent_shape*np.log(self.varpar_concent_rate)
				+
				self.concentration_prior_rate*self.concentration
				)

	def get_num_clusters(self):
		return self.varpar_stick.shape[0]+1

	def get_log_posterior_weights(self):
		log_E_stick = np.log(self.varpar_stick)-np.log(self.sum_stick)[:,np.newaxis]
		log_E_pi = np.append(log_E_stick[:,0], 0)-np.append(0, np.cumsum(log_E_stick[:,1]))
		return log_E_pi