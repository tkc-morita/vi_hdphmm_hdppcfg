# coding: utf-8

"""
Forward-backward algorithm for HMM.
"""

import numpy as np
import scipy.misc as spm



class Forward_Backward_Algorithm_Multivariate_Emission_Sort_By_Length_Log_Sum_Exp(object):
	def __init__(self, log_transition_weights, log_emission_pdf, log_init_weights, length_weight_func = None):
		self.log_transition_weights = log_transition_weights
		# self.log_transition_normalizer = np.sum(self.transition_rule_weights)
		# self.log_transition_weights /= self.log_transition_normalizer # Make sure that the weights are under 1.
		# self.log_transition_normalizer = np.log(self.log_transition_normalizer)
		self.log_emission_pdf = log_emission_pdf
		self.log_init_weights = log_init_weights
		self.length_weight_func = length_weight_func

	def _set_strings(self, string_batch):
		"""
		(Re)set emitted strings.
		Strings of the same length are assumed to be sorted.
		"""
		self.log_emission_probs = self.log_emission_pdf(string_batch) # -> num_strings x string_len x num_states
		if not self.length_weight_func is None:
			self.log_emission_probs = self.length_weight_func(string_batch, self.log_emission_probs)
		self.string_length = string_batch.shape[1]
		self.num_strings = string_batch.shape[0]


	def get_expected_visit_counts(self, string_batch):
		self._set_strings(string_batch)
		self._forward()
		self._backward()

		log_like = spm.logsumexp(self.current_log_forward_probs, axis=-1)
		expected_transition_per_pos = np.exp(self.log_expected_transition_per_pos - log_like[:,np.newaxis,np.newaxis,np.newaxis])
		expected_trans_counts = np.sum(expected_transition_per_pos, axis=1)
		expected_state_visit_per_pos = np.zeros((self.num_strings, self.string_length, self.log_init_weights.size))
		expected_state_visit_per_pos[:,:-1,:] = np.sum(expected_transition_per_pos, axis=-1)
		expected_state_visit_per_pos[:,-1,:] = np.exp(self.current_log_forward_probs - log_like[:,np.newaxis])

		return expected_trans_counts, expected_state_visit_per_pos, log_like


	def _forward(self):
		self.current_log_forward_probs = self.log_init_weights[np.newaxis,:] + self.log_emission_probs[:,0,:]
		self.log_forward_x_trans = np.zeros((self.num_strings, self.string_length - 1) + self.log_transition_weights.shape)
		for pos in xrange(1,self.string_length):
			self.log_forward_x_trans[:,pos-1,:,:] = (
													self.current_log_forward_probs[:,:,np.newaxis]
													+
													self.log_transition_weights[np.newaxis,:,:]
												)
			self.current_log_forward_probs = (
										self.log_emission_probs[:,pos,:]
										+
										spm.logsumexp(
											self.log_forward_x_trans[:,pos-1,:,:]
											,
											axis=-2
											)
									)


	def _backward(self):
		self.log_expected_transition_per_pos = np.zeros(self.log_forward_x_trans.shape)
		current_log_backward_probs = np.zeros(self.current_log_forward_probs.shape)
		for pos in xrange(self.string_length-1, 0, -1):
			log_emission_x_backward = self.log_emission_probs[:,pos,:] + current_log_backward_probs
			self.log_expected_transition_per_pos[:,pos-1,:,:] = self.log_forward_x_trans[:,pos-1,:,:] + log_emission_x_backward[:,np.newaxis,:]
			current_log_backward_probs = spm.logsumexp(
													self.log_transition_weights[np.newaxis,:,:]
													+
													log_emission_x_backward[:,np.newaxis,:]
													,
													axis=-1
												)