# coding: utf-8

import numpy as np

class Terminal(object):
	def __init__(self, label, mother=None):
		self.label = label
		if not mother is None:
			self.set_mother(mother)

	def set_mother(self, mother):
		self.mother = mother
		self.mother.add_child(self)

	def set_label(self, label):
		self.label = label

	def get_label(self):
		return self.label

	def print_structure(self, decoder=None):
		if decoder is None:
			return str(self.label)
		else:
			return str(decoder[self.label])

	def is_branching(self):
		return False

class Node(Terminal):
	def __init__(self, label, mother=None):
		super(Node, self).__init__(label, mother=mother)
		self.children = []

	def add_child(self, child):
		self.children.append(child)

	def print_structure(self, decoder=None):
		return (
				'('
				+
				str(self.label)
				+
				' '
				+
				' '.join([child.print_structure(decoder=decoder) for child in self.children])
				+
				')'
				)


	def is_right_branching(self):
		if self.is_branching():
			is_rightmost_child_right_branching = self.children[-1].is_right_branching()
			l = [not child.is_branching() for child in self.children[:-1]]
			are_other_children_non_branching = np.all(l)
			# are_other_children_non_branching = np.all([not child.is_branching for child in self.children[:-1]])
			return is_rightmost_child_right_branching and are_other_children_non_branching
		else:
			return True # Non-branching is considered right-branching

	def is_left_branching(self):
		if self.is_branching():
			is_leftmost_child_left_branching = self.children[0].is_left_branching()
			are_other_children_non_branching = np.all([not child.is_branching() for child in self.children[1:]])
			return is_leftmost_child_left_branching and are_other_children_non_branching
		else:
			return True # Non-branching is considered left-branching

	def is_branching(self):
		return len(self.children) > 1 # Whether or not having more than 1 child.







