# coding: utf-8
"""
Stolcke's (1995) inner-outer algorithm for
a fully connected PCFG in a Chomsky normal form.
"""

import numpy as np
import scipy.misc as spm
import tree

class Inner_Outer_Algorithm(object):
	"""
	Stolcke's (1995) inner-outer algorithm for
	a fully connected PCFG in a Chomsky normal form.
	"""
	def __init__(
			self,
			log_branch_rule_weights,
			emission_rule_weights,
			log_init_weights,
			):
		"""
		Initialize rule weights.
		"""
		self.log_branch_rule_weights = np.log(log_branch_rule_weights)
		self.log_emission_rule_weights = np.log(emission_rule_weights)
		self.log_init_weights = np.log(log_init_weights)

	def get_log_likelihoods(self, string):
		"""
		Get log likelihoods of string without counting expected rule use.
		"""
		self._set_strings(string)
		if self.string_length == 1:
			return spm.logsumexp(
						self.log_init_weights
						+
						self.log_emission_rule_weights[:,string[0]]
						# ,
						# axis=0
						)
		else:
			self._calc_inner_probs()
			return spm.logsumexp(
								self.log_init_weights
								+
								spm.logsumexp(self.log_inner_branch[-1,0,:,:,:,2], axis=(-2,-1))
								)

	def _set_strings(self, string):
		"""
		(Re)set emitted strings.
		Strings of the same length are assumed to be sorted.
		"""
		self.string = string
		self.string_length = len(self.string)
		# self.string_ids = np.arange(self.string.shape[0])


	def get_expected_rule_counts(self, string):
		"""
		Returns expected # of use of each rule,
		together with log likelihoods of strings.
		"""
		self._set_strings(string)
		if self.string_length == 1: # No branching for strings of length 1
			expected_log_init_weights = self.log_init_weights+self.log_emission_rule_weights[:,string[0]]
			log_like = spm.logsumexp(expected_log_init_weights)
			expected_log_init_weights -= log_like
			expected_branch_rule_counts = np.zeros(self.log_branch_rule_weights.shape)
			expected_emission_rule_counts = np.zeros(self.log_emission_rule_weights.shape)
			expected_emission_rule_counts[:,string[0]] += np.exp(expected_log_init_weights)
		else:
			self._calc_inner_probs()
			self._calc_outer_probs()
			log_likelihoods_for_each_root_non_terminal=(
										spm.logsumexp(self.log_inner_branch[-1,0,:,:,:,2], axis=(1,2))
										+
										self.log_init_weights
										)
			log_like=np.logsumexp(
									log_likelihoods_for_each_root_non_terminal
								)
			expected_branch_rule_counts=(
											spm.logsumexp(
												self.log_outer_branch[...,0].diagonal(axis1=0, axis2=1) # Scaled.
												+
												self.log_inner_branch[...,0].diagonal(axis1=0, axis2=1) # Not scaled.
												,
												axis=-1 # diagonalized dimension comes last.
												) # num_strings x num_nt x num_nt x num_nt
											-
											log_like
											)
			
			expected_emission_rule_counts=(
										spm.logsumexp(
											self.log_outer_emit[...,0] # Scaled.
											+
											self.log_inner_emit[...,0] # Not scaled.
											,
											axis=0
											) # num_strings x num_nt x num_symbols
										-
										log_like
										)
			expected_log_init_weights = (
									log_likelihoods_for_each_root_non_terminal
									-
									log_like
									)
		return expected_branch_rule_counts, expected_emission_rule_counts, expected_log_init_weights, log_like


	def get_viterbi_parse(self, string):
		"""
		Get Viterbi (or most probable leftmost) parse of the string.
		"""
		self._set_strings(string)
		if self.string_length == 1: # No branching for strings of length 1
			expected_log_init_weights = self.log_init_weights+self.log_emission_rule_weights[:,string[0]]
			most_probable_root = np.argmax(expected_log_init_weights)
			viterbi_parse = tree.Node(label=most_probable_root)
			terminal = tree.Terminal(string[0], mother=viterbi_parse)
		else:
			self._calc_viterbi_probs()
			reshaped_log_viterbi_branch = self.log_viterbi_branch[-1,0,:,:,:,2].reshape(
																self.log_viterbi_branch.shape[2],
																self.log_viterbi_branch.shape[2]**2
																)
			max_ids = np.argmax(
							reshaped_log_viterbi_branch
							, axis=(
								# -2,
								-1
								)
							)
			# 
			max_root_branch = reshaped_log_viterbi_branch[np.arange(self.log_viterbi_branch.shape[2]),max_ids]
			root_label=np.argmax(
								max_root_branch
								+
								self.log_init_weights
								)
			rhs_left, rhs_right = np.unravel_index(max_ids[root_label], self.log_inner_branch.shape[3:5])
			viterbi_parse = self._get_viterbi_subparse_branch(-1, 0, root_label, rhs_left, rhs_right, 2)
		return viterbi_parse



	def _get_viterbi_subparse_branch(self, i, k, lhs, rhs_left, rhs_right, dot_pos, mother_node = None):
		if dot_pos == 0:
			subtree_root = tree.Node(label=lhs, mother=mother_node)
			return subtree_root
		elif dot_pos == 1:
			child_lhs = rhs_left
		else:
			child_lhs = rhs_right
		branch_or_emit = self.viterbi_predecessor[i,k,lhs,rhs_left,rhs_right,dot_pos,0]
		j = self.viterbi_predecessor[i,k,lhs,rhs_left,rhs_right,dot_pos,1]
		child_rhs_left_or_terminal = self.viterbi_predecessor[i,k,lhs,rhs_left,rhs_right,dot_pos,2]
		child_rhs_right = self.viterbi_predecessor[i,k,lhs,rhs_left,rhs_right,dot_pos,3]
		subtree_root = self._get_viterbi_subparse_branch(j, k, lhs, rhs_left, rhs_right, dot_pos-1, mother_node = mother_node)
		if branch_or_emit: # if the predecessor is emission.
			child_tree = self._get_viterbi_subparse_emit(i,child_lhs,child_rhs_left_or_terminal, 1, subtree_root)
		else:
			child_tree = self._get_viterbi_subparse_branch(i, j, child_lhs, child_rhs_left_or_terminal, child_rhs_right, 2, mother_node = subtree_root)
		return subtree_root


	def _get_viterbi_subparse_emit(self, i, lhs, emitted_symbol, dot_pos, mother_node):
		if dot_pos == 0:
			part_of_speech = tree.Node(label=lhs, mother=mother_node)
			return part_of_speech
		else:
			part_of_speech = self._get_viterbi_subparse_emit(i-1, lhs, emitted_symbol, 0, mother_node)
			terminal = tree.Terminal(emitted_symbol, mother=part_of_speech)
			return part_of_speech







	def _calc_inner_probs(self):
		"""
		Calculates inner probabilities.
		Forward probabilities are omitted
		because they are not used for obtaining expected counts of rules.
		Underflow is avoided by logsumexp.
		"""
		self.log_inner_branch=np.full(
							(
								self.string_length+1, # penultimate pos i
								self.string_length+1, # start pos k
								self.log_branch_rule_weights.shape[0], # LHS non-terminal
								self.log_branch_rule_weights.shape[1], # Left RHS non-terminal
								self.log_branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot pos
							)
							,
							-np.inf
							)
		self.log_inner_emit=np.full(
							(
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.log_emission_rule_weights.shape[0], # LHS non-terminal
							self.log_emission_rule_weights.shape[1], # Emitted emitted_symbol
							2 # dot pos
							)
							,
							-np.inf
							)
		
		
		self.log_inner_branch[0,0,:,:,:,0]=self.log_branch_rule_weights

		emitted_symbol = self.string[0]

		self.log_inner_emit[0,:,emitted_symbol,0] = self.log_emission_rule_weights[:,emitted_symbol]

		self.completed_log_inner_branch=np.full( # To be reused.
											(
											self.string_length+1, # i
											self.string_length+1, # j
											self.log_branch_rule_weights.shape[0],
											)
											,
											-np.inf
											)
		[
			(
				self._inner_routine(
						pos,
						emitted_symbol
						)
			)
			for 
			pos,
			emitted_symbol
			in enumerate(self.string, start=1) # for i=1....
		]

	def _calc_viterbi_probs(self):
		"""
		Calculates forward, inner, and viterbi probabilities.
		Underflow is avoided by scaling.
		(Forward and inner probs are used for scaling.)
		"""

		self.log_inner_branch=np.full(
							(
								self.string_length+1, # penultimate pos i
								self.string_length+1, # start pos k
								self.log_branch_rule_weights.shape[0], # LHS non-terminal
								self.log_branch_rule_weights.shape[1], # Left RHS non-terminal
								self.log_branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot pos
							)
							,
							-np.inf
							)
		self.log_inner_emit=np.full(
							(
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.log_emission_rule_weights.shape[0], # LHS non-terminal
							self.log_emission_rule_weights.shape[1], # Emitted emitted_symbol
							2 # dot pos
							)
							,
							-np.inf
							)

		self.log_viterbi_branch=np.full(self.log_inner_branch.shape, -np.inf)
		self.log_viterbi_emit=np.full(self.log_inner_emit.shape, -np.inf)
		
		self.log_inner_branch[0,0,:,:,:,0]=self.log_branch_rule_weights
		self.log_viterbi_branch[0,0,:,:,:,0]=self.log_branch_rule_weights

		emitted_symbol = self.string[0]

		self.log_inner_emit[0,:,emitted_symbol,0] = self.log_emission_rule_weights[:,emitted_symbol]
		self.log_viterbi_emit[0,:,emitted_symbol,0] = self.log_emission_rule_weights[:,emitted_symbol]

		self.completed_log_inner_branch=np.full( # To be reused.
											(
											self.string_length+1, # i
											self.string_length+1, # j
											self.log_branch_rule_weights.shape[0],
											)
											,
											-np.inf
											)

		self.completed_viterbi_branch=np.full( # To be reused.
											(
											self.string_length+1, # i
											self.string_length+1, # j
											self.log_branch_rule_weights.shape[0],
											)
											,
											-np.inf
											)
		self.viterbi_predecessor = np.full(
										(
											# ID
											self.string_length+1, # penultimate pos i
											self.string_length+1, # start pos k
											self.log_branch_rule_weights.shape[0], # LHS non-terminal
											self.log_branch_rule_weights.shape[1], # Left RHS non-terminal
											self.log_branch_rule_weights.shape[2], # Right RHS non-terminal
											3, # dot pos
											# ======================
											# Info
											4 # 1st entry for branch (0) vs. emission (1).
											  # 2nd entry for j
											  # 3rd entry for RHS_left/terminal
											  # 4th entry for RHS_right
										)
										,
										# np.nan
										100
										)
		self.viterbi_predecessor_for_completed = np.full(
													(
														# ID
														self.string_length+1, # i
														self.string_length+1, # j
														self.log_branch_rule_weights.shape[0], # LHS non-terminal
														# ======================
														# Info
														2 # 1st entry for RHS_left.
														  # 2nd entry for RHS_right.
													)
													,
													# np.nan
													100
													)
		[
			(
				self._viterbi_routine(
						pos,
						emitted_symbol
						)
			)
			for 
			pos,
			emitted_symbol
			in enumerate(self.string, start=1) # for i=1....
		]




	def _inner_routine(
						self,
						pos,
						emitted_symbol,
						):
		self._scan(pos,emitted_symbol)
		if pos > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last(pos,emitted_symbol)
		if pos < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid(pos,emitted_symbol)
			self._predict(pos)


	def _viterbi_routine(
						self,
						pos,
						emitted_symbol,
						):
		self._scan(pos,emitted_symbol)
		self._scan_viterbi(pos,emitted_symbol)
		if pos > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last(pos,emitted_symbol)
			self._complete_mid_to_last_viterbi(pos,emitted_symbol)
		if pos < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid(pos,emitted_symbol)
			self._complete_start_to_mid_viterbi(pos,emitted_symbol)

			self._predict(pos)
			self._predict_viterbi(pos)
		

	def _scan(
			self,
			pos,
			emitted_symbol,
			):
		"""
		The "scan" step of the inner algorithm.
		"""
		# Scanning
		self.log_inner_emit[pos,:,emitted_symbol,1]=self.log_inner_emit[pos-1,:,emitted_symbol,0]

	def _scan_viterbi(
			self,
			pos,
			emitted_symbol,
			):
		"""
		The "scan" step of the Viterbi algorithm.
		"""
		# Scanning
		self.log_viterbi_emit[pos,:,emitted_symbol,1]=self.log_viterbi_emit[pos-1,:,emitted_symbol,0]


	def _complete_mid_to_last(
						self,
						pos,
						emitted_symbol
					):
		"""
		Complete X->B.Y => X->BY.
		"""
		self.log_inner_branch[
						pos, # i
						pos-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.log_inner_branch[
										pos-1,	# j=i-1
										pos-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot pos
										]
							+
							self.log_inner_emit[ # Scanned above.
										pos,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										emitted_symbol,		# Symbol scanned above.
										1
										]
							)
		self.completed_log_inner_branch[pos,pos-2,:]=spm.logsumexp( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
												self.log_inner_branch[
														pos,	# i
														pos-2,		# j
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														2,		#Dot pos
														]
												,
												axis=(
													-2,	# Left RHS
													-1	# Right RHS
													)
												)
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k(
						pos,
						emitted_symbol,
						k
						)
			for k in reversed(xrange(pos-2)) # k=pos-3,...,0
		]

	def _complete_mid_to_last_viterbi(
						self,
						pos,
						emitted_symbol
					):
		"""
		Complete X->B.Y => X->BY for Viterbi probability.
		"""
		self.log_viterbi_branch[
						pos, # i
						pos-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.log_viterbi_branch[
										pos-1,	# j=i-1
										pos-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot pos
										]
							+
							self.log_viterbi_emit[ # Scanned above.
										pos,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										emitted_symbol,		# Symbol scanned above.
										1
										]
							)
		self.viterbi_predecessor[
						pos, # i
						pos-2, # k
						:, # LHS
						:, # RHS left
						:, # RHS right
						2, # dot pos
						0 # predecessor = branch (0) or emit (1)?
						] = 1
		self.viterbi_predecessor[
						pos, # i
						pos-2, # k
						:, # LHS
						:, # RHS left
						:, # RHS right
						2, # dot pos
						1 # j
						] = pos - 1
		self.viterbi_predecessor[
						pos, # i
						pos-2, # k
						:, # LHS
						:, # RHS left
						:, # RHS right
						2, # dot pos
						2 # emitted value
						] = emitted_symbol
		reshaped_log_viterbi_branch = self.log_viterbi_branch[
										pos,	# i
										pos-2,		# j
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										2,		#Dot pos
										].reshape(
											self.log_viterbi_branch.shape[2],
											self.log_viterbi_branch.shape[2]**2
											)
		max_ids = np.argmax( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
					reshaped_log_viterbi_branch
					,
					axis=(
						# -2,	# Left RHS
						-1	# Right RHS
						)
					)
		# max_ids = np.unravel_index(max_ids, (self.log_viterbi_branch.shape[2],)*3)
		self.completed_viterbi_branch[pos,pos-2,:] = reshaped_log_viterbi_branch[np.arange(self.log_viterbi_branch.shape[2]), max_ids]
		max_ids_left_right = np.unravel_index(max_ids, (self.log_viterbi_branch.shape[2],)*2)
		self.viterbi_predecessor_for_completed[pos,pos-2,:,0] = max_ids_left_right[0]
		self.viterbi_predecessor_for_completed[pos,pos-2,:,1] = max_ids_left_right[1]
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k_viterbi(
						pos,
						emitted_symbol,
						k
						)
			for k in reversed(xrange(pos-2)) # k=pos-3,...,0
		]

	def _complete_mid_to_last_per_k(
						self,
						pos,
						emitted_symbol,
						k
						):
		self.log_inner_branch[pos,k,:,:,:,2]=np.logaddexp(
										spm.logsumexp(
											self.log_inner_branch[
														k+1:pos-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot pos
														]
											+
											self.completed_log_inner_branch[
														pos,	# i
														k+1:pos-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=0 # over j
											)
										,
										(
											self.log_inner_branch[
														pos-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot pos
														]
											+
											self.log_inner_emit[ # Scanned above.
														pos,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														emitted_symbol,		# Symbol scanned above.
														1
														]
											)
										)
		self.completed_log_inner_branch[pos,k,:]=spm.logsumexp( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											self.log_inner_branch[
													pos,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot pos
													]
											,
											axis=(
												-2,	# Left RHS
												-1	# Right RHS
												)
											)


	def _complete_mid_to_last_per_k_viterbi(
						self,
						pos,
						emitted_symbol,
						k
						):
		viterbi_branch_x_completed = (self.log_viterbi_branch[
										k+1:pos-1,	# j
										k,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot pos
										]
									+
									self.completed_viterbi_branch[
												pos,	# i
												k+1:pos-1, # j
												np.newaxis, # LHS of the target.
												np.newaxis, # Left of the RHS.
												:, # Right of the RHS.
												]
									)
		reshaped_viterbi_branch_x_completed = viterbi_branch_x_completed.reshape(
													viterbi_branch_x_completed.shape[0],
													viterbi_branch_x_completed.shape[1]**3
												)
		branch_max_ids = np.argmax(
							reshaped_viterbi_branch_x_completed
							,
							axis=0 # over j
							)
		# branch_max_ids = np.unravel_index(branch_max_ids, viterbi_branch_x_completed.shape)
		max_viterbi_branch_x_completed = reshaped_viterbi_branch_x_completed[
												branch_max_ids,
												np.arange(reshaped_viterbi_branch_x_completed.shape[1])
												].reshape(
													viterbi_branch_x_completed.shape[1:]
													)

		viterbi_branch_x_emit = (
									self.log_viterbi_branch[
												pos-1,	# j=i-1
												k,		# k
												:,		# LHS
												:,		# Left RHS
												:,		# Right RHS
												1,		# dot pos
												]
									+
									self.log_viterbi_emit[ # Scanned above.
												pos,		# i
												np.newaxis,	# LHS
												np.newaxis,	# Left RHS
												:,			# Right RHS
												emitted_symbol,		# Symbol scanned above.
												1
												]
									)
		branch_vs_emit = np.array(
							(
								max_viterbi_branch_x_completed
								,
								viterbi_branch_x_emit
							)
							)
		reshaped_branch_vs_emit = branch_vs_emit.reshape(
											branch_vs_emit.shape[0],
											branch_vs_emit.shape[1]**3
										)
		branch_or_emit_ids = np.argmax(
									reshaped_branch_vs_emit
									,
									axis=0
								)
		# branch_or_emit_ids = np.unravel_index(branch_or_emit_ids, (2,)+viterbi_branch_x_emit.shape)

		self.log_viterbi_branch[pos,k,:,:,:,2]=reshaped_branch_vs_emit[
												branch_or_emit_ids,
												np.arange(reshaped_branch_vs_emit.shape[1]),
												].reshape(branch_vs_emit.shape[1:])
		branch_or_emit_ids = branch_or_emit_ids.reshape(branch_vs_emit.shape[1:])
		self.viterbi_predecessor[pos,k,:,:,:,2,0] = branch_or_emit_ids
		branch_js = np.arange(k+1,pos-1)[branch_max_ids]
		self.viterbi_predecessor[pos,k,:,:,:,2,1] = (
														branch_js.reshape(viterbi_branch_x_completed.shape[1:])
															* (branch_or_emit_ids==0)
														+
														(pos-1) * branch_or_emit_ids
													)
		branch_ids = np.tile(
						np.arange(viterbi_branch_x_completed.shape[1])
						,
						viterbi_branch_x_completed.shape[1]**2
						)
		self.viterbi_predecessor[pos,k,:,:,:,2,2] = (
														self.viterbi_predecessor_for_completed[
																pos,
																branch_js,
																branch_ids,
																0
																].reshape(branch_or_emit_ids.shape)
															* (branch_or_emit_ids==0)
														+
														emitted_symbol * branch_or_emit_ids
													)
		self.viterbi_predecessor[pos,k,:,:,:,2,3] = self.viterbi_predecessor_for_completed[
																pos,
																branch_js,
																branch_ids,
																1
																].reshape(branch_or_emit_ids.shape)
		reshaped_log_viterbi_branch = self.log_viterbi_branch[
													pos,	# i k,		# j
													k,
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot pos
													].reshape(
														self.log_viterbi_branch.shape[2],
														self.log_viterbi_branch.shape[2]**2
														)
		completed_max_ids = np.argmax( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											reshaped_log_viterbi_branch
											,
											axis=(
												# -2,	# Left RHS
												-1	# Right RHS
												)
											)
		# completed_max_ids = np.unravel_index(completed_max_ids, self.log_viterbi_branch[pos,k,:,:,:,2])
		self.completed_viterbi_branch[pos,k,:] = reshaped_log_viterbi_branch[np.arange(reshaped_log_viterbi_branch.shape[0]),completed_max_ids]
		max_ids_left_right = np.unravel_index(completed_max_ids, (self.log_viterbi_branch.shape[2],)*2)
		self.viterbi_predecessor_for_completed[pos,k,:,0] = max_ids_left_right[0]
		self.viterbi_predecessor_for_completed[pos,k,:,1] = max_ids_left_right[1]

	def _complete_start_to_mid(
						self,
						pos,
						emitted_symbol
					):
		"""
		Complete X->.YA => X->Y.A
		"""
		if pos>1: # No branch rule has been completed if pos=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(pos-1)
			self.log_inner_branch[
					pos, # i
					:pos-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.log_inner_branch[
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot pos
										]
							+
							self.completed_log_inner_branch[
										pos,	# i
										:pos-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.log_inner_branch[
				pos, # i
				pos-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.log_inner_branch[
									pos-1,	# j=i-1
									pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot pos
									]
						+
						self.log_inner_emit[ #Scanned above.
									pos,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									emitted_symbol,	# Symbol scanned above.
									1,
									]
						)

	def _complete_start_to_mid_viterbi(
						self,
						pos,
						emitted_symbol
					):
		"""
		Complete X->.YA => X->Y.A for Viterbi prob.
		"""
		if pos>1: # No branch rule has been completed if pos=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(pos-1)
			self.log_viterbi_branch[
					pos, # i
					:pos-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.log_viterbi_branch[
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot pos
										]
							+
							self.completed_viterbi_branch[
										pos,	# i
										:pos-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
			self.viterbi_predecessor[
					pos, # i
					:pos-1, # k
					:,
					:,
					:,
					1,
					0,
					] = 0
			num_non_terminals = self.viterbi_predecessor.shape[2]
			self.viterbi_predecessor[
					pos, # i
					:pos-1, # k
					:,
					:,
					:,
					1,
					1,
					] = j[:,np.newaxis,np.newaxis,np.newaxis]
			self.viterbi_predecessor[
					pos, # i
					:pos-1, # k
					:,
					:,
					:,
					1,
					2,
					] = self.viterbi_predecessor_for_completed[pos,:pos-1,np.newaxis,np.newaxis,:,0]
			self.viterbi_predecessor[
					pos, # i
					:pos-1, # k
					:,
					:,
					:,
					1,
					3,
					] = self.viterbi_predecessor_for_completed[pos,:pos-1,np.newaxis,np.newaxis,:,1]
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.log_viterbi_branch[
				pos, # i
				pos-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.log_viterbi_branch[
											pos-1,	# j=i-1
											pos-1,		# k
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											0,		# dot pos
											]
								+
								self.log_viterbi_emit[ #Scanned above.
											pos,		# i
											np.newaxis,	# LHS
											:,			# Left RHS
											np.newaxis,	# Right RHS
											emitted_symbol,	# Symbol scanned above.
											1,
											]
								)
		self.viterbi_predecessor[
				pos, # i
				pos-1, # k
				:,
				:,
				:,
				1,
				0
				] = 1
		self.viterbi_predecessor[
				pos, # i
				pos-1, # k
				:,
				:,
				:,
				1,
				1
				] = pos-1
		self.viterbi_predecessor[
				pos, # i
				pos-1, # k
				:,
				:,
				:,
				1,
				2
				] = emitted_symbol

		

	def _predict(
			self,
			pos
			):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		# Only i:k X->A.Z is introduced in the completion step and is expandable.
		self.log_inner_branch[pos,pos,:,:,:,0]=self.log_branch_rule_weights
		# self.string[pos] is the emitted_symbol to be scanned in the next iteration.
		next_emitted_symbol = self.string[pos]
		self.log_inner_emit[pos,:,next_emitted_symbol,0]=self.log_emission_rule_weights[:,next_emitted_symbol]


	def _predict_viterbi(
			self,
			pos
			):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		# Only i:k X->A.Z is introduced in the completion step and is expandable.
		self.log_viterbi_branch[pos,pos,:,:,:,0]=self.log_branch_rule_weights
		# self.string[pos] is the emitted_symbol to be scanned in the next iteration.
		self.log_viterbi_emit[pos,:,next_emitted_symbol,0]=self.log_emission_rule_weights[:,self.string[pos]]


	# ===============END OF FORWARD-INNER ALGORITHM===================






	# ==============START OF OUTER ALGORITHM==========================

	def _calc_outer_probs(self):
		"""
		Calculate outer probabilities.
		"""
		self.log_outer_branch=np.full(self.log_inner_branch.shape, -np.inf)
		self.log_outer_emit=np.full(self.log_inner_emit.shape, -np.inf)
		# Initialization
		# Reverse completion of i:j S.->A. where i=l and j=0.
		# By i:k . -> S. and j:k . -> .S where i=l and k=0.
		# This leaves the outer probability 1.
		# And then reverse completion of i:j A->BC. where i=l and j=0.
		# By i:j S.->A. and i:j
		self.log_outer_branch[
					-1,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
								-1,	# i
								0,	# j
								:,		# LHS
								:,		# Left RHS
								:,		# Right RHS
								2,		# dot pos
								]
						,
						self.log_init_weights[:,np.newaxis,np.newaxis]
						)
					
		# Reverse completion of i: jY->CD.
		# When j=0 (=> k=0), i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		# But for i=l(or -1), i: 0X->Y.A and 0: 0X->.YA are not introduced.
		[
			self._reverse_complete_last_to_another_last_per_j_init(j)
			for j in xrange(1,self.string_length-1) # -1: -2 Y->CD. is impossible!!
		]
		
		# Reverse completion of i: jY->a.
		emitted_symbol = self.string[-1]
		self.log_outer_emit[
					-1,	# i
					:,		# LHS
					emitted_symbol,	# emission
					1,		# do pos
					]=self._get_outer_x_inner_j_greater_k(-1, -2, 2, 1, (0, 1, 2))
		# Reverse completion of j: kX->B.Y k<=i-1
		self.log_outer_branch[
					:-1,	# j
					:-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]=spm.logsumexp(
						[
						self.log_outer_branch[
								:-1,	# j
								:-2,		# k
								:,		# LHS
								:,		# Left RHS
								:,		# Right RHS
								1,		# dot pos
								]
						,
						self.log_outer_branch[
									-1,	# i
									np.newaxis,	# j
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						,
						self.completed_log_inner_branch[
									-1,		#i
									:-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						]
						,
						axis=0
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.log_outer_branch[
					-2,	# j
					:-2,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]=spm.logsumexp(
						[
						self.log_outer_branch[
								-2,	# j
								:-2,	# k
								:,		# LHS
								:,		# Left RHS
								:,		# Right RHS
								1,		# dot pos
								]
						,
						self.log_outer_branch[
									-1,	# i
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						,
						self.log_inner_emit[
									-1,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									emitted_symbol,		# scanned emitted_symbol
									1,			# dot pos
									]
						]
						,
						axis=0
						)
		# Reverse completion of j: kX->.YA N/A b/c -1: 0X->Y.A is irrelevant.
		# Reverse scanning. Scaling must be changed.
		self.log_outer_emit[
					-2,	# i
					:,		# LHS
					emitted_symbol,	# Left RHS
					0,		# dot pos
					]=self.log_outer_emit[
							-1,	# i+1, k=i+1-1=i
							:,		# LHS
							emitted_symbol,		# Left RHS
							1		# dot pos
							] # dot_pos=1 (i.e. k=i-1)ではscaling facoters Ci が含まれていない。
		[
			(
				self._reverse_complete(
					pos,
					emitted_symbol
					)
				,
				self._reverse_scan(
					pos,
					emitted_symbol
					)
			)
			for pos,emitted_symbol in reversed(list(enumerate(self.string[:-1], start=1)))
		]


	def _get_outer_x_inner_j_greater_k(
					self,
					pos,
					j,
					outer_dot_pos,
					inner_dot_pos,
					marginalization_axis
					):
		"""
		Computes (outer probability) x (inner probability) when j > k.
		"""
		return spm.logsumexp(
						self.log_outer_branch[ # i:k X->BY.
									pos,	# i
									:j,		# k
									:,		# LHS
									:,		# Left RHS independent
									:,		# Right RHS
									outer_dot_pos,		# do pos
									]
						+
						self.log_inner_branch[ # j:k X->B.Y k<j
									j,	# j
									:j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do pos
									]
						,
						axis=marginalization_axis
					)


	def _get_outer_x_inner_j_eq_k(self, pos, j, outer_dot_pos, inner_dot_pos, marginalization_axis):
		"""
		Computes (outer probability) x (inner probability) when j = k.
		"""
		return spm.logsumexp(
						self.log_outer_branch[ # i:k X->Y.A
									pos,	# i
									j,	# k
									:,		# LHS
									:,		# Left RHS # Independent
									:,		# Right RHS
									outer_dot_pos,		# do pos
									]
						+
						self.log_inner_branch[ # j:k X->.YA j=k
									j,	# j <- jに依存している。
									j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do pos
									]
						,
						axis=marginalization_axis
					)

	def _reverse_complete_last_to_another_last_per_j_init(self, j):
		# Reverse completion of i: jY->CD. j<=i-2
		self.log_outer_branch[
				-1,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot pos
				]=self._get_outer_x_inner_j_greater_k(
						-1,
						j,
						2,
						1,
						(0,1,2)
						)[:,np.newaxis,np.newaxis]



	def _reverse_complete_last_to_another_last_per_j(self, pos, j):
		"""
		Reverse completion of i: jY->CD. given j.
		"""
		self.log_outer_branch[
				pos,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot pos
				]=np.logaddexp(
					self._get_outer_x_inner_j_eq_k(pos, j, 1, 0, (0,2))
					,
					self._get_outer_x_inner_j_greater_k(pos, j, 2, 1, (0,1,2))
					)[:,np.newaxis,np.newaxis]

	def _reverse_complete_last_to_another_last(self, pos):
		"""
		Reverse completion of i: jY->CD.
		"""
		# When j=0, k=0 as well.
		# i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		self.log_outer_branch[
					pos,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot pos
					]=self._get_outer_x_inner_j_eq_k(pos, 0, 1, 0, (0,2))[:,np.newaxis,np.newaxis]
		[self._reverse_complete_last_to_another_last_per_j(pos, j)
			for j in xrange(1,pos-1) # i: i-1 Y->CD. is impossible!!
		]



	def _reverse_complete_emission(self, pos, emitted_symbol):
		"""
		Reverse completion of i: jY->a.
		"""
		self.log_outer_emit[
					pos,	# i
					:,		# LHS
					emitted_symbol,		# Left RHS
					1,		# do pos
					]=np.logaddexp(
						self._get_outer_x_inner_j_eq_k(pos, pos-1, 1, 0, (0,2))
						,
						self._get_outer_x_inner_j_greater_k(pos, pos-1, 2, 1, (0,1,2))
						)

	def _reverse_complete_last_to_mid(self, pos, emitted_symbol):
		"""
		Reverse completion of j: kX->B.Y (note k<j)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->BY.
		self.log_outer_branch[
					:pos-1,	# j
					:pos-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
								:pos-1,	# j
								:pos-2,		# k
								:,		# LHS
								:,		# Left RHS
								:,		# Right RHS
								1,		# dot pos
								]
						,
						self.log_outer_branch[
									pos,	# i
									np.newaxis,	# j
									:pos-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						+
						self.completed_log_inner_branch[
									pos,		#i
									:pos-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.log_outer_branch[
					pos-1,	# j
					:pos-1,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
							pos-1,	# j
							:pos-1,	# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							1,		# dot pos
							]
						,
						self.log_outer_branch[
									pos,	# i
									:pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						+
						self.log_inner_emit[
									pos,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									emitted_symbol,		# scanned emitted_symbol
									1,			# dot pos
									]
						)

	def _reverse_complete_mid_to_start(self, pos, emitted_symbol):
		"""
		Reverse completion of j: kX->.YA (j=k is possible)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->Y.A
		self.log_outer_branch[
					:pos-1,	# j
					:pos-1,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
							:pos-1,	# j
							:pos-1,		# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							0,		# dot pos
							]
						,
						self.log_outer_branch[
									pos,	# i
									np.newaxis,	# j
									:pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot pos
									]
						+
						self.completed_log_inner_branch[
									pos,		# i
									:pos-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->Y.A
		self.log_outer_branch[
					pos-1,	# j
					:pos,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
							pos-1,	# j
							:pos,	# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							0,		# dot pos
							]
						,
						self.log_outer_branch[
									pos,	# i
									:pos,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot pos
									]
						+
						self.log_inner_emit[
									pos,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									:,			# Left RHS of branch (LHS of emission)
									np.newaxis,	# Right RHS of branch 
									emitted_symbol,		# scanned emitted_symbol
									1,			# dot pos
									]
						)

	def _reverse_complete(self, pos, emitted_symbol):
		"""
		Reverse operation of completion.
		"""
		self._reverse_complete_last_to_another_last(pos)
		self._reverse_complete_emission(pos, emitted_symbol)
		self._reverse_complete_last_to_mid(pos, emitted_symbol)
		self._reverse_complete_mid_to_start(pos, emitted_symbol)


	def _reverse_scan(self, pos, emitted_symbol):
		"""
		Reverse scanning.
		"""
		self.log_outer_emit[
					pos-1,	# i
					:,		# LHS
					emitted_symbol,	# Left RHS
					0,		# dot pos
					]=self.log_outer_emit[
							pos,	# i+1, k=i+1-1=i
							:,		# LHS
							emitted_symbol,		# Left RHS
							1,		# dot pos
							] # dot_pos=1 (i.e. k=i-1) hasn't scaled by scaling_factors[i].






# ===============================================================================
# ========================= More efficient algorthm below =======================
# ===============================================================================


def sort_data_by_length(data, max_batch_size=50):
	"""
	Sort data by string length.
	"""
	sorted_data = {}
	for string in data:
		string_length = len(string)
		if string_length in sorted_data.keys():
			batch_list = sorted_data[string_length]
			if batch_list[-1].shape[0] < max_batch_size:
				batch_list[-1] = np.append(
										batch_list[-1],
										np.array(string)[np.newaxis,:]
										,
										axis=0
										)
			else:
				batch_list.append(np.array(string)[np.newaxis,:])
		else:
			sorted_data[string_length] = [np.array(string)[np.newaxis,:]]
	return sorted_data



class Inner_Outer_Algorithm_Sort_By_Length(Inner_Outer_Algorithm):
	"""
	Stolcke's (1995) inner-outer algorithm for
	a fully connected PCFG in a Chomsky normal form.
	It simultaneously computes likelihoods of strings of the same length,
	and expected counts of rule uses to generate the strings.
	"""

	def get_log_likelihoods(self, string_batch):
		"""
		Get log likelihoods of string_batch without counting expected rule use.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1:
			return spm.logsumexp(
						self.log_init_weights[:,np.newaxis]
						+
						self.log_emission_rule_weights[:,string_batch[:,0]]
						,
						axis=0
						).transpose()
		else:
			self._calc_inner_probs()
			return spm.logsumexp(
								self.log_init_weights[np.newaxis,:]
								+
								spm.logsumexp(self.log_inner_branch[:,-1,0,:,:,:,2], axis=(2,3))
								,
								axis=0
								)


	def get_expected_rule_counts(self, string_batch):
		"""
		Returns expected # of use of each rule,
		together with log likelihoods of strings.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1: # No branching for strings of length 1
			expected_log_init_weights = self.log_init_weights[np.newaxis,:]+self.log_emission_rule_weights[:,string_batch[:,0]].transpose()
			log_like = spm.logsumexp(expected_log_init_weights, axis=-1)
			expected_log_init_weights -= log_like[:,np.newaxis]
			expected_branch_rule_counts = np.zeros((self.string_ids.size,)+self.log_branch_rule_weights.shape)
			expected_emission_rule_counts = np.zeros((self.string_ids.size,)+self.log_emission_rule_weights.shape)
			expected_emission_rule_counts[self.string_ids,:,string_batch[:,0]] += np.exp(expected_log_init_weights)
		else:
			self._calc_inner_probs()
			self._calc_outer_probs()
			log_likelihoods_for_each_root_non_terminal=(
										spm.logsumexp(self.log_inner_branch[:,-1,0,:,:,:,2], axis=(2,3))
										+
										self.log_init_weights[np.newaxis,:]
										)
			log_like=spm.logsumexp(
									log_likelihoods_for_each_root_non_terminal
									,
									axis=-1
								)
			expected_branch_rule_counts=(
											spm.logsumexp(
												self.log_outer_branch[...,0].diagonal(axis1=1, axis2=2) # Scaled.
												+
												self.log_inner_branch[...,0].diagonal(axis1=1, axis2=2) # Not scaled.
												,
												axis=-1 # diagonalized dimension comes last.
												) # num_strings x num_nt x num_nt x num_nt
											-
											log_like[:,np.newaxis,np.newaxis,np.newaxis]
											)
			
			expected_emission_rule_counts=(
										spm.logsumexp(
											self.log_outer_emit[...,0] # Scaled.
											+
											self.log_inner_emit[...,0] # Not scaled.
											,
											axis=1
											) # num_strings x num_nt x num_symbols
										-
										log_like[:,np.newaxis,np.newaxis]
										)
			expected_log_init_weights = (
									log_likelihoods_for_each_root_non_terminal
									-
									log_like[:,np.newaxis]
									)
		return expected_branch_rule_counts, expected_emission_rule_counts, expected_log_init_weights, log_like

	def _set_strings(self, string_batch):
		"""
		(Re)set emitted strings.
		Strings of the same length are assumed to be sorted.
		"""
		self.string_batch = string_batch
		self.string_length = self.string_batch.shape[1]
		self.string_ids = np.arange(self.string_batch.shape[0])



	def _calc_inner_probs(self):
		"""
		Calculates inner probabilities.
		Forward probabilities are omitted
		because they are not used for obtaining expected counts of rules.
		Underflow is avoided by logsumexp.
		"""
		self.log_inner_branch=np.full(
							(
								self.string_ids.size, # string id (among the same length)
								self.string_length+1, # penultimate pos i
								self.string_length+1, # start pos k
								self.log_branch_rule_weights.shape[0], # LHS non-terminal
								self.log_branch_rule_weights.shape[1], # Left RHS non-terminal
								self.log_branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot pos
							)
							,
							-np.inf
							)
		self.log_inner_emit=np.full(
							(
							self.string_ids.size, # string id (among the same length)
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.log_emission_rule_weights.shape[0], # LHS non-terminal
							self.log_emission_rule_weights.shape[1], # Emitted emitted_symbols
							2 # dot pos
							)
							,
							-np.inf
							)
		
		# Initial prediction
		
		self.log_inner_branch[:,0,0,:,:,:,0]=self.log_branch_rule_weights[np.newaxis,:,:,:]

		emitted_symbols = self.string_batch[:,0]

		self.log_inner_emit[self.string_ids,0,:,emitted_symbols,0] = self.log_emission_rule_weights[:,emitted_symbols].transpose()
		self.completed_log_inner_branch=np.full( # To be reused.
											(
											self.string_ids.size,
											self.string_length+1, # i
											self.string_length+1, # j
											self.log_branch_rule_weights.shape[0],
											)
											,
											-np.inf
											)
		[
			(
				self._inner_routine(
						pos,
						emitted_symbols
						)
			)
			for 
			pos,
			emitted_symbols
			in enumerate(self.string_batch.transpose(), start=1) # for i=1....
		]



	def _inner_routine(
						self,
						pos,
						emitted_symbols,
						):
		self._scan(pos,emitted_symbols)
		if pos > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last(pos,emitted_symbols)
		if pos < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid(pos,emitted_symbols)
			self._predict(pos)


	def _scan(
			self,
			pos,
			emitted_symbols,
			):
		"""
		The "scan" step of the forward/inner algorithm.
		Luong et. al. (2013) scale forward and inner probabilities at this scanning step.
		In this algorithm, on the other hand, the scaling is done at the prediction step instead.
		Also scale foward and inner emission probabilities by (already scaled) prefix probability.
		"""
		# Scanning
		self.log_inner_emit[self.string_ids,pos,:,emitted_symbols,1]=self.log_inner_emit[self.string_ids,pos-1,:,emitted_symbols,0]


	def _complete_mid_to_last(
						self,
						pos,
						emitted_symbols
					):
		"""
		Complete X->B.Y => X->BY.
		"""
		self.log_inner_branch[
						:,
						pos, # i
						pos-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.log_inner_branch[
										:,		# string id
										pos-1,	# j=i-1
										pos-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot pos
										]
							+
							self.log_inner_emit[ # Scanned above.
										self.string_ids,
										pos,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										emitted_symbols,		# Symbol scanned above.
										1
										]
							)
		self.completed_log_inner_branch[:,pos,pos-2,:]=spm.logsumexp( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
												self.log_inner_branch[
														:,		# string id
														pos,	# i
														pos-2,		# j
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														2,		#Dot pos
														]
												,
												axis=(
													2,	# Left RHS
													3	# Right RHS
													)
												)
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k(
						pos,
						emitted_symbols,
						k
						)
			for k in reversed(xrange(pos-2)) # k=pos-3,...,0
		]

	def _complete_mid_to_last_per_k(
						self,
						pos,
						emitted_symbols,
						k
						):
		self.log_inner_branch[:,pos,k,:,:,:,2]=np.logaddexp(
										spm.logsumexp(
											self.log_inner_branch[
														:,		# string id
														k+1:pos-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot pos
														]
											+
											self.completed_log_inner_branch[
														:,		# string id
														pos,	# i
														k+1:pos-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										,
										(
											self.log_inner_branch[
														:,		# string id
														pos-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot pos
														]
											+
											self.log_inner_emit[ # Scanned above.
														self.string_ids,	 # string id
														pos,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														emitted_symbols,		# Symbol scanned above.
														1
														]
											)
										)
		self.completed_log_inner_branch[:,pos,k,:]=spm.logsumexp( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											self.log_inner_branch[
													:,		# string id
													pos,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot pos
													]
											,
											axis=(
												2,	# Left RHS
												3	# Right RHS
												)
											)

	def _complete_start_to_mid(
						self,
						pos,
						emitted_symbols
					):
		"""
		Complete X->.YA => X->Y.A
		"""
		if pos>1: # No branch rule has been completed if pos=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(pos-1)
			self.log_inner_branch[
					:,	# string id
					pos, # i
					:pos-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.log_inner_branch[
										:,	# string id
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot pos
										].transpose((1,0,2,3,4))
							+
							self.completed_log_inner_branch[
										:,		# string id
										pos,	# i
										:pos-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.log_inner_branch[
				:,
				pos, # i
				pos-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.log_inner_branch[
									:,		# string id
									pos-1,	# j=i-1
									pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot pos
									]
						+
						self.log_inner_emit[ #Scanned above.
									self.string_ids,# string id
									pos,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									emitted_symbols,	# Symbol scanned above.
									1,
									]
						)


		

	def _predict(
			self,
			pos
			):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		self.log_inner_branch[:,pos,pos,:,:,:,0]=self.log_branch_rule_weights[np.newaxis,:,:,:]
		# self.string_batch[pos] is the emitted_symbols to be scanned in the next iteration.
		next_emitted_symbols = self.string_batch[:,pos]
		self.log_inner_emit[self.string_ids,pos,:,next_emitted_symbols,0]=self.log_emission_rule_weights[:,next_emitted_symbols].transpose()


	# ===============END OF FORWARD-INNER ALGORITHM===================






	# ==============START OF OUTER ALGORITHM==========================

	def _calc_outer_probs(self):
		"""
		Calculate outer probabilities.
		"""
		self.log_outer_branch=np.full(self.log_inner_branch.shape, -np.inf)
		self.log_outer_emit=np.full(self.log_inner_emit.shape, -np.inf)
		# Initialization
		# Reverse completion of i:j S.->A. where i=l and j=0.
		# By i:k . -> S. and j:k . -> .S where i=l and k=0.
		# This leaves the outer probability 1.
		# And then reverse completion of i:j A->BC. where i=l and j=0.
		# By i:j S.->A. and i:j
		self.log_outer_branch[
					:, # string id
					-1,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
							:, # string id
							-1,	# i
							0,	# j
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							2,		# dot pos
							]
						,
						self.log_init_weights[np.newaxis,:,np.newaxis,np.newaxis]
						)
					
		# Reverse completion of i: jY->CD.
		# When j=0 (=> k=0), i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		# But for i=l(or -1), i: 0X->Y.A and 0: 0X->.YA are not introduced.
		[
			self._reverse_complete_last_to_another_last_per_j_init(j)
			for j in xrange(1,self.string_length-1) # -1: -2 Y->CD. is impossible!!
		]
		
		# Reverse completion of i: jY->a.
		emitted_symbols = self.string_batch[:,-1]
		self.log_outer_emit[
					self.string_ids,
					-1,	# i
					:,		# LHS
					emitted_symbols,	# emission
					1,		# do pos
					]=self._get_outer_x_inner_j_greater_k(-1, -2, 2, 1, (1, 2, 3))
		# Reverse completion of j: kX->B.Y k<=i-1
		self.log_outer_branch[
					:,		# string id
					:-1,	# j
					:-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
							:,		# string id
							:-1,	# j
							:-2,		# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							1,		# dot pos
							]
						,
						self.log_outer_branch[
									:,	# string id
									-1,	# i
									np.newaxis,	# j
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						+
						self.completed_log_inner_branch[
									:,		# string id
									-1,		#i
									:-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.log_outer_branch[
					:,	# string id
					-2,	# j
					:-2,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
							:,	# string id
							-2,	# j
							:-2,	# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							1,		# dot pos
							]
						,
						self.log_outer_branch[
									:,	# string id
									-1,	# i
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						+
						self.log_inner_emit[
									self.string_ids,
									-1,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									emitted_symbols,		# scanned emitted_symbols
									1,			# dot pos
									]
						)
		# Reverse completion of j: kX->.YA N/A b/c -1: 0X->Y.A is irrelevant.
		# Reverse scanning. Scaling must be changed.
		self.log_outer_emit[
					self.string_ids,
					-2,	# i
					:,		# LHS
					emitted_symbols,	# Left RHS
					0,		# dot pos
					]=self.log_outer_emit[
							self.string_ids, # string id
							-1,	# i+1, k=i+1-1=i
							:,		# LHS
							emitted_symbols,		# Left RHS
							1		# dot pos
							] # dot_pos=1 (i.e. k=i-1)ではscaling facoters Ci が含まれていない。
		[
			(
				self._reverse_complete(
					pos,
					emitted_symbols
					)
				,
				self._reverse_scan(
					pos,
					emitted_symbols
					)
			)
			for pos,emitted_symbols in reversed(list(enumerate(self.string_batch[:,:-1].transpose(), start=1)))
		]

	def _get_outer_x_inner_j_geq_k(
					self,
					pos,
					j,
					outer_dot_pos,
					inner_dot_pos,
					marginalization_axis
					):
		"""
		Computes (outer probability) x (inner probability) when j > k.
		"""
		return spm.logsumexp(
						self.log_outer_branch[ # i:k X->BY.
									:,		# string id
									pos,	# i
									:j+1,		# k
									:,		# LHS
									:,		# Left RHS independent
									:,		# Right RHS
									outer_dot_pos,		# do pos
									]
						+
						self.log_inner_branch[ # j:k X->B.Y k<j
									:,	# string id
									j,	# j
									:j+1,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do pos
									]
						,
						axis=marginalization_axis
					)

	def _get_outer_x_inner_j_greater_k(
					self,
					pos,
					j,
					outer_dot_pos,
					inner_dot_pos,
					marginalization_axis
					):
		"""
		Computes (outer probability) x (inner probability) when j > k.
		"""
		return spm.logsumexp(
						self.log_outer_branch[ # i:k X->BY.
									:,		# string id
									pos,	# i
									:j,		# k
									:,		# LHS
									:,		# Left RHS independent
									:,		# Right RHS
									outer_dot_pos,		# do pos
									]
						+
						self.log_inner_branch[ # j:k X->B.Y k<j
									:,	# string id
									j,	# j
									:j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do pos
									]
						,
						axis=marginalization_axis
					)


	def _get_outer_x_inner_j_eq_k(self, pos, j, outer_dot_pos, inner_dot_pos, marginalization_axis):
		"""
		Computes (outer probability) x (inner probability) when j = k.
		"""
		return spm.logsumexp(
						self.log_outer_branch[ # i:k X->Y.A
									:,		# string id
									pos,	# i
									j,	# k
									:,		# LHS
									:,		# Left RHS # Independent
									:,		# Right RHS
									outer_dot_pos,		# do pos
									]
						+
						self.log_inner_branch[ # j:k X->.YA j=k
									:,	# string id
									j,	# j <- jに依存している。
									j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do pos
									]
						,
						axis=marginalization_axis
					)

	def _reverse_complete_last_to_another_last_per_j_init(self, j):
		# Reverse completion of i: jY->CD. j<=i-2
		self.log_outer_branch[
				:,	# string id
				-1,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot pos
				]=self._get_outer_x_inner_j_greater_k(
						-1,
						j,
						2,
						1,
						(1,2,3)
						)[:,:,np.newaxis,np.newaxis]



	def _reverse_complete_last_to_another_last_per_j(self, pos, j):
		"""
		Reverse completion of i: jY->CD. given j.
		"""
		self.log_outer_branch[
				:,	# string id
				pos,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot pos
				]=self._get_outer_x_inner_j_geq_k(pos, j, 2, 1, (1,2,3))[:,:,np.newaxis,np.newaxis]
					# np.logaddexp(
					# self._get_outer_x_inner_j_eq_k(pos, j, 1, 0, (1,3))
					# ,
					# self._get_outer_x_inner_j_greater_k(pos, j, 2, 1, (1,2,3))
					# )[:,:,np.newaxis,np.newaxis]

	def _reverse_complete_last_to_another_last(self, pos):
		"""
		Reverse completion of i: jY->CD.
		"""
		# When j=0, k=0 as well.
		# i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		self.log_outer_branch[
					:,		# string id
					pos,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot pos
					]=self._get_outer_x_inner_j_eq_k(pos, 0, 1, 0, (1,3))[:,:,np.newaxis,np.newaxis]
		[self._reverse_complete_last_to_another_last_per_j(pos, j)
			for j in xrange(1,pos-1) # i: i-1 Y->CD. is impossible!!
		]



	def _reverse_complete_emission(self, pos, emitted_symbols):
		"""
		Reverse completion of i: jY->a.
		"""
		self.log_outer_emit[
					self.string_ids, # string id
					pos,	# i
					:,		# LHS
					emitted_symbols,		# Left RHS
					1,		# do pos
					]=self._get_outer_x_inner_j_geq_k(pos, pos-1, 2, 1, (1,2,3))
						# spm.logsumexp(
						# self._get_outer_x_inner_j_eq_k(pos, pos-1, 1, 0, (1,3))
						# ,
						# self._get_outer_x_inner_j_greater_k(pos, pos-1, 2, 1, (1,2,3))
						# )

	def _reverse_complete_last_to_mid(self, pos, emitted_symbols):
		"""
		Reverse completion of j: kX->B.Y (note k<j)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->BY.
		self.log_outer_branch[
					:,		# string id
					:pos-1,	# j
					:pos-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
							:,		# string id
							:pos-1,	# j
							:pos-2,		# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							1,		# dot pos
							]
						,
						self.log_outer_branch[
									:,		# string id
									pos,	# i
									np.newaxis,	# j
									:pos-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						+
						self.completed_log_inner_branch[
									:,			# string id
									pos,		#i
									:pos-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.log_outer_branch[
					:,		# string id
					pos-1,	# j
					:pos-1,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
							:,		# string id
							pos-1,	# j
							:pos-1,	# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							1,		# dot pos
							]
						,
						self.log_outer_branch[
									:,		# string id
									pos,	# i
									:pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						+
						self.log_inner_emit[
									self.string_ids,	 # string id
									pos,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									emitted_symbols,		# scanned emitted_symbols
									1,			# dot pos
									]
						)

	def _reverse_complete_mid_to_start(self, pos, emitted_symbols):
		"""
		Reverse completion of j: kX->.YA (j=k is possible)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->Y.A
		self.log_outer_branch[
					:,		# string id
					:pos-1,	# j
					:pos-1,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
							:,		# string id
							:pos-1,	# j
							:pos-1,		# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							0,		# dot pos
							]
						,
						self.log_outer_branch[
									:,		# string id 
									pos,	# i
									np.newaxis,	# j
									:pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot pos
									]
						+
						self.completed_log_inner_branch[
									:,			# string id
									pos,		# i
									:pos-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->Y.A
		self.log_outer_branch[
					:,
					pos-1,	# j
					:pos,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
							:,
							pos-1,	# j
							:pos,	# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							0,		# dot pos
							]
						,
						self.log_outer_branch[
									:,		# string id
									pos,	# i
									:pos,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot pos
									]
						+
						self.log_inner_emit[
									self.string_ids,	# string id
									pos,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									:,			# Left RHS of branch (LHS of emission)
									np.newaxis,	# Right RHS of branch 
									emitted_symbols,		# scanned emitted_symbols
									1,			# dot pos
									]
						)

	def _reverse_complete(self, pos, emitted_symbols):
		"""
		Reverse operation of completion.
		"""
		self._reverse_complete_last_to_another_last(pos)
		self._reverse_complete_emission(pos, emitted_symbols)
		self._reverse_complete_last_to_mid(pos, emitted_symbols)
		self._reverse_complete_mid_to_start(pos, emitted_symbols)


	def _reverse_scan(self, pos, emitted_symbols):
		"""
		Reverse scanning (just scaling).
		"""
		self.log_outer_emit[
					self.string_ids, # string id
					pos-1,	# i
					:,		# LHS
					emitted_symbols,	# Left RHS
					0,		# dot pos
					]=self.log_outer_emit[
							self.string_ids, # string id
							pos,	# i+1, k=i+1-1=i
							:,		# LHS
							emitted_symbols,		# Left RHS
							1,		# dot pos
							] # dot_pos=1 (i.e. k=i-1) hasn't scaled by scaling_factors[i].




# ===============================================================================
# ========================= Generalized emission below =======================
# ===============================================================================


def sort_data_by_length_multivariate_values(data, max_batch_size=50):
	"""
	Sort multivariate data by string length.
	"""
	sorted_data = {}
	for string in data:
		string_length = len(string)
		if string_length in sorted_data.keys():
			batch_list = sorted_data[string_length]
			if batch_list[-1].shape[0] < max_batch_size:
				batch_list[-1] = np.append(
										batch_list[-1],
										np.array(string)[np.newaxis,:,:]
										,
										axis=0
										)
			else:
				batch_list.append(np.array(string)[np.newaxis,:,:])
		else:
			sorted_data[string_length] = [np.array(string)[np.newaxis,:,:]]
	return sorted_data




class Inner_Outer_Algorithm_Multivariate_Emission_Sort_By_Length(Inner_Outer_Algorithm_Sort_By_Length):
	"""
	Stolcke's (1995) inner-outer algorithm for
	a fully connected PCFG in a Chomsky normal form with general multivariate emission.
	Emission probability distributions (emission_distributions)
	must be defined and passed to the object constructor instead of log_emission_rule_weights.
	It simultaneously computes likelihoods of strings of the same length,
	and expected counts of rule uses to generate the strings.
	"""

	def __init__(
			self,
			branch_rule_weights,
			emission_distributions,
			init_weights,
			):
		"""
		Initialize rule weights.
		"""
		self.log_branch_rule_weights = np.log(branch_rule_weights)
		self.emission_distributions = emission_distributions
		self.log_init_weights = np.log(init_weights)

	def get_log_likelihoods(self, string_batch):
		"""
		Get log likelihoods of string_batch without counting expected rule use.
		CURRENTLY UNSUPPORTED.
		"""
		pass
		# self._set_strings(string_batch)
		# if self.string_length == 1:
		# 	return spm.logsumexp(
		# 				np.log(self.log_init_weights)[:,np.newaxis]
		# 				+
		# 				np.log(self.emission_distributions[:,string_batch[:,0]])
		# 				,
		# 				axis=0
		# 				).transpose()
		# else:
		# 	self._calc_inner_probs()
		# 	return spm.logsumexp(
		# 						np.log(self.log_init_weights)[np.newaxis,:]
		# 						+
		# 						np.log(np.sum(self.log_inner_branch[:,-1,0,:,:,:,2], axis=(2,3)))
		# 						-
		# 						np.sum(np.log(self.log_scaling_factors), axis=1)[:,np.newaxis]
		# 						,
		# 						axis=0
		# 						)


	def get_expected_rule_counts(self, string_batch):
		"""
		Returns expected # of use of each rule,
		together with log likelihoods of strings.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1: # No branching for strings of length 1
			log_expected_init_weights = self.log_init_weights[np.newaxis,:]+self.emission_distributions.get_log_prob(string_batch[:,0])
			log_like = spm.logsumexp(log_expected_init_weights, axis=-1)
			expected_init_weights = np.exp(log_expected_init_weights-log_like[:,np.newaxis])
			expected_branch_rule_counts = np.zeros((self.num_strings,)+self.log_branch_rule_weights.shape)
			expected_emission_counts_per_pos = expected_init_weights[:,np.newaxis,:]
		else:
			self._calc_inner_probs()
			self._calc_outer_probs()
			log_likelihoods_for_each_root_non_terminal=(
										spm.logsumexp(self.log_inner_branch[:,-1,0,:,:,:,2], axis=(2,3))
										+
										self.log_init_weights[np.newaxis,:]
										)
			log_like=spm.logsumexp(
									log_likelihoods_for_each_root_non_terminal
									,
									axis=-1
								)
			expected_branch_rule_counts=(
											spm.logsumexp(
												self.log_outer_branch[...,0].diagonal(axis1=1, axis2=2) # Scaled.
												+
												self.log_inner_branch[...,0].diagonal(axis1=1, axis2=2) # Not scaled.
												,
												axis=-1 # diagonalized dimension comes last.
												) # num_strings x num_nt x num_nt x num_nt
											-
											log_like[:,np.newaxis,np.newaxis,np.newaxis]
											)
			expected_emission_counts_per_pos=(
										(
											self.log_outer_emit[...,0] # Scaled.
											+
											self.log_inner_emit[...,0] # Not scaled.
											) # num_strings x (string_length + 1) x num_nt
										-
										log_like[:,np.newaxis,np.newaxis]
										)
			expected_init_weights = (
									log_likelihoods_for_each_root_non_terminal
									-
									log_like[:,np.newaxis]
									)
		return expected_branch_rule_counts, expected_emission_counts_per_pos, expected_init_weights, log_like

	def _set_strings(self, string_batch):
		"""
		(Re)set emitted strings.
		Strings of the same length are assumed to be sorted.
		"""
		self.string_batch = string_batch
		self.string_length = self.string_batch.shape[1]
		self.num_strings = self.string_batch.shape[0]



	def _calc_inner_probs(self):
		"""
		Calculates inner probabilities.
		Forward probabilities are omitted
		because they are not used for obtaining expected counts of rules.
		Underflow is avoided by logsumexp.
		"""
		self.log_inner_branch=np.full(
							(
								self.num_strings, # string id (among the same length)
								self.string_length+1, # penultimate pos i
								self.string_length+1, # start pos k
								self.log_branch_rule_weights.shape[0], # LHS non-terminal
								self.log_branch_rule_weights.shape[1], # Left RHS non-terminal
								self.log_branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot pos
							)
							,
							-np.inf
							)
		self.log_inner_emit=np.full(
							(
							self.num_strings, # string id (among the same length)
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.log_branch_rule_weights.shape[0], # LHS non-terminal
							2 # dot pos
							)
							,
							-np.inf
							)
		
		# Initial prediction
		self.log_inner_branch[:,0,0,:,:,:,0]=self.log_branch_rule_weights[np.newaxis,:,:,:]

		self.log_inner_emit[:,0,:,0] = self.emission_distributions.get_log_prob(self.string_batch[:,0])
		
		self.completed_inner_branch=np.full( # To be reused.
											(
											self.num_strings,
											self.string_length+1, # i
											self.string_length+1, # j
											self.log_branch_rule_weights.shape[0],
											)
											,
											-np.inf
											)
		[
			(
				self._inner_routine(
						pos
						)
			)
			for 
			pos
			in xrange(1,self.string_length+1) # for i=1....
		]
		



	def _inner_routine(
						self,
						pos,
						):
		self._scan(pos)
		if pos > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last(pos)
		if pos < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid(pos)
			self._predict(pos)


	def _scan(
			self,
			pos
			):
		"""
		The "scan" step of the inner algorithm.
		"""

		# Scanning
		self.log_inner_emit[:,pos,:,1] = self.log_inner_emit[:,pos-1,:,0]


	def _complete_mid_to_last(
						self,
						pos
					):
		"""
		Complete X->B.Y => X->BY.
		"""
		self.log_inner_branch[
						:,
						pos, # i
						pos-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.log_inner_branch[
										:,		# string id
										pos-1,	# j=i-1
										pos-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot pos
										]
							+
							self.log_inner_emit[ # Scanned above.
										:,
										pos,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										1
										]
							)
		self.completed_inner_branch[:,pos,pos-2,:]=spm.logsumexp( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
												self.log_inner_branch[
														:,		# string id
														pos,	# i
														pos-2,		# j
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														2,		#Dot pos
														]
												,
												axis=(
													2,	# Left RHS
													3	# Right RHS
													)
												)
		print self.completed_inner_branch[:,pos,pos-2,:]
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k(
						pos,
						k
						)
			for k in reversed(xrange(pos-2)) # k=pos-3,...,0
		]

	def _complete_mid_to_last_per_k(
						self,
						pos,
						k
						):
		self.log_inner_branch[:,pos,k,:,:,:,2]=np.logaddexp(
										spm.logsumexp(
											self.log_inner_branch[
														:,		# string id
														k+1:pos-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot pos
														]
											+
											self.completed_inner_branch[
														:,		# string id
														pos,	# i
														k+1:pos-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										,
										(
											self.log_inner_branch[
														:,		# string id
														pos-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot pos
														]
											+
											self.log_inner_emit[ # Scanned above.
														:,	 # string id
														pos,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														1
														]
											)
										)
		self.completed_inner_branch[:,pos,k,:]=spm.logsumexp( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											self.log_inner_branch[
													:,		# string id
													pos,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot pos
													]
											,
											axis=(
												2,	# Left RHS
												3	# Right RHS
												)
											)

	def _complete_start_to_mid(
						self,
						pos
					):
		"""
		Complete X->.YA => X->Y.A
		"""
		if pos>1: # No branch rule has been completed if pos=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(pos-1)
			self.log_inner_branch[
					:,	# string id
					pos, # i
					:pos-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.log_inner_branch[
										:,	# string id
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot pos
										].transpose((1,0,2,3,4))
							+
							self.completed_inner_branch[
										:,		# string id
										pos,	# i
										:pos-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.log_inner_branch[
				:,
				pos, # i
				pos-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.log_inner_branch[
									:,		# string id
									pos-1,	# j=i-1
									pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot pos
									]
						+
						self.log_inner_emit[ #Scanned above.
									:,# string id
									pos,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									1,
									]
						)


		

	def _predict(
			self,
			pos
			):
		"""
		The "prediction" step of the forward/inner algorithm.
		We also scale the inner and forward probabilities of emission
		at this step.
		"""
		# Prediction
		# Only i:k X->A.Z is introduced in the completion step and is expandable.
		self.log_inner_branch[:,pos,pos,:,:,:,0]=self.log_branch_rule_weights[np.newaxis,:,:,:]
		
		# self.string_batch[pos] is the emited_values to be scanned in the next iteration.
		self.log_inner_emit[:,pos,:,0] = self.emission_distributions.get_log_prob(self.string_batch[:,pos])




	# ===============END OF FORWARD-INNER ALGORITHM===================






	# ==============START OF OUTER ALGORITHM==========================

	def _calc_outer_probs(self):
		"""
		Calculate outer probabilities.
		"""
		self.log_outer_branch = np.zeros(self.log_inner_branch.shape)
		self.log_outer_emit = np.full(self.log_inner_emit.shape, -np.inf)
		# Initialization
		# Reverse completion of i:j S.->A. where i=l and j=0.
		# By i:k . -> S. and j:k . -> .S where i=l and k=0.
		# This leaves the outer probability 1.
		# And then reverse completion of i:j A->BC. where i=l and j=0.
		# By i:j S.->A. and i:j
		self.log_outer_branch[
					:, # string id
					-1,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot pos
					] = self.log_init_weights[np.newaxis,:,np.newaxis,np.newaxis]
					
		# Reverse completion of i: jY->CD.
		# When j=0 (=> k=0), i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		# But for i=l(or -1), i: 0X->Y.A and 0: 0X->.YA are not introduced.
		[
			self._reverse_complete_last_to_another_last_per_j_init(j)
			for j in xrange(1,self.string_length-1) # -1: -2 Y->CD. is impossible!!
		]
		
		# Reverse completion of i: jY->a.
		self.log_outer_emit[
					:,
					-1,	# i
					:,		# LHS
					1,		# do pos
					] = self._get_outer_x_inner_j_greater_k(-1, -2, 2, 1, (1, 2, 3))
		# Reverse completion of j: kX->B.Y k<=i-1
		self.log_outer_branch[
					:,		# string id
					:-1,	# j
					:-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					] = (
						self.log_outer_branch[
									:,	# string id
									-1,	# i
									np.newaxis,	# j
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						+
						self.completed_inner_branch[
									:,		# string id
									-1,		#i
									:-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.log_outer_branch[
					:,	# string id
					-2,	# j
					:-2,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					] = np.logaddexp(
						self.log_outer_branch[
							:,	# string id
							-2,	# j
							:-2,	# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							1,		# dot pos
							]
						,
						self.log_outer_branch[
									:,	# string id
									-1,	# i
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						+
						self.log_inner_emit[
									:,
									-1,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									1,			# dot pos
									]
						)
		# Reverse completion of j: kX->.YA N/A b/c -1: 0X->Y.A is irrelevant.
		# Reverse scanning. Scaling must be changed.
		self.log_outer_emit[
					:,
					-2,	# i
					:,		# LHS
					0,		# dot pos
					] = self.log_outer_emit[
							:, # string id
							-1,	# i+1, k=i+1-1=i
							:,		# LHS
							1		# dot pos
							] # dot_pos=1 (i.e. k=i-1)ではscaling facoters Ci が含まれていない。
		[
			(
				self._reverse_complete(
					pos
					)
				,
				self._reverse_scan(
					pos
					)
			)
			for pos in xrange(self.string_length-1,0,-1)
		]


	def _get_outer_x_inner_j_greater_k(
					self,
					pos,
					j,
					outer_dot_pos,
					inner_dot_pos,
					marginalization_axis
					):
		"""
		Computes (outer probability) x (inner probability) when j > k.
		"""
		return spm.logsumexp(
						self.log_outer_branch[ # i:k X->BY.
									:,		# string id
									pos,	# i
									:j,		# k
									:,		# LHS
									:,		# Left RHS independent
									:,		# Right RHS
									outer_dot_pos,		# do pos
									]
						+
						self.log_inner_branch[ # j:k X->B.Y k<j
									:,	# string id
									j,	# j
									:j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do pos
									]
						,
						axis=marginalization_axis
					)


	def _get_outer_x_inner_j_eq_k(self, pos, j, outer_dot_pos, inner_dot_pos, marginalization_axis):
		"""
		Computes (outer probability) x (inner probability) when j = k.
		"""
		return spm.logsumexp(
						self.log_outer_branch[ # i:k X->Y.A
									:,		# string id
									pos,	# i
									j,	# k
									:,		# LHS
									:,		# Left RHS # Independent
									:,		# Right RHS
									outer_dot_pos,		# do pos
									]
						+
						self.log_inner_branch[ # j:k X->.YA j=k
									:,	# string id
									j,	# j <- jに依存している。
									j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do pos
									]
						,
						axis=marginalization_axis
					)


	def _reverse_complete_last_to_another_last_per_j_init(self, j):
		# Reverse completion of i: jY->CD. j<=i-2
		self.log_outer_branch[
				:,	# string id
				-1,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot pos
				] = self._get_outer_x_inner_j_greater_k(
						-1,
						j,
						2,
						1,
						(1,2,3)
						)[:,:,np.newaxis,np.newaxis]



	def _reverse_complete_last_to_another_last_per_j(self, pos, j):
		"""
		Reverse completion of i: jY->CD. given j.
		"""
		self.log_outer_branch[
				:,	# string id
				pos,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot pos
				] = np.logaddexp(
					self._get_outer_x_inner_j_eq_k(pos, j, 1, 0, (1,3))
					,
					self._get_outer_x_inner_j_greater_k(pos, j, 2, 1, (1,2,3))
					)[:,:,np.newaxis,np.newaxis]

	def _reverse_complete_last_to_another_last(self, pos):
		"""
		Reverse completion of i: jY->CD.
		"""
		# When j=0, k=0 as well.
		# i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		self.log_outer_branch[
					:,		# string id
					pos,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot pos
					]=self._get_outer_x_inner_j_eq_k(pos, 0, 1, 0, (1,3))[:,:,np.newaxis,np.newaxis]
		[self._reverse_complete_last_to_another_last_per_j(pos, j)
			for j in xrange(1,pos-1) # i: i-1 Y->CD. is impossible!!
		]



	def _reverse_complete_emission(self, pos):
		"""
		Reverse completion of i: jY->a.
		"""
		self.log_outer_emit[
					:, # string id
					pos,	# i
					:,		# LHS
					1,		# do pos
					]=self._get_outer_x_inner_j_eq_k(pos, pos-1, 1, 0, (1,3))
		if pos > 1:
			self.log_outer_emit[
					:, # string id
					pos,	# i
					:,		# LHS
					1,		# do pos
					] = np.logaddexp(
						self.log_outer_emit[
							:, # string id
							pos,	# i
							:,		# LHS
							1,		# do pos
							]
						,
						self._get_outer_x_inner_j_greater_k(pos, pos-1, 2, 1, (1,2,3))
						)

	def _reverse_complete_last_to_mid(self, pos):
		"""
		Reverse completion of j: kX->B.Y (note k<j)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->BY.
		self.log_outer_branch[
					:,		# string id
					:pos-1,	# j
					:pos-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
							:,		# string id
							:pos-1,	# j
							:pos-2,		# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							1,		# dot pos
							]
						,
						self.log_outer_branch[
									:,		# string id
									pos,	# i
									np.newaxis,	# j
									:pos-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						+
						self.completed_inner_branch[
									:,			# string id
									pos,		#i
									:pos-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.log_outer_branch[
					:,		# string id
					pos-1,	# j
					:pos-1,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]=np.logaddexp(
						self.log_outer_branch[
							:,		# string id
							pos-1,	# j
							:pos-1,	# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							1,		# dot pos
							]
						,
						self.log_outer_branch[
									:,		# string id
									pos,	# i
									:pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						+
						self.log_inner_emit[
									:,	 # string id
									pos,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									1,			# dot pos
									]
						)

	def _reverse_complete_mid_to_start(self, pos):
		"""
		Reverse completion of j: kX->.YA (j=k is possible)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->Y.A
		self.log_outer_branch[
					:,		# string id
					:pos-1,	# j
					:pos-1,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot pos
					] = np.logaddexp(
						self.log_outer_branch[
							:,		# string id
							:pos-1,	# j
							:pos-1,		# k
							:,		# LHS
							:,		# Left RHS
							:,		# Right RHS
							0,		# dot pos
							]
						,
						self.log_outer_branch[
									:,		# string id 
									pos,	# i
									np.newaxis,	# j
									:pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot pos
									]
						+
						self.completed_inner_branch[
									:,			# string id
									pos,		# i
									:pos-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->Y.A
		self.log_outer_branch[
					:,
					pos-1,	# j
					:pos,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot pos
					] = np.logaddexp(
							self.log_outer_branch[
								:,
								pos-1,	# j
								:pos,	# k
								:,		# LHS
								:,		# Left RHS
								:,		# Right RHS
								0,		# dot pos
								]
							,
							self.log_outer_branch[
										:,		# string id
										pos,	# i
										:pos,	# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot pos
										]
							+
							self.log_inner_emit[
										:,	# string id
										pos,		# i
										np.newaxis,	# k
										np.newaxis,	# LHS of branch
										:,			# Left RHS of branch (LHS of emission)
										np.newaxis,	# Right RHS of branch 
										1,			# dot pos
										]
						)

	def _reverse_complete(self, pos):
		"""
		Reverse operation of completion.
		"""
		self._reverse_complete_last_to_another_last(pos)
		self._reverse_complete_emission(pos)
		self._reverse_complete_last_to_mid(pos)
		self._reverse_complete_mid_to_start(pos)


	def _reverse_scan(self, pos):
		"""
		Reverse scanning (just scaling).
		"""
		# print self.log_outer_emit[
		# 					:, # string id
		# 					pos,	# i+1, k=i+1-1=i
		# 					:,		# LHS
		# 					1,		# dot pos
		# 					] 
		# print self.log_scaling_factors[:,pos-1,np.newaxis]
		self.log_outer_emit[
					:, # string id
					pos-1,	# i
					:,		# LHS
					0,		# dot pos
					]=self.log_outer_emit[
							:, # string id
							pos,	# i+1, k=i+1-1=i
							:,		# LHS
							1,		# dot pos
							] # dot_pos=1 (i.e. k=i-1) hasn't scaled by log_scaling_factors[i].






