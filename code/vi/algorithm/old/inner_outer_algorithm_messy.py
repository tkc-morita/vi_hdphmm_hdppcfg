# coding: utf-8
"""
Stolcke's (1995) inner-outer algorithm for
a fully connected PCFG in a Chomsky normal form.
It computes likelihood of a string
and expected counts of rule uses to generate the string.
"""

import numpy as np

def get_left_corner_probs(branch_rule_weights):
	"""
	Computes left corner probabilities/weights of branching rules.
	"""
	return np.linalg.inv(
						np.identity(branch_rule_weights.shape[0])
						-
						np.sum(branch_rule_weights, axis=2)
						)



def get_forward_inner_probs(
						string,
						str_len,
						branch_array,
						emit_array,
						init_weights,
						left_corner_mat
						):
	forward_branch=np.zeros(
						(
							str_len+1, # penultimate pos i
							str_len+1, # start pos k
							branch_array.shape[0], # LHS non-terminal
							branch_array.shape[1], # Left RHS non-terminal
							branch_array.shape[2], # Right RHS non-terminal
							3 # dot pos
						)
						)
	forward_emit=np.zeros(
						(
						str_len+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
						branch_array.shape[0], # LHS non-terminal
						emit_array.shape[1], # Emitted symbol
						2 # dot pos
						)
						)
	inner_branch=np.zeros(forward_branch.shape)
	inner_emit=np.zeros(forward_emit.shape)
	
	# Initial prediction
	left_corner_from_init=np.matmul(
							init_weights
							,
							left_corner_mat
							)
	
	forward_branch[0,0,:,:,:,0]+=left_corner_from_init[:,np.newaxis,np.newaxis]*branch_array
	inner_branch[0,0,:,:,:,0]=branch_array
	forward_emit[0,:,string[0],0]+=left_corner_from_init*emit_array[:,string[0]]
	inner_emit[0,:,string[0],0]=emit_array[:,string[0]]
	scaling_factors=np.ones(str_len)
	completed_inner_branch=np.zeros( # To be reused.
										(
										str_len+1, # i
										str_len+1, # j
										branch_array.shape[0]
										)
										)
	# for i=1....
	for pos,symbol in enumerate(string, start=1):
		# Scaling by scaled prefix probability.
		scaling_factors[pos-1]=np.sum(forward_emit[pos-1,:,symbol,0])**-1
		
		# Scanning
		forward_emit[pos,:,symbol,1]=forward_emit[pos-1,:,symbol,0]*scaling_factors[pos-1]
		inner_emit[pos,:,symbol,1]=inner_emit[pos-1,:,symbol,0]*scaling_factors[pos-1]
		
		# Completion
		if pos>1: # If i(=pos)=1, i:k X->BY. is not possible yet.
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			forward_branch[
							pos, # i
							pos-2, # k
							:,
							:,
							:,
							2]=(
								forward_branch[
											pos-1,	# j=i-1
											pos-2,		# k
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											1		# dot pos
											]
								*
								inner_emit[ #Scanned above.
											pos,		# i
											np.newaxis,	# LHS
											np.newaxis,	# Left RHS
											:,			# Right RHS
											symbol,		# Symbol scanned above.
											1
											]
								)
			inner_branch[
							pos, # i
							pos-2, # k
							:,
							:,
							:,
							2]=(
								inner_branch[
											pos-1,	# j=i-1
											pos-2,		# k
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											1		# dot pos
											]
								*
								inner_emit[ # Scanned above.
											pos,		# i
											np.newaxis,	# LHS
											np.newaxis,	# Left RHS
											:,			# Right RHS
											symbol,		# Symbol scanned above.
											1
											]
								)
			completed_inner_branch[pos,pos-2,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
													inner_branch[
															pos,	# i
															pos-2,		# j
															:,		# LHS
															:,		# Left RHS
															:,		# Right RHS
															2		#Dot pos
															]
													,
													axis=(
														-2,	# Left RHS
														-1	# Right RHS
														)
													)
			# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
			for k in reversed(xrange(pos-2)): # k=pos-3,...,0
				forward_branch[pos,k,:,:,:,2]=(
												# j:k X->B.Y and i:j Y->CD. (j>k)
												np.sum(
													forward_branch[
																k+1:pos-1,	# j
																k,		#k
																:,		# LHS
																:,		# Left RHS
																:,		# Right RHS
																1		# dot pos
																]
													*
													completed_inner_branch[
																pos,	# i
																k+1:pos-1, # j
																np.newaxis, # LHS of the target.
																np.newaxis, # Left of the RHS.
																: # Right of the RHS.
																]
													,
													axis=0 # over j
													)
												+
												# i-1:k X->B.Y and i:i-1 Y->a.
												(
													forward_branch[
																pos-1,	# j=i-1
																k,		# k
																:,		# LHS
																:,		# Left RHS
																:,		# Right RHS
																1		# dot pos
																]
													*
													inner_emit[ #Scanned above.
																pos,		# i
																np.newaxis,	# LHS
																np.newaxis,	# Left RHS
																:,			# Right RHS
																symbol,		# Symbol scanned above.
																1
																]
													)
												)
				inner_branch[pos,k,:,:,:,2]=(
												np.sum(
													inner_branch[
																k+1:pos-1,	# j
																k,		#k
																:,		# LHS
																:,		# Left RHS
																:,		# Right RHS
																1		# dot pos
																]
													*
													completed_inner_branch[
																pos,	# i
																k+1:pos-1, # j
																np.newaxis, # LHS of the target.
																np.newaxis, # Left of the RHS.
																: # Right of the RHS.
																]
													,
													axis=0 # over j
													)
												+
												(
													inner_branch[
																pos-1,	# j=i-1
																k,		# k
																:,		# LHS
																:,		# Left RHS
																:,		# Right RHS
																1		# dot pos
																]
													*
													inner_emit[ # Scanned above.
																pos,		# i
																np.newaxis,	# LHS
																np.newaxis,	# Left RHS
																:,			# Right RHS
																symbol,		# Symbol scanned above.
																1
																]
													)
												)
				completed_inner_branch[pos,k,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
													inner_branch[
															pos,	# i
															k,		# j
															:,		# LHS
															:,		# Left RHS
															:,		# Right RHS
															2		#Dot pos
															]
													,
													axis=(
														-2,	# Left RHS
														-1	# Right RHS
														)
													)
		if pos<str_len: # In the final phase, no completion resulting in A->B.C nor any prediction.
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			if pos>1: # No branch rule has been completed if pos=1.
				# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
				# j:k X->.YA is possible when k=j.
				j=np.arange(pos-1)
				forward_branch[
							pos, # i
							:pos-1, # k
							:,
							:,
							:,
							1]=(
									forward_branch[
												j,	# j<=i-2
												j,	# k j=k is possible.
												:,		# LHS
												:,		# Left RHS
												:,		# Right RHS
												0		# dot pos
												]
									*
									completed_inner_branch[
												pos,	# i
												:pos-1, # j
												np.newaxis, # LHS of the target.
												:, 			# Left of the RHS.
												np.newaxis	# Right of the RHS.
												]
									)
				inner_branch[
						pos, # i
						:pos-1, # k
						:,
						:,
						:,
						1]=(#np.sum(
								inner_branch[
											j,	# j
											j,		#k
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											0		# dot pos
											]
								*
								completed_inner_branch[
											pos,	# i
											:pos-1, # j
# 											np.newaxis, # k
											np.newaxis, # LHS of the target.
											:, 			# Left of the RHS.
											np.newaxis	# Right of the RHS.
											]
								)
			# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
			forward_branch[
						pos, # i
						pos-1, # k
						:,
						:,
						:,
						1]+=(
								forward_branch[
											pos-1,	# j=i-1
											pos-1,		# k
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											0		# dot pos
											]
								*
								inner_emit[ #Scanned above.
											pos,		# i
											np.newaxis,	# LHS
											:,			# Left RHS
											symbol,		# Symbol scanned above.
											1,			# dot pos
											np.newaxis	# Right RHS
											]
								)
			inner_branch[
					pos, # i
					pos-1, # k
					:,
					:,
					:,
					1]+=(
							inner_branch[
										pos-1,	# j=i-1
										pos-1,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0		# dot pos
										]
							*
							inner_emit[ #Scanned above.
										pos,		# i
										np.newaxis,	# LHS
										:,			# Left RHS
										symbol,		# Symbol scanned above.
										1,
										np.newaxis	# Right RHS
										]
							)
			# Prediction
			# Only i:k X->A.Z is introduced above and is expandable.
			forward_x_left_corner=np.matmul( # alpha * R(Z =>L Y)
												np.sum(
													forward_branch[
															pos, #i
															:pos, #k
															:,
															:,
															:,
															1
															]
													,
													axis=(
														0, # start pos (k)
														1, # LHS non-terminal
														2, # Left RHS non-terminal.
														)
													)
												,
												left_corner_mat
												)
			forward_branch[pos,pos,:,:,:,0]=(
											forward_x_left_corner[:,np.newaxis,np.newaxis]
											*
											branch_array
											)
			inner_branch[pos,pos,:,:,:,0]=branch_array
			# string[pos] is the symbol to be scanned in the next iteration.
			forward_emit[pos,:,string[pos],0]=(
											forward_x_left_corner
											*
											emit_array[:,string[pos]]
											)
			inner_emit[pos,:,string[pos],0]=emit_array[:,string[pos]]
	return (
			forward_branch,
			inner_branch,
			forward_emit,
			inner_emit,
			scaling_factors,
			completed_inner_branch
			)



def get_outer_probs(
					string,
					str_len,
					init_weights,
					inner_branch,
					inner_emit,
					scaling_factors,
					completed_inner_branch
					):
	outer_branch=np.zeros(inner_branch.shape)
	outer_emit=np.zeros(inner_emit.shape)
	# Initialization
	# Reverse completion of i:j S.->A. where i=l and j=0.
	# By i:k . -> S. and j:k . -> .S where i=l and k=0.
	# This leaves the outer probability 1.
	# And then reverse completion of i:j A->BC. where i=l and j=0.
	# By i:j S.->A. and i:j
	outer_branch[
				-1,	# i
				0,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2		# dot pos
				]+=init_weights[:,np.newaxis,np.newaxis]
# 					]+=self.init_weight_array
	# Reverse completion of i: jY->CD.
	# When j=0 (=> k=0), i: 0X->BY. and 0: 0X->B.Y are impossible.
	# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
	# But for i=l(or -1), i: 0X->Y.A and 0: 0X->.YA are not introduced.
	for j in xrange(1,str_len-1): # -1: -2 Y->CD. is impossible!!
		# Reverse completion of i: jY->CD. j<=i-2
		outer_branch[
				-1,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2		# dot pos
				]=np.sum(
							outer_branch[
										-1,	# i
										:j,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										2		# do pos
										]
							*
							inner_branch[
										j,	# j
										:j,	# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1		# do pos
										]
							,
							axis=(
								0,	# k
								1,	# LHS
								2	# Left RHS
								)
						)[:,np.newaxis,np.newaxis]
	# Reverse completion of i: jY->a.
	outer_emit[
				-1,	# i
				:,		# LHS
				string[-1],	# Left RHS
				1		# do pos
				]=np.sum(
						outer_branch[
									-1,	# i
									:-2,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2		# do pos
									]
						*
						inner_branch[
									-2,		# j=i-1
									:-2,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1		# do pos
									]
						,
						axis=(
							0,	# k
							1,	# LHS
							2	# Left RHS
							)
						)
	# Reverse completion of j: kX->B.Y k<=i-1
	# by i:j Y->BC. and i: kX->BY. => j<=i-2
	outer_branch[
				:-1,	# j
				:-2,		# k
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				1		# dot pos
				]+=(
					outer_branch[
								-1,	# i
								np.newaxis,	# j
								:-2,	# k 
								:,		# LHS
								:,		# Left RHS
								:,		# Right RHS
								2		# dot pos
								]
					*
					completed_inner_branch[
								-1,		#i
								:-1,		# j
								np.newaxis, # k
								np.newaxis,	# LHS
								np.newaxis,	# Left RHS
								:			# Right RHS
								]
					)
	# by i:j Y->a. and i: kX->BY. => j=i-1, k<=i-2
	outer_branch[
				-2,	# j = i-1
				:-2,	# k
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				1		# dot pos
				]+=(
					outer_branch[
								-1,	# i
								:-2,	# k
								:,		# LHS
								:,		# Left RHS
								:,		# Right RHS
								2		# dot pos
								]
					*
					inner_emit[
								-1,		# i
								np.newaxis, # k
								np.newaxis,	# LHS of branch
								np.newaxis,	# Left RHS of branch
								:,			# Right RHS of branch (LHS of emission)
								string[-1],		# scanned symbol
								1,			# dot pos
								]
					)
	# Reverse completion of j: kX->.YA N/A b/c -1: 0X->Y.A is irrelevant.
	# Reverse scanning. Scaling must be changed.
	outer_emit[
				-2,	# i
				:,		# LHS
				string[-1],	# Left RHS
				0		# dot pos
				]=(
					outer_emit[
						-1,	# i+1, k=i+1-1=i
						:,		# LHS
						string[-1],		# Left RHS
						1		# dot pos
						] # dot_pos=1 (i.e. k=i-1)ではscaling facoters Ci が含まれていない。
					*
					scaling_factors[-1]
					)
	for pos,symbol in reversed(list(enumerate(string[:-1], start=1))):
		# Reverse completion of i: jY->CD.
		# When j=0, k=0 as well.
		# i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		outer_branch[
					pos,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2		# dot pos
					]=np.sum( # i: kX->Y.A and j: kX->.YA
							outer_branch[
										pos,	# i
										0,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1		# dot pos
										]
							*
							inner_branch[
										0,	# j
										0,	# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0		# dot pos
										]
							,
							axis=(
								0,	# LHS
								2	# Right RHS
								)
						)[:,np.newaxis,np.newaxis]
		for j in xrange(1,pos-1): # i: i-1 Y->CD. is impossible!!
			outer_branch[
					pos,	# i
					j,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2		# dot pos
					]=(
						np.sum(
							outer_branch[ # i:k X->Y.A
										pos,	# i
										j,	# k
										:,		# LHS
										:,		# Left RHS # Independent
										:,		# Right RHS
										1		# do pos
										]
							*
							inner_branch[ # j:k X->.YA j=k
										j,	# j <- jに依存している。
										j,	# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0		# do pos
										]
							,
							axis=(
# 									0,	# k
								-3,	# LHS
								-1	# Right RHS
								)
						)
						+
						np.sum(
							outer_branch[ # i:k X->BY.
										pos,	# i
										:j,		# k
										:,		# LHS
										:,		# Left RHS independent
										:,		# Right RHS
										2		# do pos
										]
							*
							inner_branch[ # j:k X->B.Y k<j
										j,	# j
										:j,	# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1		# do pos
										]
							,
							axis=(
								0,	# k
								1,	# LHS
								2	# Left RHS
								)
						)
						)[:,np.newaxis,np.newaxis]
		# Reverse completion of i: jY->a.
		outer_emit[
					pos,	# i
					:,		# LHS
					symbol,		# Left RHS
					1		# do pos
					]=(
						np.sum(
							outer_branch[ # post-completion
										pos,	# i
										pos-1,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1		# do pos
										]
							*
							inner_branch[ # pre-completion
										pos-1,	# j
										pos-1,	# k=j
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0		# do pos
										] 
							,
							axis=(
# 									0,	# k
								-3,	# LHS
								-1	# Right RHS
								)
						)
						+
						np.sum(
							outer_branch[
										pos,	# i
										:pos-1,		# k<j
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										2		# do pos
										]
							*
							inner_branch[
										pos-1,	# j
										:pos-1,	# k<j
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1		# do pos
										]
							,
							axis=(
								0,	# k
								1,	# LHS
								2	# Left RHS
								)
						)
						)
		# Reverse completion of j: kX->B.Y (note k<j)
		# by i: jY->CD. (j<=i-2) and i: kX->BY.
		outer_branch[
					:pos-1,	# j
					:pos-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1		# dot pos
					]+=(
						outer_branch[
									pos,	# i
									np.newaxis,	# j
									:pos-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2		# dot pos
									]
						*
						completed_inner_branch[
									pos,		#i
									:pos-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		outer_branch[
					pos-1,	# j
					:pos-1,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1		# dot pos
					]+=(
						outer_branch[
									pos,	# i
									:pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2		# dot pos
									]
						*
						inner_emit[
									pos,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									symbol,		# scanned symbol
									1,			# dot pos
									]
						)
		# Reverse completion of j: kX->.YA (j=k is possible)
		# by i: jY->CD. (j<=i-2) and i: kX->Y.A
		outer_branch[
					:pos-1,	# j
					:pos-1,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0		# dot pos
					]+=(
						outer_branch[
									pos,	# i
									np.newaxis,	# j
									:pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1		# dot pos
									]
						*
						completed_inner_branch[
									pos,		# i
									:pos-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis	# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->Y.A
		outer_branch[
					pos-1,	# j
					:pos,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0		# dot pos
					]+=(
						outer_branch[
									pos,	# i
									:pos,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1		# dot pos
									]
						*
						inner_emit[
									pos,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									:,			# Left RHS of branch (LHS of emission)
									np.newaxis,	# Right RHS of branch 
									symbol,		# scanned symbol
									1,			# dot pos
									]
						)
		
		# Reverse scanning. Scaling must be changed.
		outer_emit[
					pos-1,	# i
					:,		# LHS
					symbol,	# Left RHS
					0		# dot pos
					]=(
						outer_emit[
							pos,	# i+1, k=i+1-1=i
							:,		# LHS
							symbol,		# Left RHS
							1		# dot pos
							] # dot_pos=1 (i.e. k=i-1)ではscaling facoters Ci が含まれていない。
						*
						scaling_factors[pos-1]
						)
	return (outer_branch,outer_emit)
