# coding: utf-8

"""
Optimize a vector on a simplex (without the last entry)
with the projected gradient algorithm (Bertsekas, 1982: pp.241-242).

Reference:
Bertsekas, Dimitri P. (1982)
	"Projected Newton methods for optimization problems with simple constraints."
"""

import numpy as np
import scipy.optimize as spo
import scipy.spatial.distance as spsd

def gradient_projection_optimization(
						initial_point,
						objective_func,
						get_gradient_and_hessian,
						tolerance,
						almost_zero_base_line = 0.00001
						):
	"""
	Returns an update of initial_point
	by two-metric gradient projection method.
	"""
	current_point = initial_point
	current_fval = objective_func(current_point)
	distance = np.inf
	improvement = np.inf
	# counter = 1
	while (improvement < 0) or (improvement > tolerance):
		gradient, hessian = get_gradient_and_hessian(current_point)
		almost_zero = get_almost_zero(current_point, gradient, hessian, almost_zero_base_line)
		direction, I_plus, comp_I_plus, current_point_and_sum = get_direction(
																	current_point,
																	gradient,
																	hessian,
																	almost_zero
																	)
		new_point, new_fval = update(
								current_point_and_sum,
								direction,
								gradient,
								objective_func,
								current_fval,
								I_plus,
								comp_I_plus
								)

		improvement = current_fval - new_fval # We want to MINIMIZE the objective function!
		# if improvement<0:
			# print 'counter', counter
			# print 'worse', improvement
		# counter += 1
		current_point = new_point
		current_fval = new_fval
	return current_point


def update(
			current_point_and_sum,
			direction,
			gradient,
			objective_func,
			current_fval,
			I_plus,
			comp_I_plus,
			beta = 0.9,
			sigma = 0.0001
			):
	"""
	Find an optimal step_size such that:
	
	new_point <- orthant_projection(current_point_and_sum - step_size * direction)

	and returns new_point together with objective_func(new_point).

	step_size is given by beta**m (0 < beta < 1),
	where m is the smallest non-negative integer that satusfies:

	objective_func(current_point) - objective_func("update with beta**m")
	>=
	sigma*(
		beta**m * <gradient, direction * comp_I_plus> # Indices not in I_plus.
		+
		<gradient, (current_point - "update with beta**m") * I_plus> # Indices in I_plus
		)
	
	(0 < sigma < 0.5).
	"""
	m=0
	while True:
		proposed_step_size = beta**m
		proposed_update = orthant_projection_and_map_last_to_1(
								current_point_and_sum
								-
								proposed_step_size * direction
								)
		if 1 - np.sum(proposed_update[:-1]) > 0:
			proposed_fval = objective_func(proposed_update[:-1])
			if (
					current_fval - proposed_fval
					>=
					sigma
					*
					np.dot(
						gradient,
						proposed_step_size * direction * comp_I_plus
						+
						(current_point_and_sum - proposed_update) * I_plus
					)
				):
				break
		m+=1
	return proposed_update[:-1], proposed_fval

def get_almost_zero(current_point, gradient, hessian, base_line):
	"""
	Get a threshhold under which a vector entry is considered as zero.
	"""
	return np.minimum(
				base_line,
				spsd.euclidean(
					current_point,
					orthant_projection(
						current_point
						-
						1.0 / hessian.diagonal()[:-1]
						*
						gradient[:-1]
					)
					)
				)

def get_direction(current_point, gradient, hessian, almost_zero):
	"""
	To make projection onto a simplex easy,
	we adopt a two-metric projection method.
	i.e. The update direction is the gradient left-multiplied by
	a positive definite metrix D, which is almost the inverse of Hessian.
	See Bertsekas (1982: pp.241-242) for detail.
	"""
	current_point_and_sum = np.append(current_point, 1.0)
	I_plus = np.logical_and(current_point_and_sum<=almost_zero, gradient>0)
	comp_I_plus = ~(I_plus)
	scaling_matrix = np.linalg.inv(
							hessian
							*
							np.logical_or(
								np.logical_and(
									comp_I_plus[:,np.newaxis]
									,
									comp_I_plus[np.newaxis,:]
								)
								,
								np.eye(current_point_and_sum.size, dtype=bool)
							)
							)
	return np.matmul(scaling_matrix, gradient), I_plus, comp_I_plus, current_point_and_sum

def orthant_projection(vector):
	"""
	Take max{0, vector[i]} for each i.
	"""
	return vector.clip(min=0)

def orthant_projection_and_map_last_to_1(vector):
	"""
	Apply the orthant_projection to vector[:-1] and replace vector[-1] with 1.
	"""
	return np.append(orthant_projection(vector[:-1]), 1.0)

