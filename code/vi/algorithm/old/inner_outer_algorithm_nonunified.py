# coding: utf-8
"""
Stolcke's (1995) inner-outer algorithm for
a fully connected PCFG in a Chomsky normal form.
"""

import numpy as np
import scipy.misc as spm
import tree
# np.seterr(all='warn') # Just warn. Don't raise errors.

class Inner_Outer_Algorithm(object):
	"""
	Stolcke's (1995) inner-outer algorithm for
	a fully connected PCFG in a Chomsky normal form.
	"""
	def __init__(
			self,
			branch_rule_weights,
			emission_rule_weights,
			init_weights,
			):
		"""
		Initialize rule weights.
		"""
		self.branch_rule_weights = branch_rule_weights
		self.emission_rule_weights = emission_rule_weights
		self.init_weights = init_weights
		self.left_corner_weights = self._get_left_corner_probs()

	def get_log_likelihoods(self, string):
		"""
		Get log likelihoods of string without counting expected rule use.
		"""
		self._set_strings(string)
		if self.string_length == 1:
			return spm.logsumexp(
						np.log(self.init_weights)
						+
						np.log(self.emission_rule_weights[:,string[0]])
						# ,
						# axis=0
						)
		else:
			self._calc_forward_inner_probs()
			return spm.logsumexp(
								np.log(self.init_weights)
								+
								np.log(np.sum(self.inner_branch[-1,0,:,:,:,2], axis=(-2,-1)))
								-
								np.sum(np.log(self.scaling_factors))
								# ,
								# axis=0
								)

	def _set_strings(self, string):
		"""
		(Re)set emitted strings.
		Strings of the same length are assumed to be sorted.
		"""
		self.string = string
		self.string_length = len(self.string)
		# self.string_ids = np.arange(self.string.shape[0])

	def _get_left_corner_probs(self):
		"""
		Computes left corner probabilities/weights of branching rules.
		"""
		return np.linalg.inv(
							np.identity(self.branch_rule_weights.shape[0])
							-
							np.sum(self.branch_rule_weights, axis=2)
							)

	def get_expected_rule_counts(self, string):
		"""
		Returns expected # of use of each rule,
		together with log likelihoods of strings.
		"""
		self._set_strings(string)
		if self.string_length == 1: # No branching for strings of length 1
			expected_root_counts = self.init_weights*self.emission_rule_weights[:,string[0]]
			likelihood = np.sum(expected_root_counts)
			expected_root_counts /= likelihood
			expected_branch_rule_counts = np.zeros(self.branch_rule_weights.shape)
			expected_emission_rule_counts = np.zeros(self.emission_rule_weights.shape)
			expected_emission_rule_counts[:,string[0]] += expected_root_counts
			log_like = np.log(likelihood)
		else:
			self._calc_forward_inner_probs()
			self._calc_outer_probs()
			scaled_likelihoods_for_each_root_non_terminal=(
										np.sum(self.inner_branch[-1,0,:,:,:,2], axis=(1,2))
										*
										self.init_weights
										)
			scaled_likelihood=np.sum(
									scaled_likelihoods_for_each_root_non_terminal
								)
			expected_branch_rule_counts=(
											np.sum(
												self.outer_branch[...,0].diagonal(axis1=0, axis2=1) # Scaled.
												*
												self.inner_branch[...,0].diagonal(axis1=0, axis2=1) # Not scaled.
												,
												axis=-1 # diagonalized dimension comes last.
												) # num_strings x num_nt x num_nt x num_nt
											/
											scaled_likelihood
											)
			
			expected_emission_rule_counts=(
										np.sum(
											self.outer_emit[...,0] # Scaled.
											*
											self.inner_emit[...,0] # Not scaled.
											,
											axis=0
											) # num_strings x num_nt x num_symbols
										/
										scaled_likelihood
										)
			expected_root_counts = (
									scaled_likelihoods_for_each_root_non_terminal
									/
									scaled_likelihood
									)
			log_like = np.log(scaled_likelihood)-np.sum(np.log(self.scaling_factors), axis=-1)
		return expected_branch_rule_counts, expected_emission_rule_counts, expected_root_counts, log_like


	def get_viterbi_parse(self, string):
		"""
		Get Viterbi (or most probable leftmost) parse of the string.
		"""
		self._set_strings(string)
		if self.string_length == 1: # No branching for strings of length 1
			expected_root_counts = self.init_weights*self.emission_rule_weights[:,string[0]]
			most_probable_root = np.argmax(expected_root_counts)
			viterbi_parse = tree.Node(most_probable_root)
			terminal = tree.Terminal(string[0], mother=viterbi_parse)
		else:
			self._calc_viterbi_probs()
			reshaped_viterbi_branch = self.viterbi_branch[-1,0,:,:,:,2].reshape(
																self.viterbi_branch.shape[2],
																self.viterbi_branch.shape[2]**2
																)
			max_ids = np.argmax(
							reshaped_viterbi_branch
							, axis=(
								# -2,
								-1
								)
							)
			# 
			max_root_branch = reshaped_viterbi_branch[np.arange(self.viterbi_branch.shape[2]),max_ids]
			root_label=np.argmax(
								max_root_branch
								*
								self.init_weights
								)
			rhs_left, rhs_right = np.unravel_index(max_ids[root_label], self.inner_branch.shape[3:5])
			viterbi_parse = self._get_viterbi_subparse_branch(-1, 0, root_label, rhs_left, rhs_right, 2)
		return viterbi_parse



	def _get_viterbi_subparse_branch(self, i, k, lhs, rhs_left, rhs_right, dot_pos, mother_node = None):
		if dot_pos == 0:
			subtree_root = tree.Node(lhs, mother=mother_node)
			return subtree_root
		elif dot_pos == 1:
			child_lhs = rhs_left
		else:
			child_lhs = rhs_right
		branch_or_emit = self.viterbi_predecessor[i,k,lhs,rhs_left,rhs_right,dot_pos,0]
		j = self.viterbi_predecessor[i,k,lhs,rhs_left,rhs_right,dot_pos,1]
		child_rhs_left_or_terminal = self.viterbi_predecessor[i,k,lhs,rhs_left,rhs_right,dot_pos,2]
		child_rhs_right = self.viterbi_predecessor[i,k,lhs,rhs_left,rhs_right,dot_pos,3]
		subtree_root = self._get_viterbi_subparse_branch(j, k, lhs, rhs_left, rhs_right, dot_pos-1, mother_node = mother_node)
		if branch_or_emit: # if the predecessor is emission.
			child_tree = self._get_viterbi_subparse_emit(i,child_lhs,child_rhs_left_or_terminal, 1, subtree_root)
		else:
			child_tree = self._get_viterbi_subparse_branch(i, j, child_lhs, child_rhs_left_or_terminal, child_rhs_right, 2, mother_node = subtree_root)
		return subtree_root


	def _get_viterbi_subparse_emit(self, i, lhs, emitted_symbol, dot_pos, mother_node):
		if dot_pos == 0:
			part_of_speech = tree.Node(lhs, mother=mother_node)
			return part_of_speech
		else:
			part_of_speech = self._get_viterbi_subparse_emit(i-1, lhs, emitted_symbol, 0, mother_node)
			terminal = tree.Terminal(emitted_symbol, mother=part_of_speech)
			return part_of_speech







	def _calc_forward_inner_probs(self):
		"""
		Calculates forward and inner probabilities.
		Underflow is avoided by scaling.
		"""
		self.forward_branch=np.zeros(
							(
								self.string_length+1, # penultimate i i
								self.string_length+1, # start i k
								self.branch_rule_weights.shape[0], # LHS non-terminal
								self.branch_rule_weights.shape[1], # Left RHS non-terminal
								self.branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot i
							)
							)
		self.forward_emit=np.zeros(
							(
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.emission_rule_weights.shape[0], # LHS non-terminal
							self.emission_rule_weights.shape[1], # Emitted emitted_symbol
							2 # dot i
							)
							)
		self.inner_branch=np.zeros(self.forward_branch.shape)
		self.inner_emit=np.zeros(self.forward_emit.shape)
		
		# Initial prediction
		left_corner_from_init=np.matmul(
								self.init_weights
								,
								self.left_corner_weights
								)
		
		self.forward_branch[0,0,:,:,:,0]+=left_corner_from_init[:,np.newaxis,np.newaxis]*self.branch_rule_weights
		self.inner_branch[0,0,:,:,:,0]=self.branch_rule_weights

		emitted_symbol = self.string[0]

		self.forward_emit[0,:,emitted_symbol,0] += (
														left_corner_from_init
														*
														self.emission_rule_weights[:,emitted_symbol]
													)
		self.inner_emit[0,:,emitted_symbol,0] = self.emission_rule_weights[:,emitted_symbol]
		self.scaling_factors=np.ones(self.string_length)
		self.completed_inner_branch=np.zeros( # To be reused.
											(
											self.string_length+1, # i
											self.string_length+1, # j
											self.branch_rule_weights.shape[0],
											)
											)
		[
			(
				self._forward_inner_routine(
						i,
						emitted_symbol
						)
			)
			for 
			i,
			emitted_symbol
			in enumerate(self.string, start=1) # for i=1....
		]

	def _calc_viterbi_probs(self):
		"""
		Calculates forward, inner, and viterbi probabilities.
		Underflow is avoided by scaling.
		(Forward and inner probs are used for scaling.)
		"""
		self.forward_branch=np.zeros(
							(
								self.string_length+1, # penultimate i i
								self.string_length+1, # start i k
								self.branch_rule_weights.shape[0], # LHS non-terminal
								self.branch_rule_weights.shape[1], # Left RHS non-terminal
								self.branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot i
							)
							)
		self.forward_emit=np.zeros(
							(
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.emission_rule_weights.shape[0], # LHS non-terminal
							self.emission_rule_weights.shape[1], # Emitted emitted_symbol
							2 # dot i
							)
							)

		self.inner_branch=np.zeros(self.forward_branch.shape)
		self.inner_emit=np.zeros(self.forward_emit.shape)

		self.viterbi_branch=np.zeros(self.forward_branch.shape)
		self.viterbi_emit=np.zeros(self.forward_emit.shape)
		
		# Initial prediction
		left_corner_from_init=np.matmul(
								self.init_weights
								,
								self.left_corner_weights
								)
		
		self.forward_branch[0,0,:,:,:,0]+=left_corner_from_init[:,np.newaxis,np.newaxis]*self.branch_rule_weights
		self.inner_branch[0,0,:,:,:,0]=self.branch_rule_weights
		self.viterbi_branch[0,0,:,:,:,0]=self.branch_rule_weights

		emitted_symbol = self.string[0]

		self.forward_emit[0,:,emitted_symbol,0] += (
														left_corner_from_init[:]
														*
														self.emission_rule_weights[:,emitted_symbol]
													)
		self.inner_emit[0,:,emitted_symbol,0] = self.emission_rule_weights[:,emitted_symbol]
		self.viterbi_emit[0,:,emitted_symbol,0] = self.emission_rule_weights[:,emitted_symbol]
		self.scaling_factors=np.ones(self.string_length)

		self.completed_inner_branch = np.zeros( # To be reused.
											(
											self.string_length+1, # i
											self.string_length+1, # j
											self.branch_rule_weights.shape[0],
											)
											)

		self.completed_viterbi_branch = np.zeros(self.completed_inner_branch.shape)
		self.viterbi_predecessor = np.full(
										(
											# ID
											self.string_length+1, # penultimate i i
											self.string_length+1, # start i k
											self.branch_rule_weights.shape[0], # LHS non-terminal
											self.branch_rule_weights.shape[1], # Left RHS non-terminal
											self.branch_rule_weights.shape[2], # Right RHS non-terminal
											3, # dot i
											# ======================
											# Info
											4 # 1st entry for branch (0) vs. emission (1).
											  # 2nd entry for j
											  # 3rd entry for RHS_left/terminal
											  # 4th entry for RHS_right
										)
										,
										# np.nan
										100
										)
		self.viterbi_predecessor_for_completed = np.full(
													(
														# ID
														self.string_length+1, # i
														self.string_length+1, # j
														self.branch_rule_weights.shape[0], # LHS non-terminal
														# ======================
														# Info
														2 # 1st entry for RHS_left.
														  # 2nd entry for RHS_right.
													)
													,
													# np.nan
													100
													)
		[
			(
				self._viterbi_routine(
						i,
						emitted_symbol
						)
			)
			for 
			i,
			emitted_symbol
			in enumerate(self.string, start=1) # for i=1....
		]




	def _forward_inner_routine(
						self,
						i,
						emitted_symbol,
						):
		self._scale_and_scan(i,emitted_symbol)
		if i > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last(i,emitted_symbol)
		if i < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid(i,emitted_symbol)
			self._predict(i)


	def _viterbi_routine(
						self,
						i,
						emitted_symbol,
						):
		self._scale_and_scan(i,emitted_symbol)
		self._scale_and_scan_viterbi(i,emitted_symbol)
		if i > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last(i,emitted_symbol)
			self._complete_mid_to_last_viterbi(i,emitted_symbol)
		if i < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid(i,emitted_symbol)
			self._complete_start_to_mid_viterbi(i,emitted_symbol)

			self._predict(i)
			self._predict_viterbi(i)
		

	def _scale_and_scan(
			self,
			i,
			emitted_symbol,
			):
		"""
		The "scan" step of the forward/inner algorithm.
		Also scale foward and inner emission probabilities by (already scaled) prefix probability.
		"""
		# Get scalar
		self.scaling_factors[i-1]=np.sum(self.forward_emit[i-1,:,emitted_symbol,0])**-1
				
		# Scanning
		self.forward_emit[i,:,emitted_symbol,1]=self.forward_emit[i-1,:,emitted_symbol,0]*self.scaling_factors[i-1]
		self.inner_emit[i,:,emitted_symbol,1]=self.inner_emit[i-1,:,emitted_symbol,0]*self.scaling_factors[i-1]

	def _scale_and_scan_viterbi(
			self,
			i,
			emitted_symbol,
			):
		"""
		The "scan" step of the Viterbi algorithm.
		Also scale Viterbi emission probabilities by (already scaled) prefix probability.
		"""
		# Scanning
		self.viterbi_emit[i,:,emitted_symbol,1]=self.viterbi_emit[i-1,:,emitted_symbol,0]*self.scaling_factors[i-1]


	def _complete_mid_to_last(
						self,
						i,
						emitted_symbol
					):
		"""
		Complete X->B.Y => X->BY.
		"""
		self.forward_branch[
						i, # i
						i-2, # k
						:,
						:,
						:,
						2
						]=(
							self.forward_branch[
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.inner_emit[ #Scanned above.
										i,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										emitted_symbol,		# Symbol scanned above.
										1
										]
							)
		self.inner_branch[
						i, # i
						i-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.inner_branch[
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.inner_emit[ # Scanned above.
										i,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										emitted_symbol,		# Symbol scanned above.
										1
										]
							)
		self.completed_inner_branch[i,i-2,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
												self.inner_branch[
														i,	# i
														i-2,		# j
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														2,		#Dot i
														]
												,
												axis=(
													-2,	# Left RHS
													-1	# Right RHS
													)
												)
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k(
						i,
						emitted_symbol,
						k
						)
			for k in reversed(xrange(i-2)) # k=i-3,...,0 <- Only valid on Python 2.
		]

	def _complete_mid_to_last_viterbi(
						self,
						i,
						emitted_symbol
					):
		"""
		Complete X->B.Y => X->BY for Viterbi probability.
		"""
		self.viterbi_branch[
						i, # i
						i-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.viterbi_branch[
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.viterbi_emit[ # Scanned above.
										i,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										emitted_symbol,		# Symbol scanned above.
										1
										]
							)
		self.viterbi_predecessor[
						i, # i
						i-2, # k
						:, # LHS
						:, # RHS left
						:, # RHS right
						2, # dot i
						0 # predecessor = branch (0) or emit (1)?
						] = 1
		self.viterbi_predecessor[
						i, # i
						i-2, # k
						:, # LHS
						:, # RHS left
						:, # RHS right
						2, # dot i
						1 # j
						] = i - 1
		self.viterbi_predecessor[
						i, # i
						i-2, # k
						:, # LHS
						:, # RHS left
						:, # RHS right
						2, # dot i
						2 # emitted value
						] = emitted_symbol
		reshaped_viterbi_branch = self.viterbi_branch[
										i,	# i
										i-2,		# j
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										2,		#Dot i
										].reshape(
											self.viterbi_branch.shape[2],
											self.viterbi_branch.shape[2]**2
											)
		max_ids = np.argmax( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
					reshaped_viterbi_branch
					,
					axis=(
						# -2,	# Left RHS
						-1	# Right RHS
						)
					)
		lhss = np.indices(max_ids.shape)
		self.completed_viterbi_branch[i,i-2,:] = reshaped_viterbi_branch[lhss, max_ids]
		max_ids_left_right = np.unravel_index(max_ids, (self.viterbi_branch.shape[2],)*2)
		self.viterbi_predecessor_for_completed[i,i-2,:,0] = max_ids_left_right[0]
		self.viterbi_predecessor_for_completed[i,i-2,:,1] = max_ids_left_right[1]
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k_viterbi(
						i,
						emitted_symbol,
						k
						)
			for k in reversed(xrange(i-2)) # k=i-3,...,0 <- Only valid on Python 2.
		]

	def _complete_mid_to_last_per_k(
						self,
						i,
						emitted_symbol,
						k
						):
		self.forward_branch[i,k,:,:,:,2]=(
										# j:k X->B.Y and i:j Y->CD. (j>k)
										np.sum(
											self.forward_branch[
														k+1:i-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.completed_inner_branch[
														i,	# i
														k+1:i-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=0 # over j
											)
										+
										# i-1:k X->B.Y and i:i-1 Y->a.
										(
											self.forward_branch[
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.inner_emit[ #Scanned above.
														i,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														emitted_symbol,		# Symbol scanned above.
														1,
														]
											)
										)
		self.inner_branch[i,k,:,:,:,2]=(
										np.sum(
											self.inner_branch[
														k+1:i-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.completed_inner_branch[
														i,	# i
														k+1:i-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=0 # over j
											)
										+
										(
											self.inner_branch[
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.inner_emit[ # Scanned above.
														i,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														emitted_symbol,		# Symbol scanned above.
														1
														]
											)
										)
		self.completed_inner_branch[i,k,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											self.inner_branch[
													i,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot i
													]
											,
											axis=(
												-2,	# Left RHS
												-1	# Right RHS
												)
											)


	def _complete_mid_to_last_per_k_viterbi(
						self,
						i,
						emitted_symbol,
						k
						):
		viterbi_branch_x_completed = (self.viterbi_branch[
										k+1:i-1,	# j
										k,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
									*
									self.completed_viterbi_branch[
												i,	# i
												k+1:i-1, # j
												np.newaxis, # LHS of the target.
												np.newaxis, # Left of the RHS.
												:, # Right of the RHS.
												]
									)
		branch_max_ids = np.argmax(
							viterbi_branch_x_completed
							,
							axis=0 # over j
							)
		lhss, rhs_ls, rhs_rs = np.indices(branch_max_ids.shape)
		max_viterbi_branch_x_completed = viterbi_branch_x_completed[branch_max_ids, lhss, rhs_ls, rhs_rs]
		viterbi_branch_x_emit = (
									self.viterbi_branch[
												i-1,	# j=i-1
												k,		# k
												:,		# LHS
												:,		# Left RHS
												:,		# Right RHS
												1,		# dot i
												]
									*
									self.viterbi_emit[ # Scanned above.
												i,		# i
												np.newaxis,	# LHS
												np.newaxis,	# Left RHS
												:,			# Right RHS
												emitted_symbol,		# Symbol scanned above.
												1
												]
									)
		branch_vs_emit = np.array(
							(
								max_viterbi_branch_x_completed
								,
								viterbi_branch_x_emit
							)
							)
		branch_or_emit_ids = np.argmax(
									branch_vs_emit
									,
									axis=0
								)
		self.viterbi_branch[i,k,:,:,:,2] = branch_vs_emit[branch_or_emit_ids, lhss, rhs_ls, rhs_rs]
		self.viterbi_predecessor[i,k,:,:,:,2,0] = branch_or_emit_ids
		branch_js = np.arange(k+1,i-1)[branch_max_ids]
		self.viterbi_predecessor[i,k,:,:,:,2,1] = (
														branch_js.reshape(viterbi_branch_x_completed.shape[1:])
															* (branch_or_emit_ids==0)
														+
														(i-1) * branch_or_emit_ids
													)
		self.viterbi_predecessor[i,k,:,:,:,2,2] = (
														self.viterbi_predecessor_for_completed[
																i,
																branch_js,
																lhss,
																0
																]
															* (branch_or_emit_ids==0)
														+
														emitted_symbol * branch_or_emit_ids
													)
		self.viterbi_predecessor[i,k,:,:,:,2,3] = self.viterbi_predecessor_for_completed[
																i,
																branch_js,
																lhss,
																1
																]
		reshaped_viterbi_branch = self.viterbi_branch[
													i,	# i k,		# j
													k,
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot i
													].reshape(
														self.viterbi_branch.shape[2],
														self.viterbi_branch.shape[2]**2
														)
		completed_max_ids = np.argmax( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											reshaped_viterbi_branch
											,
											axis=(
												# -2,	# Left RHS
												-1	# Right RHS
												)
											)
		lhss = np.indices(completed_max_ids)
		self.completed_viterbi_branch[i,k,:] = reshaped_viterbi_branch[lhss,completed_max_ids]
		max_ids_left_right = np.unravel_index(completed_max_ids, (self.viterbi_branch.shape[2],)*2)
		self.viterbi_predecessor_for_completed[i,k,:,0] = max_ids_left_right[0]
		self.viterbi_predecessor_for_completed[i,k,:,1] = max_ids_left_right[1]

	def _complete_start_to_mid(
						self,
						i,
						emitted_symbol
					):
		"""
		Complete X->.YA => X->Y.A
		"""
		if i>1: # No branch rule has been completed if i=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(i-1)
			self.forward_branch[
						i, # i
						:i-1, # k
						:,
						:,
						:,
						1,
						]=(
								self.forward_branch[
											j,	# j<=i-2
											j,	# k j=k is possible.
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											0,		# dot i
											]
								*
								self.completed_inner_branch[
											i,	# i
											:i-1, # j
											np.newaxis, # LHS of the target.
											:, 			# Left of the RHS.
											np.newaxis,	# Right of the RHS.
											]
								)
			self.inner_branch[
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.inner_branch[
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										]
							*
							self.completed_inner_branch[
										i,	# i
										:i-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.forward_branch[
					i, # i
					i-1, # k
					:,
					:,
					:,
					1
					]=(
							self.forward_branch[
										i-1,	# j=i-1
										i-1,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										]
							*
							self.inner_emit[ #Scanned above.
										i,		# i
										np.newaxis,	# LHS
										:,			# Left RHS
										np.newaxis,	# Right RHS
										emitted_symbol,		# Symbol scanned above.
										1,			# dot i
										]
							)
		self.inner_branch[
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.inner_branch[
									i-1,	# j=i-1
									i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot i
									]
						*
						self.inner_emit[ #Scanned above.
									i,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									emitted_symbol,	# Symbol scanned above.
									1,
									]
						)

	def _complete_start_to_mid_viterbi(
						self,
						i,
						emitted_symbol
					):
		"""
		Complete X->.YA => X->Y.A for Viterbi prob.
		"""
		if i>1: # No branch rule has been completed if i=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(i-1)
			self.viterbi_branch[
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.viterbi_branch[
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										]
							*
							self.completed_viterbi_branch[
										i,	# i
										:i-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
			self.viterbi_predecessor[
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					0,
					] = 0
			self.viterbi_predecessor[
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					1,
					] = j[:,np.newaxis,np.newaxis,np.newaxis]
			self.viterbi_predecessor[
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					2,
					] = self.viterbi_predecessor_for_completed[i,:i-1,np.newaxis,np.newaxis,:,0]
			self.viterbi_predecessor[
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					3,
					] = self.viterbi_predecessor_for_completed[i,:i-1,np.newaxis,np.newaxis,:,1]
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.viterbi_branch[
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.viterbi_branch[
											i-1,	# j=i-1
											i-1,		# k
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											0,		# dot i
											]
								*
								self.viterbi_emit[ #Scanned above.
											i,		# i
											np.newaxis,	# LHS
											:,			# Left RHS
											np.newaxis,	# Right RHS
											emitted_symbol,	# Symbol scanned above.
											1,
											]
								)
		self.viterbi_predecessor[
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				0
				] = 1
		self.viterbi_predecessor[
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				1
				] = i-1
		self.viterbi_predecessor[
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				2
				] = emitted_symbol

		

	def _predict(
			self,
			i
			):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		# Only i:k X->A.Z is introduced in the completion step and is expandable.
		forward_x_left_corner=np.sum( # alpha * R(Z =>L Y)
											np.sum(
												self.forward_branch[
														i, #i
														:i, #k
														:,
														:,
														:,
														1
														]
												,
												axis=(
													0, # start i (k)
													1, # LHS non-terminal
													2, # Left RHS non-terminal.
													)
												)[:,np.newaxis] # num_nonterminals (right RHS)
											*
											self.left_corner_weights # num_non_terminals x num_non_terminals
											,
											axis=0
											) # num_non_terminals
		self.forward_branch[i,i,:,:,:,0]=(
										forward_x_left_corner[:,np.newaxis,np.newaxis]
										*
										self.branch_rule_weights
										)
		self.inner_branch[i,i,:,:,:,0]=self.branch_rule_weights
		# self.string[i] is the emitted_symbol to be scanned in the next iteration.
		next_emitted_symbol = self.string[i]
		used_emission_rules = self.emission_rule_weights[:,next_emitted_symbol]

		self.forward_emit[i,:,next_emitted_symbol,0]=(
										forward_x_left_corner
										*
										used_emission_rules
										)
		self.inner_emit[i,:,next_emitted_symbol,0]=used_emission_rules


	def _predict_viterbi(
			self,
			i
			):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		# Exactly the same as the inner probability.
		self.viterbi_branch[i,i,:,:,:,0]=self.inner_branch[i,i,:,:,:,0]
		self.viterbi_emit[i,:,next_emitted_symbol,0]=self.inner_emit[i,:,next_emitted_symbol,0]


	# ===============END OF FORWARD-INNER ALGORITHM===================






	# ==============START OF OUTER ALGORITHM==========================

	def _calc_outer_probs(self):
		"""
		Calculate outer probabilities.
		"""
		self.outer_branch=np.zeros(self.inner_branch.shape)
		self.outer_emit=np.zeros(self.inner_emit.shape)
		# Initialization
		# Reverse completion of i:j S.->A. where i=l and j=0.
		# By i:k . -> S. and j:k . -> .S where i=l and k=0.
		# This leaves the outer probability 1.
		# And then reverse completion of i:j A->BC. where i=l and j=0.
		# By i:j S.->A. and i:j
		self.outer_branch[
					-1,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot i
					]+=self.init_weights[:,np.newaxis,np.newaxis]
					
		# Reverse completion of i: jY->CD.
		# When j=0 (=> k=0), i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		# But for i=l(or -1), i: 0X->Y.A and 0: 0X->.YA are not introduced.
		[
			self._reverse_complete_last_to_another_last_per_j_init(j)
			for j in xrange(1,self.string_length-1) # -1: -2 Y->CD. is impossible!!
		]
		
		# Reverse completion of i: jY->a.
		emitted_symbol = self.string[-1]
		self.outer_emit[
					-1,	# i
					:,		# LHS
					emitted_symbol,	# emission
					1,		# do i
					]=self._get_outer_x_inner_j_greater_k(-1, -2, 2, 1, (0, 1, 2))
		# Reverse completion of j: kX->B.Y k<=i-1
		self.outer_branch[
					:-1,	# j
					:-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									-1,	# i
									np.newaxis,	# j
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.completed_inner_branch[
									-1,		#i
									:-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.outer_branch[
					-2,	# j
					:-2,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									-1,	# i
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.inner_emit[
									-1,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									emitted_symbol,		# scanned emitted_symbol
									1,			# dot i
									]
						)
		# Reverse completion of j: kX->.YA N/A b/c -1: 0X->Y.A is irrelevant.
		# Reverse scanning. Scaling must be changed.
		self.outer_emit[
					-2,	# i
					:,		# LHS
					emitted_symbol,	# Left RHS
					0,		# dot i
					]=(
						self.outer_emit[
							-1,	# i+1, k=i+1-1=i
							:,		# LHS
							emitted_symbol,		# Left RHS
							1		# dot i
							] # dot_pos=1 (i.e. k=i-1)ではscaling facoters Ci が含まれていない。
						*
						self.scaling_factors[-1,np.newaxis]
							)
		[
			(
				self._reverse_complete(
					i,
					emitted_symbol
					)
				,
				self._reverse_scan(
					i,
					emitted_symbol
					)
			)
			for i,emitted_symbol in reversed(list(enumerate(self.string[:-1], start=1)))
		]


	def _get_outer_x_inner_j_greater_k(
					self,
					i,
					j,
					outer_dot_pos,
					inner_dot_pos,
					marginalization_axis
					):
		"""
		Computes (outer probability) x (inner probability) when j > k.
		"""
		return np.sum(
						self.outer_branch[ # i:k X->BY.
									i,	# i
									:j,		# k
									:,		# LHS
									:,		# Left RHS independent
									:,		# Right RHS
									outer_dot_pos,		# do i
									]
						*
						self.inner_branch[ # j:k X->B.Y k<j
									j,	# j
									:j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do i
									]
						,
						axis=marginalization_axis
					)


	def _get_outer_x_inner_j_eq_k(self, i, j, outer_dot_pos, inner_dot_pos, marginalization_axis):
		"""
		Computes (outer probability) x (inner probability) when j = k.
		"""
		return np.sum(
						self.outer_branch[ # i:k X->Y.A
									i,	# i
									j,	# k
									:,		# LHS
									:,		# Left RHS # Independent
									:,		# Right RHS
									outer_dot_pos,		# do i
									]
						*
						self.inner_branch[ # j:k X->.YA j=k
									j,	# j <- jに依存している。
									j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do i
									]
						,
						axis=marginalization_axis
					)

	def _reverse_complete_last_to_another_last_per_j_init(self, j):
		# Reverse completion of i: jY->CD. j<=i-2
		self.outer_branch[
				-1,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot i
				]=self._get_outer_x_inner_j_greater_k(
						-1,
						j,
						2,
						1,
						(0,1,2)
						)[:,np.newaxis,np.newaxis]



	def _reverse_complete_last_to_another_last_per_j(self, i, j):
		"""
		Reverse completion of i: jY->CD. given j.
		"""
		self.outer_branch[
				i,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot i
				]=(
					self._get_outer_x_inner_j_eq_k(i, j, 1, 0, (0,2))
					+
					self._get_outer_x_inner_j_greater_k(i, j, 2, 1, (0,1,2))
					)[:,np.newaxis,np.newaxis]

	def _reverse_complete_last_to_another_last(self, i):
		"""
		Reverse completion of i: jY->CD.
		"""
		# When j=0, k=0 as well.
		# i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		self.outer_branch[
					i,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot i
					]=self._get_outer_x_inner_j_eq_k(i, 0, 1, 0, (0,2))[:,np.newaxis,np.newaxis]
		[self._reverse_complete_last_to_another_last_per_j(i, j)
			for j in xrange(1,i-1) # i: i-1 Y->CD. is impossible!!
		]



	def _reverse_complete_emission(self, i, emitted_symbol):
		"""
		Reverse completion of i: jY->a.
		"""
		self.outer_emit[
					i,	# i
					:,		# LHS
					emitted_symbol,		# Left RHS
					1,		# do i
					]=(
						self._get_outer_x_inner_j_eq_k(i, i-1, 1, 0, (0,2))
						+
						self._get_outer_x_inner_j_greater_k(i, i-1, 2, 1, (0,1,2))
						)

	def _reverse_complete_last_to_mid(self, i, emitted_symbol):
		"""
		Reverse completion of j: kX->B.Y (note k<j)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->BY.
		self.outer_branch[
					:i-1,	# j
					:i-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									i,	# i
									np.newaxis,	# j
									:i-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.completed_inner_branch[
									i,		#i
									:i-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.outer_branch[
					i-1,	# j
					:i-1,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									i,	# i
									:i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.inner_emit[
									i,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									emitted_symbol,		# scanned emitted_symbol
									1,			# dot i
									]
						)

	def _reverse_complete_mid_to_start(self, i, emitted_symbol):
		"""
		Reverse completion of j: kX->.YA (j=k is possible)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->Y.A
		self.outer_branch[
					:i-1,	# j
					:i-1,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot i
					]+=(
						self.outer_branch[
									i,	# i
									np.newaxis,	# j
									:i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot i
									]
						*
						self.completed_inner_branch[
									i,		# i
									:i-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->Y.A
		self.outer_branch[
					i-1,	# j
					:i,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot i
					]+=(
						self.outer_branch[
									i,	# i
									:i,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot i
									]
						*
						self.inner_emit[
									i,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									:,			# Left RHS of branch (LHS of emission)
									np.newaxis,	# Right RHS of branch 
									emitted_symbol,		# scanned emitted_symbol
									1,			# dot i
									]
						)

	def _reverse_complete(self, i, emitted_symbol):
		"""
		Reverse operation of completion.
		"""
		self._reverse_complete_last_to_another_last(i)
		self._reverse_complete_emission(i, emitted_symbol)
		self._reverse_complete_last_to_mid(i, emitted_symbol)
		self._reverse_complete_mid_to_start(i, emitted_symbol)


	def _reverse_scan(self, i, emitted_symbol):
		"""
		Reverse scanning (just scaling).
		"""
		self.outer_emit[
					i-1,	# i
					:,		# LHS
					emitted_symbol,	# Left RHS
					0,		# dot i
					]=(
						self.outer_emit[
							i,	# i+1, k=i+1-1=i
							:,		# LHS
							emitted_symbol,		# Left RHS
							1,		# dot i
							] # dot_pos=1 (i.e. k=i-1) hasn't scaled by scaling_factors[i].
						*
						self.scaling_factors[i-1,np.newaxis]
							)






# ===============================================================================
# ========================= More efficient algorthm below =======================
# ===============================================================================


def sort_data_by_length(data, max_batch_array_size=100):
	"""
	Sort data by string length.
	"""
	sorted_data = {}
	for string in data:
		string_length = len(string)
		if string_length in sorted_data.keys():
			batch_list = sorted_data[string_length]
			if batch_list[-1].size < max_batch_array_size:
				batch_list[-1] = np.append(
										batch_list[-1],
										np.array(string)[np.newaxis,:]
										,
										axis=0
										)
			else:
				batch_list.append(np.array(string)[np.newaxis,:])
		else:
			sorted_data[string_length] = [np.array(string)[np.newaxis,:]]
	return sorted_data

class Inner_Outer_Algorithm_Sort_By_Length(Inner_Outer_Algorithm):
	"""
	Stolcke's (1995) inner-outer algorithm for
	a fully connected PCFG in a Chomsky normal form.
	It simultaneously computes likelihoods of strings of the same length,
	and expected counts of rule uses to generate the strings.
	"""

	def get_log_likelihoods(self, string_batch):
		"""
		Get log likelihoods of string_batch without counting expected rule use.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1:
			return spm.logsumexp(
						np.log(self.init_weights)[:,np.newaxis]
						+
						np.log(self.emission_rule_weights[:,string_batch[:,0]])
						,
						axis=0
						).transpose()
		else:
			self._calc_forward_inner_probs()
			return spm.logsumexp(
								np.log(self.init_weights)[np.newaxis,:]
								+
								np.log(np.sum(self.inner_branch[:,-1,0,:,:,:,2], axis=(2,3)))
								-
								np.sum(np.log(self.scaling_factors), axis=1)[:,np.newaxis]
								,
								axis=0
								)


	def get_expected_rule_counts(self, string_batch):
		"""
		Returns expected # of use of each rule,
		together with log likelihoods of strings.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1: # No branching for strings of length 1
			expected_root_counts = self.init_weights[np.newaxis,:]*self.emission_rule_weights[:,string_batch[:,0]].transpose()
			likelihood = np.sum(expected_root_counts, axis=-1)
			expected_root_counts /= likelihood[:,np.newaxis]
			expected_branch_rule_counts = np.zeros((self.string_ids.size,)+self.branch_rule_weights.shape)
			expected_emission_rule_counts = np.zeros((self.string_ids.size,)+self.emission_rule_weights.shape)
			expected_emission_rule_counts[self.string_ids,:,string_batch[:,0]] += expected_root_counts
			log_like = np.log(likelihood)
		else:
			self._calc_forward_inner_probs()
			self._calc_outer_probs()
			scaled_likelihoods_for_each_root_non_terminal=(
										np.sum(self.inner_branch[:,-1,0,:,:,:,2], axis=(2,3))
										*
										self.init_weights[np.newaxis,:]
										)
			scaled_likelihood=np.sum(
									scaled_likelihoods_for_each_root_non_terminal
									,
									axis=-1
								)
			expected_branch_rule_counts=(
											np.sum(
												self.outer_branch[...,0].diagonal(axis1=1, axis2=2) # Scaled.
												*
												self.inner_branch[...,0].diagonal(axis1=1, axis2=2) # Not scaled.
												,
												axis=-1 # diagonalized dimension comes last.
												) # num_strings x num_nt x num_nt x num_nt
											/
											scaled_likelihood[:,np.newaxis,np.newaxis,np.newaxis]
											)
			
			expected_emission_rule_counts=(
										np.sum(
											self.outer_emit[...,0] # Scaled.
											*
											self.inner_emit[...,0] # Not scaled.
											,
											axis=1
											) # num_strings x num_nt x num_symbols
										/
										scaled_likelihood[:,np.newaxis,np.newaxis]
										)
			expected_root_counts = (
									scaled_likelihoods_for_each_root_non_terminal
									/
									scaled_likelihood[:,np.newaxis]
									)
			log_like = np.log(scaled_likelihood)-np.sum(np.log(self.scaling_factors), axis=-1)
		return expected_branch_rule_counts, expected_emission_rule_counts, expected_root_counts, log_like

	def _set_strings(self, string_batch):
		"""
		(Re)set emitted strings.
		Strings of the same length are assumed to be sorted.
		"""
		self.string_batch = string_batch
		self.string_length = self.string_batch.shape[1]
		self.string_ids = np.arange(self.string_batch.shape[0])




	def _calc_forward_inner_probs(self):
		"""
		Calculates forward and inner probabilities.
		Underflow is avoided by scaling.
		"""
		self.forward_branch=np.zeros(
							(
								self.string_ids.size, # string id (among the same length)
								self.string_length+1, # penultimate i i
								self.string_length+1, # start i k
								self.branch_rule_weights.shape[0], # LHS non-terminal
								self.branch_rule_weights.shape[1], # Left RHS non-terminal
								self.branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot i
							)
							)
		self.forward_emit=np.zeros(
							(
							self.string_ids.size, # string id (among the same length)
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.emission_rule_weights.shape[0], # LHS non-terminal
							self.emission_rule_weights.shape[1], # Emitted emitted_symbols
							2 # dot i
							)
							)
		self.inner_branch=np.zeros(self.forward_branch.shape)
		self.inner_emit=np.zeros(self.forward_emit.shape)
		
		# Initial prediction
		left_corner_from_init=np.matmul(
								self.init_weights
								,
								self.left_corner_weights
								)
		
		self.forward_branch[:,0,0,:,:,:,0]+=left_corner_from_init[np.newaxis,:,np.newaxis,np.newaxis]*self.branch_rule_weights[np.newaxis,:,:,:]
		self.inner_branch[:,0,0,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]

		emitted_symbols = self.string_batch[:,0]

		self.forward_emit[self.string_ids,0,:,emitted_symbols,0] += (
																		left_corner_from_init[:,np.newaxis]
																		*
																		self.emission_rule_weights[:,emitted_symbols]
																	).transpose()
		self.inner_emit[self.string_ids,0,:,emitted_symbols,0] = self.emission_rule_weights[:,emitted_symbols].transpose()
		self.scaling_factors=np.ones((self.string_ids.size,self.string_length))
		self.completed_inner_branch=np.zeros( # To be reused.
											(
											self.string_ids.size,
											self.string_length+1, # i
											self.string_length+1, # j
											self.branch_rule_weights.shape[0],
											)
											)
		[
			(
				self._forward_inner_routine(
						i,
						emitted_symbols
						)
			)
			for 
			i,
			emitted_symbols
			in enumerate(self.string_batch.transpose(), start=1) # for i=1....
		]



	def _forward_inner_routine(
						self,
						i,
						emitted_symbols,
						):
		self._scale_and_scan(i,emitted_symbols)
		if i > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last(i,emitted_symbols)
		if i < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid(i,emitted_symbols)
			self._predict(i)


	def _scale_and_scan(
			self,
			i,
			emitted_symbols,
			):
		"""
		The "scan" step of the forward/inner algorithm.
		Also scale foward and inner emission probabilities by (already scaled) prefix probability.
		"""
		# Get scalar
		self.scaling_factors[:,i-1]=np.sum(self.forward_emit[self.string_ids,i-1,:,emitted_symbols,0], axis=-1)**-1
				
		# Scanning
		self.forward_emit[self.string_ids,i,:,emitted_symbols,1]=self.forward_emit[self.string_ids,i-1,:,emitted_symbols,0]*self.scaling_factors[:,i-1,np.newaxis]
		self.inner_emit[self.string_ids,i,:,emitted_symbols,1]=self.inner_emit[self.string_ids,i-1,:,emitted_symbols,0]*self.scaling_factors[:,i-1,np.newaxis]


	def _complete_mid_to_last(
						self,
						i,
						emitted_symbols
					):
		"""
		Complete X->B.Y => X->BY.
		"""
		self.forward_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2
						]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.inner_emit[ #Scanned above.
										self.string_ids,	# string id
										i,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										emitted_symbols,		# Symbol scanned above.
										1
										]
							)
		self.inner_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.inner_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.inner_emit[ # Scanned above.
										self.string_ids,
										i,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										emitted_symbols,		# Symbol scanned above.
										1
										]
							)
		self.completed_inner_branch[:,i,i-2,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
												self.inner_branch[
														:,		# string id
														i,	# i
														i-2,		# j
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														2,		#Dot i
														]
												,
												axis=(
													2,	# Left RHS
													3	# Right RHS
													)
												)
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k(
						i,
						emitted_symbols,
						k
						)
			for k in reversed(xrange(i-2)) # k=i-3,...,0 <- Only valid on Python 2.
		]

	def _complete_mid_to_last_per_k(
						self,
						i,
						emitted_symbols,
						k
						):
		self.forward_branch[:,i,k,:,:,:,2]=(
										# j:k X->B.Y and i:j Y->CD. (j>k)
										np.sum(
											self.forward_branch[
														:,		# string id
														k+1:i-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.completed_inner_branch[
														:,		# string id
														i,	# i
														k+1:i-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										+
										# i-1:k X->B.Y and i:i-1 Y->a.
										(
											self.forward_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.inner_emit[ #Scanned above.
														self.string_ids,	# string id
														i,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														emitted_symbols,		# Symbol scanned above.
														1,
														]
											)
										)
		self.inner_branch[:,i,k,:,:,:,2]=(
										np.sum(
											self.inner_branch[
														:,		# string id
														k+1:i-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.completed_inner_branch[
														:,		# string id
														i,	# i
														k+1:i-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										+
										(
											self.inner_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.inner_emit[ # Scanned above.
														self.string_ids,	 # string id
														i,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														emitted_symbols,		# Symbol scanned above.
														1
														]
											)
										)
		self.completed_inner_branch[:,i,k,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											self.inner_branch[
													:,		# string id
													i,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot i
													]
											,
											axis=(
												2,	# Left RHS
												3	# Right RHS
												)
											)

	def _complete_start_to_mid(
						self,
						i,
						emitted_symbols
					):
		"""
		Complete X->.YA => X->Y.A
		"""
		if i>1: # No branch rule has been completed if i=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(i-1)
			self.forward_branch[
						:,	# string id
						i, # i
						:i-1, # k
						:,
						:,
						:,
						1,
						]=(
								self.forward_branch[
											:,	# string id
											j,	# j<=i-2
											j,	# k j=k is possible.
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											0,		# dot i
											].transpose((1,0,2,3,4)) # the advanced indexing by j moves the dimension to the front.
								*
								self.completed_inner_branch[
											:,		# string id
											i,	# i
											:i-1, # j
											np.newaxis, # LHS of the target.
											:, 			# Left of the RHS.
											np.newaxis,	# Right of the RHS.
											]
								)
			self.inner_branch[
					:,	# string id
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.inner_branch[
										:,	# string id
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										].transpose((1,0,2,3,4))
							*
							self.completed_inner_branch[
										:,		# string id
										i,	# i
										:i-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.forward_branch[
					:, # string id
					i, # i
					i-1, # k
					:,
					:,
					:,
					1
					]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-1,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										]
							*
							self.inner_emit[ #Scanned above.
										self.string_ids, # string id
										i,		# i
										np.newaxis,	# LHS
										:,			# Left RHS
										np.newaxis,	# Right RHS
										emitted_symbols,		# Symbol scanned above.
										1,			# dot i
										]
							)
		self.inner_branch[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.inner_branch[
									:,		# string id
									i-1,	# j=i-1
									i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot i
									]
						*
						self.inner_emit[ #Scanned above.
									self.string_ids,# string id
									i,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									emitted_symbols,	# Symbol scanned above.
									1,
									]
						)


		

	def _predict(
			self,
			i
			):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		# Only i:k X->A.Z is introduced in the completion step and is expandable.
		forward_x_left_corner=np.sum( # alpha * R(Z =>L Y)
											np.sum(
												self.forward_branch[
														:,	# string id
														i, #i
														:i, #k
														:,
														:,
														:,
														1
														]
												,
												axis=(
													1, # start i (k)
													2, # LHS non-terminal
													3, # Left RHS non-terminal.
													)
												)[:,:,np.newaxis] # num_strings x num_nonterminals (right RHS)
											*
											self.left_corner_weights[np.newaxis,:,:] # num_non_terminals x num_non_terminals
											,
											axis=1
											) # num_strings x num_non_terminals
		self.forward_branch[:,i,i,:,:,:,0]=(
										forward_x_left_corner[:,:,np.newaxis,np.newaxis]
										*
										self.branch_rule_weights[np.newaxis,:,:,:]
										)
		self.inner_branch[:,i,i,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]
		# self.string_batch[i] is the emitted_symbols to be scanned in the next iteration.
		next_emitted_symbols = self.string_batch[:,i]
		used_emission_rules = self.emission_rule_weights[:,next_emitted_symbols].transpose()

		self.forward_emit[self.string_ids,i,:,next_emitted_symbols,0]=(
										forward_x_left_corner
										*
										used_emission_rules
										)
		self.inner_emit[self.string_ids,i,:,next_emitted_symbols,0]=used_emission_rules


	# ===============END OF FORWARD-INNER ALGORITHM===================






	# ==============START OF OUTER ALGORITHM==========================

	def _calc_outer_probs(self):
		"""
		Calculate outer probabilities.
		"""
		self.outer_branch=np.zeros(self.inner_branch.shape)
		self.outer_emit=np.zeros(self.inner_emit.shape)
		# Initialization
		# Reverse completion of i:j S.->A. where i=l and j=0.
		# By i:k . -> S. and j:k . -> .S where i=l and k=0.
		# This leaves the outer probability 1.
		# And then reverse completion of i:j A->BC. where i=l and j=0.
		# By i:j S.->A. and i:j
		self.outer_branch[
					:, # string id
					-1,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot i
					]+=self.init_weights[np.newaxis,:,np.newaxis,np.newaxis]
					
		# Reverse completion of i: jY->CD.
		# When j=0 (=> k=0), i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		# But for i=l(or -1), i: 0X->Y.A and 0: 0X->.YA are not introduced.
		[
			self._reverse_complete_last_to_another_last_per_j_init(j)
			for j in xrange(1,self.string_length-1) # -1: -2 Y->CD. is impossible!!
		]
		
		# Reverse completion of i: jY->a.
		emitted_symbols = self.string_batch[:,-1]
		self.outer_emit[
					self.string_ids,
					-1,	# i
					:,		# LHS
					emitted_symbols,	# emission
					1,		# do i
					]=self._get_outer_x_inner_j_greater_k(-1, -2, 2, 1, (1, 2, 3))
		# Reverse completion of j: kX->B.Y k<=i-1
		self.outer_branch[
					:,		# string id
					:-1,	# j
					:-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									:,	# string id
									-1,	# i
									np.newaxis,	# j
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.completed_inner_branch[
									:,		# string id
									-1,		#i
									:-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.outer_branch[
					:,	# string id
					-2,	# j
					:-2,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									:,	# string id
									-1,	# i
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.inner_emit[
									self.string_ids,
									-1,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									emitted_symbols,		# scanned emitted_symbols
									1,			# dot i
									]
						)
		# Reverse completion of j: kX->.YA N/A b/c -1: 0X->Y.A is irrelevant.
		# Reverse scanning. Scaling must be changed.
		self.outer_emit[
					self.string_ids,
					-2,	# i
					:,		# LHS
					emitted_symbols,	# Left RHS
					0,		# dot i
					]=(
						self.outer_emit[
							self.string_ids, # string id
							-1,	# i+1, k=i+1-1=i
							:,		# LHS
							emitted_symbols,		# Left RHS
							1		# dot i
							] # dot_pos=1 (i.e. k=i-1)ではscaling facoters Ci が含まれていない。
						*
						self.scaling_factors[:,-1,np.newaxis]
							)
		[
			(
				self._reverse_complete(
					i,
					emitted_symbols
					)
				,
				self._reverse_scan(
					i,
					emitted_symbols
					)
			)
			for i,emitted_symbols in reversed(list(enumerate(self.string_batch[:,:-1].transpose(), start=1)))
		]


	def _get_outer_x_inner_j_greater_k(
					self,
					i,
					j,
					outer_dot_pos,
					inner_dot_pos,
					marginalization_axis
					):
		"""
		Computes (outer probability) x (inner probability) when j > k.
		"""
		return np.sum(
						self.outer_branch[ # i:k X->BY.
									:,		# string id
									i,	# i
									:j,		# k
									:,		# LHS
									:,		# Left RHS independent
									:,		# Right RHS
									outer_dot_pos,		# do i
									]
						*
						self.inner_branch[ # j:k X->B.Y k<j
									:,	# string id
									j,	# j
									:j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do i
									]
						,
						axis=marginalization_axis
					)


	def _get_outer_x_inner_j_eq_k(self, i, j, outer_dot_pos, inner_dot_pos, marginalization_axis):
		"""
		Computes (outer probability) x (inner probability) when j = k.
		"""
		return np.sum(
						self.outer_branch[ # i:k X->Y.A
									:,		# string id
									i,	# i
									j,	# k
									:,		# LHS
									:,		# Left RHS # Independent
									:,		# Right RHS
									outer_dot_pos,		# do i
									]
						*
						self.inner_branch[ # j:k X->.YA j=k
									:,	# string id
									j,	# j <- jに依存している。
									j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do i
									]
						,
						axis=marginalization_axis
					)

	def _reverse_complete_last_to_another_last_per_j_init(self, j):
		# Reverse completion of i: jY->CD. j<=i-2
		self.outer_branch[
				:,	# string id
				-1,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot i
				]=self._get_outer_x_inner_j_greater_k(
						-1,
						j,
						2,
						1,
						(1,2,3)
						)[:,:,np.newaxis,np.newaxis]



	def _reverse_complete_last_to_another_last_per_j(self, i, j):
		"""
		Reverse completion of i: jY->CD. given j.
		"""
		self.outer_branch[
				:,	# string id
				i,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot i
				]=(
					self._get_outer_x_inner_j_eq_k(i, j, 1, 0, (1,3))
					+
					self._get_outer_x_inner_j_greater_k(i, j, 2, 1, (1,2,3))
					)[:,:,np.newaxis,np.newaxis]

	def _reverse_complete_last_to_another_last(self, i):
		"""
		Reverse completion of i: jY->CD.
		"""
		# When j=0, k=0 as well.
		# i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		self.outer_branch[
					:,		# string id
					i,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot i
					]=self._get_outer_x_inner_j_eq_k(i, 0, 1, 0, (1,3))[:,:,np.newaxis,np.newaxis]
		[self._reverse_complete_last_to_another_last_per_j(i, j)
			for j in xrange(1,i-1) # i: i-1 Y->CD. is impossible!!
		]



	def _reverse_complete_emission(self, i, emitted_symbols):
		"""
		Reverse completion of i: jY->a.
		"""
		self.outer_emit[
					self.string_ids, # string id
					i,	# i
					:,		# LHS
					emitted_symbols,		# Left RHS
					1,		# do i
					]=(
						self._get_outer_x_inner_j_eq_k(i, i-1, 1, 0, (1,3))
						+
						self._get_outer_x_inner_j_greater_k(i, i-1, 2, 1, (1,2,3))
						)

	def _reverse_complete_last_to_mid(self, i, emitted_symbols):
		"""
		Reverse completion of j: kX->B.Y (note k<j)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->BY.
		self.outer_branch[
					:,		# string id
					:i-1,	# j
					:i-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									:,		# string id
									i,	# i
									np.newaxis,	# j
									:i-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.completed_inner_branch[
									:,			# string id
									i,		#i
									:i-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.outer_branch[
					:,		# string id
					i-1,	# j
					:i-1,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									:,		# string id
									i,	# i
									:i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.inner_emit[
									self.string_ids,	 # string id
									i,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									emitted_symbols,		# scanned emitted_symbols
									1,			# dot i
									]
						)

	def _reverse_complete_mid_to_start(self, i, emitted_symbols):
		"""
		Reverse completion of j: kX->.YA (j=k is possible)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->Y.A
		self.outer_branch[
					:,		# string id
					:i-1,	# j
					:i-1,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot i
					]+=(
						self.outer_branch[
									:,		# string id 
									i,	# i
									np.newaxis,	# j
									:i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot i
									]
						*
						self.completed_inner_branch[
									:,			# string id
									i,		# i
									:i-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->Y.A
		self.outer_branch[
					:,
					i-1,	# j
					:i,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot i
					]+=(
						self.outer_branch[
									:,		# string id
									i,	# i
									:i,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot i
									]
						*
						self.inner_emit[
									self.string_ids,	# string id
									i,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									:,			# Left RHS of branch (LHS of emission)
									np.newaxis,	# Right RHS of branch 
									emitted_symbols,		# scanned emitted_symbols
									1,			# dot i
									]
						)

	def _reverse_complete(self, i, emitted_symbols):
		"""
		Reverse operation of completion.
		"""
		self._reverse_complete_last_to_another_last(i)
		self._reverse_complete_emission(i, emitted_symbols)
		self._reverse_complete_last_to_mid(i, emitted_symbols)
		self._reverse_complete_mid_to_start(i, emitted_symbols)


	def _reverse_scan(self, i, emitted_symbols):
		"""
		Reverse scanning (just scaling).
		"""
		self.outer_emit[
					self.string_ids, # string id
					i-1,	# i
					:,		# LHS
					emitted_symbols,	# Left RHS
					0,		# dot i
					]=(
						self.outer_emit[
							self.string_ids, # string id
							i,	# i+1, k=i+1-1=i
							:,		# LHS
							emitted_symbols,		# Left RHS
							1,		# dot i
							] # dot_pos=1 (i.e. k=i-1) hasn't scaled by scaling_factors[i].
						*
						self.scaling_factors[:,i-1,np.newaxis]
							)



# ===============================================================================
# ========================= Generalized emission below =======================
# ===============================================================================


def sort_data_by_length_multivariate_values(data, max_batch_array_size=650):
	"""
	Sort multivariate data by string length.
	"""
	sorted_data = {}
	for string in data:
		string_length = len(string)
		if string_length in sorted_data.keys():
			batch_list = sorted_data[string_length]
			if batch_list[-1].size <= max_batch_array_size:
				batch_list[-1] = np.append(
										batch_list[-1],
										np.array(string)[np.newaxis,:,:]
										,
										axis=0
										)
			else:
				batch_list.append(np.array(string)[np.newaxis,:,:])
		else:
			sorted_data[string_length] = [np.array(string)[np.newaxis,:,:]]
	return sorted_data





class Inner_Outer_Algorithm_Multivariate_Emission_Sort_By_Length(Inner_Outer_Algorithm_Sort_By_Length):
	"""
	Stolcke's (1995) inner-outer algorithm for
	a fully connected PCFG in a Chomsky normal form with general multivariate emission.
	Log emission probability density function (log_emission_pdf)
	must be defined and passed to the object constructor instead of log_emission_rule_weights.
	It simultaneously computes likelihoods of strings of the same length,
	and expected counts of rule uses to generate the strings.
	"""

	def __init__(
			self,
			log_branch_rule_weights,
			log_emission_pdf,
			log_init_weights,
			):
		"""
		Initialize rule weights.
		"""
		self.branch_rule_weights = np.exp(log_branch_rule_weights)
		self.log_branch_normalizer = np.sum(self.branch_rule_weights)
		self.branch_rule_weights /= self.log_branch_normalizer # Make sure that the weights are under 1.
		self.log_branch_normalizer = np.log(self.log_branch_normalizer)
		self.log_emission_pdf = log_emission_pdf
		self.init_weights = np.exp(log_init_weights)
		self.left_corner_weights = self._get_left_corner_probs()

	def _set_strings(self, string_batch):
		"""
		(Re)set emitted strings.
		Strings of the same length are assumed to be sorted.
		"""
		self.log_emission_probs = self.log_emission_pdf(string_batch) # num_strings x string_len x ndim
		self.string_length = string_batch.shape[1]
		self.num_strings = string_batch.shape[0]

	def get_expected_rule_counts(self, string_batch):
		"""
		Returns expected # of use of each rule,
		together with log likelihoods of strings.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1: # No branching for strings of length 1
			log_expected_init_weights = np.log(self.init_weights[np.newaxis,:])+self.log_emission_probs[:,0,:]
			log_like = spm.logsumexp(log_expected_init_weights, axis=-1)
			expected_root_counts = np.exp(log_expected_init_weights-log_like[:,np.newaxis])
			expected_branch_rule_counts = np.zeros((self.num_strings,)+self.branch_rule_weights.shape)
			expected_emission_counts_per_pos = expected_root_counts[:,np.newaxis,:]
		else:
			self._calc_forward_inner_probs()
			self._calc_outer_probs()
			
			likelihoods_for_each_root_non_terminal = (
										np.sum(self.inner_branch[:,-1,0,:,:,:,2], axis=(2,3))
										*
										self.init_weights[np.newaxis,:]
										)
			likelihood = np.sum(
									likelihoods_for_each_root_non_terminal
									,
									axis=-1
								)
			expected_branch_rule_counts = (
											np.sum(
												self.outer_branch[...,0].diagonal(axis1=1, axis2=2) # Scaled.
												*
												self.inner_branch[...,0].diagonal(axis1=1, axis2=2) # Not scaled.
												,
												axis=-1 # diagonalized dimension comes last.
												) # num_strings x num_nt x num_nt x num_nt
											/
											likelihood[:,np.newaxis,np.newaxis,np.newaxis]
											)
			expected_emission_counts_per_pos = (
										(
											self.outer_emit[:,:-1,:] # Not scaled.
											*
											self.inner_emit[:,:-1,:] # Scaled.
											) # num_strings x (string_length + 1) x num_nt
										/
										likelihood[:,np.newaxis,np.newaxis]
										)
			expected_root_counts = (
									likelihoods_for_each_root_non_terminal
									/
									likelihood[:,np.newaxis]
									)
			log_like = np.log(likelihood) - np.sum(self.log_scaling_factors, axis=-1)
		log_like += self.log_branch_normalizer * (self.string_length - 1)
		return expected_branch_rule_counts, expected_emission_counts_per_pos, expected_root_counts, log_like



	# ============= BEGINNING OF FORWARD-INNER ALGORITHM =======================================================

	def _calc_forward_inner_probs(self):
		"""
		Calculates forward and inner probabilities.
		Underflow is avoided by scaling.
		"""
		self.forward_branch = np.zeros(
							(
								self.num_strings, # string id (among the same length)
								self.string_length+1, # penultimate i i
								self.string_length+1, # start i k
								self.branch_rule_weights.shape[0], # LHS non-terminal
								self.branch_rule_weights.shape[1], # Left RHS non-terminal
								self.branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot i
							)
							)
		# forward_emit is not quite useful except the scaling, which doesn't need to be memorized. So omitted.

		self.inner_branch=np.zeros(self.forward_branch.shape)
		self.inner_emit = np.zeros(
							(
							self.num_strings, # string id (among the same length)
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.branch_rule_weights.shape[0] # LHS non-terminal
							# dot i is redundant, so omitted.
							)
							)
		
		# Initial prediction
		left_corner_from_init=np.matmul(
								self.init_weights
								,
								self.left_corner_weights
								)
		
		self.forward_branch[:,0,0,:,:,:,0]=left_corner_from_init[np.newaxis,:,np.newaxis,np.newaxis]*self.branch_rule_weights[np.newaxis,:,:,:]
		self.inner_branch[:,0,0,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]


		log_forward_emit = (
							np.log(left_corner_from_init)[np.newaxis,:]
							+
							self.log_emission_probs[:,0,:]
							)
		# scaling
		self.log_scaling_factors = np.zeros((self.num_strings,self.string_length))
		self.log_scaling_factors[:,0] = - spm.logsumexp(log_forward_emit, axis=-1)
		self.inner_emit[:,0,:] = np.exp(self.log_emission_probs[:,0,:] + self.log_scaling_factors[:,0,np.newaxis])
		self.completed_inner_branch=np.zeros( # To be reused.
											(
											self.num_strings,
											self.string_length+1, # i
											self.string_length+1, # j
											self.branch_rule_weights.shape[0],
											)
											)
		[
			(
				self._forward_inner_routine(
						i
						)
			)
			for 
			i
			in xrange(1,self.string_length+1) # for i=1....
		]



	def _forward_inner_routine(
						self,
						i
						):
		# Scan step is now omitted, or merged into the previous prediction.
		if i > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last(i)
		if i < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid(i)
			self._predict(i)




	def _complete_mid_to_last(
						self,
						i
					):
		"""
		Complete X->B.Y => X->BY.
		"""
		self.forward_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2
						]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.inner_emit[ #Scanned above.
										:,	# string id
										i-1,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.inner_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.inner_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.inner_emit[ # Scanned above.
										:,
										i-1,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.completed_inner_branch[:,i,i-2,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
												self.inner_branch[
														:,		# string id
														i,	# i
														i-2,		# j
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														2,		#Dot i
														]
												,
												axis=(
													2,	# Left RHS
													3	# Right RHS
													)
												)
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k(
						i,
						k
						)
			for k in reversed(xrange(i-2)) # k=i-3,...,0 <- Only valid on Python 2.
		]

	def _complete_mid_to_last_per_k(
						self,
						i,
						k
						):
		self.forward_branch[:,i,k,:,:,:,2]=(
										# j:k X->B.Y and i:j Y->CD. (j>k)
										np.sum(
											self.forward_branch[
														:,		# string id
														k+1:i-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.completed_inner_branch[
														:,		# string id
														i,	# i
														k+1:i-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										+
										# i-1:k X->B.Y and i:i-1 Y->a.
										(
											self.forward_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.inner_emit[ #Scanned above.
														:,	# string id
														i-1,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														]
											)
										)
		self.inner_branch[:,i,k,:,:,:,2]=(
										np.sum(
											self.inner_branch[
														:,		# string id
														k+1:i-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.completed_inner_branch[
														:,		# string id
														i,	# i
														k+1:i-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										+
										(
											self.inner_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.inner_emit[ # Scanned above.
														:,	 # string id
														i-1,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														]
											)
										)
		self.completed_inner_branch[:,i,k,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											self.inner_branch[
													:,		# string id
													i,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot i
													]
											,
											axis=(
												2,	# Left RHS
												3	# Right RHS
												)
											)

	def _complete_start_to_mid(
						self,
						i
					):
		"""
		Complete X->.YA => X->Y.A
		"""
		if i>1: # No branch rule has been completed if i=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(i-1)
			self.forward_branch[
						:,	# string id
						i, # i
						:i-1, # k
						:,
						:,
						:,
						1,
						]=(
								self.forward_branch[
											:,	# string id
											j,	# j<=i-2
											j,	# k j=k is possible.
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											0,		# dot i
											].transpose((1,0,2,3,4)) # the advanced indexing by j moves the dimension to the front.
								*
								self.completed_inner_branch[
											:,		# string id
											i,	# i
											:i-1, # j
											np.newaxis, # LHS of the target.
											:, 			# Left of the RHS.
											np.newaxis,	# Right of the RHS.
											]
								)
			self.inner_branch[
					:,	# string id
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.inner_branch[
										:,	# string id
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										].transpose((1,0,2,3,4))
							*
							self.completed_inner_branch[
										:,		# string id
										i,	# i
										:i-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.forward_branch[
					:, # string id
					i, # i
					i-1, # k
					:,
					:,
					:,
					1
					]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-1,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										]
							*
							self.inner_emit[ #Scanned above.
										:, # string id
										i-1,		# i
										np.newaxis,	# LHS
										:,			# Left RHS
										np.newaxis,	# Right RHS
										]
							)
		self.inner_branch[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.inner_branch[
									:,		# string id
									i-1,	# j=i-1
									i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot i
									]
						*
						self.inner_emit[ #Scanned above.
									:,# string id
									i-1,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)


		

	def _predict(
			self,
			i
			):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		# Only i:k X->A.Z is introduced in the completion step and is expandable.
		forward_x_left_corner=np.sum( # alpha * R(Z =>L Y)
											np.sum(
												self.forward_branch[
														:,	# string id
														i, #i
														:i, #k
														:,
														:,
														:,
														1
														]
												,
												axis=(
													1, # start i (k)
													2, # LHS non-terminal
													3, # Left RHS non-terminal.
													)
												)[:,:,np.newaxis] # num_strings x num_nonterminals (right RHS)
											*
											self.left_corner_weights[np.newaxis,:,:] # num_non_terminals x num_non_terminals
											,
											axis=1
											) # num_strings x num_non_terminals
		self.forward_branch[:,i,i,:,:,:,0]=(
										forward_x_left_corner[:,:,np.newaxis,np.newaxis]
										*
										self.branch_rule_weights[np.newaxis,:,:,:]
										)
		self.inner_branch[:,i,i,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]
		log_forward_emit = (
								np.log(forward_x_left_corner)
								+
								self.log_emission_probs[:,i,:]
							)
		self.log_scaling_factors[:,i] = - spm.logsumexp(log_forward_emit, axis=-1)
		self.inner_emit[:,i,:] = np.exp(self.log_emission_probs[:,i,:] + self.log_scaling_factors[:,i,np.newaxis])


	# ===============END OF FORWARD-INNER ALGORITHM===================






	# ==============START OF OUTER ALGORITHM==========================

	def _calc_outer_probs(self):
		"""
		Calculate outer probabilities.
		"""
		self.outer_branch = np.zeros(self.inner_branch.shape)
		self.outer_emit = np.zeros(self.inner_emit.shape)
		# Initialization
		# Reverse completion of i:j S.->A. where i=l and j=0.
		# By i:k . -> S. and j:k . -> .S where i=l and k=0.
		# This leaves the outer probability 1.
		# And then reverse completion of i:j A->BC. where i=l and j=0.
		# By i:j S.->A. and i:j
		self.outer_branch[
					:, # string id
					-1,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot i
					] += self.init_weights[np.newaxis,:,np.newaxis,np.newaxis]
					
		# Reverse completion of i: jY->CD.
		# When j=0 (=> k=0), i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		# But for i=l(or -1), i: 0X->Y.A and 0: 0X->.YA are not introduced.
		[
			self._reverse_complete_last_to_another_last_per_j_init(j)
			for j in xrange(1,self.string_length-1) # -1: -2 Y->CD. is impossible!!
		]
		
		# Reverse completion of i: jY->a.
		self.outer_emit[
					:,
					-2,	# i
					:,		# LHS
					]=self._get_outer_x_inner_j_greater_k(-1, -2, 2, 1, (1, 2, 3))
		# Reverse completion of j: kX->B.Y k<=i-1
		self.outer_branch[
					:,		# string id
					:-1,	# j
					:-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									:,	# string id
									-1,	# i
									np.newaxis,	# j
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.completed_inner_branch[
									:,		# string id
									-1,		#i
									:-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.outer_branch[
					:,	# string id
					-2,	# j
					:-2,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									:,	# string id
									-1,	# i
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.inner_emit[
									:,
									-2,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									]
						)
		# Reverse completion of j: kX->.YA N/A b/c -1: 0X->Y.A is irrelevant.
		# Reverse scanning is no longer necessary.
		[
				self._reverse_complete(
					i
					)
			for i in xrange(self.string_length-1,0,-1)
		]


	def _get_outer_x_inner_j_greater_k(
					self,
					i,
					j,
					outer_dot_pos,
					inner_dot_pos,
					marginalization_axis
					):
		"""
		Computes (outer probability) x (inner probability) when j > k.
		"""
		return np.sum(
						self.outer_branch[ # i:k X->BY.
									:,		# string id
									i,	# i
									:j,		# k
									:,		# LHS
									:,		# Left RHS independent
									:,		# Right RHS
									outer_dot_pos,		# do i
									]
						*
						self.inner_branch[ # j:k X->B.Y k<j
									:,	# string id
									j,	# j
									:j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do i
									]
						,
						axis=marginalization_axis
					)


	def _get_outer_x_inner_j_eq_k(self, i, j, outer_dot_pos, inner_dot_pos, marginalization_axis):
		"""
		Computes (outer probability) x (inner probability) when j = k.
		"""
		return np.sum(
						self.outer_branch[ # i:k X->Y.A
									:,		# string id
									i,	# i
									j,	# k
									:,		# LHS
									:,		# Left RHS # Independent
									:,		# Right RHS
									outer_dot_pos,		# do i
									]
						*
						self.inner_branch[ # j:k X->.YA j=k
									:,	# string id
									j,	# j <- jに依存している。
									j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do i
									]
						,
						axis=marginalization_axis
					)

	def _reverse_complete_last_to_another_last_per_j_init(self, j):
		# Reverse completion of i: jY->CD. j<=i-2
		self.outer_branch[
				:,	# string id
				-1,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot i
				]=self._get_outer_x_inner_j_greater_k(
						-1,
						j,
						2,
						1,
						(1,2,3)
						)[:,:,np.newaxis,np.newaxis]



	def _reverse_complete_last_to_another_last_per_j(self, i, j):
		"""
		Reverse completion of i: jY->CD. given j.
		"""
		self.outer_branch[
				:,	# string id
				i,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot i
				]=(
					self._get_outer_x_inner_j_eq_k(i, j, 1, 0, (1,3))
					+
					self._get_outer_x_inner_j_greater_k(i, j, 2, 1, (1,2,3))
					)[:,:,np.newaxis,np.newaxis]

	def _reverse_complete_last_to_another_last(self, i):
		"""
		Reverse completion of i: jY->CD.
		"""
		# When j=0, k=0 as well.
		# i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		self.outer_branch[
					:,		# string id
					i,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot i
					]=self._get_outer_x_inner_j_eq_k(i, 0, 1, 0, (1,3))[:,:,np.newaxis,np.newaxis]
		[self._reverse_complete_last_to_another_last_per_j(i, j)
			for j in xrange(1,i-1) # i: i-1 Y->CD. is impossible!!
		]



	def _reverse_complete_emission(self, i):
		"""
		Reverse completion of i: jY->a.
		"""
		self.outer_emit[
					:, # string id
					i-1,	# i
					:,		# LHS
					]=(
						self._get_outer_x_inner_j_eq_k(i, i-1, 1, 0, (1,3))
						+
						self._get_outer_x_inner_j_greater_k(i, i-1, 2, 1, (1,2,3))
						)

	def _reverse_complete_last_to_mid(self, i):
		"""
		Reverse completion of j: kX->B.Y (note k<j)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->BY.
		self.outer_branch[
					:,		# string id
					:i-1,	# j
					:i-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									:,		# string id
									i,	# i
									np.newaxis,	# j
									:i-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.completed_inner_branch[
									:,			# string id
									i,		#i
									:i-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.outer_branch[
					:,		# string id
					i-1,	# j
					:i-1,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									:,		# string id
									i,	# i
									:i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.inner_emit[
									:,	 # string id
									i-1,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									]
						)

	def _reverse_complete_mid_to_start(self, i):
		"""
		Reverse completion of j: kX->.YA (j=k is possible)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->Y.A
		self.outer_branch[
					:,		# string id
					:i-1,	# j
					:i-1,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot i
					]+=(
						self.outer_branch[
									:,		# string id 
									i,	# i
									np.newaxis,	# j
									:i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot i
									]
						*
						self.completed_inner_branch[
									:,			# string id
									i,		# i
									:i-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->Y.A
		self.outer_branch[
					:,
					i-1,	# j
					:i,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot i
					]+=(
						self.outer_branch[
									:,		# string id
									i,	# i
									:i,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot i
									]
						*
						self.inner_emit[
									:,	# string id
									i-1,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									:,			# Left RHS of branch (LHS of emission)
									np.newaxis,	# Right RHS of branch 
									]
						)

	def _reverse_complete(self, i):
		"""
		Reverse operation of completion.
		"""
		self._reverse_complete_last_to_another_last(i)
		self._reverse_complete_emission(i)
		self._reverse_complete_last_to_mid(i)
		self._reverse_complete_mid_to_start(i)




# ===========END OF OUTER ALGORITHM =======================

# ===========START OF VITERBI ALGORITHM ===================

	def get_viterbi_parse(self, string_batch):
		"""
		Returns expected # of use of each rule,
		together with log likelihoods of strings.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1: # No branching for strings of length 1
			log_expected_init_weights = np.log(self.init_weights[np.newaxis,:])+self.log_emission_probs[:,0,:]
			most_probable_roots = np.argmax(log_expected_init_weights, axis=-1)
			viterbi_parses = [tree.Terminal(label = mpr) for mpr in most_probable_roots]
		else:
			self._calc_viterbi_probs()
			likelihoods_for_each_root_and_children = (
							self.inner_branch[:,-1,0,:,:,:,2]
							*
							self.init_weights[np.newaxis,:,np.newaxis,np.newaxis]
						)
			max_ids = np.argmax(likelihoods_for_each_root_and_children.reshape(self.num_strings,-1) ,axis=(-1))
			string_ids = np.indices(max_ids.shape)
			max_roots, max_child_left, max_child_right = np.unravel_index(max_ids, self.viterbi_branch.shape[3:6])
			viterbi_parses = [
				self._get_viterbi_subparse_branch(-1, 0, rt_lb, rhs_l, rhs_r, 2, string_id)
				for string_id,rt_lb,rhs_l,rhs_r
				in zip(string_ids, max_roots, max_child_left, max_child_right)
				]
		return viterbi_parses

	def _get_viterbi_subparse_branch(self, i, k, lhs, rhs_left, rhs_right, dot_pos, string_id, mother_node = None):
		if dot_pos == 0:
			subtree_root = tree.Node(lhs, mother=mother_node)
			return subtree_root
		elif dot_pos == 1:
			child_lhs = rhs_left
		else:
			child_lhs = rhs_right
		branch_or_emit = self.viterbi_predecessor[string_id,i,k,lhs,rhs_left,rhs_right,dot_pos,0]
		j = self.viterbi_predecessor[string_id,i,k,lhs,rhs_left,rhs_right,dot_pos,1]
		child_rhs_left_or_terminal = self.viterbi_predecessor[string_id,i,k,lhs,rhs_left,rhs_right,dot_pos,2]
		child_rhs_right = self.viterbi_predecessor[string_id,i,k,lhs,rhs_left,rhs_right,dot_pos,3]
		subtree_root = self._get_viterbi_subparse_branch(j, k, lhs, rhs_left, rhs_right, dot_pos-1, string_id, mother_node = mother_node)
		if branch_or_emit: # if the predecessor is emission.
			child_tree = self._get_viterbi_subparse_emit(child_lhs, subtree_root)
		else:
			child_tree = self._get_viterbi_subparse_branch(i, j, child_lhs, child_rhs_left_or_terminal, child_rhs_right, 2, string_id, mother_node = subtree_root)
		return subtree_root

	def _get_viterbi_subparse_emit(self, lhs, mother_node):
		"""
		Multivariate data do not have terminals.
		So, we stop at the part-of-speech.
		"""
		part_of_speech = tree.Terminal(lhs, mother=mother_node)
		return part_of_speech
		

	def _calc_viterbi_probs(self):
		"""
		Calculates forward, inner, and viterbi probabilities.
		Underflow is avoided by scaling.
		(Forward and inner probs are used for scaling.)
		"""
		self.forward_branch=np.zeros(
							(
								self.num_strings, # string id (among the same length)
								self.string_length+1, # penultimate i i
								self.string_length+1, # start i k
								self.branch_rule_weights.shape[0], # LHS non-terminal
								self.branch_rule_weights.shape[1], # Left RHS non-terminal
								self.branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot i
							)
							)
		self.inner_branch=np.zeros(self.forward_branch.shape)
		self.inner_emit=np.zeros(
							(
							self.num_strings, # string id (among the same length)
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.branch_rule_weights.shape[0], # LHS non-terminal
							)
							)

		self.viterbi_branch = np.zeros(self.inner_branch.shape)
		self.viterbi_emit = np.zeros(self.inner_emit.shape)
		
		# Initial prediction
		left_corner_from_init=np.matmul(
								self.init_weights
								,
								self.left_corner_weights
								)
		
		self.forward_branch[:,0,0,:,:,:,0]=left_corner_from_init[np.newaxis,:,np.newaxis,np.newaxis]*self.branch_rule_weights[np.newaxis,:,:,:]
		self.inner_branch[:,0,0,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]
		self.viterbi_branch[:,0,0,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]
		


		log_forward_emit = (
							np.log(left_corner_from_init)[np.newaxis,:]
							+
							self.log_emission_probs[:,0,:]
							)
		# scaling
		self.log_scaling_factors = np.zeros((self.num_strings,self.string_length))
		self.log_scaling_factors[:,0] = - spm.logsumexp(log_forward_emit, axis=-1)
		self.inner_emit[:,0,:] = np.exp(self.log_emission_probs[:,0,:] + self.log_scaling_factors[:,0,np.newaxis])
		self.viterbi_emit[:,0,:] = self.inner_emit[:,0,:]
		self.completed_inner_branch=np.zeros( # To be reused.
											(
											self.num_strings,
											self.string_length+1, # i
											self.string_length+1, # j
											self.branch_rule_weights.shape[0],
											)
											)
		self.completed_viterbi_branch = np.zeros(self.completed_inner_branch.shape)
		self.viterbi_predecessor = np.full(
										self.viterbi_branch.shape
										+
										(4,) # Info
												# 1st entry for branch (0) vs. emission (1).
												# 2nd entry for j
												# 3rd entry for RHS_left/terminal
												# 4th entry for RHS_right
										,
										100
									)
		self.viterbi_predecessor_for_completed = np.full(
										self.completed_inner_branch.shape
										+
										(2,)
										,
										100
									)
		[
			(
				self._viterbi_routine(
						i
						)
			)
			for 
			i
			in xrange(1,self.string_length+1) # for i=1....
		]



	def _viterbi_routine(
						self,
						i
						):
		# Scanning is omitted or merged to prediction.
		if i > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last(i)
			self._complete_mid_to_last_viterbi(i)
		if i < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid(i)
			self._complete_start_to_mid_viterbi(i)
			self._predict(i)
			self._predict_viterbi(i)


	def _complete_mid_to_last_viterbi(
						self,
						i
					):
		"""
		Complete X->B.Y => X->BY. for Viterbi.
		"""
		self.viterbi_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.viterbi_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.viterbi_emit[ # Scanned above.
										:,
										i-1,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.viterbi_predecessor[
			:, # string id
			i, # i
			i - 2, # k
			:, # LHS
			:, # RHS left
			:, # RHS right
			2, # dot i
			0, # info type (0): predecessor = branch (0) or emit (1)?
		] = 1 # predecessor must be emission.
		self.viterbi_predecessor[
			:, # string id
			i, # i
			i - 2, # k
			:, # LHS
			:, # RHS left
			:, # RHS right
			2, # dot i
			1, # info type (1): j
		] = i - 1
		reshaped_viterbi_branch = self.viterbi_branch[
											:, # string id
											i - 2, # j
											:, # LHS
											:, # left RHS
											:, # right RHS
											2, # dot i
										].reshape(
											self.viterbi_branch.shape[0],
											self.viterbi_branch.shape[3], # num_nt
											-1
										)
		max_ids = np.argmax(reshaped_viterbi_branch, axis=-1)
		string_ids, lhss = np.indices(max_ids.shape)
		self.completed_viterbi_branch[:,i,i-2,:] = reshaped_viterbi_branch[string_ids, lhss, max_ids]
		max_ids_left_right = np.unravel_index(max_ids, (self.viterbi_branch.shape[3],)*2)
		self.viterbi_predecessor_for_completed[:,i-2,:,0] = max_ids_left_right[0]
		self.viterbi_predecessor_for_completed[:,i-2,:,1] = max_ids_left_right[1]
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k_viterbi(
						i,
						k
						)
			for k in reversed(xrange(i-2)) # k=i-3,...,0 <- Only valid on Python 2.
		]

	def _complete_mid_to_last_per_k_viterbi(
						self,
						i,
						k
						):
		viterbi_branch_x_completed = (
										self.viterbi_branch[
											:,		# string id
											k+1:i-1,	# j
											k,		#k
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											1,		# dot i
											]
										*
										self.completed_viterbi_branch[
											:,		# string id
											i,	# i
											k+1:i-1, # j
											np.newaxis, # LHS of the target.
											np.newaxis, # Left of the RHS.
											:, # Right of the RHS.
										]
							)
		branch_max_ids = np.argmax(viterbi_branch_x_completed, axis=1)
		string_ids,lhss,rhs_ls,rhs_rs = np.indices(branch_max_ids.shape)
		max_viterbi_branch_x_completed = viterbi_branch_x_completed[string_ids,branch_max_ids,lhss,rhs_ls,rhs_rs]
		viterbi_branch_x_emit = (
								self.viterbi_branch[
									:,		# string id
									i-1,	# j=i-1
									k,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot i
									]
								*
								self.viterbi_emit[ #Scanned above.
									:,	# string id
									i-1,		# i
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
								)
		branch_vs_emit = np.array(max_viterbi_branch_x_completed, viterbi_branch_x_emit)
		branch_or_emit_ids = np.argmax(branch_vs_emit, axis=0)
		self.viterbi_branch[:,i,k,:,:,:,2] = branch_vs_emit[branch_or_emit_ids, string_ids, lhss, rhs_ls, rhs_rs]
		self.viterbi_predecessor[:,i,k,:,:,:,2,0] = branch_or_emit_ids
		branch_js = np.arange(k+1,i-1)[branch_max_ids]
		self.viterbi_predecessor[:,i,k,:,:,:,2,1] = (
															branch_js * (branch_or_emit_ids == 0)
															+
															(i-1) * branch_or_emit_ids
														)
		self.viterbi_predecessor[:,i,k,:,:,:,2,2] = self.viterbi_predecessor_for_completed[string_ids,i,branch_js,lhss,0]
		self.viterbi_predecessor[:,i,k,:,:,:,2,3] = self.viterbi_predecessor_for_completed[string_ids,i,branch_js,lhss,1]

		reshaped_viterbi_branch = self.viterbi_branch[
													:,		# string id
													i,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot i
													].reshape(
														self.viterbi_branch.shape[0],
														self.viterbi_branch.shape[3],
														self.viterbi_branch.shape[3]**2
													)
		completed_max_ids = np.argmax(reshaped_viterbi_branch, axis=-1)
		string_ids, lhss = np.indices(completed_max_ids.shape)
		self.completed_viterbi_branch[:,i,k,:] = reshaped_viterbi_branch[string_ids,lhss,completed_max_ids]
		max_ids_left_right = np.unravel_index(completed_max_ids, (self.viterbi_branch.shape[3],)*2)
		self.viterbi_predecessor_for_completed[:,i,k,:,0] = max_ids_left_right[0]
		self.viterbi_predecessor_for_completed[:,i,k,:,1] = max_ids_left_right[1]


	def _complete_start_to_mid_viterbi(
						self,
						i
					):
		"""
		Complete X->.YA => X->Y.A for Viterbi.
		"""
		if i>1: # No branch rule has been completed if i=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(i-1)
			self.viterbi_branch[
					:,	# string id
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.viterbi_branch[
										:,	# string id
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										].transpose((1,0,2,3,4))
							*
							self.completed_viterbi_branch[
										:,		# string id
										i,	# i
										:i-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
		self.viterbi_predecessor[
					:,	# string id
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					0
					] = 0
		self.viterbi_predecessor[
					:,	# string id
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					1
					] = j[np.newaxis,:,np.newaxis,np.newaxis,np.newaxis]
		self.viterbi_predecessor[
					:,	# string id
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					2
					] = self.viterbi_predecessor_for_completed[:,i,:i-1,np.newaxis,np.newaxis,:,0]
		self.viterbi_predecessor[
					:,	# string id
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					3
					] = self.viterbi_predecessor_for_completed[:,i,:i-1,np.newaxis,np.newaxis,:,1]
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.viterbi_branch[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.viterbi_branch[
									:,		# string id
									i-1,	# j=i-1
									i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot i
									]
						*
						self.viterbi_emit[ #Scanned above.
									:,# string id
									i-1,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)
		self.viterbi_predecessor[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				0
				] = 1
		self.viterbi_predecessor[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				1
				] = i - 1





	def _predict_viterbi(
			self,
			i
			):
		"""
		The "prediction" step of the forward/inner algorithm for Viterbi.
		"""
		# Prediction
		# Exactly the same as the inner probs.
		self.viterbi_branch[:,i,i,:,:,:,0]=self.inner_branch[:,i,i,:,:,:,0]
		self.viterbi_emit[:,i,:] = self.inner_emit[:,i,:]



# ============= BEGINNING OF RIGHT-BRANCHING PARSE =======================================================
	
	def get_log_like_right_branching(self, string_batch):
		"""
		Get the log likelihood of right-branching parses.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1: # All the parses are regular if length < 3.
			log_expected_init_weights = np.log(self.init_weights[np.newaxis,:])+self.log_emission_probs[:,0,:]
			log_like = spm.logsumexp(log_expected_init_weights, axis=-1)
		else:
			self._calc_forward_inner_probs_right_branching()
			likelihoods_for_each_root_non_terminal = (
										np.sum(self.inner_branch[:,-1,0,:,:,:,2], axis=(2,3))
										*
										self.init_weights[np.newaxis,:]
										)
			likelihood = np.sum(
									likelihoods_for_each_root_non_terminal
									,
									axis=-1
								)
			log_like = np.log(likelihood) - np.sum(self.log_scaling_factors, axis=-1)
			log_like += self.log_branch_normalizer * (self.string_length - 1)
		return log_like



	def _calc_forward_inner_probs_right_branching(self):
		"""
		Calculates forward and inner probabilities only allowing right-branching parses.
		Underflow is avoided by scaling.
		"""
		self.forward_branch = np.zeros(
							(
								self.num_strings, # string id (among the same length)
								self.string_length+1, # penultimate i i
								self.string_length+1, # start i k
								self.branch_rule_weights.shape[0], # LHS non-terminal
								self.branch_rule_weights.shape[1], # Left RHS non-terminal
								self.branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot i
							)
							)
		# forward_emit is not quite useful except the scaling, which doesn't need to be memorized. So omitted.

		self.inner_branch=np.zeros(self.forward_branch.shape)
		self.inner_emit = np.zeros(
							(
							self.num_strings, # string id (among the same length)
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.branch_rule_weights.shape[0] # LHS non-terminal
							# dot i is redundant, so omitted.
							)
							)
		
		# Initial prediction
		left_corner_from_init=np.matmul(
								self.init_weights
								,
								self.left_corner_weights
								)
		self.forward_branch[:,0,0,:,:,:,0]=left_corner_from_init[np.newaxis,:,np.newaxis,np.newaxis]*self.branch_rule_weights[np.newaxis,:,:,:]
		self.inner_branch[:,0,0,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]


		log_forward_emit = (
							np.log(self.init_weights)[np.newaxis,:]
							+
							self.log_emission_probs[:,0,:]
							)
		# scaling
		self.log_scaling_factors = np.zeros((self.num_strings,self.string_length))
		self.log_scaling_factors[:,0] = - spm.logsumexp(log_forward_emit, axis=-1)
		self.inner_emit[:,0,:] = np.exp(self.log_emission_probs[:,0,:] + self.log_scaling_factors[:,0,np.newaxis])
		self.completed_inner_branch=np.zeros( # To be reused.
											(
											self.num_strings,
											self.string_length+1, # i
											self.string_length+1, # j
											self.branch_rule_weights.shape[0],
											)
											)
		[
			(
				self._forward_inner_routine_right_branching(
						i
						)
			)
			for 
			i
			in xrange(1,self.string_length+1) # for i=1....
		]



	def _forward_inner_routine_right_branching(
						self,
						i
						):
		# Scan step is now omitted, or merged into the previous prediction.
		if i > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last_right_branching(i)
		if i < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid_right_branching(i)
			self._predict_right_branching(i)




	def _complete_mid_to_last_right_branching(
						self,
						i
					):
		"""
		Complete X->B.Y => X->BY.
		"""
		self.forward_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2
						]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.inner_emit[ #Scanned above.
										:,	# string id
										i-1,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.inner_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.inner_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.inner_emit[ # Scanned above.
										:,
										i-1,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.completed_inner_branch[:,i,i-2,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
												self.inner_branch[
														:,		# string id
														i,	# i
														i-2,		# j
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														2,		#Dot i
														]
												,
												axis=(
													2,	# Left RHS
													3	# Right RHS
													)
												)
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k_right_branching(
						i,
						k
						)
			for k in reversed(xrange(i-2)) # k=i-3,...,0 <- Only valid on Python 2.
		]

	def _complete_mid_to_last_per_k_right_branching(
						self,
						i,
						k
						):
		self.forward_branch[:,i,k,:,:,:,2]=(
										# j:k X->B.Y and i:j Y->CD. (j>k)
										np.sum(
											self.forward_branch[
														:,		# string id
														k+1:i-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.completed_inner_branch[
														:,		# string id
														i,	# i
														k+1:i-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										+
										# i-1:k X->B.Y and i:i-1 Y->a.
										(
											self.forward_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.inner_emit[ #Scanned above.
														:,	# string id
														i-1,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														]
											)
										)
		self.inner_branch[:,i,k,:,:,:,2]=(
										np.sum(
											self.inner_branch[
														:,		# string id
														k+1:i-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.completed_inner_branch[
														:,		# string id
														i,	# i
														k+1:i-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										+
										(
											self.inner_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.inner_emit[ # Scanned above.
														:,	 # string id
														i-1,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														]
											)
										)
		self.completed_inner_branch[:,i,k,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											self.inner_branch[
													:,		# string id
													i,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot i
													]
											,
											axis=(
												2,	# Left RHS
												3	# Right RHS
												)
											)

	def _complete_start_to_mid_right_branching(
						self,
						i
					):
		"""
		Complete X->.YA => X->Y.A
		Y must be emission.
		"""
		# i:j Y->a.
		self.forward_branch[
					:, # string id
					i, # i
					i-1, # k
					:,
					:,
					:,
					1
					]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-1,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										]
							*
							self.inner_emit[ #Scanned above.
										:, # string id
										i-1,		# i
										np.newaxis,	# LHS
										:,			# Left RHS
										np.newaxis,	# Right RHS
										]
							)
		self.inner_branch[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.inner_branch[
									:,		# string id
									i-1,	# j=i-1
									i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot i
									]
						*
						self.inner_emit[ #Scanned above.
									:,# string id
									i-1,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)


		

	def _predict_right_branching(
			self,
			i
			):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		# Only i:k X->A.Z is introduced in the completion step and is expandable.
		forward_x_left_corner=np.sum( # alpha * R(Z =>L Y)
											np.sum(
												self.forward_branch[
														:,	# string id
														i, #i
														:i, #k
														:,
														:,
														:,
														1
														]
												,
												axis=(
													1, # start i (k)
													2, # LHS non-terminal
													3, # Left RHS non-terminal.
													)
												)[:,:,np.newaxis] # num_strings x num_nonterminals (right RHS)
											*
											self.left_corner_weights[np.newaxis,:,:] # num_non_terminals x num_non_terminals
											,
											axis=1
											) # num_strings x num_non_terminals
		self.forward_branch[:,i,i,:,:,:,0]=(
										forward_x_left_corner[:,:,np.newaxis,np.newaxis]
										*
										self.branch_rule_weights[np.newaxis,:,:,:]
										)
		self.inner_branch[:,i,i,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]
		log_forward_emit = (
								np.log(forward_x_left_corner)
								+
								self.log_emission_probs[:,i,:]
							)
		self.log_scaling_factors[:,i] = - spm.logsumexp(log_forward_emit, axis=-1)
		self.inner_emit[:,i,:] = np.exp(self.log_emission_probs[:,i,:] + self.log_scaling_factors[:,i,np.newaxis])





	# ===============END OF RIGHT-BRANCHING PARSE===================



	# ===============START OF LEFT-BRANCHING PARSE=====================


	def get_log_like_left_branching(self, string_batch):
		"""
		Get the log likelihood ratio of left-branching parses to the whole.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1: # All the parses are regular if length < 3.
			log_expected_init_weights = np.log(self.init_weights[np.newaxis,:])+self.log_emission_probs[:,0,:]
			log_like = spm.logsumexp(log_expected_init_weights, axis=-1)
		else:
			self._calc_forward_inner_probs_left_branching()
			likelihoods_for_each_root_non_terminal = (
										np.sum(self.inner_branch[:,-1,0,:,:,:,2], axis=(2,3))
										*
										self.init_weights[np.newaxis,:]
										)
			likelihood = np.sum(
									likelihoods_for_each_root_non_terminal
									,
									axis=-1
								)
			log_like = np.log(likelihood) - np.sum(self.log_scaling_factors, axis=-1)
			log_like += self.log_branch_normalizer * (self.string_length - 1)
		return log_like



	def _calc_forward_inner_probs_left_branching(self):
		"""
		Calculates forward and inner probabilities.
		Only right branching.
		Underflow is avoided by scaling.
		"""
		self.forward_branch = np.zeros(
							(
								self.num_strings, # string id (among the same length)
								self.string_length+1, # penultimate i i
								self.string_length+1, # start i k
								self.branch_rule_weights.shape[0], # LHS non-terminal
								self.branch_rule_weights.shape[1], # Left RHS non-terminal
								self.branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot i
							)
							)
		# forward_emit is not quite useful except the scaling, which doesn't need to be memorized. So omitted.

		self.inner_branch=np.zeros(self.forward_branch.shape)
		self.inner_emit = np.zeros(
							(
							self.num_strings, # string id (among the same length)
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.branch_rule_weights.shape[0] # LHS non-terminal
							# dot i is redundant, so omitted.
							)
							)
		
		# Initial prediction
		left_corner_from_init=np.matmul(
								self.init_weights
								,
								self.left_corner_weights
								)
		
		self.forward_branch[:,0,0,:,:,:,0]=left_corner_from_init[np.newaxis,:,np.newaxis,np.newaxis]*self.branch_rule_weights[np.newaxis,:,:,:]
		self.inner_branch[:,0,0,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]


		log_forward_emit = (
							np.log(left_corner_from_init)[np.newaxis,:]
							+
							self.log_emission_probs[:,0,:]
							)
		# scaling
		self.log_scaling_factors = np.zeros((self.num_strings,self.string_length))
		self.log_scaling_factors[:,0] = - spm.logsumexp(log_forward_emit, axis=-1)
		self.inner_emit[:,0,:] = np.exp(self.log_emission_probs[:,0,:] + self.log_scaling_factors[:,0,np.newaxis])
		self.completed_inner_branch=np.zeros( # To be reused.
											(
											self.num_strings,
											self.string_length+1, # i
											self.string_length+1, # j
											self.branch_rule_weights.shape[0],
											)
											)
		[
			(
				self._forward_inner_routine_left_branching(
						i
						)
			)
			for 
			i
			in xrange(1,self.string_length+1) # for i=1....
		]



	def _forward_inner_routine_left_branching(
						self,
						i
						):
		# Scan step is now omitted, or merged into the previous prediction.
		if i > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last_left_branching(i)
		if i < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid_left_branching(i)
			self._predict_left_branching(i)




	def _complete_mid_to_last_left_branching(
						self,
						i
					):
		"""
		Complete X->B.Y => X->BY.
		Y must be emission.
		"""
		self.forward_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2
						]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.inner_emit[ #Scanned above.
										:,	# string id
										i-1,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.inner_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.inner_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.inner_emit[ # Scanned above.
										:,
										i-1,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.completed_inner_branch[:,i,i-2,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
												self.inner_branch[
														:,		# string id
														i,	# i
														i-2,		# j
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														2,		#Dot i
														]
												,
												axis=(
													2,	# Left RHS
													3	# Right RHS
													)
												)
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k_left_branching(
						i,
						k
						)
			for k in reversed(xrange(i-2)) # k=i-3,...,0 <- Only valid on Python 2.
		]

	def _complete_mid_to_last_per_k_left_branching(
						self,
						i,
						k
						):
		self.forward_branch[:,i,k,:,:,:,2]=(
										# j:k X->B.Y and i:j Y->CD. (j>k) are impossible.
										# i-1:k X->B.Y and i:i-1 Y->a.
										(
											self.forward_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.inner_emit[ #Scanned above.
														:,	# string id
														i-1,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														]
											)
										)
		self.inner_branch[:,i,k,:,:,:,2]=(
										(
											self.inner_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.inner_emit[ # Scanned above.
														:,	 # string id
														i-1,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														]
											)
										)
		self.completed_inner_branch[:,i,k,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											self.inner_branch[
													:,		# string id
													i,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot i
													]
											,
											axis=(
												2,	# Left RHS
												3	# Right RHS
												)
											)

	def _complete_start_to_mid_left_branching(
						self,
						i
					):
		"""
		Complete X->.YA => X->Y.A
		"""
		if i>1: # No branch rule has been completed if i=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(i-1)
			self.forward_branch[
						:,	# string id
						i, # i
						:i-1, # k
						:,
						:,
						:,
						1,
						]=(
								self.forward_branch[
											:,	# string id
											j,	# j<=i-2
											j,	# k j=k is possible.
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											0,		# dot i
											].transpose((1,0,2,3,4)) # the advanced indexing by j moves the dimension to the front.
								*
								self.completed_inner_branch[
											:,		# string id
											i,	# i
											:i-1, # j
											np.newaxis, # LHS of the target.
											:, 			# Left of the RHS.
											np.newaxis,	# Right of the RHS.
											]
								)
			self.inner_branch[
					:,	# string id
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.inner_branch[
										:,	# string id
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										].transpose((1,0,2,3,4))
							*
							self.completed_inner_branch[
										:,		# string id
										i,	# i
										:i-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.forward_branch[
					:, # string id
					i, # i
					i-1, # k
					:,
					:,
					:,
					1
					]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-1,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										]
							*
							self.inner_emit[ #Scanned above.
										:, # string id
										i-1,		# i
										np.newaxis,	# LHS
										:,			# Left RHS
										np.newaxis,	# Right RHS
										]
							)
		self.inner_branch[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.inner_branch[
									:,		# string id
									i-1,	# j=i-1
									i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot i
									]
						*
						self.inner_emit[ #Scanned above.
									:,# string id
									i-1,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)


		

	def _predict_left_branching(
			self,
			i
			):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		# Only i:k X->A.Z is introduced in the completion step and is expandable.
		forward_x_left_corner=np.sum( # alpha * R(Z =>L Y)
											np.sum(
												self.forward_branch[
														:,	# string id
														i, #i
														:i, #k
														:,
														:,
														:,
														1
														]
												,
												axis=(
													1, # start i (k)
													2, # LHS non-terminal
													3, # Left RHS non-terminal.
													)
												)[:,:,np.newaxis] # num_strings x num_nonterminals (right RHS)
											*
											self.left_corner_weights[np.newaxis,:,:] # num_non_terminals x num_non_terminals
											,
											axis=1
											) # num_strings x num_non_terminals
		self.forward_branch[:,i,i,:,:,:,0]=(
										forward_x_left_corner[:,:,np.newaxis,np.newaxis]
										*
										self.branch_rule_weights[np.newaxis,:,:,:]
										)
		self.inner_branch[:,i,i,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]
		log_forward_emit = (
								np.log(forward_x_left_corner)
								+
								self.log_emission_probs[:,i,:]
							)
		self.log_scaling_factors[:,i] = - spm.logsumexp(log_forward_emit, axis=-1)
		self.inner_emit[:,i,:] = np.exp(self.log_emission_probs[:,i,:] + self.log_scaling_factors[:,i,np.newaxis])


	# ===============END OF LEFT-BRANCHING PARSE===================




# ========================================================================================================
# Emission from terminals. Probably useful when there are many emission clusters.
# =========================================================================================================

class Inner_Outer_Algorithm_Multivariate_Emission_With_Terminals(Inner_Outer_Algorithm_Multivariate_Emission_Sort_By_Length):
	"""
	Stolcke's (1995) inner-outer algorithm for
	a fully connected PCFG in a Chomsky normal form with general multivariate emission.
	Emission is from terminals, which are distinct from non-terminals.
	Log emission probability density function (log_emission_pdf)
	must be defined and passed to the object constructor in addition to the log_nt2t_rule_weights for part-of-speech emission.
	It simultaneously computes likelihoods of strings of the same length,
	and expected counts of rule uses to generate the strings.
	"""

	def __init__(
			self,
			log_branch_rule_weights,
			log_nt2t_rule_weights,
			log_emission_pdf,
			log_init_weights,
			):
		"""
		Initialize rule weights.
		"""
		self.branch_rule_weights = np.exp(log_branch_rule_weights)
		self.log_branch_normalizer = np.sum(self.branch_rule_weights)
		self.branch_rule_weights /= self.log_branch_normalizer # Make sure that the weights are under 1.
		self.log_branch_normalizer = np.log(self.log_branch_normalizer)
		self.log_nt2t_rule_weights = log_nt2t_rule_weights
		self.log_emission_pdf = log_emission_pdf
		self.init_weights = np.exp(log_init_weights)
		self.left_corner_weights = self._get_left_corner_probs()

	def _set_strings(self, string_batch):
		"""
		(Re)set emitted strings.
		Strings of the same length are assumed to be sorted.
		"""
		self.log_emission_probs = self.log_emission_pdf(string_batch) # num_strings x string_len x ndim
		self.string_length = string_batch.shape[1]
		self.num_strings = string_batch.shape[0]

	def get_expected_rule_counts(self, string_batch):
		"""
		Returns expected # of use of each rule,
		together with log likelihoods of strings.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1: # No branching for strings of length 1
			log_like_per_parse = (
											np.log(self.init_weights)[np.newaxis,:,np.newaxis]
											+
											self.log_nt2t_rule_weights[np.newaxis,:,:]
											+
											self.log_emission_probs[:,0,np.newaxis,:]
										)
			log_like = spm.logsumexp(log_like_per_parse, axis=(-2,-1))
			expected_nt2t_rule_counts_per_pos = np.exp(log_like_per_parse-log_like[:,np.newaxis,np.newaxis])
			expected_root_counts = np.sum(expected_nt2t_rule_counts_per_pos, axis=-1)
			expected_branch_rule_counts = np.zeros((self.num_strings,)+self.branch_rule_weights.shape)
			expected_nt2t_rule_counts_per_pos = expected_nt2t_rule_counts_per_pos[:,np.newaxis,:,:]
		else:
			self._calc_forward_inner_probs()
			self._calc_outer_probs()
			
			likelihoods_for_each_root_non_terminal = (
										np.sum(self.inner_branch[:,-1,0,:,:,:,2], axis=(2,3))
										*
										self.init_weights[np.newaxis,:]
										)
			likelihood = np.sum(
									likelihoods_for_each_root_non_terminal
									,
									axis=-1
								)
			expected_branch_rule_counts = (
											np.sum(
												self.outer_branch[...,0].diagonal(axis1=1, axis2=2) # Scaled.
												*
												self.inner_branch[...,0].diagonal(axis1=1, axis2=2) # Not scaled.
												,
												axis=-1 # diagonalized dimension comes last.
												) # num_strings x num_nt x num_nt x num_nt
											/
											likelihood[:,np.newaxis,np.newaxis,np.newaxis]
											)
			expected_nt2t_rule_counts_per_pos = (
										(
											self.outer_emit[:,:-1,:,np.newaxis] # Not scaled. <- shorter.
											*
											self.inner_emit[:,:-1,:,:] # Scaled.
											) # num_strings x (string_length + 1) x num_nt
										/
										likelihood[:,np.newaxis,np.newaxis,np.newaxis]
										)
			expected_root_counts = (
									likelihoods_for_each_root_non_terminal
									/
									likelihood[:,np.newaxis]
									)
			log_like = np.log(likelihood) - np.sum(self.log_scaling_factors, axis=-1)
		log_like += self.log_branch_normalizer * (self.string_length - 1)
		return expected_branch_rule_counts, expected_nt2t_rule_counts_per_pos, expected_root_counts, log_like



	# ============= BEGINNING OF FORWARD-INNER ALGORITHM =======================================================

	def _calc_forward_inner_probs(self):
		"""
		Calculates forward and inner probabilities.
		Underflow is avoided by scaling.
		"""
		self.forward_branch = np.zeros(
							(
								self.num_strings, # string id (among the same length)
								self.string_length+1, # penultimate i i
								self.string_length+1, # start i k
								self.branch_rule_weights.shape[0], # LHS non-terminal
								self.branch_rule_weights.shape[1], # Left RHS non-terminal
								self.branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot i
							)
							)
		# forward_emit is not quite useful except the scaling, which doesn't need to be memorized. So omitted.

		self.inner_branch=np.zeros(self.forward_branch.shape)
		self.inner_emit = np.zeros(
							(
							self.num_strings, # string id (among the same length)
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.log_nt2t_rule_weights.shape[0], # LHS non-terminal
							self.log_nt2t_rule_weights.shape[1] # Terminal
							# dot i is redundant, so omitted.
							)
							)
		
		# Initial prediction
		left_corner_from_init=np.matmul(
								self.init_weights
								,
								self.left_corner_weights
								)
		
		self.forward_branch[:,0,0,:,:,:,0]=left_corner_from_init[np.newaxis,:,np.newaxis,np.newaxis]*self.branch_rule_weights[np.newaxis,:,:,:]
		self.inner_branch[:,0,0,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]

		log_nt2t_and_emission = (
						self.log_nt2t_rule_weights[np.newaxis,:,:]
						+
						self.log_emission_probs[:,0,np.newaxis,:]
						)
		log_forward_emit = (
							np.log(left_corner_from_init)[np.newaxis,:,np.newaxis]
							+
							log_nt2t_and_emission
							)
		# scaling
		self.log_scaling_factors = np.zeros((self.num_strings,self.string_length))
		self.log_scaling_factors[:,0] = - spm.logsumexp(log_forward_emit, axis=(-2,-1))
		self.inner_emit[:,0,:,:] = np.exp(log_nt2t_and_emission + self.log_scaling_factors[:,0,np.newaxis,np.newaxis])

		# Summation to be reused.
		self.completed_inner_branch=np.zeros(
											(
											self.num_strings,
											self.string_length+1, # i
											self.string_length+1, # j
											self.branch_rule_weights.shape[0],
											)
											)
		self.scanned_inner_emit = np.zeros(self.inner_emit.shape[:-1])
		self.scanned_inner_emit[:,0,:] = np.sum(self.inner_emit[:,0,:,:], axis=-1)
		[
			self._forward_inner_routine(i)
			for i
			in xrange(1,self.string_length+1) # for i=1....
		]



	def _forward_inner_routine(self,i):
		# Scan step is now omitted, or merged into the previous prediction.
		if i > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last(i)
		if i < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid(i)
			self._predict(i)




	def _complete_mid_to_last(self,i):
		"""
		Complete X->B.Y => X->BY.
		"""
		self.forward_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2
						]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.scanned_inner_emit[
										:,	# string id
										i-1,
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.inner_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.inner_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.scanned_inner_emit[
										:,	# string id
										i-1,
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.completed_inner_branch[:,i,i-2,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
												self.inner_branch[
														:,		# string id
														i,	# i
														i-2,		# j
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														2,		#Dot i
														]
												,
												axis=(
													2,	# Left RHS
													3	# Right RHS
													)
												)
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k(
						i,
						k
						)
			for k in reversed(xrange(i-2)) # k=i-3,...,0 <- Only valid on Python 2.
		]

	def _complete_mid_to_last_per_k(self,i,k):
		self.forward_branch[:,i,k,:,:,:,2]=(
										# j:k X->B.Y and i:j Y->CD. (j>k)
										np.sum(
											self.forward_branch[
														:,		# string id
														k+1:i-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.completed_inner_branch[
														:,		# string id
														i,	# i
														k+1:i-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										+
										# i-1:k X->B.Y and i:i-1 Y->a.
										(
											self.forward_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.scanned_inner_emit[ #Scanned above.
														:,	# string id
														i-1,
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														]
											)
										)
		self.inner_branch[:,i,k,:,:,:,2]=(
										np.sum(
											self.inner_branch[
														:,		# string id
														k+1:i-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.completed_inner_branch[
														:,		# string id
														i,	# i
														k+1:i-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										+
										(
											self.inner_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.scanned_inner_emit[
														:,	 # string id
														i-1,
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														]
											)
										)
		self.completed_inner_branch[:,i,k,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											self.inner_branch[
													:,		# string id
													i,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot i
													]
											,
											axis=(
												2,	# Left RHS
												3	# Right RHS
												)
											)

	def _complete_start_to_mid(self,i):
		"""
		Complete X->.YA => X->Y.A
		"""
		if i>1: # No branch rule has been completed if i=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(i-1)
			self.forward_branch[
						:,	# string id
						i, # i
						:i-1, # k
						:,
						:,
						:,
						1,
						]=(
								self.forward_branch[
											:,	# string id
											j,	# j<=i-2
											j,	# k j=k is possible.
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											0,		# dot i
											].transpose((1,0,2,3,4)) # the advanced indexing by j moves the dimension to the front.
								*
								self.completed_inner_branch[
											:,		# string id
											i,	# i
											:i-1, # j
											np.newaxis, # LHS of the target.
											:, 			# Left of the RHS.
											np.newaxis,	# Right of the RHS.
											]
								)
			self.inner_branch[
					:,	# string id
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.inner_branch[
										:,	# string id
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										].transpose((1,0,2,3,4))
							*
							self.completed_inner_branch[
										:,		# string id
										i,	# i
										:i-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.forward_branch[
					:, # string id
					i, # i
					i-1, # k
					:,
					:,
					:,
					1
					]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-1,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										]
							*
							self.scanned_inner_emit[
										:, # string id
										i-1,
										np.newaxis,	# LHS
										:,			# Left RHS
										np.newaxis,	# Right RHS
										]
							)
		self.inner_branch[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.inner_branch[
									:,		# string id
									i-1,	# j=i-1
									i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot i
									]
						*
						self.scanned_inner_emit[
									:,# string id
									i-1,
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)


		

	def _predict(self,i):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		# Only i:k X->A.Z is introduced in the completion step and is expandable.
		forward_x_left_corner=np.sum( # alpha * R(Z =>L Y)
											np.sum(
												self.forward_branch[
														:,	# string id
														i, #i
														:i, #k
														:,
														:,
														:,
														1
														]
												,
												axis=(
													1, # start i (k)
													2, # LHS non-terminal
													3, # Left RHS non-terminal.
													)
												)[:,:,np.newaxis] # num_strings x num_nonterminals (right RHS)
											*
											self.left_corner_weights[np.newaxis,:,:] # num_non_terminals x num_non_terminals
											,
											axis=1
											) # num_strings x num_non_terminals
		self.forward_branch[:,i,i,:,:,:,0]=(
										forward_x_left_corner[:,:,np.newaxis,np.newaxis]
										*
										self.branch_rule_weights[np.newaxis,:,:,:]
										)
		self.inner_branch[:,i,i,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]

		log_nt2t_and_emission = (
						self.log_nt2t_rule_weights[np.newaxis,:,:]
						+
						self.log_emission_probs[:,i,np.newaxis,:]
						)
		log_forward_emit = (
							np.log(forward_x_left_corner)[:,:,np.newaxis]
							+
							log_nt2t_and_emission
							)
		# scaling
		self.log_scaling_factors[:,i] = - spm.logsumexp(log_forward_emit, axis=(-2,-1))
		self.inner_emit[:,i,:,:] = np.exp(log_nt2t_and_emission + self.log_scaling_factors[:,i,np.newaxis,np.newaxis])

		self.scanned_inner_emit[:,i,:] = np.sum(self.inner_emit[:,i,:,:], axis=-1)


	# ===============END OF FORWARD-INNER ALGORITHM===================






	# ==============START OF OUTER ALGORITHM==========================

	def _calc_outer_probs(self):
		"""
		Calculate outer probabilities.
		"""
		self.outer_branch = np.zeros(self.inner_branch.shape)
		self.outer_emit = np.zeros(self.inner_emit.shape[:-1]) # outer prob. is indep. of emitted value.
		# Initialization
		# Reverse completion of i:j S.->A. where i=l and j=0.
		# By i:k . -> S. and j:k . -> .S where i=l and k=0.
		# This leaves the outer probability 1.
		# And then reverse completion of i:j A->BC. where i=l and j=0.
		# By i:j S.->A. and i:j
		self.outer_branch[
					:, # string id
					-1,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot i
					] += self.init_weights[np.newaxis,:,np.newaxis,np.newaxis]
					
		# Reverse completion of i: jY->CD.
		# When j=0 (=> k=0), i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		# But for i=l(or -1), i: 0X->Y.A and 0: 0X->.YA are not introduced.
		[
			self._reverse_complete_last_to_another_last_per_j_init(j)
			for j in xrange(1,self.string_length-1) # -1: -2 Y->CD. is impossible!!
		]
		
		# Reverse completion of i: jY->a.
		self.outer_emit[
					:,
					-2,	# i
					:,		# LHS
					]=self._get_outer_x_inner_j_greater_k(-1, -2, 2, 1, (1, 2, 3))
		# Reverse completion of j: kX->B.Y k<=i-1
		self.outer_branch[
					:,		# string id
					:-1,	# j
					:-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									:,	# string id
									-1,	# i
									np.newaxis,	# j
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.completed_inner_branch[
									:,		# string id
									-1,		#i
									:-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.outer_branch[
					:,	# string id
					-2,	# j
					:-2,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									:,	# string id
									-1,	# i
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.scanned_inner_emit[
									:,
									-2,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									]
						)
		# Reverse completion of j: kX->.YA N/A b/c -1: 0X->Y.A is irrelevant.
		# Reverse scanning is no longer necessary.
		[
				self._reverse_complete(
					i
					)
			for i in xrange(self.string_length-1,0,-1)
		]


	def _get_outer_x_inner_j_greater_k(
					self,
					i,
					j,
					outer_dot_pos,
					inner_dot_pos,
					marginalization_axis
					):
		"""
		Computes (outer probability) x (inner probability) when j > k.
		"""
		return np.sum(
						self.outer_branch[ # i:k X->BY.
									:,		# string id
									i,	# i
									:j,		# k
									:,		# LHS
									:,		# Left RHS independent
									:,		# Right RHS
									outer_dot_pos,		# do i
									]
						*
						self.inner_branch[ # j:k X->B.Y k<j
									:,	# string id
									j,	# j
									:j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do i
									]
						,
						axis=marginalization_axis
					)


	def _get_outer_x_inner_j_eq_k(self, i, j, outer_dot_pos, inner_dot_pos, marginalization_axis):
		"""
		Computes (outer probability) x (inner probability) when j = k.
		"""
		return np.sum(
						self.outer_branch[ # i:k X->Y.A
									:,		# string id
									i,	# i
									j,	# k
									:,		# LHS
									:,		# Left RHS # Independent
									:,		# Right RHS
									outer_dot_pos,		# do i
									]
						*
						self.inner_branch[ # j:k X->.YA j=k
									:,	# string id
									j,	# j <- jに依存している。
									j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do i
									]
						,
						axis=marginalization_axis
					)

	def _reverse_complete_last_to_another_last_per_j_init(self, j):
		# Reverse completion of i: jY->CD. j<=i-2
		self.outer_branch[
				:,	# string id
				-1,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot i
				]=self._get_outer_x_inner_j_greater_k(
						-1,
						j,
						2,
						1,
						(1,2,3)
						)[:,:,np.newaxis,np.newaxis]



	def _reverse_complete_last_to_another_last_per_j(self, i, j):
		"""
		Reverse completion of i: jY->CD. given j.
		"""
		self.outer_branch[
				:,	# string id
				i,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot i
				]=(
					self._get_outer_x_inner_j_eq_k(i, j, 1, 0, (1,3))
					+
					self._get_outer_x_inner_j_greater_k(i, j, 2, 1, (1,2,3))
					)[:,:,np.newaxis,np.newaxis]

	def _reverse_complete_last_to_another_last(self, i):
		"""
		Reverse completion of i: jY->CD.
		"""
		# When j=0, k=0 as well.
		# i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		self.outer_branch[
					:,		# string id
					i,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot i
					]=self._get_outer_x_inner_j_eq_k(i, 0, 1, 0, (1,3))[:,:,np.newaxis,np.newaxis]
		[self._reverse_complete_last_to_another_last_per_j(i, j)
			for j in xrange(1,i-1) # i: i-1 Y->CD. is impossible!!
		]



	def _reverse_complete_emission(self, i):
		"""
		Reverse completion of i: jY->a.
		"""
		self.outer_emit[
					:, # string id
					i-1,	# i
					:,		# LHS
					]=(
						self._get_outer_x_inner_j_eq_k(i, i-1, 1, 0, (1,3))
						+
						self._get_outer_x_inner_j_greater_k(i, i-1, 2, 1, (1,2,3))
						)

	def _reverse_complete_last_to_mid(self, i):
		"""
		Reverse completion of j: kX->B.Y (note k<j)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->BY.
		self.outer_branch[
					:,		# string id
					:i-1,	# j
					:i-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									:,		# string id
									i,	# i
									np.newaxis,	# j
									:i-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.completed_inner_branch[
									:,			# string id
									i,		#i
									:i-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.outer_branch[
					:,		# string id
					i-1,	# j
					:i-1,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot i
					]+=(
						self.outer_branch[
									:,		# string id
									i,	# i
									:i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot i
									]
						*
						self.scanned_inner_emit[
									:,	 # string id
									i-1,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									]
						)

	def _reverse_complete_mid_to_start(self, i):
		"""
		Reverse completion of j: kX->.YA (j=k is possible)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->Y.A
		self.outer_branch[
					:,		# string id
					:i-1,	# j
					:i-1,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot i
					]+=(
						self.outer_branch[
									:,		# string id 
									i,	# i
									np.newaxis,	# j
									:i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot i
									]
						*
						self.completed_inner_branch[
									:,			# string id
									i,		# i
									:i-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->Y.A
		self.outer_branch[
					:,
					i-1,	# j
					:i,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot i
					]+=(
						self.outer_branch[
									:,		# string id
									i,	# i
									:i,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot i
									]
						*
						self.scanned_inner_emit[
									:,	# string id
									i-1,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									:,			# Left RHS of branch (LHS of emission)
									np.newaxis,	# Right RHS of branch 
									]
						)

	def _reverse_complete(self, i):
		"""
		Reverse operation of completion.
		"""
		self._reverse_complete_last_to_another_last(i)
		self._reverse_complete_emission(i)
		self._reverse_complete_last_to_mid(i)
		self._reverse_complete_mid_to_start(i)




	# ============= BEGINNING OF RIGHT-BRANCHING PARSE =======================================================
	
	def get_log_like_right_branching(self, string_batch):
		"""
		Get the log likelihood of right-branching parses.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1: # All the parses are right-branching if length < 3.
			log_like_per_parse = (
									np.log(self.init_weights)[np.newaxis,:,np.newaxis]
									+
									self.log_nt2t_rule_weights[np.newaxis,:,:]
									+
									self.log_emission_probs[:,0,np.newaxis,:]
								)
			log_like = spm.logsumexp(log_like_per_parse, axis=(-2,-1))
		else:
			self._calc_forward_inner_probs_right_branching()
			likelihoods_for_each_root_non_terminal = (
										np.sum(self.inner_branch[:,-1,0,:,:,:,2], axis=(2,3))
										*
										self.init_weights[np.newaxis,:]
										)
			likelihood = np.sum(
									likelihoods_for_each_root_non_terminal
									,
									axis=-1
								)
			log_like = np.log(likelihood) - np.sum(self.log_scaling_factors, axis=-1)
		log_like += self.log_branch_normalizer * (self.string_length - 1)
		return log_like



	def _calc_forward_inner_probs_right_branching(self):
		"""
		Calculates forward and inner probabilities only allowing right-branching parses.
		Underflow is avoided by scaling.
		"""
		self.forward_branch = np.zeros(
							(
								self.num_strings, # string id (among the same length)
								self.string_length+1, # penultimate i i
								self.string_length+1, # start i k
								self.branch_rule_weights.shape[0], # LHS non-terminal
								self.branch_rule_weights.shape[1], # Left RHS non-terminal
								self.branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot i
							)
							)
		# forward_emit is not quite useful except the scaling, which doesn't need to be memorized. So omitted.

		self.inner_branch=np.zeros(self.forward_branch.shape)
		self.inner_emit = np.zeros(
							(
							self.num_strings, # string id (among the same length)
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.log_nt2t_rule_weights.shape[0], # LHS non-terminal
							self.log_nt2t_rule_weights.shape[1] # Terminal
							# dot i is redundant, so omitted.
							)
							)
		
		# Initial prediction
		left_corner_from_init=np.matmul(
								self.init_weights
								,
								self.left_corner_weights
								)
		self.forward_branch[:,0,0,:,:,:,0]=left_corner_from_init[np.newaxis,:,np.newaxis,np.newaxis]*self.branch_rule_weights[np.newaxis,:,:,:]
		self.inner_branch[:,0,0,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]

		log_nt2t_and_emission = (
						self.log_nt2t_rule_weights[np.newaxis,:,:]
						+
						self.log_emission_probs[:,0,np.newaxis,:]
						)
		log_forward_emit = (
							np.log(left_corner_from_init)[np.newaxis,:,np.newaxis]
							+
							log_nt2t_and_emission
							)
		# scaling
		self.log_scaling_factors = np.zeros((self.num_strings,self.string_length))
		self.log_scaling_factors[:,0] = - spm.logsumexp(log_forward_emit, axis=(-2,-1))
		self.inner_emit[:,0,:,:] = np.exp(log_nt2t_and_emission + self.log_scaling_factors[:,0,np.newaxis,np.newaxis])

		# Summation to be reused.
		self.completed_inner_branch=np.zeros(
											(
											self.num_strings,
											self.string_length+1, # i
											self.string_length+1, # j
											self.branch_rule_weights.shape[0],
											)
											)
		self.scanned_inner_emit = np.zeros(self.inner_emit.shape[:-1])
		self.scanned_inner_emit[:,0,:] = np.sum(self.inner_emit[:,0,:,:], axis=-1)
		[
			(
				self._forward_inner_routine_right_branching(i)
			)
			for i
			in xrange(1,self.string_length+1) # for i=1....
		]



	def _forward_inner_routine_right_branching(self,i):
		# Scan step is now omitted, or merged into the previous prediction.
		if i > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last_right_branching(i)
		if i < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid_right_branching(i)
			self._predict_right_branching(i)




	def _complete_mid_to_last_right_branching(self,i):
		"""
		Complete X->B.Y => X->BY.
		"""
		self.forward_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2
						]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.scanned_inner_emit[
										:,	# string id
										i-1,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.inner_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.inner_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.scanned_inner_emit[
										:,
										i-1,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.completed_inner_branch[:,i,i-2,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
												self.inner_branch[
														:,		# string id
														i,	# i
														i-2,		# j
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														2,		#Dot i
														]
												,
												axis=(
													2,	# Left RHS
													3	# Right RHS
													)
												)
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k_right_branching(
						i,
						k
						)
			for k in reversed(xrange(i-2)) # k=i-3,...,0 <- Only valid on Python 2.
		]

	def _complete_mid_to_last_per_k_right_branching(self,i,k):
		self.forward_branch[:,i,k,:,:,:,2]=(
										# j:k X->B.Y and i:j Y->CD. (j>k)
										np.sum(
											self.forward_branch[
														:,		# string id
														k+1:i-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.completed_inner_branch[
														:,		# string id
														i,	# i
														k+1:i-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										+
										# i-1:k X->B.Y and i:i-1 Y->a.
										(
											self.forward_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.scanned_inner_emit[
														:,	# string id
														i-1,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														]
											)
										)
		self.inner_branch[:,i,k,:,:,:,2]=(
										np.sum(
											self.inner_branch[
														:,		# string id
														k+1:i-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.completed_inner_branch[
														:,		# string id
														i,	# i
														k+1:i-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										+
										(
											self.inner_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.scanned_inner_emit[
														:,	 # string id
														i-1,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														]
											)
										)
		self.completed_inner_branch[:,i,k,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											self.inner_branch[
													:,		# string id
													i,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot i
													]
											,
											axis=(
												2,	# Left RHS
												3	# Right RHS
												)
											)

	def _complete_start_to_mid_right_branching(self,i):
		"""
		Complete X->.YA => X->Y.A
		Y must be emission.
		"""
		# i:j Y->a.
		self.forward_branch[
					:, # string id
					i, # i
					i-1, # k
					:,
					:,
					:,
					1
					]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-1,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										]
							*
							self.scanned_inner_emit[ #Scanned above.
										:, # string id
										i-1,		# i
										np.newaxis,	# LHS
										:,			# Left RHS
										np.newaxis,	# Right RHS
										]
							)
		self.inner_branch[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.inner_branch[
									:,		# string id
									i-1,	# j=i-1
									i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot i
									]
						*
						self.scanned_inner_emit[ #Scanned above.
									:,# string id
									i-1,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)


		

	def _predict_right_branching(self,i):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		# Only i:k X->A.Z is introduced in the completion step and is expandable.
		forward_x_left_corner=np.sum( # alpha * R(Z =>L Y)
											np.sum(
												self.forward_branch[
														:,	# string id
														i, #i
														:i, #k
														:,
														:,
														:,
														1
														]
												,
												axis=(
													1, # start i (k)
													2, # LHS non-terminal
													3, # Left RHS non-terminal.
													)
												)[:,:,np.newaxis] # num_strings x num_nonterminals (right RHS)
											*
											self.left_corner_weights[np.newaxis,:,:] # num_non_terminals x num_non_terminals
											,
											axis=1
											) # num_strings x num_non_terminals
		self.forward_branch[:,i,i,:,:,:,0]=(
										forward_x_left_corner[:,:,np.newaxis,np.newaxis]
										*
										self.branch_rule_weights[np.newaxis,:,:,:]
										)
		self.inner_branch[:,i,i,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]
		log_nt2t_and_emission = (
						self.log_nt2t_rule_weights[np.newaxis,:,:]
						+
						self.log_emission_probs[:,i,np.newaxis,:]
						)
		log_forward_emit = (
							np.log(forward_x_left_corner)[:,:,np.newaxis]
							+
							log_nt2t_and_emission
							)
		# scaling
		self.log_scaling_factors[:,i] = - spm.logsumexp(log_forward_emit, axis=(-2,-1))
		self.inner_emit[:,i,:,:] = np.exp(log_nt2t_and_emission + self.log_scaling_factors[:,i,np.newaxis,np.newaxis])

		self.scanned_inner_emit[:,i,:] = np.sum(self.inner_emit[:,i,:,:], axis=-1)





	# ===============END OF RIGHT-BRANCHING PARSE===================



	# ===============START OF LEFT-BRANCHING PARSE=====================


	def get_log_like_left_branching(self, string_batch):
		"""
		Get the log likelihood ratio of left-branching parses to the whole.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1: # All the parses are regular if length < 3.
			log_like_per_parse = (
											np.log(self.init_weights)[np.newaxis,:,np.newaxis]
											+
											self.log_nt2t_rule_weights[np.newaxis,:,:]
											+
											self.log_emission_probs[:,0,np.newaxis,:]
										)
			log_like = spm.logsumexp(log_like_per_parse, axis=(-2,-1))
		else:
			self._calc_forward_inner_probs_left_branching()
			likelihoods_for_each_root_non_terminal = (
										np.sum(self.inner_branch[:,-1,0,:,:,:,2], axis=(2,3))
										*
										self.init_weights[np.newaxis,:]
										)
			likelihood = np.sum(
									likelihoods_for_each_root_non_terminal
									,
									axis=-1
								)
			log_like = np.log(likelihood) - np.sum(self.log_scaling_factors, axis=-1)
		log_like += self.log_branch_normalizer * (self.string_length - 1)
		return log_like



	def _calc_forward_inner_probs_left_branching(self):
		"""
		Calculates forward and inner probabilities.
		Only right branching.
		Underflow is avoided by scaling.
		"""
		self.forward_branch = np.zeros(
							(
								self.num_strings, # string id (among the same length)
								self.string_length+1, # penultimate i i
								self.string_length+1, # start i k
								self.branch_rule_weights.shape[0], # LHS non-terminal
								self.branch_rule_weights.shape[1], # Left RHS non-terminal
								self.branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot i
							)
							)
		# forward_emit is not quite useful except the scaling, which doesn't need to be memorized. So omitted.

		self.inner_branch=np.zeros(self.forward_branch.shape)
		self.inner_emit = np.zeros(
							(
							self.num_strings, # string id (among the same length)
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.log_nt2t_rule_weights.shape[0], # LHS non-terminal
							self.log_nt2t_rule_weights.shape[1] # Terminal
							# dot i is redundant, so omitted.
							)
							)
		
		# Initial prediction
		left_corner_from_init=np.matmul(
								self.init_weights
								,
								self.left_corner_weights
								)
		
		self.forward_branch[:,0,0,:,:,:,0]=left_corner_from_init[np.newaxis,:,np.newaxis,np.newaxis]*self.branch_rule_weights[np.newaxis,:,:,:]
		self.inner_branch[:,0,0,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]


		log_nt2t_and_emission = (
						self.log_nt2t_rule_weights[np.newaxis,:,:]
						+
						self.log_emission_probs[:,0,np.newaxis,:]
						)
		log_forward_emit = (
							np.log(left_corner_from_init)[np.newaxis,:,np.newaxis]
							+
							log_nt2t_and_emission
							)
		# scaling
		self.log_scaling_factors = np.zeros((self.num_strings,self.string_length))
		self.log_scaling_factors[:,0] = - spm.logsumexp(log_forward_emit, axis=(-2,-1))
		self.inner_emit[:,0,:,:] = np.exp(log_nt2t_and_emission + self.log_scaling_factors[:,0,np.newaxis,np.newaxis])

		# Summation to be reused.
		self.completed_inner_branch=np.zeros(
											(
											self.num_strings,
											self.string_length+1, # i
											self.string_length+1, # j
											self.branch_rule_weights.shape[0],
											)
											)
		self.scanned_inner_emit = np.zeros(self.inner_emit.shape[:-1])
		self.scanned_inner_emit[:,0,:] = np.sum(self.inner_emit[:,0,:,:], axis=-1)
		[
			self._forward_inner_routine_left_branching(i)
			for i
			in xrange(1,self.string_length+1) # for i=1....
		]



	def _forward_inner_routine_left_branching(self,i):
		# Scan step is now omitted, or merged into the previous prediction.
		if i > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last_left_branching(i)
		if i < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid_left_branching(i)
			self._predict_left_branching(i)




	def _complete_mid_to_last_left_branching(self,i):
		"""
		Complete X->B.Y => X->BY.
		Y must be emission.
		"""
		self.forward_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2
						]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.scanned_inner_emit[
										:,	# string id
										i-1,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.inner_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.inner_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.scanned_inner_emit[
										:,
										i-1,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.completed_inner_branch[:,i,i-2,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
												self.inner_branch[
														:,		# string id
														i,	# i
														i-2,		# j
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														2,		#Dot i
														]
												,
												axis=(
													2,	# Left RHS
													3	# Right RHS
													)
												)
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k_left_branching(
						i,
						k
						)
			for k in reversed(xrange(i-2)) # k=i-3,...,0 <- Only valid on Python 2.
		]

	def _complete_mid_to_last_per_k_left_branching(self,i,k):
		self.forward_branch[:,i,k,:,:,:,2]=(
										# j:k X->B.Y and i:j Y->CD. (j>k) are impossible.
										# i-1:k X->B.Y and i:i-1 Y->a.
										(
											self.forward_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.scanned_inner_emit[
														:,	# string id
														i-1,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														]
											)
										)
		self.inner_branch[:,i,k,:,:,:,2]=(
										(
											self.inner_branch[
														:,		# string id
														i-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot i
														]
											*
											self.scanned_inner_emit[
														:,	 # string id
														i-1,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														]
											)
										)
		self.completed_inner_branch[:,i,k,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											self.inner_branch[
													:,		# string id
													i,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot i
													]
											,
											axis=(
												2,	# Left RHS
												3	# Right RHS
												)
											)

	def _complete_start_to_mid_left_branching(self,i):
		"""
		Complete X->.YA => X->Y.A
		"""
		if i>1: # No branch rule has been completed if i=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(i-1)
			self.forward_branch[
						:,	# string id
						i, # i
						:i-1, # k
						:,
						:,
						:,
						1,
						]=(
								self.forward_branch[
											:,	# string id
											j,	# j<=i-2
											j,	# k j=k is possible.
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											0,		# dot i
											].transpose((1,0,2,3,4)) # the advanced indexing by j moves the dimension to the front.
								*
								self.completed_inner_branch[
											:,		# string id
											i,	# i
											:i-1, # j
											np.newaxis, # LHS of the target.
											:, 			# Left of the RHS.
											np.newaxis,	# Right of the RHS.
											]
								)
			self.inner_branch[
					:,	# string id
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.inner_branch[
										:,	# string id
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										].transpose((1,0,2,3,4))
							*
							self.completed_inner_branch[
										:,		# string id
										i,	# i
										:i-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.forward_branch[
					:, # string id
					i, # i
					i-1, # k
					:,
					:,
					:,
					1
					]=(
							self.forward_branch[
										:,		# string id
										i-1,	# j=i-1
										i-1,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										]
							*
							self.scanned_inner_emit[
										:, # string id
										i-1,		# i
										np.newaxis,	# LHS
										:,			# Left RHS
										np.newaxis,	# Right RHS
										]
							)
		self.inner_branch[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.inner_branch[
									:,		# string id
									i-1,	# j=i-1
									i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot i
									]
						*
						self.scanned_inner_emit[
									:,# string id
									i-1,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)


		

	def _predict_left_branching(self,i):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		# Only i:k X->A.Z is introduced in the completion step and is expandable.
		forward_x_left_corner=np.sum( # alpha * R(Z =>L Y)
											np.sum(
												self.forward_branch[
														:,	# string id
														i, #i
														:i, #k
														:,
														:,
														:,
														1
														]
												,
												axis=(
													1, # start i (k)
													2, # LHS non-terminal
													3, # Left RHS non-terminal.
													)
												)[:,:,np.newaxis] # num_strings x num_nonterminals (right RHS)
											*
											self.left_corner_weights[np.newaxis,:,:] # num_non_terminals x num_non_terminals
											,
											axis=1
											) # num_strings x num_non_terminals
		self.forward_branch[:,i,i,:,:,:,0]=(
										forward_x_left_corner[:,:,np.newaxis,np.newaxis]
										*
										self.branch_rule_weights[np.newaxis,:,:,:]
										)
		self.inner_branch[:,i,i,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]
		log_nt2t_and_emission = (
						self.log_nt2t_rule_weights[np.newaxis,:,:]
						+
						self.log_emission_probs[:,i,np.newaxis,:]
						)
		log_forward_emit = (
							np.log(forward_x_left_corner)[:,:,np.newaxis]
							+
							log_nt2t_and_emission
							)
		# scaling
		self.log_scaling_factors[:,i] = - spm.logsumexp(log_forward_emit, axis=(-2,-1))
		self.inner_emit[:,i,:,:] = np.exp(log_nt2t_and_emission + self.log_scaling_factors[:,i,np.newaxis,np.newaxis])

		self.scanned_inner_emit[:,i,:] = np.sum(self.inner_emit[:,i,:,:], axis=-1)


	# ===============END OF LEFT-BRANCHING PARSE===================

	# ===========START OF VITERBI ALGORITHM ===================

	def get_viterbi_parse(self, string_batch):
		"""
		Returns the viterbi (most probable) parse of the input strings,
		together with log likelihoods of the strings given the parse.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1: # No branching for strings of length 1
			log_like_per_parse = (
											np.log(self.init_weights)[np.newaxis,:,np.newaxis]
											+
											self.log_nt2t_rule_weights[np.newaxis,:,:]
											+
											self.log_emission_probs[:,0,np.newaxis,:]
										)
			max_ids = np.argmax(log_like_per_parse.reshape(self.num_strings, -1), axis=-1)
			max_roots,max_terminals = np.unravel_index(max_ids, log_like_per_parse.shape[1:])
			string_ids = np.arange(self.num_strings)
			viterbi_parses = [
				self._get_viterbi_subparse_emit(r,t)
				for r,t
				in zip(max_roots,max_terminals)
				]
			max_log_like = log_like_per_parse[string_ids,max_roots,max_terminals]
		else:
			self._calc_viterbi_probs()
			likelihoods_for_each_root_and_children = (
							self.inner_branch[:,-1,0,:,:,:,2]
							*
							self.init_weights[np.newaxis,:,np.newaxis,np.newaxis]
						)
			max_ids = np.argmax(likelihoods_for_each_root_and_children.reshape(self.num_strings,-1) ,axis=(-1))
			string_ids = np.arange(self.num_strings)
			max_roots, max_child_left, max_child_right = np.unravel_index(max_ids, self.viterbi_branch.shape[3:6])
			viterbi_parses = [
				self._get_viterbi_subparse_branch(-1, 0, rt_lb, rhs_l, rhs_r, 2, string_id)
				for string_id,rt_lb,rhs_l,rhs_r
				in zip(string_ids, max_roots, max_child_left, max_child_right)
				]
			max_log_like = np.log(likelihoods_for_each_root_and_children[string_ids,max_roots,max_child_left,max_child_right]) - np.sum(self.log_scaling_factors, axis=-1)
		max_log_like += self.log_branch_normalizer * (self.string_length - 1)
		return viterbi_parses, max_log_like

	def _get_viterbi_subparse_branch(self, i, k, lhs, rhs_left, rhs_right, dot_pos, string_id, mother_node = None):
		if dot_pos == 0:
			subtree_root = tree.Node(lhs, mother=mother_node)
			return subtree_root
		elif dot_pos == 1:
			child_lhs = rhs_left
		else:
			child_lhs = rhs_right
		branch_or_emit = self.viterbi_predecessor[string_id,i,k,lhs,rhs_left,rhs_right,dot_pos,0]
		j = self.viterbi_predecessor[string_id,i,k,lhs,rhs_left,rhs_right,dot_pos,1]
		child_rhs_left_or_terminal = self.viterbi_predecessor[string_id,i,k,lhs,rhs_left,rhs_right,dot_pos,2]
		child_rhs_right = self.viterbi_predecessor[string_id,i,k,lhs,rhs_left,rhs_right,dot_pos,3]
		subtree_root = self._get_viterbi_subparse_branch(j, k, lhs, rhs_left, rhs_right, dot_pos-1, string_id, mother_node = mother_node)
		if branch_or_emit: # if the predecessor is emission.
			child_tree = self._get_viterbi_subparse_emit(child_lhs, child_rhs_left_or_terminal, mother_node = subtree_root)
		else:
			child_tree = self._get_viterbi_subparse_branch(i, j, child_lhs, child_rhs_left_or_terminal, child_rhs_right, 2, string_id, mother_node = subtree_root)
		return subtree_root

	def _get_viterbi_subparse_emit(self, lhs, terminal_symbol, mother_node = None):
		"""
		Parse the non-terminal-to-terminal subtree.
		"""
		pre_terminal = tree.Node(lhs, mother=mother_node)
		terminal = tree.Terminal(terminal_symbol, mother=pre_terminal)
		return pre_terminal
		

	def _calc_viterbi_probs(self):
		"""
		Calculates forward, inner, and viterbi probabilities.
		Underflow is avoided by scaling.
		(Forward and inner probs are used for scaling.)
		"""
		self.forward_branch=np.zeros(
							(
								self.num_strings, # string id (among the same length)
								self.string_length+1, # penultimate i i
								self.string_length+1, # start i k
								self.branch_rule_weights.shape[0], # LHS non-terminal
								self.branch_rule_weights.shape[1], # Left RHS non-terminal
								self.branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot i
							)
							)
		self.inner_branch=np.zeros(self.forward_branch.shape)
		self.inner_emit = np.zeros(
							(
							self.num_strings, # string id (among the same length)
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.log_nt2t_rule_weights.shape[0], # LHS non-terminal
							self.log_nt2t_rule_weights.shape[1] # Terminal
							# dot i is redundant, so omitted.
							)
							)

		self.viterbi_branch = np.zeros(self.inner_branch.shape)
		self.viterbi_emit = np.zeros(self.inner_emit.shape)
		
		# Initial prediction
		left_corner_from_init=np.matmul(
								self.init_weights
								,
								self.left_corner_weights
								)
		
		self.forward_branch[:,0,0,:,:,:,0]=left_corner_from_init[np.newaxis,:,np.newaxis,np.newaxis]*self.branch_rule_weights[np.newaxis,:,:,:]
		self.inner_branch[:,0,0,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]
		self.viterbi_branch[:,0,0,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]
		

		log_nt2t_and_emission = (
						self.log_nt2t_rule_weights[np.newaxis,:,:]
						+
						self.log_emission_probs[:,0,np.newaxis,:]
						)
		log_forward_emit = (
							np.log(left_corner_from_init)[np.newaxis,:,np.newaxis]
							+
							log_nt2t_and_emission
							)
		log_forward_emit = (
							np.log(left_corner_from_init)[np.newaxis,:,np.newaxis]
							+
							log_nt2t_and_emission
							)
		# scaling
		self.log_scaling_factors = np.zeros((self.num_strings,self.string_length))
		self.log_scaling_factors[:,0] = - spm.logsumexp(log_forward_emit, axis=(-2,-1))
		self.inner_emit[:,0,:,:] = np.exp(log_nt2t_and_emission + self.log_scaling_factors[:,0,np.newaxis,np.newaxis])
		self.viterbi_emit[:,0,:,:] = self.inner_emit[:,0,:,:]

		# Summation to be reused.
		self.completed_inner_branch=np.zeros( # To be reused.
											(
											self.num_strings,
											self.string_length+1, # i
											self.string_length+1, # j
											self.branch_rule_weights.shape[0],
											)
											)
		self.completed_viterbi_branch = np.zeros(self.completed_inner_branch.shape)
		self.scanned_inner_emit = np.zeros(self.inner_emit.shape[:-1])
		self.scanned_inner_emit[:,0,:] = np.sum(self.inner_emit[:,0,:,:], axis=-1)

		# Viterbi unfo
		error_value = np.max((2,self.string_length,self.log_nt2t_rule_weights.shape[0],self.log_nt2t_rule_weights.shape[1])) + 1 # This value should be too big to use for indexation.
		self.viterbi_predecessor = np.full(
										self.viterbi_branch.shape
										+
										(4,) # Info
												# 1st entry for branch (0) vs. emission (1).
												# 2nd entry for j
												# 3rd entry for RHS_left/terminal
												# 4th entry for RHS_right/
										,
										error_value
									)
		self.viterbi_predecessor_for_completed = np.full(
										self.completed_inner_branch.shape
										+
										(2,)
										,
										error_value
									)

		self.scanned_viterbi_emit = np.zeros(self.scanned_inner_emit.shape)
		self.viterbi_terminal = np.full(
										self.scanned_viterbi_emit.shape
										,
										error_value
									)
		self.viterbi_terminal[:,0,:] = np.argmax(self.viterbi_emit[:,0,:,:], axis=-1)
		string_ids, lhss = np.indices(self.viterbi_terminal[:,0,:].shape)
		self.scanned_viterbi_emit[:,0,:] = self.viterbi_emit[string_ids,0,lhss,self.viterbi_terminal[:,0,:]]

		
		[
			(
				self._viterbi_routine(
						i
						)
			)
			for 
			i
			in xrange(1,self.string_length+1) # for i=1....
		]



	def _viterbi_routine(self,i):
		# Scanning is omitted or merged to prediction.
		if i > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last(i)
			self._complete_mid_to_last_viterbi(i)
		if i < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid(i)
			self._complete_start_to_mid_viterbi(i)
			self._predict(i)
			self._predict_viterbi(i)


	def _complete_mid_to_last_viterbi(self,i):
		"""
		Complete X->B.Y => X->BY. for Viterbi.
		"""
		self.viterbi_branch[
						:,
						i, # i
						i-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.viterbi_branch[
										:,		# string id
										i-1,	# j=i-1
										i-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot i
										]
							*
							self.scanned_viterbi_emit[
										:,
										i-1,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										]
							)
		self.viterbi_predecessor[
			:, # string id
			i, # i
			i - 2, # k
			:, # LHS
			:, # RHS left
			:, # RHS right
			2, # dot i
			0, # info type (0): predecessor = branch (0) or emit (1)?
		] = 1 # predecessor must be emission.
		self.viterbi_predecessor[
			:, # string id
			i, # i
			i - 2, # k
			:, # LHS
			:, # RHS left
			:, # RHS right
			2, # dot i
			1, # info type (1): j
		] = i - 1
		self.viterbi_predecessor[
			:, # string id
			i, # i
			i - 2, # k
			:, # LHS
			:, # RHS left
			:, # RHS right
			2, # dot i
			2, # info type (2): terminal
		] = self.viterbi_terminal[
									:,
									i-1,		# i
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
		reshaped_viterbi_branch = self.viterbi_branch[
											:, # string id
											i, # i
											i - 2, # k
											:, # LHS
											:, # left RHS
											:, # right RHS
											2, # dot i
										].reshape(
											self.viterbi_branch.shape[0],
											self.viterbi_branch.shape[3], # num_nt
											-1
										)
		max_ids = np.argmax(reshaped_viterbi_branch, axis=-1)
		string_ids, lhss = np.indices(max_ids.shape)
		self.completed_viterbi_branch[:,i,i-2,:] = reshaped_viterbi_branch[string_ids, lhss, max_ids]
		max_ids_left_right = np.unravel_index(max_ids, self.viterbi_branch.shape[4:6])
		self.viterbi_predecessor_for_completed[:,i,i-2,:,0] = max_ids_left_right[0]
		self.viterbi_predecessor_for_completed[:,i,i-2,:,1] = max_ids_left_right[1]
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k_viterbi(
						i,
						k
						)
			for k in reversed(xrange(i-2)) # k=i-3,...,0 <- Only valid on Python 2.
		]

	def _complete_mid_to_last_per_k_viterbi(self,i,k):
		viterbi_branch_x_completed = (
										self.viterbi_branch[
											:,		# string id
											k+1:i-1,	# j
											k,		#k
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											1,		# dot i
											]
										*
										self.completed_viterbi_branch[
											:,		# string id
											i,	# i
											k+1:i-1, # j
											np.newaxis, # LHS of the target.
											np.newaxis, # Left of the RHS.
											:, # Right of the RHS.
										]
							)
		branch_max_ids = np.argmax(viterbi_branch_x_completed, axis=1)
		string_ids,lhss,rhs_ls,rhs_rs = np.indices(branch_max_ids.shape)
		max_viterbi_branch_x_completed = viterbi_branch_x_completed[string_ids,branch_max_ids,lhss,rhs_ls,rhs_rs]
		viterbi_branch_x_emit = (
								self.viterbi_branch[
									:,		# string id
									i-1,	# j=i-1
									k,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot i
									]
								*
								self.scanned_viterbi_emit[
									:,	# string id
									i-1,		# i
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
								)
		branch_vs_emit = np.array((max_viterbi_branch_x_completed, viterbi_branch_x_emit))
		branch_or_emit_ids = np.argmax(branch_vs_emit, axis=0)
		self.viterbi_branch[:,i,k,:,:,:,2] = branch_vs_emit[branch_or_emit_ids, string_ids, lhss, rhs_ls, rhs_rs]
		self.viterbi_predecessor[:,i,k,:,:,:,2,0] = branch_or_emit_ids
		branch_js = np.arange(k+1,i-1)[branch_max_ids]
		self.viterbi_predecessor[:,i,k,:,:,:,2,1] = (
															branch_js * (branch_or_emit_ids == 0)
															+
															(i-1) * branch_or_emit_ids
														)
		self.viterbi_predecessor[:,i,k,:,:,:,2,2] = (
															self.viterbi_predecessor_for_completed[string_ids,i,branch_js,lhss,0] * (branch_or_emit_ids == 0)
															+
															self.viterbi_terminal[:,i-1,np.newaxis,np.newaxis,:] * branch_or_emit_ids
														)
		self.viterbi_predecessor[:,i,k,:,:,:,2,3] = self.viterbi_predecessor_for_completed[string_ids,i,branch_js,lhss,1] * (branch_or_emit_ids == 0)

		reshaped_viterbi_branch = self.viterbi_branch[
													:,		# string id
													i,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot i
													].reshape(
														self.viterbi_branch.shape[0],
														self.viterbi_branch.shape[3],
														self.viterbi_branch.shape[3]**2
													)
		completed_max_ids = np.argmax(reshaped_viterbi_branch, axis=-1)
		string_ids, lhss = np.indices(completed_max_ids.shape)
		self.completed_viterbi_branch[:,i,k,:] = reshaped_viterbi_branch[string_ids,lhss,completed_max_ids]
		max_ids_left_right = np.unravel_index(completed_max_ids, self.viterbi_branch.shape[4:6])
		self.viterbi_predecessor_for_completed[:,i,k,:,0] = max_ids_left_right[0]
		self.viterbi_predecessor_for_completed[:,i,k,:,1] = max_ids_left_right[1]


	def _complete_start_to_mid_viterbi(self,i):
		"""
		Complete X->.YA => X->Y.A for Viterbi.
		"""
		if i>1: # No branch rule has been completed if i=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(i-1)
			self.viterbi_branch[
					:,	# string id
					i, # i
					:i-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.viterbi_branch[
										:,	# string id
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot i
										].transpose((1,0,2,3,4))
							*
							self.completed_viterbi_branch[
										:,		# string id
										i,	# i
										:i-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
			self.viterbi_predecessor[
						:,	# string id
						i, # i
						:i-1, # k
						:,
						:,
						:,
						1,
						0
						] = 0
			self.viterbi_predecessor[
						:,	# string id
						i, # i
						:i-1, # k
						:,
						:,
						:,
						1,
						1
						] = j[np.newaxis,:,np.newaxis,np.newaxis,np.newaxis]
			self.viterbi_predecessor[
						:,	# string id
						i, # i
						:i-1, # k
						:,
						:,
						:,
						1,
						2
						] = self.viterbi_predecessor_for_completed[:,i,:i-1,np.newaxis,np.newaxis,:,0]
			self.viterbi_predecessor[
						:,	# string id
						i, # i
						:i-1, # k
						:,
						:,
						:,
						1,
						3
						] = self.viterbi_predecessor_for_completed[:,i,:i-1,np.newaxis,np.newaxis,:,1]
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.viterbi_branch[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.viterbi_branch[
									:,		# string id
									i-1,	# j=i-1
									i-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot i
									]
						*
						self.scanned_viterbi_emit[
									:,# string id
									i-1,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)
		self.viterbi_predecessor[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				0
				] = 1
		self.viterbi_predecessor[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				1
				] = i - 1
		self.viterbi_predecessor[
				:,
				i, # i
				i-1, # k
				:,
				:,
				:,
				1,
				2
				] = self.viterbi_terminal[
									:,# string id
									i-1,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]





	def _predict_viterbi(self,i):
		"""
		The "prediction" step of the forward/inner algorithm for Viterbi.
		"""
		# Prediction
		# Exactly the same as the inner probs.
		self.viterbi_branch[:,i,i,:,:,:,0] = self.inner_branch[:,i,i,:,:,:,0]
		self.viterbi_emit[:,i,:,:] = self.inner_emit[:,i,:,:]

		self.viterbi_terminal[:,i,:] = np.argmax(self.viterbi_emit[:,i,:,:], axis=-1)
		string_ids, lhss = np.indices(self.viterbi_terminal[:,i,:].shape)
		self.scanned_viterbi_emit[:,i,:] = self.viterbi_emit[string_ids,i,lhss,self.viterbi_terminal[:,i,:]]