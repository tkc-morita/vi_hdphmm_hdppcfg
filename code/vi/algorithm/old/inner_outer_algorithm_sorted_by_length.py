# coding: utf-8
"""
Stolcke's (1995) inner-outer algorithm for
a fully connected PCFG in a Chomsky normal form.
It simultaneously computes likelihoods of strings of the same length,
and expected counts of rule uses to generate the strings.
"""

import numpy as np
import scipy.misc as spm

def sort_data_by_length(data, max_batch_size=50):
	sorted_data = {}
	for string in data:
		string_length = len(string)
		if string_length in sorted_data.keys():
			batch_list = sorted_data[string_length]
			if batch_list[-1].shape[0] < max_batch_size:
				batch_list[-1] = np.append(
										batch_list[-1],
										np.array(string)[np.newaxis,:]
										,
										axis=0
										)
			else:
				batch_list.append(np.array(string)[np.newaxis,:])
		else:
			sorted_data[string_length] = [np.array(string)[np.newaxis,:]]
	return sorted_data

class InnerOuterAlgorithm(object):
	def __init__(
			self,
			branch_rule_weights,
			emission_rule_weights,
			init_weights,
			):
		"""
		Initialize rule weights.
		"""
		self.branch_rule_weights = branch_rule_weights
		self.emission_rule_weights = emission_rule_weights
		self.init_weights = init_weights
		self.left_corner_weights = self._get_left_corner_probs()

	def get_log_likelihoods(self, string_batch):
		"""
		Get log likelihoods of string_batch without counting expected rule use.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1:
			return spm.logsumexp(
						np.log(self.init_weights)[:,np.newaxis]
						+
						np.log(self.emission_rule_weights[:,string_batch[:,0]])
						,
						axis=0
						).transpose()
		else:
			self._calc_forward_inner_probs()
			return spm.logsumexp(
								np.log(self.init_weights)[np.newaxis,:]
								+
								np.log(np.sum(self.inner_branch[:,-1,0,:,:,:,2], axis=(2,3)))
								-
								np.sum(np.log(self.scaling_factors), axis=1)[:,np.newaxis]
								,
								axis=0
								)


	def get_expected_rule_counts(self, string_batch):
		"""
		Returns expected # of use of each rule,
		together with log likelihoods of strings.
		"""
		self._set_strings(string_batch)
		if self.string_length == 1: # No branching for strings of length 1
			expected_init_weights = self.init_weights[np.newaxis,:]*self.emission_rule_weights[:,string_batch[:,0]].transpose()
			likelihood = np.sum(expected_init_weights, axis=-1)
			expected_init_weights /= likelihood[:,np.newaxis]
			expected_branch_rule_counts = np.zeros((self.string_ids.size,)+self.branch_rule_weights.shape)
			expected_emission_rule_counts = np.zeros((self.string_ids.size,)+self.emission_rule_weights.shape)
			expected_emission_rule_counts[self.string_ids,:,string_batch[:,0]] += expected_init_weights
			log_like = np.log(likelihood)
		else:
			self._calc_forward_inner_probs()
			self._calc_outer_probs()
			scaled_likelihoods_for_each_root_non_terminal=(
										np.sum(self.inner_branch[:,-1,0,:,:,:,2], axis=(2,3))
										*
										self.init_weights[np.newaxis,:]
										)
			scaled_likelihood=np.sum(
									scaled_likelihoods_for_each_root_non_terminal
									,
									axis=-1
								)
			expected_branch_rule_counts=(
											np.sum(
												self.outer_branch[...,0].diagonal(axis1=1, axis2=2) # Scaled.
												*
												self.inner_branch[...,0].diagonal(axis1=1, axis2=2) # Not scaled.
												,
												axis=-1 # diagonalized dimension comes last.
												) # num_strings x num_nt x num_nt x num_nt
											/
											scaled_likelihood[:,np.newaxis,np.newaxis,np.newaxis]
											)
			
			expected_emission_rule_counts=(
										np.sum(
											self.outer_emit[...,0] # Scaled.
											*
											self.inner_emit[...,0] # Not scaled.
											,
											axis=1
											) # num_strings x num_nt x num_symbols
										/
										scaled_likelihood[:,np.newaxis,np.newaxis]
										)
			expected_init_weights = (
									scaled_likelihoods_for_each_root_non_terminal
									/
									scaled_likelihood[:,np.newaxis]
									)
			log_like = np.log(scaled_likelihood)-np.sum(np.log(self.scaling_factors), axis=-1)
		return expected_branch_rule_counts, expected_emission_rule_counts, expected_init_weights, log_like

	def _set_strings(self, string_batch):
		"""
		(Re)set emitted strings.
		Strings of the same length are assumed to be sorted.
		"""
		self.string_batch = string_batch
		self.string_length = self.string_batch.shape[1]
		self.string_ids = np.arange(self.string_batch.shape[0])

	def _get_left_corner_probs(self):
		"""
		Computes left corner probabilities/weights of branching rules.
		"""
		return np.linalg.inv(
							np.identity(self.branch_rule_weights.shape[0])
							-
							np.sum(self.branch_rule_weights, axis=2)
							)



	def _calc_forward_inner_probs(self):
		"""
		Calculates forward and inner probabilities.
		Underflow is avoided by scaling.
		"""
		self.forward_branch=np.zeros(
							(
								self.string_ids.size, # string id (among the same length)
								self.string_length+1, # penultimate pos i
								self.string_length+1, # start pos k
								self.branch_rule_weights.shape[0], # LHS non-terminal
								self.branch_rule_weights.shape[1], # Left RHS non-terminal
								self.branch_rule_weights.shape[2], # Right RHS non-terminal
								3 # dot pos
							)
							)
		self.forward_emit=np.zeros(
							(
							self.string_ids.size, # string id (among the same length)
							self.string_length+1, # i. j=i if dot_pos=0 and j=i+1 if dot_pos=1
							self.emission_rule_weights.shape[0], # LHS non-terminal
							self.emission_rule_weights.shape[1], # Emitted emitted_symbols
							2 # dot pos
							)
							)
		self.inner_branch=np.zeros(self.forward_branch.shape)
		self.inner_emit=np.zeros(self.forward_emit.shape)
		
		# Initial prediction
		left_corner_from_init=np.matmul(
								self.init_weights
								,
								self.left_corner_weights
								)
		
		self.forward_branch[:,0,0,:,:,:,0]+=left_corner_from_init[np.newaxis,:,np.newaxis,np.newaxis]*self.branch_rule_weights[np.newaxis,:,:,:]
		self.inner_branch[:,0,0,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]

		emitted_symbols = self.string_batch[:,0]

		self.forward_emit[self.string_ids,0,:,emitted_symbols,0] += (
																		left_corner_from_init[:,np.newaxis]
																		*
																		self.emission_rule_weights[:,emitted_symbols]
																	).transpose()
		self.inner_emit[self.string_ids,0,:,emitted_symbols,0] = self.emission_rule_weights[:,emitted_symbols].transpose()
		self.scaling_factors=np.ones((self.string_ids.size,self.string_length))
		self.completed_inner_branch=np.zeros( # To be reused.
											(
											self.string_ids.size,
											self.string_length+1, # i
											self.string_length+1, # j
											self.branch_rule_weights.shape[0],
											)
											)
		[
			(
				self._forward_inner_routine(
						pos,
						emitted_symbols
						)
			)
			for 
			pos,
			emitted_symbols
			in enumerate(self.string_batch.transpose(), start=1) # for i=1....
		]



	def _forward_inner_routine(
						self,
						pos,
						emitted_symbols,
						):
		self._scale_and_scan(pos,emitted_symbols)
		if pos > 1:
			# Completion of a rule X->B.Y => X->BY. can feed another completion. So, serialism is needed.
			# j:j X->B.Y is impossible. => k<=j-1.
			# i:i-1 Y->a. is possible. j=i-1
			self._complete_mid_to_last(pos,emitted_symbols)
		if pos < self.string_length:
			# Then, complete X->.YA => X->Y.A, which does not feed another.
			self._complete_start_to_mid(pos,emitted_symbols)
			self._predict(pos)


	def _scale_and_scan(
			self,
			pos,
			emitted_symbols,
			):
		"""
		The "scan" step of the forward/inner algorithm.
		Also scale foward and inner emission probabilities by (already scaled) prefix probability.
		"""
		# Get scalar
		self.scaling_factors[:,pos-1]=np.sum(self.forward_emit[self.string_ids,pos-1,:,emitted_symbols,0], axis=-1)**-1
				
		# Scanning
		self.forward_emit[self.string_ids,pos,:,emitted_symbols,1]=self.forward_emit[self.string_ids,pos-1,:,emitted_symbols,0]*self.scaling_factors[:,pos-1,np.newaxis]
		self.inner_emit[self.string_ids,pos,:,emitted_symbols,1]=self.inner_emit[self.string_ids,pos-1,:,emitted_symbols,0]*self.scaling_factors[:,pos-1,np.newaxis]


	def _complete_mid_to_last(
						self,
						pos,
						emitted_symbols
					):
		"""
		Complete X->B.Y => X->BY.
		"""
		self.forward_branch[
						:,
						pos, # i
						pos-2, # k
						:,
						:,
						:,
						2
						]=(
							self.forward_branch[
										:,		# string id
										pos-1,	# j=i-1
										pos-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot pos
										]
							*
							self.inner_emit[ #Scanned above.
										self.string_ids,	# string id
										pos,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										emitted_symbols,		# Symbol scanned above.
										1
										]
							)
		self.inner_branch[
						:,
						pos, # i
						pos-2, # k
						:,
						:,
						:,
						2,
						]=(
							self.inner_branch[
										:,		# string id
										pos-1,	# j=i-1
										pos-2,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										1,		# dot pos
										]
							*
							self.inner_emit[ # Scanned above.
										self.string_ids,
										pos,		# i
										np.newaxis,	# LHS
										np.newaxis,	# Left RHS
										:,			# Right RHS
										emitted_symbols,		# Symbol scanned above.
										1
										]
							)
		self.completed_inner_branch[:,pos,pos-2,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
												self.inner_branch[
														:,		# string id
														pos,	# i
														pos-2,		# j
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														2,		#Dot pos
														]
												,
												axis=(
													2,	# Left RHS
													3	# Right RHS
													)
												)
		# i:i-1 Y->CD. is impossible. => j=<i-2 for Y->CD. => k<=j-1<=i-3
		[
			self._complete_mid_to_last_per_k(
						pos,
						emitted_symbols,
						k
						)
			for k in reversed(xrange(pos-2)) # k=pos-3,...,0
		]

	def _complete_mid_to_last_per_k(
						self,
						pos,
						emitted_symbols,
						k
						):
		self.forward_branch[:,pos,k,:,:,:,2]=(
										# j:k X->B.Y and i:j Y->CD. (j>k)
										np.sum(
											self.forward_branch[
														:,		# string id
														k+1:pos-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot pos
														]
											*
											self.completed_inner_branch[
														:,		# string id
														pos,	# i
														k+1:pos-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										+
										# i-1:k X->B.Y and i:i-1 Y->a.
										(
											self.forward_branch[
														:,		# string id
														pos-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot pos
														]
											*
											self.inner_emit[ #Scanned above.
														self.string_ids,	# string id
														pos,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														emitted_symbols,		# Symbol scanned above.
														1,
														]
											)
										)
		self.inner_branch[:,pos,k,:,:,:,2]=(
										np.sum(
											self.inner_branch[
														:,		# string id
														k+1:pos-1,	# j
														k,		#k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot pos
														]
											*
											self.completed_inner_branch[
														:,		# string id
														pos,	# i
														k+1:pos-1, # j
														np.newaxis, # LHS of the target.
														np.newaxis, # Left of the RHS.
														:, # Right of the RHS.
														]
											,
											axis=1 # over j
											)
										+
										(
											self.inner_branch[
														:,		# string id
														pos-1,	# j=i-1
														k,		# k
														:,		# LHS
														:,		# Left RHS
														:,		# Right RHS
														1,		# dot pos
														]
											*
											self.inner_emit[ # Scanned above.
														self.string_ids,	 # string id
														pos,		# i
														np.newaxis,	# LHS
														np.newaxis,	# Left RHS
														:,			# Right RHS
														emitted_symbols,		# Symbol scanned above.
														1
														]
											)
										)
		self.completed_inner_branch[:,pos,k,:]=np.sum( # i:k X->BY. is now a new input in the form i:j' Y'->CD.
											self.inner_branch[
													:,		# string id
													pos,	# i
													k,		# j
													:,		# LHS
													:,		# Left RHS
													:,		# Right RHS
													2,		#Dot pos
													]
											,
											axis=(
												2,	# Left RHS
												3	# Right RHS
												)
											)

	def _complete_start_to_mid(
						self,
						pos,
						emitted_symbols
					):
		"""
		Complete X->.YA => X->Y.A
		"""
		if pos>1: # No branch rule has been completed if pos=1.
			# i:j Y->BC. is impossible for i-j<2. Thus, j<=i-2 for Y->BC.
			# j:k X->.YA is possible when k=j.
			j=np.arange(pos-1)
			self.forward_branch[
						:,	# string id
						pos, # i
						:pos-1, # k
						:,
						:,
						:,
						1,
						]=(
								self.forward_branch[
											:,	# string id
											j,	# j<=i-2
											j,	# k j=k is possible.
											:,		# LHS
											:,		# Left RHS
											:,		# Right RHS
											0,		# dot pos
											].transpose((1,0,2,3,4)) # the advanced indexing by j moves the dimension to the front.
								*
								self.completed_inner_branch[
											:,		# string id
											pos,	# i
											:pos-1, # j
											np.newaxis, # LHS of the target.
											:, 			# Left of the RHS.
											np.newaxis,	# Right of the RHS.
											]
								)
			self.inner_branch[
					:,	# string id
					pos, # i
					:pos-1, # k
					:,
					:,
					:,
					1,
					]=(
							self.inner_branch[
										:,	# string id
										j,	# j
										j,		#k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot pos
										].transpose((1,0,2,3,4))
							*
							self.completed_inner_branch[
										:,		# string id
										pos,	# i
										:pos-1, # j
	# 											np.newaxis, # k
										np.newaxis, # LHS of the target.
										:, 			# Left of the RHS.
										np.newaxis,	# Right of the RHS.
										]
							)
		# i:j Y->a. is possible when j=i-1. Thus, k=j=i-1.
		self.forward_branch[
					:, # string id
					pos, # i
					pos-1, # k
					:,
					:,
					:,
					1
					]=(
							self.forward_branch[
										:,		# string id
										pos-1,	# j=i-1
										pos-1,		# k
										:,		# LHS
										:,		# Left RHS
										:,		# Right RHS
										0,		# dot pos
										]
							*
							self.inner_emit[ #Scanned above.
										self.string_ids, # string id
										pos,		# i
										np.newaxis,	# LHS
										:,			# Left RHS
										np.newaxis,	# Right RHS
										emitted_symbols,		# Symbol scanned above.
										1,			# dot pos
										]
							)
		self.inner_branch[
				:,
				pos, # i
				pos-1, # k
				:,
				:,
				:,
				1,
				]=(
						self.inner_branch[
									:,		# string id
									pos-1,	# j=i-1
									pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									0,		# dot pos
									]
						*
						self.inner_emit[ #Scanned above.
									self.string_ids,# string id
									pos,		# i
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									emitted_symbols,	# Symbol scanned above.
									1,
									]
						)


		

	def _predict(
			self,
			pos
			):
		"""
		The "prediction" step of the forward/inner algorithm.
		"""
		# Prediction
		# Only i:k X->A.Z is introduced in the completion step and is expandable.
		forward_x_left_corner=np.sum( # alpha * R(Z =>L Y)
											np.sum(
												self.forward_branch[
														:,	# string id
														pos, #i
														:pos, #k
														:,
														:,
														:,
														1
														]
												,
												axis=(
													1, # start pos (k)
													2, # LHS non-terminal
													3, # Left RHS non-terminal.
													)
												)[:,:,np.newaxis] # num_strings x num_nonterminals (right RHS)
											*
											self.left_corner_weights[np.newaxis,:,:] # num_non_terminals x num_non_terminals
											,
											axis=1
											) # num_strings x num_non_terminals
		self.forward_branch[:,pos,pos,:,:,:,0]=(
										forward_x_left_corner[:,:,np.newaxis,np.newaxis]
										*
										self.branch_rule_weights[np.newaxis,:,:,:]
										)
		self.inner_branch[:,pos,pos,:,:,:,0]=self.branch_rule_weights[np.newaxis,:,:,:]
		# self.string_batch[pos] is the emitted_symbols to be scanned in the next iteration.
		next_emitted_symbols = self.string_batch[:,pos]
		used_emission_rules = self.emission_rule_weights[:,next_emitted_symbols].transpose()

		self.forward_emit[self.string_ids,pos,:,next_emitted_symbols,0]=(
										forward_x_left_corner
										*
										used_emission_rules
										)
		self.inner_emit[self.string_ids,pos,:,next_emitted_symbols,0]=used_emission_rules


	# ===============END OF FORWARD-INNER ALGORITHM===================






	# ==============START OF OUTER ALGORITHM==========================

	def _calc_outer_probs(self):
		"""
		Calculate outer probabilities.
		"""
		self.outer_branch=np.zeros(self.inner_branch.shape)
		self.outer_emit=np.zeros(self.inner_emit.shape)
		# Initialization
		# Reverse completion of i:j S.->A. where i=l and j=0.
		# By i:k . -> S. and j:k . -> .S where i=l and k=0.
		# This leaves the outer probability 1.
		# And then reverse completion of i:j A->BC. where i=l and j=0.
		# By i:j S.->A. and i:j
		self.outer_branch[
					:, # string id
					-1,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot pos
					]+=self.init_weights[np.newaxis,:,np.newaxis,np.newaxis]
					
		# Reverse completion of i: jY->CD.
		# When j=0 (=> k=0), i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		# But for i=l(or -1), i: 0X->Y.A and 0: 0X->.YA are not introduced.
		[
			self._reverse_complete_last_to_another_last_per_j_init(j)
			for j in xrange(1,self.string_length-1) # -1: -2 Y->CD. is impossible!!
		]
		
		# Reverse completion of i: jY->a.
		emitted_symbols = self.string_batch[:,-1]
		self.outer_emit[
					self.string_ids,
					-1,	# i
					:,		# LHS
					emitted_symbols,	# emission
					1,		# do pos
					]=self._get_outer_x_inner_j_greater_k(-1, -2, 2, 1, (1, 2, 3))
		# Reverse completion of j: kX->B.Y k<=i-1
		self.outer_branch[
					:,		# string id
					:-1,	# j
					:-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]+=(
						self.outer_branch[
									:,	# string id
									-1,	# i
									np.newaxis,	# j
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						*
						self.completed_inner_branch[
									:,		# string id
									-1,		#i
									:-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.outer_branch[
					:,	# string id
					-2,	# j
					:-2,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]+=(
						self.outer_branch[
									:,	# string id
									-1,	# i
									:-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						*
						self.inner_emit[
									self.string_ids,
									-1,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									emitted_symbols,		# scanned emitted_symbols
									1,			# dot pos
									]
						)
		# Reverse completion of j: kX->.YA N/A b/c -1: 0X->Y.A is irrelevant.
		# Reverse scanning. Scaling must be changed.
		self.outer_emit[
					self.string_ids,
					-2,	# i
					:,		# LHS
					emitted_symbols,	# Left RHS
					0,		# dot pos
					]=(
						self.outer_emit[
							self.string_ids, # string id
							-1,	# i+1, k=i+1-1=i
							:,		# LHS
							emitted_symbols,		# Left RHS
							1		# dot pos
							] # dot_pos=1 (i.e. k=i-1)ではscaling facoters Ci が含まれていない。
						*
						self.scaling_factors[:,-1,np.newaxis]
							)
		[
			(
				self._reverse_complete(
					pos,
					emitted_symbols
					)
				,
				self._reverse_scan(
					pos,
					emitted_symbols
					)
			)
			for pos,emitted_symbols in reversed(list(enumerate(self.string_batch[:,:-1].transpose(), start=1)))
		]


	def _get_outer_x_inner_j_greater_k(
					self,
					pos,
					j,
					outer_dot_pos,
					inner_dot_pos,
					marginalization_axis
					):
		"""
		Computes (outer probability) x (inner probability) when j > k.
		"""
		return np.sum(
						self.outer_branch[ # i:k X->BY.
									:,		# string id
									pos,	# i
									:j,		# k
									:,		# LHS
									:,		# Left RHS independent
									:,		# Right RHS
									outer_dot_pos,		# do pos
									]
						*
						self.inner_branch[ # j:k X->B.Y k<j
									:,	# string id
									j,	# j
									:j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do pos
									]
						,
						axis=marginalization_axis
					)


	def _get_outer_x_inner_j_eq_k(self, pos, j, outer_dot_pos, inner_dot_pos, marginalization_axis):
		"""
		Computes (outer probability) x (inner probability) when j = k.
		"""
		return np.sum(
						self.outer_branch[ # i:k X->Y.A
									:,		# string id
									pos,	# i
									j,	# k
									:,		# LHS
									:,		# Left RHS # Independent
									:,		# Right RHS
									outer_dot_pos,		# do pos
									]
						*
						self.inner_branch[ # j:k X->.YA j=k
									:,	# string id
									j,	# j <- jに依存している。
									j,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									inner_dot_pos,		# do pos
									]
						,
						axis=marginalization_axis
					)

	def _reverse_complete_last_to_another_last_per_j_init(self, j):
		# Reverse completion of i: jY->CD. j<=i-2
		self.outer_branch[
				:,	# string id
				-1,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot pos
				]=self._get_outer_x_inner_j_greater_k(
						-1,
						j,
						2,
						1,
						(1,2,3)
						)[:,:,np.newaxis,np.newaxis]



	def _reverse_complete_last_to_another_last_per_j(self, pos, j):
		"""
		Reverse completion of i: jY->CD. given j.
		"""
		self.outer_branch[
				:,	# string id
				pos,	# i
				j,	# j
				:,		# LHS
				:,		# Left RHS
				:,		# Right RHS
				2,		# dot pos
				]=(
					self._get_outer_x_inner_j_eq_k(pos, j, 1, 0, (1,3))
					+
					self._get_outer_x_inner_j_greater_k(pos, j, 2, 1, (1,2,3))
					)[:,:,np.newaxis,np.newaxis]

	def _reverse_complete_last_to_another_last(self, pos):
		"""
		Reverse completion of i: jY->CD.
		"""
		# When j=0, k=0 as well.
		# i: 0X->BY. and 0: 0X->B.Y are impossible.
		# i: 0X->Y.A and 0: 0X->.YA are possible on the other hand.
		self.outer_branch[
					:,		# string id
					pos,	# i
					0,	# j
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					2,		# dot pos
					]=self._get_outer_x_inner_j_eq_k(pos, 0, 1, 0, (1,3))[:,:,np.newaxis,np.newaxis]
		[self._reverse_complete_last_to_another_last_per_j(pos, j)
			for j in xrange(1,pos-1) # i: i-1 Y->CD. is impossible!!
		]



	def _reverse_complete_emission(self, pos, emitted_symbols):
		"""
		Reverse completion of i: jY->a.
		"""
		self.outer_emit[
					self.string_ids, # string id
					pos,	# i
					:,		# LHS
					emitted_symbols,		# Left RHS
					1,		# do pos
					]=(
						self._get_outer_x_inner_j_eq_k(pos, pos-1, 1, 0, (1,3))
						+
						self._get_outer_x_inner_j_greater_k(pos, pos-1, 2, 1, (1,2,3))
						)

	def _reverse_complete_last_to_mid(self, pos, emitted_symbols):
		"""
		Reverse completion of j: kX->B.Y (note k<j)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->BY.
		self.outer_branch[
					:,		# string id
					:pos-1,	# j
					:pos-2,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]+=(
						self.outer_branch[
									:,		# string id
									pos,	# i
									np.newaxis,	# j
									:pos-2,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						*
						self.completed_inner_branch[
									:,			# string id
									pos,		#i
									:pos-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									np.newaxis,	# Left RHS
									:,			# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->BY.
		self.outer_branch[
					:,		# string id
					pos-1,	# j
					:pos-1,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					1,		# dot pos
					]+=(
						self.outer_branch[
									:,		# string id
									pos,	# i
									:pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									2,		# dot pos
									]
						*
						self.inner_emit[
									self.string_ids,	 # string id
									pos,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									np.newaxis,	# Left RHS of branch
									:,			# Right RHS of branch (LHS of emission)
									emitted_symbols,		# scanned emitted_symbols
									1,			# dot pos
									]
						)

	def _reverse_complete_mid_to_start(self, pos, emitted_symbols):
		"""
		Reverse completion of j: kX->.YA (j=k is possible)
		"""
		# by i: jY->CD. (j<=i-2) and i: kX->Y.A
		self.outer_branch[
					:,		# string id
					:pos-1,	# j
					:pos-1,		# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot pos
					]+=(
						self.outer_branch[
									:,		# string id 
									pos,	# i
									np.newaxis,	# j
									:pos-1,		# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot pos
									]
						*
						self.completed_inner_branch[
									:,			# string id
									pos,		# i
									:pos-1,		# j
									np.newaxis,	# k
									np.newaxis,	# LHS
									:,			# Left RHS
									np.newaxis,	# Right RHS
									]
						)
		# by i: jY->a. (j=i-1) and i: kX->Y.A
		self.outer_branch[
					:,
					pos-1,	# j
					:pos,	# k
					:,		# LHS
					:,		# Left RHS
					:,		# Right RHS
					0,		# dot pos
					]+=(
						self.outer_branch[
									:,		# string id
									pos,	# i
									:pos,	# k
									:,		# LHS
									:,		# Left RHS
									:,		# Right RHS
									1,		# dot pos
									]
						*
						self.inner_emit[
									self.string_ids,	# string id
									pos,		# i
									np.newaxis,	# k
									np.newaxis,	# LHS of branch
									:,			# Left RHS of branch (LHS of emission)
									np.newaxis,	# Right RHS of branch 
									emitted_symbols,		# scanned emitted_symbols
									1,			# dot pos
									]
						)

	def _reverse_complete(self, pos, emitted_symbols):
		"""
		Reverse operation of completion.
		"""
		self._reverse_complete_last_to_another_last(pos)
		self._reverse_complete_emission(pos, emitted_symbols)
		self._reverse_complete_last_to_mid(pos, emitted_symbols)
		self._reverse_complete_mid_to_start(pos, emitted_symbols)


	def _reverse_scan(self, pos, emitted_symbols):
		"""
		Reverse scanning (just scaling).
		"""
		self.outer_emit[
					self.string_ids, # string id
					pos-1,	# i
					:,		# LHS
					emitted_symbols,	# Left RHS
					0,		# dot pos
					]=(
						self.outer_emit[
							self.string_ids, # string id
							pos,	# i+1, k=i+1-1=i
							:,		# LHS
							emitted_symbols,		# Left RHS
							1,		# dot pos
							] # dot_pos=1 (i.e. k=i-1) hasn't scaled by scaling_factors[i].
						*
						self.scaling_factors[:,pos-1,np.newaxis]
							)




