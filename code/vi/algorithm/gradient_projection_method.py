# coding: utf-8

"""
Optimize a vector on a simplex (without the last entry)
with the projected gradient algorithm (Bertsekas, 1982: pp.241-242).

Reference:
Bertsekas, Dimitri P. (1982)
	"Projected Newton methods for optimization problems with simple constraints."
"""

import numpy as np
import scipy.optimize as spo
import scipy.spatial.distance as spsd

def gradient_projection_optimization(
						initial_point,
						objective_func,
						get_gradient_and_hessian,
						tolerance,
						almost_zero_base_line = 0.00001
						):
	"""
	Returns the locally optimal update of initial_point
	by two-metric gradient projection method.
	"""
	# ========================
	# Initialization
	# ========================
	current_point = initial_point
	improvement = np.inf

	# ================================================
	# Main loop
	# ================================================
	while (improvement < 0) or (improvement > tolerance):
		# ==================================================================
		# Transformation of current_point (from x to y in Bertsekas (1982)).
		# ==================================================================
		amax = np.argmax(current_point)
		current_transformed_point = np.copy(current_point)
		current_transformed_point[amax] = 1.0

		# ==================================================================
		# Get update direction.
		# ==================================================================
		gradient, hessian = get_gradient_and_hessian(current_point)
		transformed_gradient, transformed_hessian = _transform_gradient_and_hessian(gradient, hessian, amax)
		almost_zero = _get_almost_zero(current_transformed_point, transformed_gradient, transformed_hessian, amax, almost_zero_base_line)
		direction, I_plus, comp_I_plus = _get_direction(
														current_transformed_point,
														transformed_gradient,
														transformed_hessian,
														almost_zero
														)

		# ==================================================================
		# Update.
		# ==================================================================
		current_point, improvement = _update(
										current_transformed_point,
										direction,
										transformed_gradient,
										objective_func,
										I_plus,
										comp_I_plus,
										amax
										)
	return current_point


def _update(
			current_transformed_point,
			direction,
			transformed_gradient,
			objective_func,
			I_plus,
			comp_I_plus,
			amax,
			beta = 0.9,
			sigma = 0.0001
			):
	"""
	Find an optimal step_size such that:
	
	new_point <- _orthant_projection(current_transformed_point - step_size * direction)

	and returns new_point together with objective_func(new_point).

	step_size is given by beta**m (0 < beta < 1),
	where m is the smallest non-negative integer that satusfies:

	objective_func(current_transformed_point) - objective_func("update with beta**m")
	>=
	sigma*(
		beta**m * <transformed_gradient, direction * comp_I_plus> # Indices not in I_plus.
		+
		<transformed_gradient, (current_transformed_point - "update with beta**m") * I_plus> # Indices in I_plus
		)
	
	(0 < sigma < 0.5).
	"""
	# ========================
	# Initialization
	# ========================
	m=0

	# =================================================================================
	# Due to a precision issue in the transformed (= greatest) entry
	# (current_point[amax] may not be equal to 1 - np.sum(other entries)),
	# we need to re-calculate current_point from current_transformed_point
	# and current_fval based on the re-calculated current_point.
	# ==================================================================================
	current_point = np.copy(current_transformed_point)
	current_point[amax] = 1.0 - (
													np.sum(
														current_point[:amax]
														)
													+
													np.sum(
														current_point[amax+1:]
														)
													)
	current_fval = objective_func(current_point)

	# ================================================
	# Main loop
	# ================================================
	while True:
		proposed_step_size = beta**m
		proposed_transformed_update = _orthant_projection_and_map_ge_to_1(
								current_transformed_point
								-
								proposed_step_size * direction
								,
								amax
								)
		proposed_greatest_entry = 1.0 - (
													np.sum(
														proposed_transformed_update[:amax]
														)
													+
													np.sum(
														proposed_transformed_update[amax+1:]
														)
													)
		if proposed_greatest_entry > 0: # we want every entry to be positive.
			proposed_update = np.copy(proposed_transformed_update)
			proposed_update[amax] = proposed_greatest_entry
			improvement = current_fval - objective_func(proposed_update)
			if (
					improvement
					>=
					sigma
					*
					np.dot(
						transformed_gradient,
						proposed_step_size * direction * comp_I_plus
						+
						(current_transformed_point - proposed_transformed_update) * I_plus
					)
				):
				break
		m+=1
	return proposed_update, improvement

def _get_almost_zero(current_transformed_point, transformed_gradient, transformed_hessian, amax, base_line):
	"""
	Get a threshhold under which a vector entry is considered as zero.
	"""
	return np.minimum(
				base_line,
				spsd.euclidean(
					current_transformed_point,
					_orthant_projection_and_map_ge_to_1(
						current_transformed_point
						-
						1.0 / transformed_hessian.diagonal()
						*
						transformed_gradient
						,
						amax
					)
					)
				)

def _get_direction(current_transformed_point, transformed_gradient, transformed_hessian, almost_zero):
	"""
	To make projection onto a simplex easy,
	we adopt a two-metric projection method.
	i.e. The update direction is the transformed_gradient left-multiplied by
	a positive definite metrix D, which is almost the inverse of Hessian.
	See Bertsekas (1982: pp.241-242) for detail.
	"""
	I_plus = np.logical_and(current_transformed_point<=almost_zero, transformed_gradient>0)
	comp_I_plus = ~(I_plus)
	scaling_matrix = np.linalg.inv(
							transformed_hessian
							*
							np.logical_or(
								np.logical_and(
									comp_I_plus[:,np.newaxis]
									,
									comp_I_plus[np.newaxis,:]
								)
								,
								np.eye(current_transformed_point.size, dtype=bool)
							)
							)
	return np.matmul(scaling_matrix, transformed_gradient), I_plus, comp_I_plus

def _orthant_projection(vector):
	"""
	Take max{0, vector[i]} for each i.
	"""
	return vector.clip(min=0)

def _orthant_projection_and_map_ge_to_1(vector,amax):
	"""
	Apply the _orthant_projection to vector[:-1] and replace vector[amax] with 1.
	"""
	projected = _orthant_projection(vector)
	projected[amax] = 1.0
	return projected


def _transform_gradient_and_hessian(gradient, hessian, amax):
	"""
	Transform gradient and hessian from x-based into y-based
	in Bertsekas' (1982: pp. 241-242) sense.
	"""
	transformed_gradient = gradient - gradient[amax]
	transformed_gradient[amax] = gradient[amax]

	transformed_hessian = (
							hessian
							-
							hessian[:,amax,np.newaxis]
							-
							hessian[amax,np.newaxis,:]
							+
							hessian[amax,amax]
							)
	transformed_hessian[amax,:] = hessian[amax,:] - hessian[amax,amax]
	transformed_hessian[:,amax] = hessian[:,amax] - hessian[amax,amax]
	transformed_hessian[amax,amax] = hessian[amax,amax]
	return transformed_gradient, transformed_hessian