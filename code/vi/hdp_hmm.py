# coding: utf-8

import numpy as np
import scipy.special as sps
import algorithm.forward_backward_algorithm as fba
import distributions


class HDP_HMM(object):
	"""
	HDP-HMM with Gaussian emission.
	"""
	def __init__(
			self,
			data,
			trans_params,
			emission_params,
			length_params,
			max_batch_array_size = 2500
			):
		self.data = data
		if not self.data.grouped:
			self.data.group_by_length(max_batch_array_size=max_batch_array_size)
		
		# Emission
		self.emission_distributions = getattr(distributions, emission_params['distribution'])(emission_params)


		# Length determiner
		if length_params['no_length']:
			self.length_distributions = No_length()
		else:
			self.length_distributions = Bernoulli_length(length_params)

		# Transition
		self.trans_hdp = distributions.HDP_trans(trans_params)
		
		self._update_arrays()
		self._update_expected_counts()




	def _update_arrays(self):
		trans_and_init = self.trans_hdp.get_log_weights()
		self.log_transition_weights = trans_and_init[:-1,:]
		self.log_init_weights=trans_and_init[-1,:]
		self.emission_distributions.set_prob_function()




	

	def _update_expected_counts(self):
		"""
		Forward-backward algorithm.
		"""
		self.expected_trans_counts = np.zeros(self.log_transition_weights.shape)
		self.emission_distributions.reset_expected_counts()
		self.length_distributions.reset_expected_counts()
		self.expected_init_counts = np.zeros(self.log_init_weights.shape)
		self.forward_backward_algorithm = fba.Forward_Backward_Algorithm_Multivariate_Emission_Sort_By_Length_Log_Sum_Exp(
											self.log_transition_weights,
											self.emission_distributions.get_log_prob_for_strings,
											self.log_init_weights,
											length_weight_func=self.length_distributions.weight_log_emission
										)
		self.log_like = 0.0
		[self._increment_expected_rule_counts(
										string_batch
										)
			for batch_list in self.data.iter_data()
			for string_batch in batch_list
			]

		self.trans_hdp.update_expected_counts(np.append(self.expected_trans_counts, self.expected_init_counts[np.newaxis,:], axis=0))
	
	def _increment_expected_rule_counts(
								self,
								string_batch
								):
		(
			expected_trans_counts,
			expected_state_visit_per_loc,
			log_like
		) = self.forward_backward_algorithm.get_expected_visit_counts(string_batch)
		self.expected_trans_counts += np.sum(expected_trans_counts, axis=0)
		self.emission_distributions.increment_expected_counts(
														string_batch,
														expected_state_visit_per_loc
														)
		self.length_distributions.increment_expected_counts(np.sum(expected_state_visit_per_loc, axis=0))
		self.expected_init_counts += np.sum(expected_state_visit_per_loc[:,0,:], axis=0)
		self.log_like += np.sum(log_like)

	def set_log_posterior_expectation(self):
		pass



	def get_log_posterior_arrays(self):
		pass
							
	def get_log_posterior_pred(self, test_data):
		pass

		

	def update_varpars(self):
		self.trans_hdp.update_varpars()
		self.emission_distributions.update_varpars()
		self.length_distributions.update_varpars()
		self._update_arrays()
		self._update_expected_counts()



	def get_var_bound(self):
		return (
				self.log_like
				+
				self.trans_hdp.get_var_bound()
				+
				self.emission_distributions.get_var_bound()
				+
				self.length_distributions.get_var_bound()
				)




class Bernoulli_length(distributions.HDP_trans):
	def __init__(self, params):
		self.priors = np.array(params['base_counts'])
		num_states = params['num_states']

		self.varpar_stop = np.random.gamma(
									1, 20,
									size=(num_states,2)
									)
		self.sum_stop = np.sum(self.varpar_stop, axis=-1)
		self.E_log_stop = sps.digamma(self.varpar_stop) - sps.digamma(self.sum_stop)[:,np.newaxis]

		self.stop_counts = np.zeros((num_states))
		self.cont_counts = np.zeros((num_states))



	def _update_varpar_stop(self):
		self.varpar_stop[:,0] = self.stop_counts + self.priors[0]
		self.varpar_stop[:,1] = self.cont_counts + self.priors[1]
		self.sum_stop = np.sum(self.varpar_stop, axis=-1)
		self.E_log_stop = sps.digamma(self.varpar_stop) - sps.digamma(self.sum_stop)[:,np.newaxis]


	def update_varpars(self):
		self._update_varpar_stop()

	def increment_expected_counts(self, extected_visits_per_loc):
		self.stop_counts += extected_visits_per_loc[-1,:]
		self.cont_counts += np.sum(extected_visits_per_loc[:-1,:], axis=0)

	def reset_expected_counts(self):
		self.stop_counts = np.zeros(self.stop_counts.shape)
		self.cont_counts = np.zeros(self.cont_counts.shape)

	def weight_log_emission(self, string_batch, log_emission_probs):
		"""
		Weight the log_emission_probs by (non)-stop probability.
		log_emission_probs: num_strings x string_len x num_states
		"""
		log_emission_probs[:,:-1,:] += self.E_log_stop[np.newaxis,np.newaxis,:,1]
		log_emission_probs[:,-1,:] += self.E_log_stop[np.newaxis,:,0]
		return log_emission_probs


	def get_E_log_p(self):
		return np.sum(
				(self.priors - 1) * self.E_log_stop
				)

	def get_E_log_q(self):
		return np.sum(
					np.sum(self.E_log_stop*(self.varpar_stop-1))
					-
					np.sum(
						sps.gammaln(self.varpar_stop),
						)
					+
					np.sum(
						sps.gammaln(self.sum_stop)
					)
					)

	def set_log_posterior_expectation(self):
		self.log_posterior_expectation = np.log(self.varpar_stop)-np.log(self.sum_stop)[:,np.newaxis]

	def weight_log_emission_by_posterior(self, string_batch, log_emission_probs):
		log_emission_probs[:,:-1,:] += self.log_posterior_expectation[np.newaxis,np.newaxis,:,1]
		log_emission_probs[:,-1,:] += self.log_posterior_expectation[np.newaxis,:,0]
		return log_emission_probs

class No_length(object):
	"""
	Dummy class for HMM without length prediction.
	The resulting probability distribution is over infinite strings.
	"""
	def __init__(self, *args, **kwargs):
		pass

	def reset_expected_counts(self):
		pass

	def weight_log_emission(self, string_batch, log_emission_probs):
		return log_emission_probs

	def increment_expected_counts(self, *args):
		pass

	def update_varpars(self):
		pass

	def get_var_bound(self):
		return 0.0

	def set_log_posterior_expectation(self):
		pass

	def weight_log_emission_by_posterior(self, string_batch, log_emission_probs):
		return log_emission_probs

class Poisson_length(distributions.HDP_trans):
	"""
	Unsupported.
	"""
	def __init__(self, priors, data):
		self.prior_shape = priors[0]
		self.prior_rate = priors[1]

		self.varpar_length = np.random.gamma(
									1, 20,
									size=(2)
									)
		self.E_length = self.varpar_length[0] / self.varpar_length[1]
		self.E_log_length = sps.digamma(self.varpar_length[0]) - np.log(self.varpar_length[1])

		self.data = data



	def _update_varpar_length(self):
		self.varpar_length[0] = self.sum_expected_length + self.prior_shape
		self.varpar_length[1] = self.expected_num_obs + self.prior_rate
		self.E_length = self.varpar_length[0] / self.varpar_length[1]
		self.E_log_length = sps.digamma(self.varpar_length[0]) - np.log(self.varpar_length[1])

	def get_log_like(self):
		return (
				self.data
				*
				self.E_log_length
				-
				self.E_length
				)

	def update_varpars(self):
		self._update_varpar_length()

	def update_expected_counts(self):
		self.sum_expected_length = np.sum(
										self.data
										,
										axis=0
									)
		self.expected_num_obs = self.data.size

	def get_E_log_p(self):
		return np.sum(
				(self.prior_shape - 1) * self.E_log_length
				-
				self.prior_rate * self.E_length
				)

	def get_E_log_q(self):
		return np.sum(
					self.varpar_length[0] * np.log(self.varpar_length[1])
					-
					sps.gammaln(self.varpar_length[0])
					+
					(self.varpar_length[0] - 1) * self.E_log_length
					-
					self.varpar_length[0]
					)