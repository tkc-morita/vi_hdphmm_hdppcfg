# coding: utf-8

from vi import variational_inference as vi
import numpy as np
import os,datetime,json
import argparse
import misc.data_parser as dtp

if __name__=='__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument(
				"-n", 
				"--num_states",
				type=int,
				default=10,
				help="Max # of states per HMM"
				)
	parser.add_argument(
				"-d",
				"--data",
				type=str,
				help="Path to data"
				)
	parser.add_argument(
				"-i",
				"--max_iterations",
				type=int,
				default=10000,
				help="Maxmum # of iterations"
				)
	parser.add_argument(
				"-T",
				"--tolerance",
				type=float,
				default=0.01,
				help="Threshold to detect convergence"
				)
	parser.add_argument(
				"-N",
				"--trial_name",
				type=str,
				help="Trial name used to name directory to save results."
				)
	parser.add_argument(
				"-S",
				"--stop_base_counts",
				type = float,
				default = 1.0,
				help = "Base counts of stop at states parameterizing the beta prior of the length model."
	)
	parser.add_argument(
				"-C",
				"--cont_base_counts",
				type = float,
				default = 1.0,
				help = "Base counts of continue at states parameterizing the beta prior of the length model."
	)
	parser.add_argument(
				"-b", "--batch_array_size",
				type=int,
				default=650,
				help="Max batch size of data array to be processed in parallel in forward-backward algorithm."
				)
	parser.add_argument(
				"-s",
				"--save_dir",
				type=str,
				help="Path to the directory where results are saved. If unspecified, result_dir_info.txt is read and information there is extracted.",
				default=None
	)
	parser.add_argument(
				"-e",
				"--emission_distribution",
				type=str,
				help='Distribution assumed for the data emission. Gaussian by default.',
				default='Gaussian'
	)
	parser.add_argument(
				"--concentration_categorical_emission",
				type=float,
				help='Concentration parameter of the Dirichlet prior on the categorical data emission.',
				default=1.0
	)
	parser.add_argument(
				"--manual_num_categories",
				type=int,
				help='# of discrete data categories. Either equal to or greater than # of categories included in the given data. If unspecified, use # of categories included in the data.',
				default=None,
	)
	parser.add_argument(
				"--no_length",
				action='store_true',
				help="If selected, no predictions of the string length. (i.e., distribution over infinite strings.)"
	)
	parser.add_argument(
				"--seq_col",
				type=str,
				default='sequence_ix',
				help="Name of the csv column containing the index of the sequences."
	)
	parser.add_argument(
				"--time_col",
				type=str,
				default='time_ix',
				help="Name of the csv column containing the discrete time indexes of the sequences."
	)
	parser.add_argument(
				"--dim_col",
				type=str,
				default='dim',
				help="Name of the csv column containing the index of the dimensions of the data points."
	)
	parser.add_argument(
				"--val_col",
				type=str,
				default='value',
				help="Name of the csv column containing the data values."
	)
	parser.add_argument(
		'-O',
		'--old_data_format',
		action='store_true',
		help='If selected, each row of the data file is assumed to represent a single sequence, each discrete time is tab-stop-separated, and each data dimension is comma-separated.'
	)
	parser.add_argument(
					'--seed',
					type=str,
					default='111',
					help='Random seed.'
				)
	options=parser.parse_args()
	


	vi_tolerance = options.tolerance

	file_dir = os.path.dirname(os.path.realpath(os.path.abspath(__file__)))
	if options.data is None:
		datapath = os.path.join(file_dir, '../sample_data/20xPoissin_5x3-iid-samples-from-Gauss-0-I.csv')
	else:
		datapath = options.data
	
	now = datetime.datetime.now().strftime("%y%m%d-%H%M%S-%f")
	if options.trial_name is None:
		trial_dir = now
	else:
		trial_dir = options.trial_name+'_'+now
	
	
	data_prefix=os.path.basename(os.path.splitext(datapath)[0])
	data = dtp.Dataset_of_Sequences(
				datapath,
				seq_col=options.seq_col,
				time_col=options.time_col,
				dim_col=options.dim_col,
				val_col=options.val_col,
				discrete=True if options.emission_distribution=='Categorical' else False,
				manual_num_categories=options.manual_num_categories,
				old_format=options.old_data_format
				)
	
	
	gradient_projection_tolerance = vi_tolerance # We don't need improvement tolerant to variational inference as a whole.


	if options.save_dir is None:
		with open(os.path.join(file_dir, 'result_dir_info.txt'), 'r') as f:
			options.save_dir = f.readlines()[0].strip()

		
	result_path = os.path.join(
						options.save_dir
						,
						data_prefix
						,
						('HDP-HMM_%s_num-states-%i' % (options.emission_distribution, options.num_states))
						,
						trial_dir
						)
	os.makedirs(result_path)

	random_seed = options.seed
	if not random_seed.isdigit():
		random_seed = ''.join([s for s in random_seed if s.isdigit()])
		if random_seed=='':
			random_seed = '111'
	random_seed = int(random_seed[-9:]) # Seed must be Seed must be between 0 and 2**32 - 1 < 10**10.


	vi_hmm = vi.Variational_Inference_HDP_HMM(
								data,
								options.num_states,
								result_path,
								emission_distribution=options.emission_distribution,
								stop_base_counts=options.stop_base_counts,
								cont_base_counts=options.cont_base_counts,
								concentration_categorical_emission=options.concentration_categorical_emission,
								gradient_projection_tolerance=gradient_projection_tolerance,
								max_batch_array_size=options.batch_array_size,
								no_length_prediction=options.no_length,
								random_seed=random_seed
								)
	
	vi_hmm.train(options.max_iterations, vi_tolerance)
	vi_hmm.save_results()
	if not data.label2ix is None:
		with open(os.path.join(result_path,'label2ix.json'), 'w') as f:
			json.dump(data.label2ix, f)