# coding: utf-8

from vi import variational_inference as vi
import numpy as np
import os,datetime
import argparse
import misc.data_parser as dtp




if __name__=='__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument(
				"-n",
				"--num_non_terminals",
				type=int,
				default=10,
				help="Max # of non-terminals"
				)
	parser.add_argument(
				"-t",
				"--num_terminals",
				type=int,
				default=10,
				help="Max # of terminals"
				)
	parser.add_argument(
				"-d",
				"--data",
				type=str,
				help="Path to data"
				)
	parser.add_argument(
				"-i",
				"--iterations",
				type=int,
				default=10000,
				help="Max # of iterations"
				)
	parser.add_argument(
				"-T",
				"--tolerance",
				type=float,
				default=0.01,
				help="Threshold to detect convergence"
				)
	parser.add_argument(
				"-b", "--batch_size",
				type=int,
				default=650,
				help="Max batch size of data array to be processed in parallel in inner-outer algorithm."
				)
	parser.add_argument(
				"-N",
				"--trial_name",
				type=str,
				help="Trial name used to name directory to save results."
				)
	parser.add_argument(
				"-s",
				"--save_dir",
				type=str,
				help="Path to the directory where results are saved. If unspecified, result_dir_info.txt is read and information there is extracted.",
				default=None
	)
	parser.add_argument(
				"-e",
				"--emission_distribution",
				type=str,
				help='Distribution assumed for the data emission. Gaussian by default.',
				default='Gaussian'
	)
	parser.add_argument(
				"--seq_col",
				type=str,
				default='sequence_ix',
				help="Name of the csv column containing the index of the sequences."
	)
	parser.add_argument(
				"--time_col",
				type=str,
				default='time_ix',
				help="Name of the csv column containing the discrete time indexes of the sequences."
	)
	parser.add_argument(
				"--dim_col",
				type=str,
				default='dim',
				help="Name of the csv column containing the index of the dimensions of the data points."
	)
	parser.add_argument(
				"--val_col",
				type=str,
				default='value',
				help="Name of the csv column containing the data values."
	)
	parser.add_argument(
		'-O',
		'--old_data_format',
		action='store_true',
		help='If selected, each row of the data file is assumed to represent a single sequence, each discrete time is tab-stop-separated, and each data dimension is comma-separated.'
	)
	parser.add_argument(
					'--seed',
					type=str,
					default='111',
					help='Random seed.'
				)
	options=parser.parse_args()
	



	vi_tolerance = options.tolerance


	file_dir = os.path.dirname(os.path.realpath(os.path.abspath(__file__)))
	if options.data is None:
		datapath = os.path.join(file_dir, '../sample_data/20xPoissin_5x3-iid-samples-from-Gauss-0-I.csv')
	else:
		datapath = options.data

	now = datetime.datetime.now().strftime("%y%m%d-%H%M%S-%f")
	if options.trial_name is None:
		trial_dir = now
	else:
		trial_dir = options.trial_name+'_'+now

	
	data_prefix=os.path.basename(os.path.splitext(datapath)[0])
	data = dtp.Dataset_of_Sequences(datapath, seq_col=options.seq_col, time_col=options.time_col, dim_col=options.dim_col, val_col=options.val_col, old_format=options.old_data_format)

	gradient_projection_tolerance = vi_tolerance # We don't need improvement tolerant to variational inference as a whole.


	
	if options.save_dir is None:
		with open(os.path.join(file_dir, 'result_dir_info.txt'), 'r') as f:
			options.save_dir = f.readlines()[0].strip()


	result_path = os.path.join(
						options.save_dir
						,
						data_prefix
						,
						('HDP-PCFG_%s_num-non-terminals-%i_num-terminals-%i' % (options.emission_distribution, options.num_non_terminals, options.num_terminals))
						,
						trial_dir
						)
	os.makedirs(result_path)

	random_seed = options.seed
	if not random_seed.isdigit():
		random_seed = ''.join([s for s in random_seed if s.isdigit()])
		if random_seed=='':
			random_seed = '111'
	random_seed = int(random_seed[-9:]) # Seed must be Seed must be between 0 and 2**32 - 1 < 10**10.
	
	vi_hdp_cfg = vi.Variational_Inference_HDP_PCFG(
						data,
						options.num_non_terminals,
						result_path,
						emission_distribution=options.emission_distribution,
						num_terminals = options.num_terminals,
						gradient_projection_tolerance=gradient_projection_tolerance,
						max_batch_array_size=options.batch_size,
						random_seed=random_seed
						)
	vi_hdp_cfg.train(options.iterations, vi_tolerance)
	vi_hdp_cfg.save_results()
	