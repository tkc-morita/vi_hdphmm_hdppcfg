# coding: utf-8

from vi import variational_inference as vi
import numpy as np
import os,datetime
import pandas as pd
import argparse

if __name__=='__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument(
				"-n", 
				"--num_clusters",
				type=int,
				default=10,
				help="Max # of clusters"
				)
	parser.add_argument(
				"-d",
				"--data",
				type=str,
				help="Path to data"
				)
	parser.add_argument(
				"-i",
				"--max_iterations",
				type=int,
				default=10000,
				help="Maxmum # of iterations"
				)
	parser.add_argument(
				"-T",
				"--tolerance",
				type=float,
				default=0.01,
				help="Threshold to detect convergence"
				)
	parser.add_argument(
				"-N",
				"--trial_name",
				type=str,
				help="Trial name used to name directory to save results."
				)
	parser.add_argument(
				"-C",
				"--concentration",
				type = float,
				default = 1.0,
				help = "DP concentration."
	)
	parser.add_argument(
				"-S",
				"--concentration_prior_shape",
				type = float,
				default = None,
				help = "The shape parameter of the gamma prior on the DP concentration."
	)
	parser.add_argument(
				"-R",
				"--concentration_prior_rate",
				type = float,
				default = None,
				help = "The rate parameter of the gamma prior on the DP concentration."
	)
	parser.add_argument(
				"-s",
				"--save_dir",
				type=str,
				help="Path to the directory where results are saved. If unspecified, result_dir_info.txt is read and information there is extracted.",
				default=None
	)
	parser.add_argument(
				"-e",
				"--emission_distribution",
				type=str,
				help='Distribution assumed for the data emission. Gaussian by default.',
				default='Gaussian'
	)
	parser.add_argument(
				"--data_ix_col",
				type=str,
				default='data_ix',
				help="Name of the csv column containing the index of the data points."
	)
	parser.add_argument(
				"--dim_col",
				type=str,
				default='dim',
				help="Name of the csv column containing the index of the dimensions of the data points."
	)
	parser.add_argument(
				"--val_col",
				type=str,
				default='value',
				help="Name of the csv column containing the data values."
	)
	parser.add_argument(
				"-b", "--batch_array_size",
				type=int,
				default=650,
				help="Max batch size of data array to be processed in parallel when computing expected counts."
				)
	parser.add_argument(
					'--seed',
					type=str,
					default='111',
					help='Random seed.'
				)
	options=parser.parse_args()
	

	vi_tolerance = options.tolerance

	file_dir = os.path.dirname(os.path.realpath(os.path.abspath(__file__)))
	if options.data is None:
		datapath = os.path.join(file_dir, '../sample_data/20x3-iid-samples-from-Gauss-0-I.csv')
	else:
		datapath = options.data
	
	now = datetime.datetime.now().strftime("%y%m%d-%H%M%S-%f")
	if options.trial_name is None:
		trial_dir = now
	else:
		trial_dir = options.trial_name+'_'+now
	
	
	data_prefix=os.path.basename(os.path.splitext(datapath)[0])
	df_data = pd.read_csv(datapath).sort_values([options.data_ix_col, options.dim_col])
	data = df_data.pivot(index=options.data_ix_col, columns=options.dim_col, values=options.val_col).values



	if options.save_dir is None:
		with open(os.path.join(file_dir, 'result_dir_info.txt'), 'r') as f:
			options.save_dir = f.readlines()[0].strip()

		
	result_path = os.path.join(
						options.save_dir
						,
						data_prefix
						,
						('DP-Mix_%s_num-clusters-%i' % (options.emission_distribution, options.num_clusters))
						,
						trial_dir
						)
	os.makedirs(result_path)

	random_seed = options.seed
	if not random_seed.isdigit():
		random_seed = ''.join([s for s in random_seed if s.isdigit()])
		if random_seed=='':
			random_seed = '111'
	random_seed = int(random_seed[-9:]) # Seed must be Seed must be between 0 and 2**32 - 1 < 10**10.

	vi_dp_mix = vi.Variational_Inference_DP_Mix(
								data,
								options.num_clusters,
								result_path,
								emission_distribution=options.emission_distribution,
								concentration=options.concentration,
								concentration_prior_shape=options.concentration_prior_shape,
								concentration_prior_rate=options.concentration_prior_rate,
								max_batch_array_size=options.batch_array_size,
								random_seed=random_seed
								)
	
	vi_dp_mix.train(options.max_iterations, vi_tolerance)
	vi_dp_mix.save_results()
	